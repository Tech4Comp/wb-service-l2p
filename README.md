<p align="center">
  <img src="https://tech4comp.de/wp-content/uploads/2019/08/1logotech4comp_farbig_RGB.jpg" />
</p>
<h1 align="center">Mentoring Workbench Service</h1>

This project contains the mentoring workbench las2peer service.    

For documentation on the las2peer service API, please refer to the [wiki](https://github.com/rwth-acis/las2peer-Template-Project/wiki).

The javadoc of this project starts here: 
[Javadoc](wb-service/javadoc/index.html)

## Preparations

### Java

las2peer uses **Java 17**.

### Build Dependencies

* Gradle

## Quick Setup of your Service Development Environment

### Build 
```shell
./gradlew build
```

### Run
```shell
./bin/start_network.sh
```

### Check
```shell
http://localhost:8080/mwb/version
```

### Setup postgres database
You can either use the live database with your service or run a docker container with
a postgres database on your local machine. 

Please follow the steps described in `/wb-service/docker/readme.md to set up a docker container.

If you want to connect to a live database, copy  `db-authentication.json`
from `/wb-service/docker/` to `/wb-service/resources/` and  adjust the connection
data and credentials in that file. 

**IMPORTANT** But take care to **NEVER** commit credentials to git, i.e. this file should stay listed in .gitignore 

## Helpful/necessary steps after restarting/pushing the repo
### (deprecated) Manually getting bot logs 
(currently necessary after each restart)

**Please note** that bot logs are currently not supported by the frontend

```
curl -X GET -i -H "Authorization: Basic {auth}" https://workbench.tech4comp.dbis.rwth-aachen.de/api/mwb/get_bot_logs/bitbot/2022-06-14T09:54:00.000Z?semester=ss22
```
for each bot and each of the semesters "ws22" and "ss22". 

`{auth}` is base64(`LRSname:LRSsub`) of an allowed user (who are defined in the List `fileAccessAllowed`)

Logs are fetched for the interval t1 to t2, where t2 is the timestamp given in the interval, and t1 is
either the beginning of the semester or the time up to which the logs were fetched when the same call was done the last time fot the same bot
(if there is already data there).

## Other
Restarting TUD onyx proxy
```
curl -X POST -i -H "Authorization:Basic {auth}" https://git.tech4comp.dbis.rwth-aachen.de/onyx
```


