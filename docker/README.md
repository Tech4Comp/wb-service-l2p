# setup docker with databases
## install docker (if not already on your machine)
https://docs.docker.com/get-docker/

You may either install Docker Engine and Docker Compose as standalone applications or you can install Docker Desktop which includes both.

## start/stop docker desktop
(Linux, Terminal)

`systemctl --user start docker-desktop`  
`systemctl --user stop docker-desktop`

## compose container
(Terminal, change to directory containing the file `compose.yaml`, this should be the same directory this readme file is located in)  

Startup docker desktop and run the following command to create a docker container.

`docker compose up -d`

This will set up a container with (in sub containers)
- Postgres 
- PGAdmin
- MongoDB
- MongoExpress
 On first time it will take a while to fetch the images.

## create database and test data
(Terminal, still in the directory this readme file is located in)

get the container_id for the postgres Service:  
`docker ps` 

Then copy the sql-files into the postgres container (please take care to use the containerId of the postgres container) 
`docker cp createDatabase.sql {containerId}:/createDatabase.sql`  
`docker cp initTables.sql {containerId}:/initTables.sql`  
`docker cp {pathToFile}/insertSecrets.sql {containerId}:/insertSecrets.sql`  

**Note**: the sql file `insertSecrets.sql` contains credentials for the LRS and some external services, so this file should **never** be committed to git.  
Please ask the person in charge, if you need it.

Now use docker exec to run the sql commands in the postgres container:  
`docker exec -it local_pgdb sh -c "psql -U postgres -a -f createDatabase.sql"`  
`docker exec -it local_pgdb sh -c "psql -U postgres -d mentoring-workbench -a -f initTables.sql"`  
`docker exec -it local_pgdb sh -c "psql -U postgres -d mentoring-workbench -a -f insertSecrets.sql"`  

## credentials
Copy the file `db-authentication.json` containing the credentials for the local postgres database to the directory `/wb-service/resources`

## PGAdmin
If you need a nice GUI for manipulating the Postgres database, you can use PGAdmin, running:  
`http://localhost:8888`
User Email: `pgadmin@local-tech4comp.de`  
Password: `mwb_pgadmin`  

When connecting the first time, you will have to add a new server connected to the postgres container:

Click on `Add New Server` (or in sidenav `Server->Register->Server`)
and configure the server:

##### Tab `General`:  
Name: `PostgreSQL DB`

##### Tab `Connection`:  
Host name/address: `db`  
Port: `5432`  
Username: `postgres`  
Password: `mwb_postgres`  
(optional) Save password: `enable`

## Mongo Express
If you need a nice GUI for manipulating the mongoDB, you can use MongoExpress running  `http://localhost:8081`
You will be asked for authentication:
Username: `mexpress`  
Password: `mwb_mexpress`  
# run
Since the docker desktop demands a good share of resources, you may start and stop the service as you need.  
Once set up, your database will be available as soon as the docker desktop is up and running.
