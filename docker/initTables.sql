COMMENT ON DATABASE "mentoring-workbench"
    IS 'Configuration and administration of courses and students and their related workbenches';



CREATE TABLE IF NOT EXISTS person (
    id serial PRIMARY KEY,
    email character varying(100) NOT NULL,
    firstname character varying(100) DEFAULT ''::text,
    lastname character varying(100) DEFAULT ''::text,
    CONSTRAINT unique_person_email UNIQUE (email)
);
COMMENT ON TABLE person
    IS 'Personal data';    



CREATE TABLE IF NOT EXISTS role (
    id serial PRIMARY KEY,
    roleType character varying(20) NOT NULL,
    CONSTRAINT unique_role_type UNIQUE (roleType)
);
COMMENT ON TABLE role
    IS 'List of available roles';

CREATE TABLE IF NOT EXISTS objectForRole (
    id serial PRIMARY KEY,
    objectType character varying(20) NOT NULL,
    CONSTRAINT unique_object_type UNIQUE (objectType)
);
COMMENT ON TABLE objectForRole
    IS 'List of available objects for roles';

CREATE TABLE IF NOT EXISTS hasRole (     
    id serial PRIMARY KEY,
    personId integer NOT NULL REFERENCES person(id) ON UPDATE NO ACTION ON DELETE CASCADE,
    roleId integer NOT NULL REFERENCES role(id) ON UPDATE NO ACTION ON DELETE CASCADE,
    objectForRoleId integer NOT NULL REFERENCES objectForRole(id) ON UPDATE NO ACTION ON DELETE CASCADE,
    objectId integer,
    CONSTRAINT unique_hasRole UNIQUE (personId, roleId, objectForRoleId, objectId)
);
COMMENT ON TABLE hasRole
    IS 'General roles assigned to a user';    
COMMENT ON COLUMN hasRole.personId
    IS 'personId of the person the role is assigned to';
COMMENT ON COLUMN hasRole.objectForRoleId
    IS 'predefined object type of the object the role is assigned to';
COMMENT ON COLUMN hasRole.objectId
    IS 'optional id of the object the role is assigned to';



CREATE TABLE IF NOT EXISTS baseCourse (
    id serial PRIMARY KEY,
    name character varying(100) NOT NULL,
    lms character varying(50) DEFAULT ''::text
);
COMMENT ON TABLE baseCourse
    IS 'Basic data of a recurring course';    
COMMENT ON COLUMN baseCourse.name
	IS 'name for the base course, will be displayed along course name';
COMMENT ON COLUMN baseCourse.lms
	IS 'optional type of lms, if base course belongs to a course in a lms. Details can be found in the corresponding detail tables.';




CREATE TABLE IF NOT EXISTS course
(
    id serial PRIMARY KEY,
    baseCourseId integer NOT NULL REFERENCES baseCourse(id) ON UPDATE NO ACTION ON DELETE CASCADE,
    shortname character varying(50) DEFAULT ''::text,
    surname character varying(50) DEFAULT ''::text,
    validFrom date,
    validUntil date,
    CONSTRAINT unique_course_shortname UNIQUE (shortname)
);
COMMENT ON TABLE course
    IS 'Specific instance of a base course';
COMMENT ON COLUMN course.shortname
    IS 'Readable link to that course in MWB';
COMMENT ON COLUMN course.surname
    IS 'Name of the specific instance, e.g. SS23 (short name for summer semester 2023)';
COMMENT ON COLUMN course.validFrom
    IS 'Valid from including this date';
COMMENT ON COLUMN course.validUntil
    IS 'Valid until including this date';

    
CREATE TABLE IF NOT EXISTS lastVisited (     
    uuid uuid NOT NULL REFERENCES personMapping(uuid) ON UPDATE NO ACTION ON DELETE CASCADE,
    courseId integer NOT NULL REFERENCES course(id) ON UPDATE NO ACTION ON DELETE CASCADE,
    timestamp timestamp with time zone NOT NULL,
    PRIMARY KEY(uuid, courseId)
);
COMMENT ON TABLE lastVisited
    IS 'Stores timestamp of last time a user has visited a course';
COMMENT ON COLUMN lastVisited.courseId
    IS 'courseId of the visited course';
COMMENT ON COLUMN lastVisited.timestamp
    IS 'timestamp of last visit';


CREATE TABLE IF NOT EXISTS componentType
(
    id serial PRIMARY KEY,
    type character varying(100) NOT NULL,
    supportsAnonymousView boolean DEFAULT false,
    supportsComponentPrivacy boolean DEFAULT false,
    CONSTRAINT unique_componentType_type UNIQUE (type)
);
COMMENT ON TABLE componentType
    IS 'Valid types for components in MWB';    



CREATE TABLE IF NOT EXISTS courseComponent
(
    courseId integer NOT NULL REFERENCES course(id) ON UPDATE NO ACTION ON DELETE CASCADE,
    componentTypeId integer NOT NULL REFERENCES componentType(id) ON UPDATE NO ACTION ON DELETE CASCADE,
    allowedForAnonymous boolean DEFAULT false,
    PRIMARY KEY(courseId, componentTypeId)
);
COMMENT ON TABLE courseComponent
    IS 'List of components assigned to a course. Each component type can only occur a single time in a course.';    

    


CREATE TABLE IF NOT EXISTS privacystatement
(
    id serial PRIMARY KEY,
    courseId integer NOT NULL REFERENCES course(id) ON UPDATE NO ACTION ON DELETE CASCADE,
    purpose text NOT NULL DEFAULT ''::text,
    comment text DEFAULT ''::text,
    link text DEFAULT ''::text,
    filebucketId integer REFERENCES fileBucket(id) ON UPDATE NO ACTION ON DELETE NO ACTION,
    version smallint NOT NULL,
    lastModified timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT unique_privacystatement_purpose_per_course UNIQUE(courseId, purpose)
);
COMMENT ON TABLE privacystatement
    IS 'Details of a privacy statement students have to agree';
COMMENT ON COLUMN privacystatement.purpose
    IS 'Set to empty string if privacy statement belongs to the course itself. Otherwise specify the purpose for which an additional privacy statement (and consent) is required.';
COMMENT ON COLUMN privacystatement.link
    IS 'Link to the privacy statement';
COMMENT ON COLUMN privacystatement.fileId
    IS 'Id of the file containing the privacy statement if stored in MongoDB';

        

CREATE TABLE IF NOT EXISTS personMapping
(
    uuid uuid PRIMARY KEY NOT NULL,
    personId integer NOT NULL REFERENCES person(id) ON UPDATE NO ACTION ON DELETE CASCADE,
    CONSTRAINT unique_personMapping_personId UNIQUE (personId)
);
COMMENT ON TABLE personMapping
    IS 'master key table to map a uuid with a person';



CREATE TABLE IF NOT EXISTS consent
(
    uuid uuid NOT NULL REFERENCES personMapping(uuid) ON UPDATE NO ACTION ON DELETE CASCADE,
    privacystatementId integer NOT NULL REFERENCES privacystatement(id) ON UPDATE NO ACTION ON DELETE CASCADE,
    timestamp timestamp with time zone NOT NULL,
    PRIMARY KEY(uuid, privacystatementId)
);
COMMENT ON TABLE consent
    IS 'Consent given for a privacy statement';


    
CREATE TABLE IF NOT EXISTS courseChatbot
(
    courseId integer PRIMARY KEY NOT NULL REFERENCES course(id) ON UPDATE NO ACTION ON DELETE CASCADE,
    chatbot character varying(50) NOT NULL
);
COMMENT ON TABLE public.courseChatbot
    IS 'Name of the chatbot used for a course';



CREATE TABLE IF NOT EXISTS courseFaq
(
    courseId integer NOT NULL REFERENCES course(id) ON UPDATE NO ACTION ON DELETE CASCADE,
    intent character varying(150) NOT NULL ,
    answer text,
    PRIMARY KEY(courseId, intent)
);
COMMENT ON TABLE courseFaq
    IS 'chatbot answers for intents defined in NLU training';
COMMENT ON COLUMN courseFaq.intent
    IS 'intent recognized';
COMMENT ON COLUMN courseFaq.answer
    IS 'chatbot answer assigned to that intent';

    
CREATE TABLE IF NOT EXISTS courseWritingTasks
(
    courseId integer PRIMARY KEY NOT NULL REFERENCES course(id) ON UPDATE NO ACTION ON DELETE CASCADE,
    validFrom timestamp with time zone,
    validUntil timestamp with time zone,
    timelinerowid integer,
    movable boolean DEFAULT false
);
COMMENT ON TABLE courseWritingTasks
    IS 'General info for the writing task component';
COMMENT ON COLUMN courseWritingTasks.validFrom
    IS 'Writing tasks generally available from this timestamp';
COMMENT ON COLUMN courseWritingTasks.validUntil
    IS 'Writing tasks generally available until this timestamp';
COMMENT ON COLUMN courseWritingTasks.timelinerowid
    IS 'If using timeline: row id to add writing task to';
COMMENT ON COLUMN courseWritingTasks.movable
    IS 'If using timeline: items for writing task movable?';


CREATE TABLE IF NOT EXISTS writingTask
(
    courseId integer NOT NULL REFERENCES course(id) ON UPDATE NO ACTION ON DELETE CASCADE,
    nr integer NOT NULL,
    title character varying(150) DEFAULT ''::text,
    text text DEFAULT ''::text,
    validFrom timestamp with time zone,
    validUntil timestamp with time zone,
    PRIMARY KEY(courseId, nr)
);
COMMENT ON TABLE writingTask
    IS 'Writing tasks for a course';
COMMENT ON COLUMN writingTask.validFrom
    IS 'starting time for task';
COMMENT ON COLUMN writingTask.validUntil
    IS 'end time for task';


    
CREATE TABLE IF NOT EXISTS courseSearchIndex
(
    courseId integer PRIMARY KEY NOT NULL REFERENCES course(id) ON UPDATE NO ACTION ON DELETE CASCADE,
    searchId character varying(50) NOT NULL,
    searchIndex character varying(50) NOT NULL
);
COMMENT ON TABLE courseSearchIndex
    IS 'Search index used for the search in resources of a course';

    
CREATE TABLE IF NOT EXISTS opalCourse
(
    basecourseId integer PRIMARY KEY NOT NULL REFERENCES basecourse(id) ON UPDATE NO ACTION ON DELETE CASCADE,
    resource character varying(50) NOT NULL,
    lmsWorkerId integer NOT NULL REFERENCES lmsWorker(id) ON UPDATE NO ACTION ON DELETE  NO ACTION
);
COMMENT ON TABLE opalCourse
    IS 'OPAL specific data for a course';
COMMENT ON COLUMN opalCourse.resource
    IS 'Resource Id for the course in OPAL';
COMMENT ON COLUMN opalCourse.lmsWorkerId
    IS 'Account used for authorization via the OPAL API';
    
CREATE TABLE IF NOT EXISTS moodleCourse
(
    basecourseId integer PRIMARY KEY NOT NULL REFERENCES basecourse(id) ON UPDATE NO ACTION ON DELETE CASCADE,
    resource character varying(50) NOT NULL,
    lmsWorkerId integer NOT NULL REFERENCES lmsWorker(id) ON UPDATE NO ACTION ON DELETE  NO ACTION
);
COMMENT ON TABLE moodleCourse
    IS 'Moodle specific data for a course';
COMMENT ON COLUMN moodleCourse.resource
    IS 'Resource Id for the course in Moodle';
COMMENT ON COLUMN moodleCourse.lmsWorkerId
    IS 'Account used for authorization via the Moodle API';

    
CREATE TABLE IF NOT EXISTS courseOnyxProxy
(
    courseId integer PRIMARY KEY NOT NULL REFERENCES course(id) ON UPDATE NO ACTION ON DELETE CASCADE,
    validFrom date,
    validUntil date
);
COMMENT ON TABLE courseOnyxProxy
    IS 'Basic settings for the Opal-Onyx Proxy';
COMMENT ON COLUMN courseOnyxProxy.validFrom
    IS 'Valid from including this date';
COMMENT ON COLUMN courseOnyxProxy.validUntil
    IS 'Valid until including this date';




CREATE TABLE IF NOT EXISTS courseExam
(
    id serial PRIMARY KEY,
    courseId integer NOT NULL REFERENCES course(id) ON UPDATE NO ACTION ON DELETE CASCADE,
    testTool character varying(50) DEFAULT ''::text,
    name character varying(150) DEFAULT ''::text,
    validFrom timestamp with time zone,
    validUntil timestamp with time zone
);
COMMENT ON TABLE courseExam
    IS 'Exams available for component ExtendedExamFeedback';
COMMENT ON COLUMN courseExam.testTool
    IS 'Test tool used for exam, e.g. onyx';
COMMENT ON COLUMN courseExam.validFrom
    IS 'Valid from including this timestamp';
COMMENT ON COLUMN courseExam.validUntil
    IS 'Valid until including this timestamp';


CREATE TABLE IF NOT EXISTS easLitExam
(
    courseExamId integer PRIMARY KEY NOT NULL REFERENCES courseExam(id) ON UPDATE NO ACTION ON DELETE CASCADE,
    easlitProjectId character varying(50) DEFAULT ''::text,
    topic character varying(150) DEFAULT ''::text
);
COMMENT ON TABLE easLitExam
    IS 'Details for annotations in EasLit for an exam';
COMMENT ON COLUMN easLitExam.easlitProjectId
    IS 'Id of the easlit project to annotate test items';
COMMENT ON COLUMN easLitExam.topic
    IS 'Topic to filter test items with that topic annotated in easlit';


CREATE TABLE IF NOT EXISTS onyxExam
(
    courseExamId integer PRIMARY KEY NOT NULL REFERENCES courseExam(id) ON UPDATE NO ACTION ON DELETE CASCADE,
    onyxTestId character varying(50) DEFAULT ''::text,
    opalNodeId character varying(50) DEFAULT ''::text
);
COMMENT ON TABLE onyxExam
    IS 'Details for ONYX test';
COMMENT ON COLUMN onyxExam.onyxTestId
    IS 'Id of the ONYX test needed to access the results via API';
COMMENT ON COLUMN onyxExam.opalNodeId
    IS 'Node id in OPAL for the ONYX test';

    
    
    
CREATE TABLE IF NOT EXISTS courseTimeline
(
    courseId integer PRIMARY KEY NOT NULL REFERENCES course(id) ON UPDATE NO ACTION ON DELETE CASCADE,
    startsOn date,
    endsOn date
);
COMMENT ON TABLE courseTimeline
    IS 'basic settings for timeline component';
COMMENT ON COLUMN courseTimeline.startsOn
    IS 'starting date of item';
COMMENT ON COLUMN courseTimeline.endsOn
    IS 'end date of item';
    

CREATE TABLE IF NOT EXISTS courseTimelineRow
(
    id serial PRIMARY KEY,
    courseId integer NOT NULL REFERENCES course(id) ON UPDATE NO ACTION ON DELETE CASCADE,
    row integer default 0,    
    description text DEFAULT ''::text,
    isDetail boolean DEFAULT false,
    createNewItems boolean DEFAULT false
);
COMMENT ON TABLE courseTimelineRow
    IS 'properties for a row in a timeline component';
COMMENT ON COLUMN courseTimelineRow.row
    IS 'row number in timeline display';
COMMENT ON COLUMN courseTimelineRow.isDetail
    IS 'row is only displayed in detail/full view';
COMMENT ON COLUMN courseTimelineRow.description
    IS 'description for row, displayed in fixed column in timeline';
COMMENT ON COLUMN courseTimelineRow.createNewItems
    IS 'user is allowed to create new items in this row';
    
    
CREATE TABLE IF NOT EXISTS courseTimelineItem
(
    id serial PRIMARY KEY,
    rowId integer NOT NULL REFERENCES courseTimelineRow(id) ON UPDATE NO ACTION ON DELETE CASCADE,
    title character varying(50) DEFAULT ''::text,
    startsOn timestamp with time zone NOT NULL,
    endsOn timestamp with time zone NOT NULL,
    description text DEFAULT ''::text,
    expandToCalendarWeek boolean DEFAULT false,
    editableTime boolean DEFAULT false,
    editableTitle boolean DEFAULT false
);
COMMENT ON TABLE courseTimelineItem
    IS 'items for timeline component';
COMMENT ON COLUMN courseTimelineItem.title
    IS 'title displayed in the timeline bar';
COMMENT ON COLUMN courseTimelineItem.startsOn
    IS 'starting time of item';
COMMENT ON COLUMN courseTimelineItem.endsOn
    IS 'end time of item';
COMMENT ON COLUMN courseTimelineItem.description
    IS 'detailed description for that item';
COMMENT ON COLUMN courseTimelineItem.expandToCalendarWeek
    IS 'if set to true this item will expand its range view to beginning and end of the according calender week';
COMMENT ON COLUMN courseTimelineItem.editableTime
    IS 'user may change start and/or end date';
COMMENT ON COLUMN courseTimelineItem.editableTitle
    IS 'user may change the title';
    
    
CREATE TABLE IF NOT EXISTS userTimelineItem
(
    id serial PRIMARY KEY,
    uuid uuid NOT NULL,
    courseTimelineItemId integer REFERENCES courseTimelineItem(id) ON UPDATE NO ACTION ON DELETE CASCADE,
    rowId integer NOT NULL REFERENCES courseTimelineRow(id) ON UPDATE NO ACTION ON DELETE CASCADE,
    title character varying(50),
    description text,
    startsOn timestamp with time zone,
    endsOn timestamp with time zone,
    done boolean default false,
    writingtasknr integer,
    expandToCalendarWeek boolean DEFAULT false
);
COMMENT ON TABLE userTimelineItem
    IS 'user defined items for timeline component';
COMMENT ON COLUMN userTimelineItem.uuid
    IS 'uuid of the user for that item';
COMMENT ON COLUMN userTimelineItem.courseTimelineItemId
    IS 'reference to original timeline item (optional, is NULL, if it is a user created item)';
COMMENT ON COLUMN userTimelineItem.rowId
    IS 'id of the row the usertimelineitem will be attached to (used as a short way to determine associated courseId)';
COMMENT ON COLUMN userTimelineItem.title
    IS 'title displayed in the timeline bar';
COMMENT ON COLUMN userTimelineItem.description
    IS 'description for that item';
COMMENT ON COLUMN userTimelineItem.startsOn
    IS 'starting time of item';
COMMENT ON COLUMN userTimelineItem.endsOn
    IS 'end time of item';
COMMENT ON COLUMN userTimelineItem.expandToCalendarWeek
    IS 'if set to true this item will expand its range view to beginning and end of the according calender week';
COMMENT ON COLUMN userTimelineItem.done
    IS 'item is done';
COMMENT ON COLUMN userTimelineItem.writingtasknr
    IS 'if item belongs to writing task: nr of that writing task. NOTE: rowId is used for reference to courseId';    



CREATE TABLE IF NOT EXISTS courseHint
(
    id serial PRIMARY KEY,
    courseId integer NOT NULL REFERENCES course(id) ON UPDATE NO ACTION ON DELETE CASCADE,
    sequence integer default 0,
    text text DEFAULT ''::text
);
COMMENT ON TABLE courseHint
    IS 'Hints like a link to feedback displayed in the hints component of a course';

    

CREATE TABLE IF NOT EXISTS courseLayout
(
    courseId integer NOT NULL REFERENCES course(id) ON UPDATE NO ACTION ON DELETE CASCADE,
    size character varying(5) NOT NULL,
    json text DEFAULT ''::text,
    PRIMARY KEY(courseId, size)
);
COMMENT ON TABLE courseLayout
    IS 'Layout for components of a course for different screen sizes';

       

CREATE TABLE IF NOT EXISTS lrsStoreForCourse
(
    courseId integer PRIMARY KEY NOT NULL REFERENCES course(id) ON UPDATE NO ACTION ON DELETE CASCADE,
    clientkey character varying(100) NOT NULL,
    clientsecret character varying(100) NOT NULL
);
COMMENT ON TABLE lrsStoreForCourse
    IS 'Credentials of the LRS store used for a course';



CREATE TABLE IF NOT EXISTS lmsWorker (
    id serial PRIMARY KEY,
    lms character varying(50) DEFAULT ''::text
    shortname character varying(50) DEFAULT ''::text,
    username character varying(100),
    password character varying(100),
    CONSTRAINT lmsworker_unique_lms_username UNIQUE (lms, username);
);
COMMENT ON TABLE lmsWorker
    IS 'Credentials for LMS accounts needed to connect to LMS';    
COMMENT ON COLUMN lmsWorker.lms
    IS 'LMS the worker is assigned to';
COMMENT ON COLUMN lmsWorker.shortname
    IS 'Optional name for lmsWorker for easier identification';



CREATE TABLE IF NOT EXISTS opalCourse (
  baseCourseId integer PRIMARY KEY NOT NULL REFERENCES baseCourse(id) ON UPDATE NO ACTION ON DELETE CASCADE,
  resource character varying(50) NOT NULL,
  lmsWorkerId integer REFERENCES lmsWorker(id) ON UPDATE NO ACTION ON DELETE  NO ACTION
);
COMMENT ON TABLE opalCourse
    IS 'OPAL specific data for a course';   
COMMENT ON COLUMN opalCourse.resource
    IS 'Resource Id for the course in OPAL';
COMMENT ON COLUMN opalCourse.lmsWorkerId
    IS 'Account used for authorization via the OPAL API';




CREATE TABLE IF NOT EXISTS moodleCourse (
  baseCourseId integer PRIMARY KEY NOT NULL REFERENCES baseCourse(id) ON UPDATE NO ACTION ON DELETE CASCADE,
  resource character varying(50) NOT NULL,
  lmsWorkerId integer NOT NULL REFERENCES lmsWorker(id) ON UPDATE NO ACTION ON DELETE  NO ACTION
);
COMMENT ON TABLE moodleCourse
    IS 'Moodle specific data for a course';    
COMMENT ON COLUMN moodleCourse.resource
    IS 'Resource Id for the course in Moodle';
COMMENT ON COLUMN moodleCourse.lmsWorkerId
    IS 'Account used for authorization via the Moodle API';


CREATE TABLE IF NOT EXISTS fileBucket (
    id serial PRIMARY KEY,
    baseCourseId integer NOT NULL REFERENCES baseCourse(id) ON UPDATE NO ACTION ON DELETE CASCADE,
    fileId character varying(50),
    filename character varying(100),
    purpose character varying(50),
    description text,
    lastModified timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP
);
COMMENT ON TABLE fileBucket
    IS 'stores details for files stored in MongoDB file bucket';    
COMMENT ON COLUMN fileBucket.baseCourseId
	IS 'Id of the base course the file belongs to';
COMMENT ON COLUMN fileBucket.fileId
	IS 'Id of the file in MongoDB file bucket';
COMMENT ON COLUMN fileBucket.filename
	IS 'Filename of the file in MongoDB file bucket';
COMMENT ON COLUMN fileBucket.purpose
	IS 'purpose of the file, e.g. privacystatement, etc, usually handled by MWB backend service';
COMMENT ON COLUMN fileBucket.description
	IS 'description for the files content';


CREATE TABLE IF NOT EXISTS externalservice
(
    servicename character varying(50) PRIMARY KEY NOT NULL,
    url text NOT NULL,
    username character varying(100),
    password character varying(100)
);
COMMENT ON TABLE externalservice
    IS 'credentials needed to connect to external services';    
    
CREATE TABLE IF NOT EXISTS ratingChatbotResponse
(
    id serial PRIMARY KEY,
    courseId integer NOT NULL REFERENCES course(id) ON UPDATE NO ACTION ON DELETE CASCADE,
    uuid uuid NOT NULL,
    isThumbsUp boolean DEFAULT false,
    originalMessage text NOT NULL,
    responseMessage text NOT NULL,
    comment text DEFAULT ''::text,
    timestamp timestamp with time zone
);
COMMENT ON TABLE ratingChatbotResponse
    IS 'user ratings for chatbot responses with optional comment';
COMMENT ON COLUMN ratingChatbotResponse.courseId
    IS 'courseId for the course where the chat is taking place';
COMMENT ON COLUMN ratingChatbotResponse.uuid
    IS 'uuid of the user rating the response';
COMMENT ON COLUMN ratingChatbotResponse.isThumbsUp
    IS 'true, if user gives a thumbs up, false else';
COMMENT ON COLUMN ratingChatbotResponse.originalMessage
    IS 'the original user message the chatbot responses to';
COMMENT ON COLUMN ratingChatbotResponse.responseMessage
    IS 'the response of the chatbot';
COMMENT ON COLUMN ratingChatbotResponse.comment
    IS 'optional user comment for the rating';
COMMENT ON COLUMN ratingChatbotResponse.timestamp
    IS 'timestamp the rating was stored in db';
    
    
-- add basic settings
INSERT INTO componentType (type, supportsAnonymousView, supportsComponentPrivacy) VALUES ('CHATBOT', false, false);
INSERT INTO componentType (type, supportsAnonymousView, supportsComponentPrivacy) VALUES ('KNOWLEDGEGRAPHTOOL', true, false);
INSERT INTO componentType (type, supportsAnonymousView, supportsComponentPrivacy) VALUES ('WRITINGTASKS', false, false);
INSERT INTO componentType (type, supportsAnonymousView, supportsComponentPrivacy) VALUES ('EXTENDEDEXAMFEEDBACK', true, false);
INSERT INTO componentType (type, supportsAnonymousView, supportsComponentPrivacy) VALUES ('SEARCHINRESOURCES', true, false);
INSERT INTO componentType (type, supportsAnonymousView, supportsComponentPrivacy) VALUES ('MWBHINTS', true, false);
INSERT INTO componentType (type, supportsAnonymousView, supportsComponentPrivacy) VALUES ('OPALONYXPROXY', false, true);
INSERT INTO componentType (type, supportsAnonymousView, supportsComponentPrivacy) VALUES ('TIMELINE', true, false);
INSERT INTO componentType (type, supportsAnonymousView, supportsComponentPrivacy) VALUES ('DASHBOARD', false, false);

INSERT INTO role (roleType) VALUES ('OWN');
INSERT INTO role (roleType) VALUES ('CREATE');

INSERT INTO objectForRole (objectType) VALUES ('BASECOURSE');
INSERT INTO objectForRole (objectType) VALUES ('COURSE');


-- add basic role
INSERT INTO person (email) VALUES (maria.bez@tu-dresden.de);
INSERT INTO hasRole (personId, roleId, objectForRoleId) VALUES (1, 2, 1);


-- add test data
-- 1 TUD BT1
INSERT INTO baseCourse (name) VALUES ('Bildungstechnologie I');
INSERT INTO hasRole (personId, roleId, objectForRoleId, objectId) VALUES (1, 1, 1, 1);
INSERT INTO course (baseCourseId, shortname, surname) VALUES (1, 'tud-bt1-test', 'Test (local database)');
INSERT INTO courseComponent (courseId, componentTypeId) VALUES (1, 1);
INSERT INTO courseComponent (courseId, componentTypeId, allowedForAnonymous) VALUES (1, 2, true);
INSERT INTO courseComponent (courseId, componentTypeId) VALUES (1, 3);
INSERT INTO courseComponent (courseId, componentTypeId) VALUES (1, 4);
INSERT INTO courseComponent (courseId, componentTypeId, allowedForAnonymous) VALUES (1, 5, true);
INSERT INTO courseComponent (courseId, componentTypeId, allowedForAnonymous) VALUES (1, 6, true);
INSERT INTO courseComponent (courseId, componentTypeId, allowedForAnonymous) VALUES (1, 8, true);
INSERT INTO privacystatement (courseId, version, link) VALUES (1, 1, '/assets/TUD/DSE/BT1_SS23_v1.pdf');
INSERT INTO privacystatement (courseId, purpose, version, link) VALUES (1, 'EXTENDEDEXAMFEEDBACK', 1, '/assets/TUD/DSE/BT1_SS23_v1.pdf');
INSERT INTO courseChatbot (courseId, chatbot) VALUES (1, 'Bitbot1');
INSERT INTO courseLayout (courseId, size, json) VALUES (1, 'S', '[{"componentType": "CHATBOT", "cols": 100, "rows": 10}, {"componentType": "WRITINGTASKS", "cols": 100, "rows": 4}, {"componentType": "SEARCHINRESOURCES", "cols": 100, "rows": 4}, {"componentType": "KNOWLEDGEGRAPHTOOL", "cols": 100, "rows": 20}, {"componentType": "TIMELINE", "cols": 100, "rows": 6}, {"componentType": "EXTENDEDEXAMFEEDBACK", "cols": 100, "rows": 6}, {"componentType": "MWBHINTS", "cols": 100, "rows": 4}]');
INSERT INTO courseLayout (courseId, size, json) VALUES (1, 'M', '[{"componentType": "CHATBOT", "cols": 30, "rows": 20}, {"componentType": "WRITINGTASKS", "cols": 25, "rows": 4}, {"componentType": "SEARCHINRESOURCES", "cols": 45, "rows": 4}, {"componentType": "KNOWLEDGEGRAPHTOOL", "cols": 70, "rows": 16}, {"componentType": "TIMELINE", "cols": 100, "rows": 6}, {"componentType": "MWBHINTS", "cols": 30, "rows": 5}, {"componentType": "EXTENDEDEXAMFEEDBACK", "cols": 70, "rows": 5}]');
INSERT INTO courseSearchIndex (courseId, searchId, searchIndex) VALUES (1, 'ud', 'ud_ss23');
INSERT INTO courseHint (courseId, sequence, text) VALUES (1, 1, 'Hast du Kritik, Lob und/oder Verbesserungsvorschläge? Kontaktiere gerne einen Projektmitarbeiter deines Vertrauens.');
INSERT INTO courseWritingTasks (courseId, validFrom, validUntil) VALUES (1, '2023-10-01', '2024-03-31');
INSERT INTO writingTask (courseId, nr, title, text) VALUES (1, 1, '"information literacy" - Medienkompetenz', 'Im Rahmen der Vorlesung haben Sie verschiedene Konzepte zur Beschreibung von Kompetenzen für die gesellschaftliche Teilhabe in einer digitalisierten Lebenswelt kennengelernt.<br><br>Erklären Sie einem Kommilitonen bzw. einer Kommilitonin, der/die die Vorlesung nicht besucht, inwiefern sich die beiden Ihnen bekannten Konzepte von "information literacy" und "Medienkompetenz" unterscheiden.<br> Beschreiben Sie auch, wo Ihre Aufgaben als zukünftige Lehrkraft bei der Vermittlung dieser Kompetenzen liegen können.<br><br>Schreiben Sie Ihre Antwort in einem zusammenhängenden Fließtext (mind. 350 Wörter) und laden Sie Ihren Text über den Chatbot im Kursbaustein "Mentoring Workbench" des OPAL-Kurses der Vorlesung  hoch. Sie erhalten dort unmittelbar ein Feedback zu Ihrem Text sowie eine visuelle Darstellung Ihres Textes in Form einer Wissenslandkarte, welche die wesentlichen von Ihnen erfassten Konzepte und deren Verbindungen abbildet.');


-- 2 TUD BT2
INSERT INTO baseCourse (name) VALUES ('Bildungstechnologie II');
INSERT INTO course (baseCourseId, shortname, surname) VALUES (2, 'tud-bt2-test', 'Test (local database)');
INSERT INTO courseComponent (courseId, componentTypeId) VALUES (2, 1);
INSERT INTO courseComponent (courseId, componentTypeId, allowedForAnonymous) VALUES (2, 2, true);
INSERT INTO courseComponent (courseId, componentTypeId) VALUES (2, 3);
INSERT INTO courseComponent (courseId, componentTypeId) VALUES (2, 4);
INSERT INTO courseComponent (courseId, componentTypeId, allowedForAnonymous) VALUES (2, 5, true);
INSERT INTO courseComponent (courseId, componentTypeId, allowedForAnonymous) VALUES (2, 6, true);
INSERT INTO courseComponent (courseId, componentTypeId, allowedForAnonymous) VALUES (2, 8, true);
INSERT INTO privacystatement (courseId, version, link) VALUES (2, 1, '/assets/TUD/DSE/BT2_WS23_24_v1.pdf');
INSERT INTO privacystatement (courseId, purpose, version, link) VALUES (2, 'EXTENDEDEXAMFEEDBACK', 1, '/assets/TUD/DSE/BT2_WS23_24_v1.pdf');
INSERT INTO courseChatbot (courseId, chatbot) VALUES (2, 'Bitbot2');
INSERT INTO courseLayout (courseId, size, json) VALUES (2, 'S', '[{"componentType": "CHATBOT", "cols": 100, "rows": 10}, {"componentType": "WRITINGTASKS", "cols": 100, "rows": 4}, {"componentType": "SEARCHINRESOURCES", "cols": 100, "rows": 4}, {"componentType": "KNOWLEDGEGRAPHTOOL", "cols": 100, "rows": 20}, {"componentType": "TIMELINE", "cols": 100, "rows": 6}, {"componentType": "EXTENDEDEXAMFEEDBACK", "cols": 100, "rows": 6}, {"componentType": "MWBHINTS", "cols": 100, "rows": 4}]');
INSERT INTO courseLayout (courseId, size, json) VALUES (2, 'M', '[{"componentType": "CHATBOT", "cols": 30, "rows": 20}, {"componentType": "WRITINGTASKS", "cols": 25, "rows": 4}, {"componentType": "SEARCHINRESOURCES", "cols": 45, "rows": 4}, {"componentType": "KNOWLEDGEGRAPHTOOL", "cols": 70, "rows": 16}, {"componentType": "TIMELINE", "cols": 100, "rows": 6}, {"componentType": "MWBHINTS", "cols": 30, "rows": 5}, {"componentType": "EXTENDEDEXAMFEEDBACK", "cols": 70, "rows": 5}]');
INSERT INTO courseSearchIndex (courseId, searchId, searchIndex) VALUES (2, 'ud', 'ud_ws23');
INSERT INTO courseHint (courseId, sequence, text) VALUES (2, 1, 'Hast du Kritik, Lob und/oder Verbesserungsvorschläge? Kontaktiere gerne einen Projektmitarbeiter deines Vertrauens.');
INSERT INTO courseWritingTasks (courseId, validFrom, validUntil) VALUES (2, '2023-10-01', '2024-03-31');
INSERT INTO writingTask (courseId, nr, title, text) VALUES (2, 1, 'Schreibauftrag zur Vorlesung: „Überblick Lernen“', 'Im Rahmen Ihres Studiums haben Sie in verschiedenen Lehrveranstaltungen und bei unterschiedlichen Lehrenden bereits Definitionen für den Begriff „Lernen“ kennengelernt. In unserer aktuellen Online-Vorlesung „Überblick Lernen“ werden Ihnen verschiedene Lerntheorien vorgestellt.<br><br>Erklären Sie einem Kommilitonen bzw. einer Kommilitonin, der/die die Vorlesung nicht besucht, inwiefern es sich bei den Ihnen bekannten Definitionen von „Lernen“ um ein konnektivistisches Begriffsverständnis handelt. Gehen Sie dabei auch auf die Besonderheiten dieser Lerntheorie gegenüber den anderen Ihnen bekannten Lerntheorien ein.<br><br>Schreiben Sie Ihre Antwort in einem zusammenhängenden Fließtext (mind. 350 Wörter) und laden Sie Ihren Text über den Chatbot im Kursbaustein "Mentoring Workbench" des OPAL-Kurses der Vorlesung  hoch. Sie erhalten dort unmittelbar ein Feedback zu Ihrem Text sowie eine visuelle Darstellung Ihres Textes in Form einer Wissenslandkarte, welche die wesentlichen von Ihnen erfassten Konzepte und deren Verbindungen abbildet.');
INSERT INTO courseExam (courseId, name) VALUES (2, 'Probeklausur');
INSERT INTO onyxExam (courseExamId, onyxTestId, opalNodeId) VALUES (1, 33420607567, 1652841063736657011);
INSERT INTO easLitExam (courseExamId, easlitProjectId, topic) VALUES (1, '26', 'Bildungstechnologien II');
INSERT INTO courseTimeline (courseId, startson, endson) VALUES (2, '2024-04-01', '2024-09-30');


-- 3 UL BiWi5
INSERT INTO baseCourse (name) VALUES ('BiWi5');
INSERT INTO course (baseCourseId, shortname, surname) VALUES (3, 'ul-biwi5-test', 'Test (local database)');
INSERT INTO courseComponent (courseId, componentTypeId) VALUES (3, 1);
INSERT INTO courseComponent (courseId, componentTypeId) VALUES (3, 2);
INSERT INTO courseComponent (courseId, componentTypeId) VALUES (3, 3);
INSERT INTO courseComponent (courseId, componentTypeId) VALUES (3, 5);
INSERT INTO courseComponent (courseId, componentTypeId) VALUES (3, 6);
INSERT INTO courseComponent (courseId, componentTypeId) VALUES (3, 8);
INSERT INTO courseComponent (courseId, componentTypeId) VALUES (3, 9);
INSERT INTO privacystatement (courseId, version, link) VALUES (3, 1, '/assets/TUD/DSE/BiWi5_SS23_v1.pdf');
INSERT INTO courseChatbot (courseId, chatbot) VALUES (3, 'Feedbot');
INSERT INTO courseLayout (courseId, size, json) VALUES (3, 'S', '[{"componentType": "DASHBOARD", "cols": 100, "rows": 4}, {"componentType": "CHATBOT", "cols": 100, "rows": 10}, {"componentType": "WRITINGTASKS", "cols": 100, "rows": 4}, {"componentType": "SEARCHINRESOURCES", "cols": 100, "rows": 4}, {"componentType": "KNOWLEDGEGRAPHTOOL", "cols": 100, "rows": 20}, {"componentType": "TIMELINE", "cols": 100, "rows": 8}, {"componentType": "MWBHINTS", "cols": 100, "rows": 8}]');
INSERT INTO courseLayout (courseId, size, json) VALUES (3, 'M', '[{"componentType": "DASHBOARD", "cols": 30, "rows": 4}, {"componentType": "WRITINGTASKS", "cols": 25, "rows": 4}, {"componentType": "SEARCHINRESOURCES", "cols": 45, "rows": 4}, {"componentType": "CHATBOT", "cols": 30, "rows": 20}, {"componentType": "KNOWLEDGEGRAPHTOOL", "cols": 70, "rows": 20}, {"componentType": "TIMELINE", "cols": 100, "rows": 8}, {"componentType": "MWBHINTS", "cols": 100, "rows": 6}]');
INSERT INTO courseSearchIndex (courseId, searchId, searchIndex) VALUES (3, 'ul', 'ul_s01');
INSERT INTO courseHint (courseId, sequence, text) VALUES (3, 1, 'Bei Schwierigkeiten mit der Workbench nutze bitte das Workbench-Forum im Moodle. Bei technischen Problemen kontaktiere bitte den <a href="mailto: mwb-support@mailbox.tu-dresden.de">Mentoring-Workbench-Support</a>.');
INSERT INTO courseHint (courseId, sequence, text) VALUES (3, 2, 'Hast du Kritik, Lob und/oder Verbesserungsvorschläge? Kontaktiere gerne einen Projektmitarbeiter deines Vertrauens.');
INSERT INTO courseTimeline (courseId, startson, endson) VALUES (3, '2024-04-01', '2024-09-30');
INSERT INTO courseTimelineRow (courseId, row, description, isDetail, createNewItems) VALUES (3, 1, 'Vorlesung', false, false);  
INSERT INTO courseTimelineRow (courseId, row, description, isDetail, createNewItems) VALUES (3, 2, 'Material', true, false);  
INSERT INTO courseTimelineRow (courseId, row, description, isDetail, createNewItems) VALUES (3, 3, 'Schreibaufgaben', false, true);  
INSERT INTO courseTimelineRow (courseId, row, description, isDetail, createNewItems) VALUES (3, 4, 'eigene Einträge', false, true);  
INSERT INTO courseTimelineItem (rowId, title, description, startson, endson, editableTime, editableTitle) VALUES (1, 'Video Vorlesung 1', '', '2024-03-24 23:00:00', '2024-03-31 22:59:00', true, false);
INSERT INTO courseTimelineItem (rowId, title, description, startson, endson, editableTime, editableTitle) VALUES (2, 'Material Moodle 1', '', '2024-03-24 23:00:00', '2024-03-31 22:59:00', true, false);
INSERT INTO courseTimelineItem (rowId, title, description, startson, endson, editableTime, editableTitle) VALUES (3, 'Schreibaufgabe 1', '', '2024-03-24 23:00:00', '2024-03-31 22:59:00', true, false);

INSERT INTO courseWritingTasks (courseId, validUntil) VALUES (3, '2024-02-02  23:59:00');
INSERT INTO writingTask (courseId, nr, text) VALUES (3, 1, 'Aus Sicht des Strukturfunktionalismus erscheint Gesellschaft als Zusammenwirken verschiedener Subsysteme. Dabei erfüllen diese Systeme jeweils verschiedene Funktionen.<br>(A) Beschreiben Sie diese strukturfunktionalistische Sichtweise näher.<br>(B) Erläutern Sie dabei den Zusammenhang der drei wichtigsten Teilsysteme.');
INSERT INTO writingTask (courseId, nr, text) VALUES (3, 2, 'Bildungssysteme sind in unterschiedlichen Ländern sehr unterschiedlich ausgestaltet. Bray und Jiang (2007) beschreiben vor diesem Hintergrund verschiedene Probleme, die sich beim Vergleich von Bildungssystemen ergeben.<br>(A) Erläutern Sie diese Probleme und nehmen Sie Bezug auf den Vergleich zwischen Kanada und Deutschland.<br>(B) Beschreiben Sie dabei, wie Deutschland und Kanadas Bildungssysteme mit  Migration umgehen.');
INSERT INTO writingTask (courseId, nr, text) VALUES (3, 3, 'Die staatliche Steuerung von Bildungssystemen hat sich in den letzten Jahrzehnten verändert und greift dabei verstärkt auf Verfahren der Evaluation zurück.<br>(A) Erläutern Sie wesentliche Aspekte dieser Veränderung.<br>(B) Beschreiben Sie, wie externe Schulevaluation in Sachsen organisiert ist und in welcher Weise unterschiedliche Akteure damit umgehen.');
INSERT INTO writingTask (courseId, nr, text) VALUES (3, 4, 'Die Rede von "Bildung" meint oft sehr unterschiedliche Dinge. Es handelt sich dabei um ein Konzept, das viele Facetten besitzt und stetigem Wandel unterliegt.<br>(A) Beschreiben Sie wichtige Aspekte dieses Konzepts und gehen Sie auf einige historische Veränderung ein, die es durchlaufen hat.<br>(B) Erläutern Sie, wie die These der "digitalen Verstärkung" das Spannungsfeld von Bildung und Digitalisierung zu verstehen hilft.');
INSERT INTO writingTask (courseId, nr, text) VALUES (3, 5, 'Auch wenn alle Menschen Erziehungsprozesse durchlaufen haben, bleibt das Konzept der Erziehung häufig unscharf.<br>(A) Beschreiben Sie wichtige Aspekte dieses Konzepts und gehen Sie auf einige historische Veränderungen ein, die es durchlaufen hat.<br>(B) Erläutern Sie, auf welche Weise Kurt Lewins Forschung zu Führungsstilen neue Einblicke in Grundfragen der Erziehung ermöglicht hat.');
INSERT INTO writingTask (courseId, nr, text) VALUES (3, 6, 'In der Moderne wurden die Konzepte der Bildung und Erziehung um das der "Sozialisation" erweitert.<br>(A) Erläutern Sie, was unter "Sozialisation" verstanden wird und inwiefern dieses Konzept andere Aspekte berücksichtigt als "Bildung" und "Erziehung".<br>(B) Beschreiben Sie die wichtigsten Phasen der Sozialisation in ihrem Zusammenhang und stellen Sie die Besonderheit der Schule als Sozialisationsinstanz dar.');
INSERT INTO writingTask (courseId, nr, text) VALUES (3, 7, 'Ob und wie Menschen an Bildung teilhaben können, macht einen großen Unterschied aus. Aus diesem Grund gilt Gerechtigkeit als ein wichtiges Kriterium von Bildungssystemen.<br>(A) Beschreiben Sie, welche Charakteristika Giesinger für das "Standverständnis von Bildungsgerechtigkeit" nennt und wie er sein "Schwellenkonzept" davon abgrenzt.<br>(B) Erläutern Sie, auf welche Probleme Giesinger mit dem "Schwellenkonzept" reagiert und wie er sie lösen will.');
INSERT INTO writingTask (courseId, nr, text) VALUES (3, 8, 'Geißler (2012) greift aktuelle Debatten um die Bildungsgerechtigkeit auf, argumentiert jedoch, dass Chancengleichheit eine "Illusion" sei.<br>(A) Beschreiben Sie, mit welchen Argumenten Geißler seine These von der Bildungsbenachteiligung in Deutschland stützt.<br>(B) Erläutern Sie, inwiefern seine Argumentation auch auf die gegenwärtige Situation des Bildungssystems zutrifft');
INSERT INTO writingTask (courseId, nr, text) VALUES (3, 9, 'Der Bericht "Bildung in Sachsen" (DIPF 2019) identifiziert Trends und Problemlagen innerhalb des sächsischen Bildungssystems und ordnet diese vor dem Hintergrund der Entwicklungen auf Bundesebene ein.<br>Beschreiben Sie anhand selbst gewählter Statistiken aus diesem Bericht die Entwicklung in Sachsen hinsichtlich<br>(A) ganztägiger Bildung und Betreuung sowie Personalausstattung im Schulwesen.<br>(B) Migrationshintergrund und Zuwanderungserfahrung.');
INSERT INTO writingTask (courseId, nr, text) VALUES (3, 10, 'Die bildungspolitische Diskussion um "Inklusion" ist alles andere als übersichtlich.<br>(A) Beschreiben Sie zunächst, welche Rolle die Salamanca-Erklärung und die UN-Behindertenrechtskonvention in diesem Zusammenhang spielen.<br>(B) Gehen Sie dann auf die verschiedenen Formen des Umgang mit Vielfalt ein und erläutern Sie diese aus allgemeinpädagogischer Sicht.');
INSERT INTO writingTask (courseId, nr, text) VALUES (3, 11, 'Auch das sächsische Bildungssystem lässt sich im Hinblick auf seinen Umgang mit Vielfalt betrachten.<br>(A) Beschreiben Sie zunächst, wie Sachsen auf die Anforderungen der UN-BRK reagiert und diese in der Novellierung des sächsischen Schulgesetzes umsetzt.<br>(B) Erläutern Sie anhand statistischer Daten, welche Dimensionen der Ungleichheit in Leipzig und Sachsen zu beobachten sind und wie diese mit dem Bildungssystem in Verbindung stehen.');
INSERT INTO writingTask (courseId, nr, text) VALUES (3, 12, 'Kanada wird häufig als Vorreiter für inklusive Bildungspolitik dargestellt.<br>(A) Beschreiben Sie, mit welchen Maßnahmen in New Brunswick der Anspruch der Inklusion umgesetzt werden soll.<br>(B) Erläutern Sie, wie Schröder (2017) die erziehungswissenschaftliche Perspektive auf Kanada beurteilt und wie sie den dortigen Umgang mit Vielfalt einschätzt.');


-- 4 TUD MB
INSERT INTO baseCourse (name) VALUES ('Medienbildung');
INSERT INTO course (baseCourseId, shortname, surname) VALUES (4, 'tud-mb-test', 'Test (local database)');
INSERT INTO courseComponent (courseId, componentTypeId) VALUES (4, 1);
INSERT INTO courseComponent (courseId, componentTypeId, allowedForAnonymous) VALUES (4, 2, true);
INSERT INTO courseComponent (courseId, componentTypeId) VALUES (4, 3);
INSERT INTO courseComponent (courseId, componentTypeId, allowedForAnonymous) VALUES (4, 5, true);
INSERT INTO courseComponent (courseId, componentTypeId, allowedForAnonymous) VALUES (4, 6, true);
INSERT INTO courseComponent (courseId, componentTypeId, allowedForAnonymous) VALUES (4, 8, true);
INSERT INTO privacystatement (courseId, version, link) VALUES (4, 1, '/assets/TUD/DSE/MB_WS23_24_v1.pdf');
INSERT INTO courseChatbot (courseId, chatbot) VALUES (4, 'Bitbot1');
INSERT INTO courseLayout (courseId, size, json) VALUES (4, 'S', '[{"componentType": "CHATBOT", "cols": 100, "rows": 10}, {"componentType": "WRITINGTASKS", "cols": 100, "rows": 4}, {"componentType": "SEARCHINRESOURCES", "cols": 100, "rows": 4}, {"componentType": "KNOWLEDGEGRAPHTOOL", "cols": 100, "rows": 20}]');
INSERT INTO courseLayout (courseId, size, json) VALUES (4, 'M', '[{"componentType": "CHATBOT", "cols": 30, "rows": 20}, {"componentType": "WRITINGTASKS", "cols": 25, "rows": 4}, {"componentType": "SEARCHINRESOURCES", "cols": 45, "rows": 4}, {"componentType": "KNOWLEDGEGRAPHTOOL", "cols": 70, "rows": 16}, {"componentType": "MWBHINTS", "cols": 100, "rows": 3}]');
INSERT INTO courseSearchIndex (courseId, searchId, searchIndex) VALUES (4, 'ud', 'ud_mb23');
INSERT INTO courseHint (courseId, sequence, text) VALUES (4, 1, 'Hast du Kritik, Lob und/oder Verbesserungsvorschläge? Kontaktiere gerne einen Projektmitarbeiter deines Vertrauens.');
INSERT INTO courseWritingTasks (courseId, validFrom, validUntil) VALUES (4, '2023-10-01', '2024-03-31');


-- 5 HTWK Mathe
INSERT INTO baseCourse (name) VALUES ('Mathematik');
INSERT INTO course (baseCourseId, shortname, surname) VALUES (5, 'htwk-mathe-test', 'Test (local database)');
INSERT INTO courseComponent (courseId, componentTypeId) VALUES (5, 7);
INSERT INTO privacystatement (courseId, purpose, version, link) VALUES (5, 'OPALONYXPROXY', 1, '/assets/TUD/DSE/BT1_SS23_v1.pdf');
INSERT INTO courseLayout (courseId, size, json) VALUES (5, 'S', '[{"componentType": "OPALONYXPROXY", "cols": 100, "rows": 8}, {"componentType": "MWBHINTS", "cols": 100, "rows": 4}]');
INSERT INTO courseLayout (courseId, size, json) VALUES (5, 'M', '[{"componentType": "OPALONYXPROXY", "cols": 100, "rows": 8}, {"componentType": "MWBHINTS", "cols": 100, "rows": 4}]');
INSERT INTO courseHint (courseId, sequence, text) VALUES (5, 1, 'Hast du Kritik, Lob und/oder Verbesserungsvorschläge? Kontaktiere gerne einen Projektmitarbeiter deines Vertrauens.');


-- 6 TU Chemnitz
INSERT INTO baseCourse (name) VALUES ('TU Chemnitz');
INSERT INTO course (baseCourseId, shortname, surname) VALUES (6, 'tuc-testbed-test', 'Test (local database)');
INSERT INTO courseComponent (courseId, componentTypeId) VALUES (6, 1);
INSERT INTO courseComponent (courseId, componentTypeId) VALUES (6, 2);
INSERT INTO courseComponent (courseId, componentTypeId) VALUES (6, 3);
INSERT INTO courseComponent (courseId, componentTypeId) VALUES (6, 6);
INSERT INTO courseComponent (courseId, componentTypeId) VALUES (6, 8);
INSERT INTO privacystatement (courseId, version, link) VALUES (6, 1, '/assets/TUD/DSE/BT2_WS23_24_v1.pdf');
INSERT INTO courseChatbot (courseId, chatbot) VALUES (6, 'Chemnitzbot');
INSERT INTO courseLayout (courseId, size, json) VALUES (6, 'S', '[{"componentType": "CHATBOT", "cols": 100, "rows": 10}, {"componentType": "WRITINGTASKS", "cols": 100, "rows": 4}, {"componentType": "KNOWLEDGEGRAPHTOOL", "cols": 100, "rows": 20}, {"componentType": "MWBHINTS", "cols": 100, "rows": 8}]');
INSERT INTO courseLayout (courseId, size, json) VALUES (6, 'M', '[{"componentType": "CHATBOT", "cols": 30, "rows": 24}, {"componentType": "WRITINGTASKS", "cols": 25, "rows": 4}, {"componentType": "KNOWLEDGEGRAPHTOOL", "cols": 70, "rows": 20}, {"componentType": "MWBHINTS", "cols": 100, "rows": 6}]');
INSERT INTO courseHint (courseId, sequence, text) VALUES (6, 1, 'Hast du Kritik, Lob und/oder Verbesserungsvorschläge? Kontaktiere gerne einen Projektmitarbeiter deines Vertrauens.');
INSERT INTO courseWritingTasks (courseId) VALUES (6);

