CREATE TABLE IF NOT EXISTS GenericChatBotIntents
(
    intent character varying(150) NOT NULL ,
    answer text,
    PRIMARY KEY(intent)
);
COMMENT ON TABLE GenericChatBotIntents
    IS 'default chatbot responses for intents defined in generic chat bot';
COMMENT ON COLUMN GenericChatBotIntents.answer
    IS 'response corresponding to the intent';

INSERT INTO genericChatBotIntents (intent, answer) VALUES ('welcomeUser', 'Hallo %1.');

