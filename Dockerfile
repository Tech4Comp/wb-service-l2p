### Get JAVA

FROM openjdk:17-alpine

ENV HTTP_PORT=8080
ENV HTTPS_PORT=8443
ENV LAS2PEER_PORT=9011

RUN apk add --update bash && rm -f /var/cache/apk/*
RUN addgroup -g 1000 -S las2peer && \
    adduser -u 1000 -S las2peer -G las2peer

### 3. Get Python, PIP

RUN apk add --no-cache python3 \
&& python3 -m ensurepip \
&& pip3 install --upgrade pip setuptools \
&& rm -r /usr/lib/python*/ensurepip && \
if [ ! -e /usr/bin/pip ]; then ln -s pip3 /usr/bin/pip ; fi && \
if [[ ! -e /usr/bin/python ]]; then ln -sf /usr/bin/python3 /usr/bin/python; fi && \
rm -r /root/.cache

RUN pip install --no-cache-dir requests

COPY --chown=las2peer:las2peer . /src
WORKDIR /src

# run the rest as unprivileged user
USER las2peer
#RUN dos2unix gradlew
#RUN dos2unix gradle.properties
#RUN dos2unix /src/docker-entrypoint.sh
#RUN dos2unix /src/etc/i5.las2peer.connectors.webConnector.WebConnector.properties
#RUN dos2unix /src/etc/i5.las2peer.services.mwbService.MwbService.properties
#RUN chmod -R a+rwx /src 
RUN chmod +x gradlew && ./gradlew build --exclude-task test
RUN chmod +x /src/docker-entrypoint.sh

EXPOSE $HTTP_PORT
EXPOSE $HTTPS_PORT
EXPOSE $LAS2PEER_PORT
ENTRYPOINT ["/src/docker-entrypoint.sh"]