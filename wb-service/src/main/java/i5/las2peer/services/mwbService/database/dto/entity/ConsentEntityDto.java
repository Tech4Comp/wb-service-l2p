package i5.las2peer.services.mwbService.database.dto.entity;


import lombok.AllArgsConstructor;
import lombok.Data;

import java.sql.Timestamp;
import java.util.UUID;

/**
 * Entity dto corresponding to database table "consent"
 */
@Data
@AllArgsConstructor
public class ConsentEntityDto {
    private UUID uuid;
    private Integer privacyStatementId;
    private Timestamp timestamp;

    /**
     * default constructor
     */
    public ConsentEntityDto() {}
}
