package i5.las2peer.services.mwbService.database.dto.entity;


import i5.las2peer.services.mwbService.database.dto.LmsCourseDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Entity dto corresponding to database table moodleCourse
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class MoodleCourseEntityDto  extends LmsCourseDto {
    private String moodleResource;

    /**
     * default constructor
     */
    public MoodleCourseEntityDto() {}

    /**
     * constructor
     *
     * @param lmsCourseDto basic data as LmsCourseDto
     * @param resource -
     */
    public MoodleCourseEntityDto(LmsCourseDto lmsCourseDto, String resource) {
        super(lmsCourseDto);
        this.moodleResource = resource;
    }

}
