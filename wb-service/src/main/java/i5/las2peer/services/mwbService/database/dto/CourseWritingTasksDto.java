package i5.las2peer.services.mwbService.database.dto;

import i5.las2peer.services.mwbService.database.dto.entity.WritingTaskEntityDto;
import i5.las2peer.services.mwbService.database.dto.entity.CourseWritingTasksEntityDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;


/**
 * Dto for data of course component WRITINGTASKS
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class CourseWritingTasksDto extends CourseWritingTasksEntityDto {
    private List<WritingTaskEntityDto> tasks;

    /**
     * default constructor
     */
    public CourseWritingTasksDto() {}

    /**
     * constructor
     *
     * @param courseWritingTasksEntityDto basic data as WritingTasksEntityDto
     * @param tasks list of WritingTaskEntityDto
     */
    public CourseWritingTasksDto(CourseWritingTasksEntityDto courseWritingTasksEntityDto, List<WritingTaskEntityDto> tasks) {
        super(courseWritingTasksEntityDto);
        this.tasks = tasks;
    }
}
