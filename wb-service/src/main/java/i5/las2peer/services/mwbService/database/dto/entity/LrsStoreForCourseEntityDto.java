package i5.las2peer.services.mwbService.database.dto.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Entity dto corresponding to database table "lrsStoreForCourse"
 */
@Data
@AllArgsConstructor
public class LrsStoreForCourseEntityDto {
    private Integer courseId;
    private String clientKey;
    private String clientSecret;

    /**
     * default constructor
     */
    public LrsStoreForCourseEntityDto() {}
}