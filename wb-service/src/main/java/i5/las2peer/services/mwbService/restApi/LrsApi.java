package i5.las2peer.services.mwbService.restApi;


import com.fasterxml.jackson.databind.ObjectMapper;
import i5.las2peer.services.mwbService.Authentication;
import i5.las2peer.services.mwbService.MwbService;
import i5.las2peer.services.mwbService.database.LazyPostgresConnection;
import i5.las2peer.services.mwbService.database.dao.entity.LrsStoreForCourseEntityDao;
import i5.las2peer.services.mwbService.database.dao.entity.PrivacyStatementEntityDao;
import i5.las2peer.services.mwbService.database.dto.entity.LrsStoreForCourseEntityDto;
import i5.las2peer.services.mwbService.database.dto.entity.PrivacyStatementEntityDto;
import i5.las2peer.services.mwbService.externalServices.LrsConnection;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *  handles REST API calls related to LRS stores
 */
@Path("/lrs")
@RolesAllowed("authenticated")
public class LrsApi {

    /**
     * Get lrs data for course
     * @param courseId -
     * @return  200 with lrs data as JSON
     *          400 Bad Request if something went wrong
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @GET
    @Path("/{courseId}")
    @RolesAllowed("authenticated")
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Get lrs data for course",
            response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Failed to upsert lrs data for course"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Missing rights"),
            @ApiResponse(code = 500, message = "Failed to establish connection to PostgresDB")
    })
    public Response getLrsData(
            @PathParam("courseId") Integer courseId
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthorizationForCourse(connection.getConnection(), courseId);
            LrsStoreForCourseEntityDto lrsStoreForCourseEntityDto = LrsStoreForCourseEntityDao.readData(connection.getConnection(), courseId);
            return MwbService.responseOK(new ObjectMapper().writeValueAsString(lrsStoreForCourseEntityDto));
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * Creates a new entry with lrs data for a course or updates an existing one
     * @param lrsStoreForCourseEntityDto to create/update as LrsStoreForCourseEntityDto as JSON object.
     * @return  200 OK, if successful
     *          400 Bad Request if something went wrong
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @POST
    @RolesAllowed("authenticated")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Upsert lrs data for course",
            response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Failed to upsert lrs data for course"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Missing rights"),
            @ApiResponse(code = 500, message = "Failed to establish connection to PostgresDB")
    })
    public Response upsertLrsData(
            @ApiParam(value = "The privacy statement to create/update as a JSON object", required = true) LrsStoreForCourseEntityDto lrsStoreForCourseEntityDto
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthorizationForCourse(connection.getConnection(), lrsStoreForCourseEntityDto.getCourseId());
            LrsStoreForCourseEntityDao lrsStoreForCourseEntityDao = new LrsStoreForCourseEntityDao(connection.getConnection(), lrsStoreForCourseEntityDto);
            lrsStoreForCourseEntityDao.upsert();
            return MwbService.responseOK();
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * Check if lrs data for course is valid
     * @param courseId -
     * @return  200 OK, if successful
     *          400 Bad Request if something went wrong
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @GET
    @Path("/check/{courseId}")
    @RolesAllowed("authenticated")
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Check lrs data for course",
            response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Failed to upsert lrs data for course"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Missing rights"),
            @ApiResponse(code = 500, message = "Failed to establish connection to PostgresDB")
    })
    public Response checkLrsData(
            @PathParam("courseId") Integer courseId
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthorizationForCourse(connection.getConnection(), courseId);
            LrsConnection lrsConnection = new LrsConnection(connection.getConnection(), courseId);
            boolean hasAuthorization = lrsConnection.checkAuthorization();
            return MwbService.response(hasAuthorization);
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    //******************************************************************************************
    // endpoints allowed for maintainers only (mainly for testing purposes)
    //******************************************************************************************

    /**
     * cleanup method
     * deletes the specified statement from the LRS store associated with that course
     *
     * @param courseId -
     * @param statementId - statement.id
     * @return message whether the item was successfully deleted
     */
    @DELETE
    @Path("/deleteStatement/{courseId}/{statementId}")
    public Response deleteLrsStatement(
            @PathParam("courseId") Integer courseId,
            @PathParam("statementId") String statementId) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkMaintainer();
            new LrsConnection(connection.getConnection(), courseId).deleteStatement(statementId);
            return MwbService.responseOK();
        }
        catch (Exception e) {
            return MwbService.responseError(e);
        }
    }
}
