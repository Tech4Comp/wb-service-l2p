package i5.las2peer.services.mwbService.database.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import java.sql.Timestamp;

/**
 * Dto with basic course info including property last visited
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class CourseLastVisitedDto extends CourseDto {
    private Timestamp lastVisited;

    /**
     * @param course data as CourseDto
     * @param lastVisited as timestamp
     */
    public CourseLastVisitedDto(CourseDto course, Timestamp lastVisited) {
        super(course);
        this.lastVisited = lastVisited;
    }
}
