package i5.las2peer.services.mwbService.database.dao.entity;


import i5.las2peer.services.mwbService.database.dao.DataAccessObject;
import i5.las2peer.services.mwbService.database.dto.entity.HasRoleEntityDto;

import java.sql.*;


/**
 * Entity dao to handle basic database operations for table "hasRole"
 */
public class HasRoleEntityDao extends DataAccessObject {
    private final HasRoleEntityDto hasRoleEntityDto;

    /**
     * Table name for data
     */
    public static final String TABLE = "hasRole";

    /**
     * constructor
     * @param connection to the database
     * @param hasRoleEntityDto -
     */
    public HasRoleEntityDao(Connection connection, HasRoleEntityDto hasRoleEntityDto) {
        super(connection);
        this.hasRoleEntityDto = hasRoleEntityDto;
    }

    /**
     * constructor
     * @param connection to the database
     * @param personId -
     * @param roleId -
     * @param objectForRoleId -
     * @param objectId -
     */
    public HasRoleEntityDao(Connection connection, Integer personId, Integer roleId, Integer objectForRoleId, Integer objectId) throws Exception {
        super(connection);
        this.hasRoleEntityDto = readData(connection, personId, roleId, objectForRoleId, objectId);
    }


    //******************************************************************************************
    // instance methods
    //******************************************************************************************

    public Integer getId() throws Exception {
        assertNotNull();
        return this.hasRoleEntityDto.getId();
    }

    public void assertNotNull() throws Exception {
        if (this.hasRoleEntityDto == null)
            throw getInvalidDataException("Role not found.");
    }


    //******************************************************************************************
    // basic database operations
    //******************************************************************************************

    /**
     * insert a new record into the database
     *
     * @throws Exception if something went wrong
     */
    public void insert() throws Exception {
        String sqlStatement = "INSERT INTO " + TABLE +
                " (personId, roleId, objectForRoleId, objectId) VALUES(?,?,?,?);";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, hasRoleEntityDto.getPersonId());
            preparedStatement.setInt(2, hasRoleEntityDto.getRoleId());
            preparedStatement.setInt(3, hasRoleEntityDto.getObjectForRoleId());
            preparedStatement.setInt(4, hasRoleEntityDto.getObjetId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw DataAccessObject.createDatabaseException("Failed to insert role data.", e);
        }
    }


    /**
     * delete record
     * @throws Exception -
     */
    public void delete() throws Exception {
        String sqlStatement = "DELETE FROM " + TABLE +
                " WHERE personId=? AND roleId=? AND objectForRoleId=? AND objectId=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, this.hasRoleEntityDto.getPersonId());
            preparedStatement.setInt(2, this.hasRoleEntityDto.getRoleId());
            preparedStatement.setInt(3, this.hasRoleEntityDto.getObjectForRoleId());
            preparedStatement.setInt(4, this.hasRoleEntityDto.getObjetId());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to delete role.", e);
        }
    }


    //******************************************************************************************
    // static methods for database operations
    //******************************************************************************************

    public static HasRoleEntityDto readData(Connection connection, Integer personId, Integer roleId, Integer objectForRoleId, Integer objectId) throws Exception {
        String sqlStatement = "SELECT * FROM " + TABLE   +
                " WHERE personId=? AND roleId=? AND objectForRoleId=? AND objectId=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, personId);
            preparedStatement.setInt(2, roleId);
            preparedStatement.setInt(3, objectForRoleId);
            preparedStatement.setInt(4, objectId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return createHasRoleEntityDto(resultSet);
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve data for role.", e);
        }
        return null;
    }

    /**
     * delete record with given id
     *
     * @param connection to database
     * @param id of the record to delete
     * @throws Exception -
     */
    public static void delete(Connection connection, Integer id) throws Exception {
        String sqlStatement = "DELETE FROM " + TABLE +
                " WHERE id=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to delete role.", e);
        }
    }


    //******************************************************************************************
    // static methods to create role related DTOs from a resultSet
    //******************************************************************************************

    /**
     * create CourseEntityDto using data from resultSet
     *
     * @param resultSet containing data
     * @return HasRoleEntityDto
     * @throws Exception -
     */
    public static HasRoleEntityDto createHasRoleEntityDto(ResultSet resultSet) throws Exception {
        Integer id = resultSet.getInt("id");
        Integer personId = resultSet.getInt("personId");
        Integer roleId = resultSet.getInt("roleId");
        Integer objectForRoleId = resultSet.getInt("objectForRoleId");
        Integer objetId = resultSet.getInt("objetId");
        return new HasRoleEntityDto(id, personId, roleId, objectForRoleId, objetId);
    }

}
