package i5.las2peer.services.mwbService.database.dao.entity;

import i5.las2peer.services.mwbService.database.dao.DataAccessObject;
import i5.las2peer.services.mwbService.database.dao.LmsCourseDao;
import i5.las2peer.services.mwbService.database.dto.entity.BaseCourseEntityDto;
import i5.las2peer.services.mwbService.database.dto.LmsCourseDto;
import i5.las2peer.services.mwbService.database.dto.entity.OpalCourseEntityDto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/**
 * Entity dao to handle basic database operations for table "opalCourse"
 */
public class OpalCourseEntityDao extends LmsCourseDao {
    private final OpalCourseEntityDto opalCourseEntityDto;

    /**
     * Table name for entity
     */
    public static final String TABLE = "opalCourse";

    /**
     * LMS type
     */
    public static final String LMS =  "opal";

    /**
     * constructor
     *
     * @param connection to the database
     * @param opalCourseEntityDto -
     */
    public OpalCourseEntityDao(Connection connection, OpalCourseEntityDto opalCourseEntityDto) {
        super(connection);
        this.opalCourseEntityDto = opalCourseEntityDto;
    }

    /**
     * constructor
     *
     * @param connection to the database
     * @param baseCourseId -
     * @throws Exception if requested connection could not be established
     */
    public OpalCourseEntityDao(Connection connection, Integer baseCourseId) throws Exception {
        super(connection);
        this.opalCourseEntityDto = readData(connection, baseCourseId) ;
    }


    //******************************************************************************************
    // basic database operations
    //******************************************************************************************

    /**
     * insert a new record / update existing record to the database
     *
     * @throws Exception if something went wrong
     */
    public void upsert() throws Exception {
        String sqlStatement = "INSERT INTO " + TABLE + " (baseCourseId, lmsWorkerId, resource) VALUES(?, ?, ?)"+
                " ON CONFLICT ON CONSTRAINT " + TABLE + "_pKey DO UPDATE" +
                " SET lmsWorkerId=?, resource=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, opalCourseEntityDto.getBaseCourseId());
            preparedStatement.setInt(2, opalCourseEntityDto.getLmsWorkerId());
            preparedStatement.setString(3, opalCourseEntityDto.getOpalResource());
            preparedStatement.setInt(4, opalCourseEntityDto.getLmsWorkerId());
            preparedStatement.setString(5, opalCourseEntityDto.getOpalResource());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw DataAccessObject.createDatabaseException("Failed to insert/update data for opal lms data for base course " + opalCourseEntityDto.getBaseCourseId() + ".", e);
        }
    }

    /**
     * delete record with given id
     *
     * @param connection to database
     * @param baseCourseId of the record to delete
     * @throws Exception -
     */
    public static void delete(Connection connection, Integer baseCourseId) throws Exception {
        LmsCourseDao.delete(connection,TABLE, baseCourseId);
    }


    //******************************************************************************************
    // static methods for database operations
    //******************************************************************************************

    public static OpalCourseEntityDto readData(Connection connection, Integer baseCourseId) throws Exception {
        String sqlStatement = "SELECT * FROM " + TABLE   + " WHERE baseCourseId=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, baseCourseId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return createDto(resultSet);
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve opal lms data for base course " + baseCourseId + ".", e);
        }
        return null;
    }


    //******************************************************************************************
    // static method to create DTOs from a resultSet
    //******************************************************************************************

    /**
     * create OpalCourseEntityDto using data from resultSet
     *
     * @param resultSet containing data
     * @return OpalCourseEntityDto
     * @throws Exception -
     */
    public static OpalCourseEntityDto createDto(ResultSet resultSet) throws Exception {
        LmsCourseDto lmsCourseDto = LmsCourseDao.createFromResultSet(resultSet);
        String resource = resultSet.getString("resource");
        return new OpalCourseEntityDto(lmsCourseDto, resource);
    }


    //******************************************************************************************
    // implementations for abstract methods of base class
    //******************************************************************************************

    /**
     * @return type of LMS as String
     */
    public String getLms() { return LMS; }
    public LmsCourseDto getLmsCourseDto() { return opalCourseEntityDto; }

    public List<BaseCourseEntityDto> getBaseCoursesForLmsCourse(LmsCourseDto lmsCourseDto) throws Exception {
        String sqlStatement = "SELECT baseCourse.* "+
                " FROM (" +
                "  SELECT baseCourseId FROM " + TABLE + " WHERE resource=?" +
                " ) AS lms" +
                " JOIN " + BaseCourseEntityDao.TABLE + " ON baseCourse.id = lms.baseCourseId AND baseCourse.lms = ?;";
        try (PreparedStatement preparedStatement = this.connection.prepareStatement(sqlStatement)) {
            preparedStatement.setString(1, opalCourseEntityDto.getOpalResource());
            preparedStatement.setString(2, LMS);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<BaseCourseEntityDto> result = new ArrayList<>();
            while (resultSet.next()) {
                BaseCourseEntityDto baseCourseEntityDto = BaseCourseEntityDao.createBaseCourseEntityDto(resultSet);
                result.add(baseCourseEntityDto);
            }
            return result;
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve list of base courses associated to opal course.", e);
        }
    }
}
