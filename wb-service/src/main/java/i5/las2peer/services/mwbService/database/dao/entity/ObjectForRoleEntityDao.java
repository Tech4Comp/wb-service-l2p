package i5.las2peer.services.mwbService.database.dao.entity;


import i5.las2peer.services.mwbService.database.dao.DataAccessObject;
import i5.las2peer.services.mwbService.database.dto.entity.ObjectForRoleEntityDto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Entity dao to handle basic database operations for table "objectForRole"
 */
public class ObjectForRoleEntityDao extends DataAccessObject {
    private final ObjectForRoleEntityDto objectForRoleEntityDto;

    /**
     * Table name for data
     */
    public static final String TABLE = "objectForRole";

    public static final String OBJECT_TYPE_BASECOURSE = "BASECOURSE";

    /**
     * constructor
     * @param connection to the database
     * @param id -
     */
    public ObjectForRoleEntityDao(Connection connection, Integer id) throws Exception {
        super(connection);
        this.objectForRoleEntityDto = readData(connection, id);
        if (this.objectForRoleEntityDto == null)
            throw getInvalidDataException("invalid id for objectForRole");
    }

    /**
     * constructor
     * @param connection to the database
     * @param objectType -
     */
    public ObjectForRoleEntityDao(Connection connection, String objectType) throws Exception {
        super(connection);
        this.objectForRoleEntityDto = readData(connection, objectType);
        if (this.objectForRoleEntityDto == null)
            throw getInvalidDataException("invalid objectType for role");
    }


    //******************************************************************************************
    // instance methods
    //******************************************************************************************

    public Integer getId() throws Exception {
        assertNotNull();
        return this.objectForRoleEntityDto.getId();
    }

    public void assertNotNull() throws Exception {
        if (this.objectForRoleEntityDto == null)
            throw getInvalidDataException("Object for role not found.");
    }

    //******************************************************************************************
    // static methods for database operations
    //******************************************************************************************

    public static ObjectForRoleEntityDto readData(Connection connection, Integer id) throws Exception {
        String sqlStatement = "SELECT * FROM " + TABLE   + " WHERE id=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return createObjectForRoleEntityDto(resultSet);
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve data for objectForRole.", e);
        }
        return null;
    }

    public static ObjectForRoleEntityDto readData(Connection connection, String objectType) throws Exception {
        String sqlStatement = "SELECT * FROM " + TABLE   + " WHERE objectType=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setString(1, objectType);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return createObjectForRoleEntityDto(resultSet);
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve data for objectType.", e);
        }
        return null;
    }

    //******************************************************************************************
    // static methods to create related DTO from a resultSet
    //******************************************************************************************

    /**
     * create ObjectForRoleEntityDto using data from resultSet
     *
     * @param resultSet containing data
     * @return ObjectForRoleEntityDto
     * @throws Exception -
     */
    public static ObjectForRoleEntityDto createObjectForRoleEntityDto(ResultSet resultSet) throws Exception {
        Integer id = resultSet.getInt("id");
        String objectType = resultSet.getString("objectType");
        return new ObjectForRoleEntityDto(id, objectType);
    }
}
