package i5.las2peer.services.mwbService.utilities;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 *  JSON related utility methods
 */
public class JsonUtilities {
    /**
     * checks, if a string is proper JSON
     * @param json - string to be checked
     * @return true, if string is JSON formatted
     */
    public static Boolean isProperJson(String json) {
        try {
            new ObjectMapper().readTree(json);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
     * returns JsonNode with given key/value pair if found, null els
     *
     * @param json as JsonNode
     * @param key -
     * @param value -
     * @return -
     */
    public static JsonNode getJsonNodeWithValueForKey(JsonNode json, String key, String value) {
        for (JsonNode object: json) {
            if (object.get(key).asText().equals(value)) {
                return object;
            }
        }
        return null;
    }

    /**
     * returns the JsonNode for the key or key+"s"
     * for example, if no node with key "item" is found, the node for key "items" will be returned
     *
     * @param parsedJson -
     * @param key -
     * @return -
     */
    public static JsonNode getJsonNodeFromParsedJson(JsonNode parsedJson, String key) {
        JsonNode node = parsedJson.get(key);
        return node == null ? parsedJson.get(key+"s") : node;
    }
}
