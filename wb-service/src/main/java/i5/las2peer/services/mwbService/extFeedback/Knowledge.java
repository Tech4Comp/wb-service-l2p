package i5.las2peer.services.mwbService.extFeedback;

import java.util.EnumMap;

/**
 * defines the knowledge dimensions used in easlit items and
 * provides methods to sum up the correct answers for each knowledge dimension
 */
public class Knowledge {

    /**
     * enumerates types of knowledge (maps to easlit knowledge dimensions)
     */
    public enum Dimension {
        /**
         * knowledge dimension factual
         */
        FACTUAL,

        /**
         * knowledge dimension conceptual
         */
        CONCEPTUAL,

        /**
         * knowledge dimension procedural
         */
        PROCEDURAL
    }

    /**
     * @return EnumMap mapping knowledge dimensions to the corresponding CorrectAnswersStatistics
     */
    public static EnumMap<Dimension, CorrectAnswersStatistics> createCorrectAnswersStatistic() {
        EnumMap<Dimension, CorrectAnswersStatistics> correctAnswersStatistics = new EnumMap<>(Dimension.class);
        for (Dimension dimension : Dimension.values()) {
            correctAnswersStatistics.put(dimension, new CorrectAnswersStatistics());
        }
        return correctAnswersStatistics;
    }

    /**
     * add value/max value to the stats for a given knowledge dimension
     * @param statistics to add the values to
     * @param dimension - knowledge dimension to increase the value for
     * @param max - max value for that test item
     * @param value - value to add
     */
    public static void sumUp(EnumMap<Dimension, CorrectAnswersStatistics> statistics, Dimension dimension, Double value, Double max) {
        statistics.get(dimension).increase(value, max);
    }

    /**
     * @param statistics for knowledge dimensions as EnumMap
     * @return a String displaying the current stats
     */
    public static String toString(EnumMap<Dimension, CorrectAnswersStatistics> statistics) {
        StringBuilder result = new StringBuilder();
        for (Dimension dimension : Dimension.values()) {
            String dimensionValueToString = dimension.name() + ": "+ statistics.get(dimension).toString();
            if (result.length() > 0)
                result.append(", ");
            result.append(dimensionValueToString);
        }
        return "{"+result+"}";
    }
}
