package i5.las2peer.services.mwbService.database.dao.entity;


import i5.las2peer.services.mwbService.database.dao.DataAccessObject;
import i5.las2peer.services.mwbService.database.dto.entity.WritingTaskEntityDto;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Entity dao to handle basic database operations for table "writingTask"
 */
public class WritingTaskEntityDao extends DataAccessObject {
    private final WritingTaskEntityDto writingTaskEntityDto;

    /**
     * Table name for course data
     */
    public static final String TABLE = "writingTask";

    /**
     * field list for copy course data
     */
    public static final List<String> copyFieldList = Arrays.asList("nr", "title", "text", "validFrom", "validUntil");

    /**
     * constructor
     * @param connection to the database
     * @param writingTaskEntityDto -
     */
    public WritingTaskEntityDao(Connection connection, WritingTaskEntityDto writingTaskEntityDto) {
        super(connection);
        this.writingTaskEntityDto = writingTaskEntityDto;
    }


    //******************************************************************************************
    // basic database operations
    //******************************************************************************************

    /**
     * insert a new record into the database
     *
     * @throws Exception if something went wrong
     */
    public void insert() throws Exception {
        String sqlStatement = "INSERT INTO " + TABLE + " (courseId, nr, title, text, validFrom, validUntil) VALUES (?, ?, ?, ?, ?, ?);";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, writingTaskEntityDto.getCourseId());
            preparedStatement.setInt(2, writingTaskEntityDto.getNr());
            preparedStatement.setString(3, writingTaskEntityDto.getTitle());
            preparedStatement.setString(4, writingTaskEntityDto.getText());
            preparedStatement.setTimestamp(5, writingTaskEntityDto.getValidFrom());
            preparedStatement.setTimestamp(6, writingTaskEntityDto.getValidUntil());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw DataAccessObject.createDatabaseException("Failed to insert task.", e);
        }
    }

    /**
     * update a record
     *
     * @throws Exception if something went wrong
     */
    public void update() throws Exception {
        String sqlStatement = "UPDATE " + TABLE + " SET title=?, text=?, validFrom=?, validUntil=?" +
                " WHERE courseId=? AND nr=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            setOptionalStringForResultSet(preparedStatement,1, writingTaskEntityDto.getTitle());
            preparedStatement.setString(2, writingTaskEntityDto.getText());
            setOptionalTimestampForResultSet(preparedStatement, 3, writingTaskEntityDto.getValidFrom());
            setOptionalTimestampForResultSet(preparedStatement, 4, writingTaskEntityDto.getValidUntil());
            preparedStatement.setInt(5, writingTaskEntityDto.getCourseId());
            preparedStatement.setInt(6, writingTaskEntityDto.getNr());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw DataAccessObject.createDatabaseException("Failed to update task.", e);
        }
    }


    //******************************************************************************************
    // static methods for database operations
    //******************************************************************************************

    public static List<WritingTaskEntityDto> readData(Connection connection, Integer courseId) throws Exception {
        String sqlStatement = "SELECT * FROM " + TABLE + " WHERE courseId=? ORDER BY nr;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, courseId);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<WritingTaskEntityDto> writingTaskList = new ArrayList<>();
            while (resultSet.next()) {
                WritingTaskEntityDto writingTaskDto = createDto(resultSet);
                writingTaskList.add(writingTaskDto);
            }
            return writingTaskList;
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve list of writing tasks for component WRITINGTASKS for course " + courseId + " from database.", e);
        }
    }

    /**
     * delete record with given courseId and nr
     *
     * @param connection to database
     * @param courseId of the record to delete
     * @param nr of the record to delete
     * @throws Exception -
     */
    public static void delete(Connection connection, Integer courseId, Integer nr) throws Exception {
        String sqlStatement = "DELETE FROM " + TABLE +
                " WHERE courseId=? AND nr=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, courseId);
            preparedStatement.setInt(2, nr);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to delete writing task.", e);
        }
    }


    //******************************************************************************************
    // static methods to create course related DTOs from a resultSet
    //******************************************************************************************

    /**
     * create WritingTaskEntityDto using data from resultSet
     *
     * @param resultSet containing data
     * @return WritingTaskEntityDto
     * @throws Exception -
     */
    public static WritingTaskEntityDto createDto(ResultSet resultSet) throws Exception {
        Integer courseId = resultSet.getInt("courseId");
        Integer nr = resultSet.getInt("nr");
        String title = resultSet.getString("title");
        String text = resultSet.getString("text");
        Timestamp validFrom = resultSet.getTimestamp("validFrom");
        Timestamp validUntil = resultSet.getTimestamp("validUntil");
        return new WritingTaskEntityDto(courseId, nr, title, text, validFrom, validUntil);
    }
}
