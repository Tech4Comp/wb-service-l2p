package i5.las2peer.services.mwbService.restApi;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import i5.las2peer.services.mwbService.Authentication;
import i5.las2peer.services.mwbService.MwbService;
import i5.las2peer.services.mwbService.database.LazyPostgresConnection;
import i5.las2peer.services.mwbService.database.dao.entity.LmsWorkerEntityDao;
import i5.las2peer.services.mwbService.database.dto.BaseCourseDto;
import i5.las2peer.services.mwbService.database.dto.entity.LmsWorkerEntityDto;
import i5.las2peer.services.mwbService.externalServices.LMS.LmsConnection;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 *  REST API to manage lms workers
 */
@Path("/lmsWorker")
public class LmsWorkerApi {

    /**
     * Retrieves all worker names (NO credentials) from the database.
     * @return  200 list with available worker names as JSON object
     *          400 Bad Request if something goes wrong
     *          401 not Authenticated or missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @GET
    @Path("/list")
    @RolesAllowed("authenticated")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(
            value = "Retrieve all worker names from the database (without credentials).",
            response = BaseCourseDto.class)
    public Response getAllWorkers() {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthenticated();
            List<LmsWorkerEntityDto> lmsWorkers = LmsWorkerEntityDao.getLmsWorkerListWithoutCredentials(connection.getConnection());
            return MwbService.responseOK(new ObjectMapper().writeValueAsString(lmsWorkers));
        } catch (Exception e) {
            return MwbService.responseError(e);
        }

    }

    /**
     * adding a lms worker
     * @param lmsWorker as LmsWorkerEntityDto with data
     * @return  200 OK if lms worker was added/updated successfully
     *          400 Bad Request if something went wrong
     *          401 lmsWorker not authenticated in LMS
     *          403 lmsWorker misses rights
     *          500 Internal Server Error if the database connection is not established, or something else goes wrong
     */
    @POST
    @Produces(MediaType.TEXT_PLAIN)
    public Response insertLmsWorker(
            @ApiParam(value = "data for lms worker as a JSON object", required = true) LmsWorkerEntityDto lmsWorker
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthorizationForLms(lmsWorker);
            LmsWorkerEntityDao lmsWorkerDao = new LmsWorkerEntityDao(connection.getConnection(), lmsWorker);
            lmsWorkerDao.insert();
            return MwbService.responseOK();
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * updating a lms worker
     * @param lmsWorkerEntityDto as LmsWorkerEntityDto as JSON with data
     * @return  200 OK if lms worker was added/updated successfully
     *          400 Bad Request if something went wrong
     *          401 lmsWorker not authenticated in LMS
     *          403 lmsWorker misses rights
     *          500 Internal Server Error if the database connection is not established, or something else goes wrong
     */
    @PUT
    @Produces(MediaType.TEXT_PLAIN)
    public Response updateLmsWorker(
            @ApiParam(value = "data for lms worker as a JSON object", required = true) LmsWorkerEntityDto lmsWorkerEntityDto
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthorizationForLms(lmsWorkerEntityDto);
            LmsWorkerEntityDao lmsWorkerEntityDao = new LmsWorkerEntityDao(connection.getConnection(), lmsWorkerEntityDto);
            lmsWorkerEntityDao.update();
            return MwbService.responseOK();
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * checking rights for a lms worker
     * @param lmsWorker as LmsWorkerEntityDto with data for lms worker
     * @return  200 string json object with authorization status
     *          400 Bad Request if something went wrong
     *          401 not authenticated
     *          403 missing rights
     *          500 Internal Server Error if the database connection is not established
     */
    @POST
    @Path("/checkWorkerAuthorization")
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed("authenticated")
    public Response checkWorkerAuthorization(
            @ApiParam(value = "data for lms worker as a JSON object", required = true) LmsWorkerEntityDto lmsWorker
    ) {
        try {
            Authentication.checkAuthenticated();
            ObjectNode json = new ObjectMapper().createObjectNode();
            LmsConnection lmsConnection = LmsConnection.createLmsConnection(lmsWorker.getLms());
            lmsConnection.checkWorkerAuthorization(lmsWorker, json);
            return MwbService.responseOK(json.toString());
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }
}
