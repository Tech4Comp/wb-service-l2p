package i5.las2peer.services.mwbService.database.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Dto to handle layout for MWB components
 */
@Data
@AllArgsConstructor
public class CourseLayoutDto {
    private String size;
    private ComponentLayoutDto[] layout;

    /**
     * default constructor
     */
    public CourseLayoutDto() {}
}

