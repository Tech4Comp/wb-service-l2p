package i5.las2peer.services.mwbService.database.dto.entity;


import lombok.AllArgsConstructor;
import lombok.Data;

import java.sql.Timestamp;

/**
 * Entity dto corresponding to database table "courseTimelineItem"
 */
@Data
@AllArgsConstructor
public class CourseTimelineItemEntityDto {
    private Integer id;
    private Integer rowId;
    private String title;
    private Timestamp startsOn;
    private Timestamp endsOn;
    private String description;
    private Boolean editableTime;
    private Boolean editableTitle;
    private Boolean expandToCalendarWeek;

    /**
     * default constructor
     */
    public CourseTimelineItemEntityDto() {}
}
