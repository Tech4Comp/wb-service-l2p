package i5.las2peer.services.mwbService.database.dto.entity;


import lombok.AllArgsConstructor;
import lombok.Data;

import java.sql.Timestamp;

/**
 * Entity dto corresponding to database table "fileBucket"
 */
@Data
@AllArgsConstructor
public class FileBucketEntityDto {
    private Integer id;
    private Integer baseCourseId;
    private String fileId;
    private String filename;
    private String purpose;
    private String description;
    private Timestamp lastModified;

    /**
     * default constructor
     */
    public FileBucketEntityDto() {}
}
