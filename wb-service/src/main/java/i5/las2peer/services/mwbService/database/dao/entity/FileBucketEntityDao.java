package i5.las2peer.services.mwbService.database.dao.entity;


import i5.las2peer.services.mwbService.database.dao.DataAccessObject;
import i5.las2peer.services.mwbService.database.dto.entity.FileBucketEntityDto;
import lombok.Getter;

import javax.ws.rs.core.MediaType;
import java.sql.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static i5.las2peer.services.mwbService.utilities.StringsUtilities.hasNoValue;


/**
 * Entity dao to handle basic database operations for table "fileBucket"
 */
@Getter
public class FileBucketEntityDao extends DataAccessObject {
    private final FileBucketEntityDto fileBucketEntityDto;

    /**
     * Table name for data
     */
    public static final String TABLE = "fileBucket";

    /**
     * constructor
     * @param connection to the database
     * @param fileBucketEntityDto -
     */
    public FileBucketEntityDao(Connection connection, FileBucketEntityDto fileBucketEntityDto) {
        super(connection);
        this.fileBucketEntityDto = fileBucketEntityDto;
    }

    /**
     * constructor
     * @param connection to the database
     * @param id -
     */
    public FileBucketEntityDao(Connection connection, Integer id) throws Exception {
        super(connection);
        this.fileBucketEntityDto = readData(connection, id);
    }


    //******************************************************************************************
    // instance methods
    //******************************************************************************************

    public void assertNotNull() throws Exception {
        if (this.fileBucketEntityDto == null)
            throw getInvalidDataException("No file bucket entry provided.");
    }

    public Integer getBaseCourseId() throws Exception {
        assertNotNull();
        return this.fileBucketEntityDto.getBaseCourseId();
    }

    public String getFileId() throws Exception {
        assertNotNull();
        return this.fileBucketEntityDto.getFileId();
    }

    public String getFilename() throws Exception {
        assertNotNull();
        return this.fileBucketEntityDto.getFilename();
    }

    public void assertFileIdNotNull() throws Exception {
        if (hasNoValue(this.fileBucketEntityDto.getFileId()))
            throw getInvalidDataException("No file id provided in bucket entry.");
    }

    public String getFileExtensionForFile() throws Exception {
        assertFileIdNotNull();
        String filename = this.fileBucketEntityDto.getFilename();
        return getFileExtensionForFile(filename);
    }

    public static String getFileExtensionForFile(String filename) throws Exception {
        String[] splitFilename = filename.split("\\.");
        return splitFilename[splitFilename.length - 1];
    }

    public static String getMediaTypeForFile(String filename) throws Exception {
        String extension = getFileExtensionForFile(filename);
        return switch (extension) {
            case "pdf" -> "application/pdf";
            default -> MediaType.TEXT_PLAIN;
        };
    }

    //******************************************************************************************
    // basic database operations
    //******************************************************************************************

    /**
     * insert a new record into the database
     *
     * @return id of the new record
     * @throws Exception if something went wrong
     */
    public int insert() throws Exception {
        String sqlStatement = "INSERT INTO " + TABLE +
                " (baseCourseId, fileId, filename, purpose, description, lastModified) VALUES (?, ?, ?, ?, ?, ?)" +
                " RETURNING id;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            Timestamp now = Timestamp.from(Instant.now());
            preparedStatement.setInt(1, fileBucketEntityDto.getBaseCourseId());
            preparedStatement.setString(2, fileBucketEntityDto.getFileId());
            preparedStatement.setString(3, fileBucketEntityDto.getFilename());
            preparedStatement.setString(4, fileBucketEntityDto.getPurpose());
            preparedStatement.setString(5, fileBucketEntityDto.getDescription());
            preparedStatement.setTimestamp(6, now);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return resultSet.getInt("id");
        } catch (SQLException e) {
            throw DataAccessObject.createDatabaseException("Failed to insert file bucket entry.", e);
        }
        throw DataAccessObject.createDatabaseException("Failed to retrieve id of inserted file bucket entry.", null);
    }

    /**
     * update record
     *
     * @throws Exception if something went wrong
     */
    public void update() throws Exception {
        String sqlStatement = "UPDATE " + TABLE + " SET baseCourseId=?, fileId=?, filename=?, purpose=?, description=?, lastModified=?" +
                " WHERE id=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            Timestamp now = Timestamp.from(Instant.now());
            preparedStatement.setInt(1, fileBucketEntityDto.getBaseCourseId());
            preparedStatement.setString(2, fileBucketEntityDto.getFileId());
            preparedStatement.setString(3, fileBucketEntityDto.getFilename());
            preparedStatement.setString(4, fileBucketEntityDto.getPurpose());
            preparedStatement.setString(5, fileBucketEntityDto.getDescription());
            preparedStatement.setTimestamp(6, now);
            preparedStatement.setInt(7, fileBucketEntityDto.getId());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to update file bucket entry.", e);
        }
    }


    //******************************************************************************************
    // static methods for database operations
    //******************************************************************************************

    public static FileBucketEntityDto readData(Connection connection, Integer id) throws Exception {
        String sqlStatement = "SELECT * FROM " + TABLE + " WHERE id=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return createDto(resultSet);
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve data for file bucket entry.", e);
        }
        return null;
    }

    /**
     * get list of files belonging to a base course
     *
     * @param connection to database
     * @param baseCourseId id of the base course
     * @return FileBucketDto with basic and lms data of the base course
     * @throws Exception if something went wrong
     */
    public static List<FileBucketEntityDto> getFileBucketList(Connection connection, Integer baseCourseId) throws Exception {
        String sqlStatement = "SELECT * FROM " + TABLE +
                " WHERE baseCourseId=?" +
                " ORDER BY lastModified DESC;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, baseCourseId);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<FileBucketEntityDto> list = new ArrayList<>();
            while (resultSet.next())
                list.add(createDto(resultSet));
            return list;
        } catch (SQLException e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve file list in file bucket of a baseCourse.", e);
        }
    }

    /**
     * get list of files with a given purpose belonging to a base course
     *
     * @param connection to database
     * @param baseCourseId id of the base course
     * @param purpose -
     * @return FileBucketDto with basic and lms data of the base course
     * @throws Exception if something went wrong
     */
    public static List<FileBucketEntityDto> getFileBucketList(Connection connection, Integer baseCourseId, String purpose) throws Exception {
        String sqlStatement = "SELECT * FROM " + TABLE +
                " WHERE baseCourseId=? AND purpose=?" +
                " ORDER BY lastModified DESC;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, baseCourseId);
            preparedStatement.setString(2, purpose);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<FileBucketEntityDto> list = new ArrayList<>();
            while (resultSet.next())
                list.add(createDto(resultSet));
            return list;
        } catch (SQLException e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve file list in file bucket of a baseCourse.", e);
        }
    }

    /**
     * delete record with given id
     *
     * @param connection to database
     * @param id of the record to delete
     * @throws Exception -
     */
    public static void delete(Connection connection, Integer id) throws Exception {
        String sqlStatement = "DELETE FROM " + TABLE +
                " WHERE id=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to delete file bucket entry.", e);
        }
    }


    //******************************************************************************************
    // static method to create DTO from a resultSet
    //******************************************************************************************

    /**
     * create PrivacyStatementEntityDto using data from resultSet
     *
     * @param resultSet containing data
     * @return PrivacyStatementEntityDto
     * @throws Exception -
     */
    public static FileBucketEntityDto createDto(ResultSet resultSet) throws Exception {
        Integer id = resultSet.getInt("id");
        Integer baseCourseId = resultSet.getInt("baseCourseId");
        String fileId = resultSet.getString("fileId");
        String filename = resultSet.getString("filename");
        String purpose = resultSet.getString("purpose");
        String description = resultSet.getString("description");
        Timestamp lastModified = resultSet.getTimestamp("lastModified");
        return new FileBucketEntityDto(id, baseCourseId, fileId, filename, purpose, description, lastModified);
    }
}
