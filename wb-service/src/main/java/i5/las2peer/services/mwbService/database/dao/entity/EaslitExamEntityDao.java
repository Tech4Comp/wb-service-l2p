package i5.las2peer.services.mwbService.database.dao.entity;


import i5.las2peer.services.mwbService.database.dao.DataAccessObject;
import i5.las2peer.services.mwbService.database.dto.entity.ConsentEntityDto;
import i5.las2peer.services.mwbService.database.dto.entity.CourseHintEntityDto;
import i5.las2peer.services.mwbService.database.dto.entity.EaslitExamEntityDto;
import i5.las2peer.services.mwbService.database.dto.entity.PersonEntityDto;

import java.sql.*;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;

/**
 * Entity dao to handle basic database operations for table "easlitExam"
 */
public class EaslitExamEntityDao extends DataAccessObject {
    private final EaslitExamEntityDto easlitExamEntityDto;

    /**
     * Table name for data
     */
    public static final String TABLE = "easlitExam";

    /**
     * field list for copy course data
     */
    public static final List<String> copyFieldList = Arrays.asList("easlitProjectId", "topic");

    /**
     * constructor
     * @param connection to the database
     * @param easlitExamEntityDto -
     */
    public EaslitExamEntityDao(Connection connection, EaslitExamEntityDto easlitExamEntityDto) {
        super(connection);
        this.easlitExamEntityDto = easlitExamEntityDto;
    }


    //******************************************************************************************
    // basic database operations
    //******************************************************************************************

    /**
     * insert a new record / update existing record to the database
     *
     * @throws Exception if something went wrong
     */
    public void upsert() throws Exception {
        String sqlStatement = "INSERT INTO " + TABLE + " (courseExamId, easlitProjectId, topic) VALUES(?, ?, ?)"+
                " ON CONFLICT ON CONSTRAINT " + TABLE + "_pKey DO UPDATE" +
                " SET easlitProjectId=?, topic=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, easlitExamEntityDto.getCourseExamId());
            preparedStatement.setString(2, easlitExamEntityDto.getEaslitProjectId());
            preparedStatement.setString(3, easlitExamEntityDto.getTopic());
            preparedStatement.setString(4, easlitExamEntityDto.getEaslitProjectId());
            setOptionalStringForResultSet(preparedStatement, 5, easlitExamEntityDto.getTopic());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw DataAccessObject.createDatabaseException("Failed to insert/update data for easlit.", e);
        }
    }

    //******************************************************************************************
    // static methods for database operations
    //******************************************************************************************

    public static EaslitExamEntityDto readData(Connection connection, Integer courseExamId) throws Exception {
        String sqlStatement = "SELECT * FROM " + TABLE   + " WHERE courseExamId=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, courseExamId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return createDto(resultSet);
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve data for easlit exam.", e);
        }
        return null;
    }


    //******************************************************************************************
    // static method to create DTO from a resultSet
    //******************************************************************************************

    /**
     * create EaslitExamEntityDto using data from resultSet
     *
     * @param resultSet containing data
     * @return EaslitExamEntityDto
     * @throws Exception -
     */
    public static EaslitExamEntityDto createDto(ResultSet resultSet) throws Exception {
        Integer courseExamId = resultSet.getInt("courseExamId");
        String easlitProjectId = resultSet.getString("easlitProjectId");
        String topic = resultSet.getString("topic");
        return new EaslitExamEntityDto(courseExamId, easlitProjectId, topic);
    }
}
