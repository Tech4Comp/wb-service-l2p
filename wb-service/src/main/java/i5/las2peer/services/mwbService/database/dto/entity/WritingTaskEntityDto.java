package i5.las2peer.services.mwbService.database.dto.entity;


import lombok.AllArgsConstructor;
import lombok.Data;

import java.sql.Timestamp;

/**
 * Entity dto corresponding to database table writingTask
 */
@Data
@AllArgsConstructor
public class WritingTaskEntityDto {
    private Integer courseId;
    private Integer nr;
    private String title;
    private String text;
    private Timestamp validFrom;
    private Timestamp validUntil;

    /**
     * default constructor
     */
    public WritingTaskEntityDto() {}
}
