package i5.las2peer.services.mwbService.database.dto.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.sql.*;

/**
 * Entity dto corresponding to database table "course"
 */
@Data
@AllArgsConstructor
public class CourseEntityDto {
    private Integer id;
    private Integer baseCourseId;
    private String shortname;
    private String surname;
    private Date validFrom;
    private Date validUntil;

    /**
     * default constructor
     */
    public CourseEntityDto() {}

    /**
     * copy constructor (used to initialise derived classes)
     *
     * @param copy - CourseEntity with data
     */
    public CourseEntityDto(CourseEntityDto copy) {
        this.id = copy.getId();
        this.baseCourseId = copy.getBaseCourseId();
        this.shortname = copy.getShortname();
        this.surname = copy.getSurname();
        this.validFrom = copy.getValidFrom();
        this.validUntil = copy.getValidUntil();
    }
}
