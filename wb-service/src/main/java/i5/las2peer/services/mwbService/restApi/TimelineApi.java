package i5.las2peer.services.mwbService.restApi;

import com.fasterxml.jackson.databind.ObjectMapper;
import i5.las2peer.services.mwbService.Authentication;
import i5.las2peer.services.mwbService.MwbService;
import i5.las2peer.services.mwbService.database.LazyPostgresConnection;
import i5.las2peer.services.mwbService.database.dao.CourseComponentDao;
import i5.las2peer.services.mwbService.database.dao.entity.CourseTimelineRowEntityDao;
import i5.las2peer.services.mwbService.database.dao.entity.UserTimelineItemEntityDao;
import i5.las2peer.services.mwbService.database.dto.entity.UserTimelineItemEntityDto;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.Connection;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

import static i5.las2peer.services.mwbService.utilities.StringsUtilities.hasNoValue;
import static i5.las2peer.services.mwbService.utilities.StringsUtilities.hasValue;

/**
 *  handles REST API calls related to timeline
 */
@Path("/timeline")
public class TimelineApi {

    /**
     * returns user defined timeline items for the current user
     * @param courseId -
     * @return JSONArray as string user timeline items for the requested course
     */
    @GET
    @Path("/userItems/{courseId}")
    @Produces(MediaType.TEXT_PLAIN)
    @RolesAllowed("authenticated")
    public Response getUserTimelineItemsApi(@PathParam("courseId") int courseId) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthenticated();
            String user = Authentication.pseudonymizeUser(connection.getConnection());
            CourseComponentDao courseComponentDao = new CourseComponentDao(connection.getConnection());
            List<UserTimelineItemEntityDto> userTimelineItems = courseComponentDao.getUserTimelineItems(courseId, user);
            return MwbService.responseOK(new ObjectMapper().writeValueAsString(userTimelineItems));
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * Creates a new user defined timeline item for the current user
     * @param userTimelineItemDto the user timeline item to be created as JSON object
     * @return 200 Ok, if it is successfully created
     *         400 Bad Request on failure
     *         500 Internal Server Error if the database connection is not established
     */
    @POST
    @Path("/userItem")
    @RolesAllowed("authenticated")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Create a new user defined timeline item for the current user.",
            notes = "Returns 'Ok' if it is successfully created.",
            response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = String.class),
            @ApiResponse(code = 400, message = "Unable to create [userTimelineItem]"),
            @ApiResponse(code = 500, message = "Internal Server Error") })
    public Response createUserTimeline(
            @ApiParam(value = "The user timeline item to create, as a JSON object", required = true) UserTimelineItemEntityDto userTimelineItemDto
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthenticated();
            String pseudonymizedUser = Authentication.pseudonymizeUser(connection.getConnection());
            UserTimelineItemEntityDao userTimelineItemEntityDao = new UserTimelineItemEntityDao(connection.getConnection(), userTimelineItemDto);
            userTimelineItemDto.setUuid(UUID.fromString(pseudonymizedUser));
            userTimelineItemEntityDao.insert();
            Integer courseId = new CourseTimelineRowEntityDao((connection.getConnection()), userTimelineItemDto.getRowId()).getCourseId();
            if (userTimelineItemDto.getCourseTimelineItemId() == 0 && userTimelineItemDto.getWritingTaskNr() == 0) {
                trackCreateNewUserItem(connection.getConnection(), courseId, userTimelineItemDto, pseudonymizedUser);
            } else if (userTimelineItemDto.isDone() && userTimelineItemDto.getStartsOn() != null) {
                trackEditTimelineItem(connection.getConnection(), courseId, userTimelineItemDto, pseudonymizedUser, getDataStringForChange(userTimelineItemDto));
            } else if (userTimelineItemDto.isDone()) {
                trackTimelineItemSetDone(connection.getConnection(), courseId, userTimelineItemDto, pseudonymizedUser, true);
            } else if (userTimelineItemDto.getStartsOn() != null) {
                trackMoveTimelineItem(connection.getConnection(), courseId, userTimelineItemDto, pseudonymizedUser, getDataStringForStartsOnAndEndsOn(userTimelineItemDto.getStartsOn(), userTimelineItemDto.getEndsOn()));
            } else if (hasValue(userTimelineItemDto.getTitle()) || hasValue(userTimelineItemDto.getDescription())) {
                trackRenameTimelineItem(connection.getConnection(), courseId, userTimelineItemDto, pseudonymizedUser);
            }
            return MwbService.responseOK();
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * @param itemId - id of userTimelineItem to be deleted
     * @return 'Ok' if userTimelineItem was successfully deleted
     */
    @DELETE
    @Path("/userItem/{itemId}")
    @RolesAllowed("authenticated")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Delete a userTimelineItem for the current user.",
            notes = "Returns 'Ok' if userTimelineItem was successfully deleted.",
            response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = String.class),
            @ApiResponse(code = 400, message = "Unable to delete userTimelineItem [itemId]"),
            @ApiResponse(code = 500, message = "Internal Server Error") })
    public Response deleteUserTimelineItem(
            @PathParam("itemId") int itemId
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthenticated();
            String user = Authentication.pseudonymizeUser(connection.getConnection());
            UserTimelineItemEntityDto userTimelineItemDto = Authentication.checkAuthorizationForUserTimelineItem(connection.getConnection(), itemId, user);
            UserTimelineItemEntityDao.delete(connection.getConnection(), itemId);
            Integer courseId = new CourseTimelineRowEntityDao((connection.getConnection()), userTimelineItemDto.getRowId()).getCourseId();
            trackDeleteTimelineItem(connection.getConnection(), courseId, userTimelineItemDto, user);
            return MwbService.responseOK();
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * @param newValue as UserTimelineItemDto
     * @return 'Ok' if userTimelineItem was successfully updated
     */
    @PUT
    @Path("/userItem")
    @RolesAllowed("authenticated")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Update a userTimelineItem for the current user.",
            notes = "Returns 'Ok' if userTimelineItem was successfully updated.",
            response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = String.class),
            @ApiResponse(code = 400, message = "Unable to update userTimelineItem"),
            @ApiResponse(code = 500, message = "Internal Server Error") })
    public Response updateUserTimelineItem(
            @ApiParam(value = "new values", required = true, type = "UserTimelineItemDto") UserTimelineItemEntityDto newValue
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthenticated();
            String user = Authentication.pseudonymizeUser(connection.getConnection());
            UserTimelineItemEntityDto userTimelineItem = Authentication.checkAuthorizationForUserTimelineItem(connection.getConnection(), newValue.getId(), user);
            UserTimelineItemEntityDao userTimelineItemEntityDao = new UserTimelineItemEntityDao(connection.getConnection(), newValue);
            userTimelineItemEntityDao.update();
            Integer courseId = new CourseTimelineRowEntityDao((connection.getConnection()), userTimelineItem.getRowId()).getCourseId();
            String data = getDataStringForChange(newValue);
            trackEditTimelineItem(connection.getConnection(), courseId, userTimelineItem, user, data);
            return MwbService.responseOK();
        } catch (Exception e) {
            return  MwbService.responseError(e);
        }
    }

    /**
     * @param itemId - id of userTimelineItem to be changed
     * @param period - new period as Array of Timestamp: [startsOn, endsOn]
     * @return 'Ok' if userTimelineItem was successfully updated
     */
    @PUT
    @Path("/period/{itemId}")
    @RolesAllowed("authenticated")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Update period for a user defined timeline item for the current user.",
            notes = "Returns 'Ok' if userTimelineItem was successfully updated.",
            response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = String.class),
            @ApiResponse(code = 400, message = "Unable to set period for userTimelineItem [itemId]"),
            @ApiResponse(code = 500, message = "Internal Server Error") })
    public Response updatePeriodForUserTimelineItem(
            @PathParam("itemId") int itemId,
            @ApiParam(value = "timestamps for start and end of new period as array", required = true) List<Timestamp> period
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthenticated();
            if (period.size() < 2)
                return MwbService.responseError((Exception) null, "Invalid param period: list with 2 timestamps requested.");
            String user = Authentication.pseudonymizeUser(connection.getConnection());
            UserTimelineItemEntityDto userTimelineItem = Authentication.checkAuthorizationForUserTimelineItem(connection.getConnection(), itemId, user);
            userTimelineItem.setStartsOn(period.get(0));
            userTimelineItem.setEndsOn(period.get(1));
            UserTimelineItemEntityDao userTimelineItemEntityDao = new UserTimelineItemEntityDao(connection.getConnection(), userTimelineItem);
            userTimelineItemEntityDao.update();
            Integer courseId = new CourseTimelineRowEntityDao((connection.getConnection()), userTimelineItem.getRowId()).getCourseId();
            String data = getDataStringForStartsOnAndEndsOn(period.get(0), period.get(1));
            trackMoveTimelineItem(connection.getConnection(), courseId, userTimelineItem, user, data);
            return MwbService.responseOK();
        } catch (Exception e) {
            return  MwbService.responseError(e);
        }
    }

    /**
     * @param itemId - id of userTimelineItem to be changed
     * @param done - new period as Array of Timestamp: [startsOn, endsOn]
     * @return 'Ok' if userTimelineItem was successfully updated
     */
    @PUT
    @Path("/done/{itemId}")
    @RolesAllowed("authenticated")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Update value for done for a user defined timeline item for the current user.",
            notes = "Returns 'Ok' if userTimelineItem was successfully updated.",
            response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = String.class),
            @ApiResponse(code = 400, message = "Unable to update done for userTimelineItem [itemId]"),
            @ApiResponse(code = 500, message = "Internal Server Error") })
    public Response updateDoneForUserTimelineItem(
            @PathParam("itemId") int itemId,
            @ApiParam(value = "new value for done", required = true) boolean done
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthenticated();
            String user = Authentication.pseudonymizeUser(connection.getConnection());
            UserTimelineItemEntityDto userTimelineItem = Authentication.checkAuthorizationForUserTimelineItem(connection.getConnection(), itemId, user);
            userTimelineItem.setDone(done);
            UserTimelineItemEntityDao userTimelineItemEntityDao = new UserTimelineItemEntityDao(connection.getConnection(), userTimelineItem);
            userTimelineItemEntityDao.update();
            Integer courseId = new CourseTimelineRowEntityDao((connection.getConnection()), userTimelineItem.getRowId()).getCourseId();
            trackTimelineItemSetDone(connection.getConnection(), courseId, userTimelineItem, user, done);
            return MwbService.responseOK();
        } catch (Exception e) {
            return  MwbService.responseError(e);
        }
    }

    //******************************************************************************************
    // private methods
    //******************************************************************************************

    private static String getTrackingData(UserTimelineItemEntityDto userTimelineItemDto, String data) {
        int courseTimelineItemId = userTimelineItemDto.getCourseTimelineItemId();
        int writingTaskNr = userTimelineItemDto.getWritingTaskNr();
        String itemId = writingTaskNr > 0 ? "\"itemId\": %d".formatted(courseTimelineItemId) : "\"writingTaskNr\": %d".formatted(writingTaskNr);
        String jsonData = hasNoValue(data) ? "" : ", \"data\": %s".formatted(data);
        return """
            {\
                %s\
                %s
            }\
            """.formatted(itemId, jsonData);
    }

    private String getDataStringForStartsOnAndEndsOn(Timestamp startsOn, Timestamp endsOn) {
        return """
            {\
                "startsOn": "%s",\
                "endsOn": "%s"
            }\
            """.formatted(startsOn.toString(), endsOn.toString());
    }

    private String getDataStringForChange(UserTimelineItemEntityDto userTimelineItemDto) {
        String labelData = userTimelineItemDto.getCourseTimelineItemId() > 0 ?
            ", \"title\": \"%s\", \"description\": \"%s\"".formatted(userTimelineItemDto.getTitle(), userTimelineItemDto.getDescription()) :
            "" ;
        String startsOn =  userTimelineItemDto.getStartsOn() == null ? "" : userTimelineItemDto.getStartsOn().toString();
        String endsOn =  userTimelineItemDto.getEndsOn() == null ? "" : userTimelineItemDto.getEndsOn().toString();
        return """
            {\
                "done": %s,\
                "startsOn": "%s",\
                "endsOn": "%s"\
                %s
            }\
            """.formatted(userTimelineItemDto.isDone(), startsOn, endsOn, labelData);
    }

    private void trackCreateNewUserItem(Connection connection, Integer courseId, UserTimelineItemEntityDto userTimelineItem, String actor) throws Exception {
        String verb = "created_new_user_item";
        String object = "timeline";
        TrackApi.trackToLrs(connection, courseId, actor, verb, object, getTrackingData(userTimelineItem, ""));
    }

    private void trackDeleteTimelineItem(Connection connection, Integer courseId, UserTimelineItemEntityDto userTimelineItem, String actor) throws Exception {
        String verb = "deleted_user_item";
        String object = "timeline";
        TrackApi.trackToLrs(connection, courseId, actor, verb, object, getTrackingData(userTimelineItem, ""));
    }

    private void trackEditTimelineItem(Connection connection, Integer courseId, UserTimelineItemEntityDto userTimelineItem, String actor, String data) throws Exception {
        String verb = "changed_item";
        String object = "timeline";
        TrackApi.trackToLrs(connection, courseId, actor, verb, object, getTrackingData(userTimelineItem, data));
    }

    private void trackMoveTimelineItem(Connection connection, Integer courseId, UserTimelineItemEntityDto userTimelineItem, String actor, String data) throws Exception {
        String verb = "moved_item";
        String object = "timeline";
        TrackApi.trackToLrs(connection, courseId, actor, verb, object, getTrackingData(userTimelineItem, data));
    }

    private void trackRenameTimelineItem(Connection connection, Integer courseId, UserTimelineItemEntityDto userTimelineItem, String actor) throws Exception {
        String verb = "renamed_item";
        String object = "timeline";
        String data = getTrackingData(userTimelineItem, "");
        TrackApi.trackToLrs(connection, courseId, actor, verb, object, data);
    }

    private void trackTimelineItemSetDone(Connection connection, Integer courseId, UserTimelineItemEntityDto userTimelineItem, String actor, boolean done) throws Exception {
        String verb = done ? "set_done" : "unset_done";
        String object = "timeline";
        String data = getTrackingData(userTimelineItem, "");
        TrackApi.trackToLrs(connection, courseId, actor, verb, object, data);
    }
}
