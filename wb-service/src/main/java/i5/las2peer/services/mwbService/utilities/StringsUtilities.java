package i5.las2peer.services.mwbService.utilities;

/**
 *  String related utility methods
 */
public class StringsUtilities {
    /**
     * @param string string to check for value
     * @return true, if string is null or has no content other than whitespace, false else
     */
    public static boolean hasNoValue(String string) { return string == null || string.isBlank(); }

    /**
     * @param string string to check for value
     * @return true, if string is not null and has content other than whitespace, false else
     */
    public static boolean hasValue(String string) { return !hasNoValue(string); }

    /**
     * @param string string to be escaped
     * @return string escaped control chars
     */
    public static String removeControlChars(String string) {
        return string
                .replace('\f', ' ')
                .replace('\b', ' ')
                .replace('\n', ' ')
                .replace('\r', ' ')
                .replace('\t', ' ');
    }


    /**
     * @param string - string containing the value
     * @param delimiter - string indicating the start of the value
     * @return the remaining part of the string after the last occurrence of delimiter
     */
    public static String splitStringAndGetLastChunk(String string, String delimiter) {
        String[] split = string.split(delimiter);
        return split[split.length-1];
    }
}
