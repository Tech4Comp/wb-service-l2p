package i5.las2peer.services.mwbService.database;

import i5.las2peer.services.mwbService.MwbService;
import i5.las2peer.services.mwbService.database.dao.DataAccessObject;
import i5.las2peer.services.mwbService.externalServices.ExternalService;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * handling database connection to Postgres database
 * ToDo: use connection pooling
 *  remember to set autocommit to false when retrieving a connection from the pool or releasing it
 */
public class PostgresDBConnectionManager extends ExternalService {
    private String dbUrl;
    private Properties dbProperties;

    /**
     * default constructor
     */
    public PostgresDBConnectionManager() {
        super("PostgresDB");
    }

    /**
     * check whether connection properties are set
     * @return true if connection properties are already set
     */
    public boolean isInitialized() {
        return (dbProperties != null && dbUrl != null);
    }

    /**
     * set connection properties
     * @param dbHost -
     * @param dbPort -
     * @param dbName -
     * @param dbUser -
     * @param dbPassword -
     */
    public void setConnectionProperties(String dbHost, int dbPort, String dbName, String dbUser, String dbPassword) {
        dbUrl = "jdbc:postgresql://" + dbHost + ":" + dbPort + "/" + dbName;
        dbProperties = new Properties();
        dbProperties.setProperty("user", dbUser);
        dbProperties.setProperty("password", dbPassword);
    }

    /**
     * opens a connection to the Postgres DB
     * @return connection
     * @throws Exception on failure
     */
    public Connection getConnection() throws Exception {
        try {
            return DriverManager.getConnection(dbUrl, dbProperties);
        } catch (Exception e) {
            String message = "Failed to establish connection to PostgresDB";
            MwbService.logger.warning(message+": "+e.getMessage());
            throw getServiceExceptionInternalError(message+".");
        }
    }

    /**
     * release a connection, if it is not null
     * @param connection to release
     */
    public void releaseConnection(Connection connection) {
        if (connection == null)
            return;
        try {
            //MwbService.logger.info("close postgres DB connection");
            connection.close();
        } catch (SQLException e) {
            MwbService.logger.warning("failed to close database connection");
        }
    }

    /**
     * rollback current transaction for a connection
     * @param connection to rollback
     * @param exception -
     * @return exception enriched with error message in case rollback failed
     */
    public Exception rollback(Connection connection, Exception exception)  {
        if (connection == null)
            return null;
        try {
            connection.rollback();
        } catch (SQLException e) {
            return DataAccessObject.createDatabaseException("Failed to rollback changes on error ("+exception.getMessage()+").", e);
        }
        return exception;
    }
}
