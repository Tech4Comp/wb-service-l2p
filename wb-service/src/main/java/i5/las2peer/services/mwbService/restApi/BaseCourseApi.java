package i5.las2peer.services.mwbService.restApi;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import i5.las2peer.services.mwbService.Authentication;
import i5.las2peer.services.mwbService.MwbService;
import i5.las2peer.services.mwbService.database.LazyPostgresConnection;
import i5.las2peer.services.mwbService.database.dao.BaseCourseDao;
import i5.las2peer.services.mwbService.database.dao.RoleDao;
import i5.las2peer.services.mwbService.database.dao.entity.BaseCourseEntityDao;
import i5.las2peer.services.mwbService.database.dao.LmsCourseDao;
import i5.las2peer.services.mwbService.database.dto.*;
import i5.las2peer.services.mwbService.database.dto.entity.BaseCourseEntityDto;
import i5.las2peer.services.mwbService.database.dto.LmsCourseDto;
import i5.las2peer.services.mwbService.database.dto.entity.LmsWorkerEntityDto;
import i5.las2peer.services.mwbService.database.dto.entity.PersonEntityDto;
import i5.las2peer.services.mwbService.externalServices.LMS.LmsConnection;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 *  REST API to manage base courses
 */
@Path("/baseCourse")
public class BaseCourseApi {

    /**
     * Creates a new base course
     * @param baseCourseDto the base course to be created as a JSON object.
     * @return  200 Ok
     *          400 Bad Request if something goes wrong,
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @POST
    @RolesAllowed("authenticated")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Creates base course in the database.",
            response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad request, Failed to insert base course"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Missing rights"),
            @ApiResponse(code = 500, message = "Internal Server Error") })
    public Response createBaseCourse(
            @ApiParam(value = "The base course to create, as a JSON object", required = true) BaseCourseDto baseCourseDto
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthenticated();
            String userEmail = Authentication.getUserAgentEmail();
            BaseCourseDao baseCourseDao = new BaseCourseDao(connection.getConnection());
            RoleDao roleDao = new RoleDao(connection.getConnection());
            if (!roleDao.mayCreateBaseCourse(userEmail)) {
                LmsConnection lmsConnection = baseCourseDao.setupLmsConnection(baseCourseDto);
                boolean hasAuthorizationForLmsCourse = lmsConnection.hasAuthorizationForLmsCourse(baseCourseDto.getLmsCourseDto(), userEmail);
                if (!hasAuthorizationForLmsCourse)
                    throw Authentication.missingRights();
            }
            LmsCourseDao lmsCourseDao = baseCourseDao.checkAndGetLmsDataAndWorker(baseCourseDto);
            BaseCourseEntityDao baseCourseEntityDao = new BaseCourseEntityDao(connection.getConnection(), baseCourseDto);
            try {
                connection.getConnection().setAutoCommit(false);
                int baseCourseId = baseCourseEntityDao.insert();
                baseCourseDto.setId(baseCourseId);
                roleDao.createRoleAsBaseCourseOwner(baseCourseId, userEmail);
                if (lmsCourseDao != null) {
                    lmsCourseDao.getLmsCourseDto().setBaseCourseId(baseCourseId);
                    lmsCourseDao.upsert();
                }
                connection.getConnection().commit();
                return MwbService.responseOK();
            } catch (Exception e) {
                Exception exception = MwbService.postgresDBConnectionManager.rollback(connection.getConnection(), e);
                return MwbService.responseError(exception);
            }
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * Updating a base course
     * @param baseCourseDto with the new data of the base course as a JSON object.
     * @return  200 Ok
     *          400 Bad Request if something goes wrong,
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @PUT
    @RolesAllowed("authenticated")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(value = "Updating a base course in the database.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad request, Failed to update base course"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Missing rights"),
            @ApiResponse(code = 500, message = "Internal Server Error") })
    public Response updateBaseCourse(
            @ApiParam(value = "The new data of the base course as a JSON object", required = true) BaseCourseDto baseCourseDto
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthorizationForBaseCourse(connection.getConnection(), baseCourseDto.getId());
            BaseCourseDao baseCourseDao = new BaseCourseDao(connection.getConnection());
            LmsCourseDao lmsCourseDao = baseCourseDao.checkAndGetLmsDataAndWorker(baseCourseDto);
            BaseCourseEntityDao baseCourseEntityDao = new BaseCourseEntityDao(connection.getConnection(), baseCourseDto);
            try {
                connection.getConnection().setAutoCommit(false);
                baseCourseEntityDao.update();
                if (lmsCourseDao != null)
                    lmsCourseDao.upsert();
                connection.getConnection().commit();
                return MwbService.responseOK();
            } catch (Exception e) {
                Exception exception = MwbService.postgresDBConnectionManager.rollback(connection.getConnection(), e);
                return MwbService.responseError(exception);
            }
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * deletes a base course
     * @param baseCourseId -
     * @return  200 Ok
     *          400 Bad Request if something goes wrong,
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @DELETE
    @Path("/{baseCourseId}")
    @RolesAllowed("authenticated")
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(value = "Deleting a base course in the database.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad request, Failed to delete base course"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Missing rights"),
            @ApiResponse(code = 500, message = "Internal Server Error") })
    public Response updateBaseCourse(
            @PathParam("baseCourseId") Integer baseCourseId
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthorizationForBaseCourse(connection.getConnection(), baseCourseId);
            BaseCourseEntityDao.delete(connection.getConnection(), baseCourseId);
            return MwbService.responseOK();
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * get data needed for managing a base course
     * @param baseCourseId of the base course
     * @return  200 with data as BaseCourseInfoDto as Json
     *          400 Bad Request if something goes wrong,
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @GET
    @Path("/info/{baseCourseId}")
    @RolesAllowed("authenticated")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getBaseCourseInfo(
            @PathParam("baseCourseId") Integer baseCourseId
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthorizationForBaseCourse(connection.getConnection(), baseCourseId);
            BaseCourseDao baseCourseDao = new BaseCourseDao(connection.getConnection());
            BaseCourseInfoDto baseCourseInfo = baseCourseDao.getBaseCourseInfo(baseCourseId);
            return MwbService.responseOK(new ObjectMapper().writeValueAsString(baseCourseInfo));
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * Retrieves a list with supported lms types.
     * @return  200 list with supported lms types as JSON
     *          400 Bad Request if something goes wrong
     */
    @GET
    @Path("/supportedLmsList")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(
            value = "Retrieves a list with supported lms types.",
            response = String.class)
    public Response getListOfSupportedLms() {
        try {
            return MwbService.responseOK(new ObjectMapper().writeValueAsString(LmsCourseDao.getSupportedLms()));
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * Retrieves all base courses owned by the user from the database.
     * @return  200 list with base courses as BaseCourseEntityDto as JSON
     *          400 Bad Request if something goes wrong
     *          401 Unauthorized
     *          500 Internal Server Error if the database connection is not established.
     */
    @GET
    @Path("/owned")
    @RolesAllowed("authenticated")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(
            value = "Retrieve all base courses owned by the user from the database.",
            response = BaseCourseDto.class)
    public Response getAllBaseCourseOwned() {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthenticated();
            RoleDao roleDao = new RoleDao(connection.getConnection());
            List<BaseCourseEntityDto> baseCourses = roleDao.getBaseCoursesOwnedByEmail(Authentication.getUserAgentEmail());
            return MwbService.responseOK(new ObjectMapper().writeValueAsString(baseCourses));
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * gets a list of base courses associated with a lms course
     * @param lmsCourseDto as LmsCourseDto with lms data of course
     * @return  200 list of base courses associated with that lms course,
     *          400 Bad Request if something goes wrong
     *          401 Unauthorized
     *          500 Internal Server Error if the database connection is not established, or something else goes wrong
     */
    @POST
    @Path("/forLmsCourse")
    @Produces(MediaType.TEXT_PLAIN)
    @RolesAllowed("authenticated")
    public Response getBaseCoursesForLmsResource(
            @ApiParam(value = "lms data for course, as a JSON object", required = true) LmsCourseDto lmsCourseDto
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthenticated();
            BaseCourseDao baseCourseDao = new BaseCourseDao(connection.getConnection());
            List<BaseCourseEntityDto> baseCourses = baseCourseDao.getBaseCoursesForLmsCourse(lmsCourseDto);
            return MwbService.responseOK(new ObjectMapper().writeValueAsString(baseCourses));
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * checks if current user is listed as maintainer for the LMS course
     * @param baseCourseDto as BaseCourseDto with data of lms course to check
     * @return  200 YES if user is authorised,
     *          200 NO if user is not authorised,
     *          400 Bad Request if something went wrong
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established, or something else goes wrong
     */
    @POST
    @Path("/hasLmsAuthorization")
    @Produces(MediaType.TEXT_PLAIN)
    @RolesAllowed("authenticated")
    public Response isLmsCourseOwner(
            @ApiParam(value = "lms data for course as a JSON object", required = true) BaseCourseDto baseCourseDto
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthenticated();
            BaseCourseDao baseCourseDao = new BaseCourseDao(connection.getConnection());
            LmsConnection lmsConnection = baseCourseDao.setupLmsConnection(baseCourseDto);
            boolean hasAuthorizationForLmsCourse = lmsConnection.hasAuthorizationForLmsCourse(baseCourseDto.getLmsCourseDto(), Authentication.getUserAgentEmail());
            return MwbService.response(hasAuthorizationForLmsCourse);
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }


    /**
     * checks if current user has the rights to create a new base course
     * @return  200 YES if user is authorised,
     *          200 NO if user is not authorised,
     *          400 Bad Request if something goes wrong,
     *          401 not Authenticated
     *          500 Internal Server Error if the database connection is not established.
     */
    @GET
    @Path("/mayCreate")
    @Produces(MediaType.TEXT_PLAIN)
    @RolesAllowed("authenticated")
    public Response mayCreateBaseCourse(
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthenticated();
            RoleDao roleDao = new RoleDao(connection.getConnection());
            boolean hasRights = roleDao.mayCreateBaseCourse(Authentication.getUserAgentEmail());
            return MwbService.response(hasRights);
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }


    /**
     * add current user as owner of the base course, if s/he is owner of the LMS course
     * @param baseCourseId of the base course
     * @return  200 OK, if the ownership was successfully added.
     *          400 Bad Request if something goes wrong,
     *          401 Unauthorized
     *          403 Missing rights
    */
    @POST
    @Path("/useLmsAuthorization/{baseCourseId}")
    @RolesAllowed("authenticated")
    @Produces(MediaType.TEXT_PLAIN)
    public Response addLmsOwnerAsBaseCourseOwner(
            @PathParam("baseCourseId") Integer baseCourseId
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthenticated();
            String userEmail = Authentication.getUserAgentEmail();
            RoleDao roleDao = new RoleDao(connection.getConnection());
            boolean isOwner = roleDao.ownsBaseCourse(baseCourseId, userEmail);
            if (isOwner)
                return MwbService.responseOK();
            BaseCourseDao baseCourseDao = new BaseCourseDao(connection.getConnection());
            BaseCourseDto baseCourse = baseCourseDao.getBaseCourse(baseCourseId);
            LmsConnection lmsConnection = baseCourseDao.setupLmsConnection(baseCourse);
            boolean hasAuthorizationForLmsCourse = lmsConnection.hasAuthorizationForLmsCourse(baseCourse.getLmsCourseDto(), userEmail);
            if (!hasAuthorizationForLmsCourse)
                throw Authentication.missingRights();
            roleDao.createRoleAsBaseCourseOwner(baseCourseId, userEmail);
            return MwbService.responseOK();
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * checking rights for a lms worker
     * @param baseCourseDto as BaseCourseDto with lms data to check
     * @return  200 string json object with authorization status
     *          400 Bad Request if something went wrong
     *          401 not authenticated
     *          403 missing rights
     *          500 Internal Server Error if the database connection is not established, or something else goes wrong
     */
    @POST
    @Path("/checkWorkerAuthorization")
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed("authenticated")
    public Response checkWorkerAuthorization(
        @ApiParam(value = "lms data for base course as a JSON object", required = true) BaseCourseDto baseCourseDto
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthenticated();
            BaseCourseDao baseCourseDao = new BaseCourseDao(connection.getConnection());
            LmsCourseDao lmsCourseDao = baseCourseDao.checkAndGetLmsData(baseCourseDto);
            LmsWorkerEntityDto lmsWorker = baseCourseDao.checkAndGetLmsWorker(baseCourseDto, lmsCourseDao);
            ObjectNode json = new ObjectMapper().createObjectNode();
            LmsConnection lmsConnection = LmsConnection.createLmsConnection(lmsWorker.getLms());
            if (lmsConnection.checkWorkerAuthorization(lmsWorker, json))
                lmsConnection.checkWorkerAuthorizationForLmsCourse(lmsWorker, lmsCourseDao.getLmsCourseDto(), json);
            return MwbService.responseOK(json.toString());
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * Adds a person as owner for a base course
     * @param baseCourseId of the base course
     * @param personEntityDto the person to be added as owner as a JSON object.
     * @return  200 Ok
     *          400 Bad Request if something goes wrong,
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @POST
    @Path("/owner/{baseCourseId}")
    @RolesAllowed("authenticated")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Adds a person as owner for a base course.",
            response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad request, Failed to insert base course"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Missing rights"),
            @ApiResponse(code = 500, message = "Internal Server Error") })
    public Response addBaseCourseOwner(
        @PathParam("baseCourseId") Integer baseCourseId,
        @ApiParam(value = "The person to add as owner as a JSON object", required = true) PersonEntityDto personEntityDto
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthorizationForBaseCourse(connection.getConnection(), baseCourseId);
            RoleDao roleDao = new RoleDao(connection.getConnection());
            roleDao.createRoleAsBaseCourseOwner(baseCourseId, personEntityDto.getEmail());
            return MwbService.responseOK();
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * Revokes ownership of a person for a base course
     * @param baseCourseId of the base course
     * @param personId of the person to delete ownership for
     * @return  200 Ok
     *          400 Bad Request if something goes wrong,
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @DELETE
    @Path("/owner/{baseCourseId}/{personId}")
    @RolesAllowed("authenticated")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Revokes ownership of a person for a base course.",
            response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad request, Failed to insert base course"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Missing rights"),
            @ApiResponse(code = 500, message = "Internal Server Error") })
    public Response revokeBaseCourseOwner(
            @PathParam("baseCourseId") Integer baseCourseId,
            @PathParam("personId") Integer personId
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthorizationForBaseCourse(connection.getConnection(), baseCourseId);
            RoleDao roleDao = new RoleDao(connection.getConnection());
            roleDao.revokeRoleAsBaseCourseOwner(baseCourseId, personId);
            return MwbService.responseOK();
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }
}
