package i5.las2peer.services.mwbService.database.dao;

import i5.las2peer.services.mwbService.database.dao.entity.*;
import i5.las2peer.services.mwbService.database.dto.CourseDto;
import i5.las2peer.services.mwbService.database.dto.entity.BaseCourseEntityDto;
import i5.las2peer.services.mwbService.database.dto.entity.HasRoleEntityDto;
import i5.las2peer.services.mwbService.database.dto.entity.PersonEntityDto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/**
 * DAO to handle complex database requests for managing user rights/roles
 */
public class RoleDao extends DataAccessObject {

    /**
     * constructor
     * @param connection to the database
     */
    public RoleDao(Connection connection) {
        super(connection);
    }


    //******************************************************************************************
    // instance methods
    //******************************************************************************************

    /**
     * checks if user is allowed to create a base course
     * @param email of user
     * @return true, if user has necessary rights, false if not
     * @throws Exception if something went wrong
     */
    public boolean mayCreateBaseCourse(String email) throws Exception {
        PersonEntityDao personEntityDao = new PersonEntityDao(this.connection, email);
        personEntityDao.assertNotNull();
        String sqlStatement = "SELECT objectId" +
                " FROM " + HasRoleEntityDao.TABLE +
                " JOIN " + RoleEntityDao.TABLE + " ON " + RoleEntityDao.TABLE + ".id = " + HasRoleEntityDao.TABLE + ".roleId" +
                " JOIN " + ObjectForRoleEntityDao.TABLE + " ON " + ObjectForRoleEntityDao.TABLE + ".id = " + HasRoleEntityDao.TABLE + ".objectForRoleId" +
                " WHERE roleType = '" + RoleEntityDao.ROLE_TYPE_CREATE + "' AND objectType = '" + ObjectForRoleEntityDao.OBJECT_TYPE_BASECOURSE + "' AND personId=?";
        try (PreparedStatement preparedStatement = this.connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, personEntityDao.getPersonEntityDto().getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            return resultSet.next();
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to check rights for creating new base courses.", e);
        }
    }


    /**
     * @param email of user
     * @return list of base courses the user owns
     * @throws Exception if something went wrong
     */
    public List<BaseCourseEntityDto> getBaseCoursesOwnedByEmail(String email) throws Exception {
        List<BaseCourseEntityDto> baseCourseDtoList = new ArrayList<>();
        PersonEntityDao personEntityDao = new PersonEntityDao(this.connection, email);
        if (personEntityDao.getPersonEntityDto() == null)
            return baseCourseDtoList;
        String sqlStatement = "SELECT " + BaseCourseEntityDao.TABLE + ".* "+
                " FROM " + HasRoleEntityDao.TABLE +
                " JOIN " + RoleEntityDao.TABLE + " ON " + RoleEntityDao.TABLE + ".id = " + HasRoleEntityDao.TABLE + ".roleId" +
                " JOIN " + ObjectForRoleEntityDao.TABLE + " ON " + ObjectForRoleEntityDao.TABLE + ".id = " + HasRoleEntityDao.TABLE + ".objectForRoleId" +
                " JOIN " + BaseCourseEntityDao.TABLE + " ON " + BaseCourseEntityDao.TABLE + ".id = " + HasRoleEntityDao.TABLE + ".objectId" +
                " WHERE roleType = '" + RoleEntityDao.ROLE_TYPE_OWN + "' AND objectType = '" + ObjectForRoleEntityDao.OBJECT_TYPE_BASECOURSE + "' AND personId=?";
        try (PreparedStatement preparedStatement = this.connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, personEntityDao.getPersonEntityDto().getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                BaseCourseEntityDto baseCourseDEntityDto = BaseCourseEntityDao.createBaseCourseEntityDto(resultSet);
                baseCourseDtoList.add(baseCourseDEntityDto);
            }
            return baseCourseDtoList;
        } catch (SQLException e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve baseCourses owned by person " +email+" from database.", e);
        }
    }

    /**
     * returns list of owners for given base course
     * @param baseCourseId -
     * @return list of PersonEntityDto with right as owner for given base course
     * @throws Exception if something went wrong
     */
    public List<PersonEntityDto> getBaseCourseOwners(Integer baseCourseId) throws Exception {
        String sqlStatement = "SELECT " + PersonEntityDao.TABLE + ".*" +
                " FROM " + HasRoleEntityDao.TABLE +
                " JOIN " + RoleEntityDao.TABLE + " ON " + RoleEntityDao.TABLE + ".id = " + HasRoleEntityDao.TABLE + ".roleId" +
                " JOIN " + ObjectForRoleEntityDao.TABLE + " ON " + ObjectForRoleEntityDao.TABLE + ".id = " + HasRoleEntityDao.TABLE + ".objectForRoleId" +
                " JOIN " + PersonEntityDao.TABLE + " ON " + PersonEntityDao.TABLE + ".id = " + HasRoleEntityDao.TABLE + ".personId" +
                " WHERE roleType = '" + RoleEntityDao.ROLE_TYPE_OWN + "' AND objectType = '" + ObjectForRoleEntityDao.OBJECT_TYPE_BASECOURSE + "' AND objectId=?";
        try (PreparedStatement preparedStatement = this.connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, baseCourseId);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<PersonEntityDto> owners = new ArrayList<>();
            while (resultSet.next()) {
                PersonEntityDto owner = PersonEntityDao.createDto(resultSet);
                owners.add(owner);
            }
            return owners;
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve owners for base course "+baseCourseId+".", e);
        }
    }

    /**
     * check if user with given email address has owner rights for base course
     * @param baseCourseId -
     * @param email -
     * @return true, if user has rights, false else
     * @throws Exception if something went wrong
     */
    public boolean ownsBaseCourse(Integer baseCourseId, String email) throws Exception {
        PersonEntityDao personEntityDao = new PersonEntityDao(this.connection, email);
        if (personEntityDao.getPersonEntityDto() == null)
            return false;
        String sqlStatement = "SELECT objectId" +
                " FROM " + HasRoleEntityDao.TABLE +
                " JOIN " + RoleEntityDao.TABLE + " ON " + RoleEntityDao.TABLE + ".id = " + HasRoleEntityDao.TABLE + ".roleId" +
                " JOIN " + ObjectForRoleEntityDao.TABLE + " ON " + ObjectForRoleEntityDao.TABLE + ".id = " + HasRoleEntityDao.TABLE + ".objectForRoleId" +
                " WHERE roleType = '" + RoleEntityDao.ROLE_TYPE_OWN + "' AND objectType = '" + ObjectForRoleEntityDao.OBJECT_TYPE_BASECOURSE + "' AND objectId=? AND personId=?";
        try (PreparedStatement preparedStatement = this.connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, baseCourseId);
            preparedStatement.setInt(2, personEntityDao.getPersonEntityDto().getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            return resultSet.next();
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to check ownership for base course "+baseCourseId+" in database.", e);
        }
    }

    /**
     * add role as owner for a base course
     * @param baseCourseId -
     * @param email of the user
     * @throws Exception if something went wrong
     */
    public void createRoleAsBaseCourseOwner(Integer baseCourseId, String email) throws Exception {
        int personId = PersonEntityDao.getOrCreatePersonId(connection, email);
        RoleEntityDao roleEntityDao = new RoleEntityDao(connection, RoleEntityDao.ROLE_TYPE_OWN);
        ObjectForRoleEntityDao objectForRoleEntityDao = new ObjectForRoleEntityDao(connection, ObjectForRoleEntityDao.OBJECT_TYPE_BASECOURSE);
        HasRoleEntityDto hasRoleEntityDto = new HasRoleEntityDto(null, personId, roleEntityDao.getId(), objectForRoleEntityDao.getId(), baseCourseId);
        HasRoleEntityDao hasRoleEntityDao = new HasRoleEntityDao(connection, hasRoleEntityDto);
        hasRoleEntityDao.insert();
    }

    /**
     * revoke role as owner for a base course
     * @param baseCourseId -
     * @param personId -
     * @throws Exception if something went wrong
     */
    public void revokeRoleAsBaseCourseOwner(Integer baseCourseId, Integer personId) throws Exception {
        RoleEntityDao roleEntityDao = new RoleEntityDao(connection, RoleEntityDao.ROLE_TYPE_OWN);
        ObjectForRoleEntityDao objectForRoleEntityDao = new ObjectForRoleEntityDao(connection, ObjectForRoleEntityDao.OBJECT_TYPE_BASECOURSE);
        HasRoleEntityDto hasRoleEntityDto = new HasRoleEntityDto(null, personId, roleEntityDao.getId(), objectForRoleEntityDao.getId(), baseCourseId);
        HasRoleEntityDao hasRoleEntityDao = new HasRoleEntityDao(connection, hasRoleEntityDto);
        hasRoleEntityDao.delete();
    }
}
