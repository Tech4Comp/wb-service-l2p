package i5.las2peer.services.mwbService.restApi;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.client.gridfs.model.GridFSFile;
import i5.las2peer.services.mwbService.Authentication;
import i5.las2peer.services.mwbService.MwbService;
import i5.las2peer.services.mwbService.database.LazyPostgresConnection;
import i5.las2peer.services.mwbService.database.dao.entity.FileBucketEntityDao;
import i5.las2peer.services.mwbService.database.dto.entity.FileBucketEntityDto;
import io.swagger.annotations.ApiParam;
import org.bson.types.ObjectId;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.*;
import java.util.List;

import static i5.las2peer.services.mwbService.utilities.StringsUtilities.hasValue;
import static org.apache.commons.io.Charsets.ISO_8859_1;
import static org.apache.commons.io.Charsets.UTF_8;

/**
 *  REST API to manage file buckets associated to base courses
 */
@Path("/files")
public class FileBucketApi {

    /**
     * get files in file bucket of base course with given purpose as List of FileBucketDto
     * @param baseCourseId -
     * @param purpose -
     * @return  200 data as json
     *          400 Bad Request if something goes wrong,
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @GET
    @Path("/list/{baseCourseId}/{purpose}")
    @RolesAllowed("authenticated")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getFileList(
            @PathParam("baseCourseId") Integer baseCourseId,
            @PathParam("purpose") String purpose
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthorizationForBaseCourse(connection.getConnection(), baseCourseId);
            List<FileBucketEntityDto> files = FileBucketEntityDao.getFileBucketList(connection.getConnection(), baseCourseId, purpose);
            return MwbService.responseOK(new ObjectMapper().writeValueAsString(files));
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * retrieves entry for the fileBucketId
     * @param fileBucketId -
     * @return  200 with file bucket entry as Json
     *          400 Bad Request if something goes wrong
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @GET
    @Path("/{fileBucketId}")
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @RolesAllowed("authenticated")
    public Response getFileBucketEntry(
            @PathParam("fileBucketId") int fileBucketId
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthorizationForFileBucket(connection.getConnection(), fileBucketId);
            FileBucketEntityDto fileBucketEntityDto = FileBucketEntityDao.readData(connection.getConnection(), fileBucketId);
            return MwbService.responseOK(new ObjectMapper().writeValueAsString(fileBucketEntityDto));
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * create a new file bucket entry
     *
     * @param fileBucketEntityDto to create
     * @return  200 with the id of the new file bucket entry if it is successfully created.
     *          400 Bad Request if something goes wrong
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @RolesAllowed("authenticated")
    public Response createFileBucketEntry(
            @ApiParam(value = "The file bucket entry to create, as a JSON object", required = true) FileBucketEntityDto fileBucketEntityDto
    )  {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthenticated();
            FileBucketEntityDao fileBucketEntityDao = new FileBucketEntityDao(connection.getConnection(), fileBucketEntityDto);
            Authentication.checkAuthorizationForBaseCourse(connection.getConnection(), fileBucketEntityDao.getBaseCourseId());
            int fileBucketId = fileBucketEntityDao.insert();
            return MwbService.responseOK(String.valueOf(fileBucketId));
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * edit file bucket entry
     *
     * @param fileBucketEntityDto to update
     * @return  200 if successful
     *          400 Bad Request if something goes wrong
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @RolesAllowed("authenticated")
    public Response editFileBucketEntry(
            @ApiParam(value = "The file bucket entry to edit, as a JSON object", required = true) FileBucketEntityDto fileBucketEntityDto
    )  {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthenticated();
            FileBucketEntityDao fileBucketEntityDao = new FileBucketEntityDao(connection.getConnection(), fileBucketEntityDto);
            Authentication.checkAuthorizationForBaseCourse(connection.getConnection(), fileBucketEntityDao.getBaseCourseId());
            fileBucketEntityDao.update();
            return MwbService.responseOK();
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * retrieves the content for the file referenced with fileBucketId
     *
     * @param fileBucketId -
     * @return  200 with file content
     *          400 Bad Request if something goes wrong
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @GET
    @Path("/content/{fileBucketId}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    @RolesAllowed("authenticated")
    public Response getFileContent(
            @PathParam("fileBucketId") int fileBucketId
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthorizationForFileBucket(connection.getConnection(), fileBucketId);
            FileBucketEntityDao fileBucketEntityDao = new FileBucketEntityDao(connection.getConnection(), fileBucketId);
            byte[] fileData = MwbService.mongoDBConnectionManager.retrieveFileFromFileBucket(fileBucketEntityDao.getFileId());
            String filename = fileBucketEntityDao.getFilename();
            return Response.ok(fileData)
                    .header("Content-Type", FileBucketEntityDao.getMediaTypeForFile(filename))
                    .header("Content-Disposition", "attachment; filename="+fileBucketEntityDao.getFilename())
                    .build();
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * upload file and store id in file bucket entry, delete old file, if existent
     *
     * @param fileBucketId to store the file id
     * @param uploadedInputStream file content
     * @param fileDetail file meta data
     * @return  200 OK
     *          400 Bad Request if something goes wrong
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @POST
    @Path("/{fileBucketId}")
    @Consumes(MediaType.MULTIPART_FORM_DATA + ";charset=UTF-8")
    @Produces(MediaType.TEXT_PLAIN)
    @RolesAllowed("authenticated")
    public Response storeFileForFileBucketId(
            @PathParam("fileBucketId") int fileBucketId,
            @FormDataParam("file") InputStream uploadedInputStream,
            @FormDataParam("file") FormDataContentDisposition fileDetail
    )  {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthorizationForFileBucket(connection.getConnection(), fileBucketId);
            FileBucketEntityDao fileBucketEntityDao = new FileBucketEntityDao(connection.getConnection(), fileBucketId);
            FileBucketEntityDto fileBucketEntityDto = fileBucketEntityDao.getFileBucketEntityDto();
            String oldFileId = fileBucketEntityDto.getFileId();
            ObjectId fileId = MwbService.mongoDBConnectionManager.storeFileInFileBucket(fileDetail.getFileName(), uploadedInputStream);
            fileBucketEntityDto.setFileId(fileId.toString());
            String fileName = fileDetail.getFileName();
            // convert filename to utf-8, frontend seems to send the filename in FormDataContentDisposition in ISO-88859-1
            // todo: what happens, if filename already IS utf-8?
            String utf8FileName = new String(fileName.getBytes(ISO_8859_1), UTF_8);
            fileBucketEntityDto.setFilename(utf8FileName);
            fileBucketEntityDao.update();
            if (hasValue(oldFileId))
                MwbService.mongoDBConnectionManager.deleteFileInFileBucket(new ObjectId(oldFileId));
            return MwbService.responseOK();
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * delete file bucket entry and corresponding file from database
     *
     * @param fileBucketId -
     * @return  200 OK, if data was successfully deleted
     *          400 Bad Request if something goes wrong
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @DELETE
    @Path("/{fileBucketId}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    @RolesAllowed("authenticated")
    public Response deleteFileBucketEntry(
            @PathParam("fileBucketId") int fileBucketId
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthorizationForFileBucket(connection.getConnection(), fileBucketId);
            FileBucketEntityDto fileBucketEntityDto = FileBucketEntityDao.readData(connection.getConnection(), fileBucketId);
            String fileId = fileBucketEntityDto == null ? null : fileBucketEntityDto.getFileId();
            FileBucketEntityDao.delete(connection.getConnection(), fileBucketId);
            if (hasValue(fileId))
                try {
                    MwbService.mongoDBConnectionManager.deleteFileInFileBucket(new ObjectId(fileId));
                } catch (Exception ignored) {}
            return MwbService.responseOK();
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }



    //******************************************************************************************
    // endpoints allowed only for maintainers (mostly for testing purposes)
    //******************************************************************************************

    /**
     * retrieves the content for the file referenced with fileBucketId
     *
     * @param mongodb -
     * @param bucketName -
     * @param fileId -
     * @return  200 with file content
     *          400 Bad Request if something goes wrong
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @GET
    @Path("/file/{mongodb}/{bucketName}/{fileId}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    @RolesAllowed("authenticated")
    public Response getMongoDBFile(
            @PathParam("mongodb") String mongodb,
            @PathParam("bucketName") String bucketName,
            @PathParam("fileId") String fileId
    ) {
        try {
            Authentication.checkMaintainer();
            GridFSFile gridFSFile = MwbService.mongoDBConnectionManager.retrieveMetaDataForFile(mongodb, bucketName, fileId);
            String filename = gridFSFile.getFilename();
            byte[] fileData = MwbService.mongoDBConnectionManager.retrieveFileFromMongoDb(mongodb, bucketName, fileId);
            return Response.ok(fileData)
                    .header("Content-Type", FileBucketEntityDao.getMediaTypeForFile(filename))
                    .header("Content-Disposition", "attachment; filename="+filename)
                    .build();
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

}
