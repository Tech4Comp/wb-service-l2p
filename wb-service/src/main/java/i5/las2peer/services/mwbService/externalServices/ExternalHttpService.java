package i5.las2peer.services.mwbService.externalServices;

import i5.las2peer.services.mwbService.Credentials;
import i5.las2peer.services.mwbService.database.dao.entity.ExternalServiceEntityDao;
import i5.las2peer.services.mwbService.utilities.StringsUtilities;
import lombok.Setter;

import javax.ws.rs.core.Response;
import java.net.http.HttpClient;
import java.net.http.HttpResponse;
import java.sql.Connection;

/**
 * provides some basic structure to access an external service needing authentication
 * with authentication stored in database
 * including error handling and checking of response code
 */
public abstract class ExternalHttpService extends ExternalService {
    private final String url;
    /**
     * @return url of the external service as String
     * @throws ServiceException with detailed info about failure
     */
    public String getUrl() throws ServiceException {
        if (StringsUtilities.hasNoValue(url))
            throw getServiceExceptionInternalError("no value for 'url' configured for external service");
        return url;
    }

    @Setter
    private Credentials credentials;
    /**
     * @return credentials for the external service as Credentials
     * @throws ServiceException if credentials are missing
     */
    public Credentials getCredentials() throws ServiceException {
        if (credentials == null)
            throw getServiceExceptionInternalError("no credentials configured for external service");
        if (StringsUtilities.hasNoValue(credentials.getUsername()))
            throw getServiceExceptionInternalError("no value for 'user' configured for external service");
        if (StringsUtilities.hasNoValue(credentials.getPassword()))
            throw getServiceExceptionInternalError("no value for 'password' configured for external service");
        return credentials;
    }

    /**
     * HttpClient used to connect to the external service
     */
    protected final HttpClient httpClient;


    /**
     * constructor
     * @param serviceName - name of the external service
     */
    public ExternalHttpService(String serviceName, String url, String username, String password) {
        super(serviceName);
        this.url = url;
        this.credentials = new Credentials(username, password);
        this.httpClient = HttpClient.newHttpClient();
    }

    /**
     * constructor using an establish database connection to retrieve the data for the external service
     * @param connection - database connection
     * @param serviceName - name of the external service
     * @throws Exception if retrieving data failed
     */
    public ExternalHttpService(Connection connection, String serviceName) throws Exception {
        super(serviceName);
        ExternalServiceEntityDao externalServiceEntityDao = new ExternalServiceEntityDao(connection, serviceName);
        this.url = externalServiceEntityDao.getUrl();
        this.credentials = externalServiceEntityDao.getCredentials();
        this.httpClient = HttpClient.newHttpClient();
    }

    /**
     * @return Basic authentication for use in Header of HTTP request as String
     * @throws Exception if no credentials are set
     */
    public String getBasicAuth() throws Exception {
        return getCredentials().createBasicAuth();
    }

    /**
     * @param requiredResponseCode -
     * @param responseCode to assert
     * @param messageIfAssertFails - exception message if assertion fails
     * @throws ServiceHttpResponseException if assertion fails
     */
    protected void assertResponseCode(Response.Status requiredResponseCode, int responseCode, String messageIfAssertFails) throws ServiceHttpResponseException {
        ServiceHttpResponseException.assertResponseCode(this.getServiceName(), requiredResponseCode, responseCode, messageIfAssertFails);
    }

    /**
     * @param responseCode to assert as OK
     * @param messageIfAssertFails - exception message if assertion fails
     * @throws ServiceHttpResponseException if assertion fails
     */
    protected void assertResponseCodeOk(int responseCode, String messageIfAssertFails) throws ServiceHttpResponseException {
        ServiceHttpResponseException.assertResponseCodeOk(this.getServiceName(), responseCode, messageIfAssertFails);
    }

    /**
     * @param response - HTTPResponse of a request
     * @throws ServiceHttpResponseException if assertion fails
     */
    protected void assertResponseCodeOk(HttpResponse<String> response) throws ServiceHttpResponseException {
        ServiceHttpResponseException.assertResponseCodeOk(this.getServiceName(), response);
    }
}
