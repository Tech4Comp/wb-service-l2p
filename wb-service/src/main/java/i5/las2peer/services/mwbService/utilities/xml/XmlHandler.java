package i5.las2peer.services.mwbService.utilities.xml;

import java.io.*;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

/**
 * parse xml to objects
 */
public class XmlHandler {
    private static final XmlMapper xmlMapper = (XmlMapper) new XmlMapper()
            .disable(DeserializationFeature.UNWRAP_ROOT_VALUE)
            .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

    /**
     * @param xml to parse
     * @param tClass of the object
     * @param <T> -
     * @return java object
     * @throws IOException if parsing failed
     */
    public static <T> T parseXmlToPojo(String xml, Class<T> tClass) throws IOException {
        return xmlMapper.readValue(xml, tClass);
    }
}
