package i5.las2peer.services.mwbService.database.dao.entity;


import i5.las2peer.services.mwbService.database.dao.DataAccessObject;
import i5.las2peer.services.mwbService.database.dto.CourseComponentDataDto;
import i5.las2peer.services.mwbService.database.dto.entity.CourseOnyxProxyEntityDto;

import java.sql.*;


/**
 * Dao to handle basic database operations for table 'courseOnyxProxy' for course component OPALONYXPROXY
 */
public class CourseOnyxProxyEntityDao extends CourseComponentDataDao {
    private final CourseOnyxProxyEntityDto courseOnyxProxyEntityDto;

    /**
     * Table name for entity
     */
    public static final String TABLE = "courseOnyxProxy";

    /**
     * constructor
     *
     * @param connection to the database
     * @param courseOnyxProxyEntityDto -
     */
    public CourseOnyxProxyEntityDao(Connection connection, CourseOnyxProxyEntityDto courseOnyxProxyEntityDto) {
        super(connection);
        this.courseOnyxProxyEntityDto = courseOnyxProxyEntityDto;
    }

    /**
     * constructor
     *
     * @param connection to the database
     * @param courseId -
     * @throws Exception if requested connection could not be established
     */
    public CourseOnyxProxyEntityDao(Connection connection, Integer courseId) throws Exception {
        super(connection);
        this.courseOnyxProxyEntityDto = readData(connection, courseId) ;
    }


    //******************************************************************************************
    // basic database operations
    //******************************************************************************************

    /**
     * insert a new record / update existing record to the database
     *
     * @throws Exception if something went wrong
     */
    public void upsert() throws Exception {
        String sqlStatement = "INSERT INTO " + TABLE + " (courseId, validFrom, validUntil) VALUES(?, ?, ?)"+
                " ON CONFLICT ON CONSTRAINT " + TABLE + "_pKey DO UPDATE" +
                " SET validFrom=?, validUntil=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, courseOnyxProxyEntityDto.getCourseId());
            preparedStatement.setDate(2, courseOnyxProxyEntityDto.getValidFrom());
            preparedStatement.setDate(3, courseOnyxProxyEntityDto.getValidUntil());
            preparedStatement.setDate(4, courseOnyxProxyEntityDto.getValidFrom());
            preparedStatement.setDate(5, courseOnyxProxyEntityDto.getValidUntil());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw DataAccessObject.createDatabaseException("Failed to insert/update data for course component OPALONYXPROXY.", e);
        }
    }


    //******************************************************************************************
    // static methods for database operations
    //******************************************************************************************

    public static CourseOnyxProxyEntityDto readData(Connection connection, Integer courseId) throws Exception {
        String sqlStatement = "SELECT * FROM " + TABLE   + " WHERE courseId=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, courseId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return createDto(resultSet);
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve data for component OPALONYXPROXY for course " + courseId + " from database.", e);
        }
        return null;
    }

    /**
     * delete record with given id
     *
     * @param connection to database
     * @param courseId of the record to delete
     * @throws Exception -
     */
    public static void delete(Connection connection, Integer courseId) throws Exception {
        CourseComponentDataDao.delete(connection,TABLE, courseId);
    }


    //******************************************************************************************
    // static method to create DTO from a resultSet
    //******************************************************************************************

    /**
     * create CourseOnyxProxyEntityDto using data from resultSet
     *
     * @param resultSet containing data
     * @return CourseTimelineEntityDto
     * @throws Exception -
     */
    public static CourseOnyxProxyEntityDto createDto(ResultSet resultSet) throws Exception {
        CourseComponentDataDto courseComponentDataDto = CourseComponentDataDao.createFromResultSet(resultSet);
        Date validFrom = resultSet.getDate("validFrom");
        Date validUntil = resultSet.getDate("validUntil");
        return new CourseOnyxProxyEntityDto(courseComponentDataDto, validFrom, validUntil);
    }
}
