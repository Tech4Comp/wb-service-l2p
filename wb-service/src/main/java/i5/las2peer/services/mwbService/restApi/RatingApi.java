package i5.las2peer.services.mwbService.restApi;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import i5.las2peer.services.mwbService.Authentication;
import i5.las2peer.services.mwbService.MwbService;
import i5.las2peer.services.mwbService.database.LazyPostgresConnection;
import i5.las2peer.services.mwbService.database.dao.entity.RatingChatbotResponseEntityDao;
import i5.las2peer.services.mwbService.database.dto.entity.RatingChatbotResponseEntityDto;
import i5.las2peer.services.mwbService.utilities.StringsUtilities;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.Connection;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;


/**
 *  REST API for ratings
 */
@Path("/rate")
public class RatingApi {

    /**
     * @param courseId -
     * @param isThumbsUp - rating is thumbs up, thumbs down, if false
     * @param data contains original message, response message and comment as json
     * @return OK
     */
    @POST
    @Path("/chatbot/{courseId}/{isThumbsUp}")
    @Produces(MediaType.TEXT_PLAIN)
    @RolesAllowed("authenticated")
    public Response trackSendMessageToChatbot(
            @PathParam("courseId") Integer courseId,
            @PathParam("isThumbsUp") boolean isThumbsUp,
            String data
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            String actor = Authentication.pseudonymizeUser(connection.getConnection());
            data = StringsUtilities.removeControlChars(data);
            storeRatingInDb(connection.getConnection(), courseId, actor, isThumbsUp, data);
            trackRatingToLrs(connection.getConnection(), courseId, actor, isThumbsUp, data);
            return MwbService.responseOK();
        } catch (Exception exception) {
            return MwbService.responseError(exception);
        }
    }

    //******************************************************************************************
    // private methods
    //******************************************************************************************

    private void trackRatingToLrs(Connection connection, Integer courseId, String actor, boolean isThumbsUp, String data) throws Exception {
        String verb = isThumbsUp ? "rated_thumbs_up" :  "rated_thumbs_down";
        String object = "chatbot_response";
        TrackApi.trackToLrs(connection, courseId, actor, verb, object, data);
    }

    private void storeRatingInDb(Connection connection, Integer courseId, String actor, boolean isThumbsUp, String data) throws Exception {
        UUID uuid = UUID.fromString(actor);
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readTree(data);
        String originalMessage = jsonNode.get("originalMessage").asText();
        String responseMessage = jsonNode.get("responseMessage").asText();
        String comment = jsonNode.get("comment").asText();
        RatingChatbotResponseEntityDto ratingChatbotResponseDto = new RatingChatbotResponseEntityDto(courseId, uuid, isThumbsUp, originalMessage, responseMessage, comment, Timestamp.from(Instant.now()));
        new RatingChatbotResponseEntityDao(connection, ratingChatbotResponseDto).insert();
    }
}
