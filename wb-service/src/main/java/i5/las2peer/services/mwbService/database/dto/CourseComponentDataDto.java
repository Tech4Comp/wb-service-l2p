package i5.las2peer.services.mwbService.database.dto;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import i5.las2peer.services.mwbService.database.dao.CourseComponentDao;
import i5.las2peer.services.mwbService.database.dto.entity.CourseChatbotEntityDto;
import i5.las2peer.services.mwbService.database.dto.entity.SearchIndexEntityDto;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * base class for DTOs with data of course components
 */
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "type"
)
@JsonSubTypes({
        @JsonSubTypes.Type(value = CourseChatbotEntityDto.class, name = CourseComponentDao.COMPONENT_TYPE_CHATBOT),
        @JsonSubTypes.Type(value = CourseWritingTasksDto.class, name = CourseComponentDao.COMPONENT_TYPE_WRITINGTASKS),
        @JsonSubTypes.Type(value = CourseHintsDto.class, name = CourseComponentDao.COMPONENT_TYPE_MWBHINTS),
        @JsonSubTypes.Type(value = SearchIndexEntityDto.class, name = CourseComponentDao.COMPONENT_TYPE_SEARCHINRESOURCES),
        @JsonSubTypes.Type(value = CourseExamsDto.class, name = CourseComponentDao.COMPONENT_TYPE_EXTENDEDEXAMFEEDBACK),
        @JsonSubTypes.Type(value = CourseTimelineDto.class, name = CourseComponentDao.COMPONENT_TYPE_TIMELINE)
})
@Data
@AllArgsConstructor
public class CourseComponentDataDto {
    private Integer courseId;

    /**
     * default constructor
     */
    public CourseComponentDataDto() {}

    /**
     * copy constructor (used to initialise derived classes)
     *
     * @param copy - ComponentDataDto with data
     */
    public CourseComponentDataDto(CourseComponentDataDto copy) {
        this.courseId = copy.getCourseId();
    }
}
