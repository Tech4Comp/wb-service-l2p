package i5.las2peer.services.mwbService.externalServices;

import i5.las2peer.services.mwbService.Authentication;
import i5.las2peer.services.mwbService.MwbService;
import i5.las2peer.services.mwbService.restApi.TrackApi;

import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;

import static i5.las2peer.services.mwbService.utilities.JsonUtilities.isProperJson;
import static i5.las2peer.services.mwbService.utilities.StringsUtilities.hasNoValue;

/**
 * handle external service for search in resources
 */
public class SearchResources extends ExternalHttpService {
    /**
     * @param connection to the database needed to retrieve data of the external service
     * @throws Exception if something went wrong
     */
    public SearchResources(Connection connection) throws Exception {
        super(connection, MwbService.EXTERNAL_SERVICE_SEARCH);
    }

    /**
     * @param searchId - id of the search (currently supported: ud, ul)
     * @return JSON representation for the info data available for the searchId
     * @throws Exception - exception with detailed info what went wrong
     */
    public String getInfoForSearch(String searchId) throws Exception {
        HttpResponse<String> response = httpClient
                .send(setupRequestForSearchInfo(searchId), HttpResponse.BodyHandlers.ofString());
        assertResponseCodeOk(response.statusCode(), "Failed to get search info for id "+searchId);
        return response.body();
    }

    /**
     * @param connection -
     * @param courseId -
     * @param searchId -
     * @param searchRequest - JSON representation as String of the search request
     * @return JSON representation as String of the search results
     * @throws Exception - exception with detailed info what went wrong
     */
    public String getSearchResultsAndTrackSearch(Connection connection, Integer courseId, String searchId, String searchRequest) throws Exception {
        if (hasNoValue(searchRequest) || !isProperJson(searchRequest))
            throw new ServiceException(MwbService.EXTERNAL_SERVICE_SEARCH, Response.Status.BAD_REQUEST, "search request is empty or has invalid format");
        String searchResponse = getSearchResults(searchId, searchRequest);
        String actor = Authentication.pseudonymizeUser(connection);
        TrackApi.trackToLrs(connection, courseId, actor, "used", "search", searchRequest);
        return searchResponse;
    }


    private String getSearchResults(String searchId, String searchRequest) throws Exception {
        HttpResponse<String> response = httpClient
                .send(setupRequestForSearch(searchId, searchRequest), HttpResponse.BodyHandlers.ofString());
        assertResponseCodeOk(response.statusCode(), searchId + ": failed to get search results for " + searchRequest);
        return response.body();
    }

    private HttpRequest setupRequestForSearchInfo(String searchId) throws Exception {
        URI url = new URI(getUrl()+"/"+searchId+"/resource");
        return HttpRequest.newBuilder()
                .uri(url)
                .header("Content-Type", "application/json; charset="+ StandardCharsets.UTF_8)
                .header("Authorization", getBasicAuth())
                .GET()
                .build();
    }

    private HttpRequest setupRequestForSearch(String searchId, String searchRequest) throws Exception {
        URI url = new URI(getUrl()+"/"+searchId+"/search");
        return HttpRequest.newBuilder()
                .uri(url)
                .header("Content-Type", "application/json; charset="+ StandardCharsets.UTF_8)
                .header("Accept", "application/json; charset="+ StandardCharsets.UTF_8)
                .header("Authorization", getBasicAuth())
                .POST(HttpRequest.BodyPublishers.ofString(searchRequest))
                .build();
    }
}
