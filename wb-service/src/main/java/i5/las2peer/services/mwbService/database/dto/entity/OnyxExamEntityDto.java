package i5.las2peer.services.mwbService.database.dto.entity;

import i5.las2peer.services.mwbService.database.dto.TestToolDto;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * Entity dto corresponding to database table "onyxExam"
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class OnyxExamEntityDto extends TestToolDto {
    private String onyxTestId;
    private String opalNodeId;

    /**
     * default constructor
     */
    public OnyxExamEntityDto() {}

    /**
     * constructor
     *
     * @param testToolDto basic data as LmsExamDto
     * @param onyxTestId -
     * @param opalNodeId -
     */
    public OnyxExamEntityDto(TestToolDto testToolDto, String onyxTestId, String opalNodeId) {
        super(testToolDto);
        this.onyxTestId = onyxTestId;
        this.opalNodeId = opalNodeId;
    }
}
