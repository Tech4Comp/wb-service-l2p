package i5.las2peer.services.mwbService.database.dao;

import i5.las2peer.services.mwbService.database.dao.entity.*;
import i5.las2peer.services.mwbService.database.dto.*;
import i5.las2peer.services.mwbService.database.dto.entity.CourseEntityDto;
import i5.las2peer.services.mwbService.database.dto.entity.CourseExamEntityDto;
import i5.las2peer.services.mwbService.database.dto.entity.EaslitExamEntityDto;

import java.sql.*;

/**
 * DAO to handle data needed for exams
 */
public class ExamDao extends DataAccessObject {

    /**
     * constructor
     * @param connection to the database
     */
    public ExamDao(Connection connection) {
        super(connection);
    }

    /**
     * retrieve data for an exam
     * @param examId - id of the exam
     * @return ExamDto with all details if available
     */
    public ExamDtoForFeedback getExamDtoForFeedback(int examId) throws Exception {
        CourseExamEntityDto courseExamEntityDto = CourseExamEntityDao.readData(connection, examId);
        if (courseExamEntityDto != null) {
            CourseEntityDto courseEntityDto = CourseEntityDao.readData(this.connection, courseExamEntityDto.getCourseId());
            if (courseEntityDto == null)
                throw getInvalidDataException("No course found for exam");
            BaseCourseDao baseCourseDao = new BaseCourseDao(this.connection);
            BaseCourseDto baseCourseDto = baseCourseDao.getBaseCourse(courseEntityDto.getBaseCourseId());
            if (baseCourseDto == null)
                throw getInvalidDataException("No base course found for course for exam");
            TestToolDto testToolDto = LmsExamDao.readDataForTestTool(this.connection, courseExamEntityDto);
            if (testToolDto == null)
                throw getInvalidDataException("No test tool data found for exam");
            EaslitExamEntityDto easlitExamEntityDto = EaslitExamEntityDao.readData(this.connection, examId);
            if (easlitExamEntityDto == null)
                throw getInvalidDataException("No easlit data found for exam");
            return new ExamDtoForFeedback(new ExamDto(courseExamEntityDto, testToolDto, easlitExamEntityDto), baseCourseDto);
        }
        return null;
    }
}
