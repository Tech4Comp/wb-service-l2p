package i5.las2peer.services.mwbService.database.dto;

import i5.las2peer.services.mwbService.database.dto.entity.FileBucketEntityDto;
import i5.las2peer.services.mwbService.database.dto.entity.PersonEntityDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * Dto for data needed to manage base courses
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class BaseCourseInfoDto extends BaseCourseDto {
    private List<CourseDto> courses;
    private List<FileBucketEntityDto> files;
    private List<PersonEntityDto> owners;

    /**
     * default constructor
     */
    public  BaseCourseInfoDto() {}

    /**
     * constructor
     *
     * @param baseCourseDto basic data as baseCourseDto
     * @param courses - list of associated courses
     * @param files - list of associated file stored in file bucket
     * @param owners - list of course managers
     */
    public BaseCourseInfoDto(BaseCourseDto baseCourseDto, List<CourseDto> courses, List<FileBucketEntityDto> files, List<PersonEntityDto> owners) {
        super(baseCourseDto);
        this.courses = courses;
        this.files = files;
        this.owners = owners;
    }
}
