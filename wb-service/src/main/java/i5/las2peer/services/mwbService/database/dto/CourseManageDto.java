package i5.las2peer.services.mwbService.database.dto;

import i5.las2peer.services.mwbService.database.dto.entity.CourseFaqEntityDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;


/**
 * Dto with data needed for the MWB view to manage a course
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class CourseManageDto extends CourseInfoDto {
    private List<CourseFaqEntityDto> botIntents;
    private boolean hasLrsData;

    /**
     * constructor
     *
     * @param courseInfoDto data as CourseInfoDto
     * @param hasLrsData for course
     */
    public CourseManageDto(CourseInfoDto courseInfoDto, List<CourseFaqEntityDto> botIntents, boolean hasLrsData) {
        super(courseInfoDto);
        this.botIntents = botIntents;
        this.hasLrsData = hasLrsData;
    }
}
