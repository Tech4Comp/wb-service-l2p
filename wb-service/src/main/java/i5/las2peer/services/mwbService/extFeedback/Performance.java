package i5.las2peer.services.mwbService.extFeedback;

import java.util.EnumMap;

/**
 * defines the performance levels used in easlit items and
 * provides methods to sum up the correct answers for each performance level
 */
public class Performance {

    /**
     * maps to easlit performance level
     */
    public enum Level {
        /**
         * Performance level 'remember'
         */
        REMEMBER,

        /**
         * Performance level 'understand'
         */
        UNDERSTAND,

        /**
         * Performance level 'apply'
         */
        APPLY,

        /**
         * Performance level 'analyze'
         */
        ANALYZE
    }

    /**
     * initialise stats for correct answers
     * @return EnumMap with Performance level as key and CorrectAnswersStatistics as value
     */
    public static EnumMap<Performance.Level, CorrectAnswersStatistics> createCorrectAnswersStatistic() {
        EnumMap<Level, CorrectAnswersStatistics> correctAnswersStatistics = new EnumMap<>(Level.class);
        for (Level level : Level.values()) {
            correctAnswersStatistics.put(level, new CorrectAnswersStatistics());
        }
        return correctAnswersStatistics;
    }

    /**
     * @param statistics to add the value
     * @param level - performance level to increase the value for
     * @param value - value to add
     * @param max value for the item
     */
    public static void sumUp(EnumMap<Performance.Level, CorrectAnswersStatistics> statistics, Level level, Double value, Double max) {
        statistics.get(level).increase(value, max);
    }
}
