package i5.las2peer.services.mwbService.database.dao.entity;

import i5.las2peer.services.mwbService.MwbService;
import i5.las2peer.services.mwbService.database.dao.DataAccessObject;
import i5.las2peer.services.mwbService.database.dto.entity.FileBucketEntityDto;
import i5.las2peer.services.mwbService.database.dto.entity.RatingChatbotResponseEntityDto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * DAO to handle database requests related to ratings
 */
public class RatingChatbotResponseEntityDao extends DataAccessObject {
    private final RatingChatbotResponseEntityDto ratingChatbotResponseEntityDto;

    /**
     * Table name for data
     */
    public static final String TABLE = "ratingChatbotResponse";

    /**
     * constructor
     * @param connection to the database
     * @param ratingChatbotResponseEntityDto -
     */
    public RatingChatbotResponseEntityDao(Connection connection, RatingChatbotResponseEntityDto ratingChatbotResponseEntityDto) {
        super(connection);
        this.ratingChatbotResponseEntityDto = ratingChatbotResponseEntityDto;
    }


    //******************************************************************************************
    // basic database operations
    //******************************************************************************************

    /**
     * insert a new record into the database
     *
     * @return id of the new record
     * @throws Exception if something went wrong
     */
    public int insert() throws Exception {
        String sqlStatement = "INSERT INTO " + TABLE +
                " (courseId, uuid, isThumbsUp, originalMessage, responseMessage, comment, timestamp) VALUES(?,?,?,?,?,?,?)" +
                " RETURNING id;";
        try (PreparedStatement preparedStatement = this.connection.prepareStatement(sqlStatement)) {
            preparedStatement.setLong(1, ratingChatbotResponseEntityDto.getCourseId());
            preparedStatement.setObject(2, ratingChatbotResponseEntityDto.getUuid());
            preparedStatement.setBoolean(3, ratingChatbotResponseEntityDto.isThumbsUp());
            preparedStatement.setString(4, ratingChatbotResponseEntityDto.getOriginalMessage());
            preparedStatement.setString(5, ratingChatbotResponseEntityDto.getResponseMessage());
            preparedStatement.setString(6, ratingChatbotResponseEntityDto.getComment());
            preparedStatement.setTimestamp(7, ratingChatbotResponseEntityDto.getTimestamp());
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return resultSet.getInt("id");
        } catch (SQLException e) {
            throw DataAccessObject.createDatabaseException("Failed to insert rating for chatbot response.", e);
        }
        throw DataAccessObject.createDatabaseException("Failed to retrieve id of inserted rating for chatbot response.", null);
    }
}
