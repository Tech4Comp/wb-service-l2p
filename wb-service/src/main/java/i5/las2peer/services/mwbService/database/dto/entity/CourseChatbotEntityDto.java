package i5.las2peer.services.mwbService.database.dto.entity;

import i5.las2peer.services.mwbService.database.dto.CourseComponentDataDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Entity dto corresponding to database table 'courseChatbot' for course component CHATBOT
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class CourseChatbotEntityDto extends CourseComponentDataDto {
    private String chatBot;

    /**
     * default constructor
     */
    public CourseChatbotEntityDto() {}

    /**
     * constructor
     *
     * @param courseComponentDataDto basic data as ComponentDataDto
     * @param chatBot -
     */
    public CourseChatbotEntityDto(CourseComponentDataDto courseComponentDataDto, String chatBot) {
        super(courseComponentDataDto);
        this.chatBot = chatBot;
    }
}
