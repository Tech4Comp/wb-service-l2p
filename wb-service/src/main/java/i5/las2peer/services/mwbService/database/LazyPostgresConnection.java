package i5.las2peer.services.mwbService.database;

import i5.las2peer.services.mwbService.MwbService;
import lombok.Data;

import java.sql.Connection;

/**
 * wrapper class to handle auto closable connections to Postgres db
 * for convenient use in try clauses
 * connection to the database will only be established on demand
 */
public class LazyPostgresConnection implements AutoCloseable {
    private Connection connection = null;

    public Connection getConnection() throws Exception {
        if (connection == null) {
            connection = MwbService.postgresDBConnectionManager.getConnection();
        }
        return connection;
    }

    @Override
    public void close() {
        MwbService.postgresDBConnectionManager.releaseConnection(connection);
    }
}
