package i5.las2peer.services.mwbService.database.dto;


import i5.las2peer.services.mwbService.database.dto.entity.CourseHintEntityDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * dto for course component MWBHINTS
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class CourseHintsDto extends CourseComponentDataDto {
    private List<CourseHintEntityDto> hints;

    /**
     * default constructor
     */
    public CourseHintsDto() {}

    /**
     * constructor
     *
     * @param courseComponentDataDto basic data as CourseComponentDataDto
     * @param hints -
     */
    public CourseHintsDto(CourseComponentDataDto courseComponentDataDto,
                          List<CourseHintEntityDto> hints) {
        super(courseComponentDataDto);
        this.hints = hints;
    }

    /**
     * copy constructor (used to initialise derived classes)
     *
     * @param copy - CourseEntity with data
     */
    public CourseHintsDto(CourseHintsDto copy) {
        super(copy);
        this.hints = copy.getHints();
    }
}
