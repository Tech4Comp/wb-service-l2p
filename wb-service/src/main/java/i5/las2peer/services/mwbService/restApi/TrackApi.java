package i5.las2peer.services.mwbService.restApi;

import i5.las2peer.services.mwbService.Authentication;
import i5.las2peer.services.mwbService.MwbService;
import i5.las2peer.services.mwbService.database.LazyPostgresConnection;
import i5.las2peer.services.mwbService.externalServices.LrsConnection;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.sql.Connection;

import static i5.las2peer.services.mwbService.externalServices.LrsConnection.xApiUriForActivityData;
import static i5.las2peer.services.mwbService.utilities.JsonUtilities.isProperJson;
import static i5.las2peer.services.mwbService.utilities.StringsUtilities.hasValue;

/**
 * API endpoints for tracking actions performed in MWB
 */
@Path("/track")
public class TrackApi {

    //******************************************************************************************
    // tracking messages
    //******************************************************************************************

    /**
     * @param courseId - used to determine LRS store
     * @param data - stored as activity in context extensions of xAPI statement
     * @return OK
     */
    @POST
    @Path("/sentMessage/chatBot/{courseId}/")
    @Produces(MediaType.TEXT_PLAIN)
    @RolesAllowed("authenticated")
    public Response trackSendMessageToChatBot(
            @PathParam("courseId") Integer courseId,
            String data
    ) {
        return track(true, courseId, "sent_message", "chat_bot", data);
    }

    /**
     * @param courseId - used to determine LRS store
     * @param data - message data as json stored as activity in context extensions of xAPI statement
     * @return OK
     */
    @POST
    @Path("/receivedMessage/chatBot/{courseId}/")
    @Produces(MediaType.TEXT_PLAIN)
    @RolesAllowed("authenticated")
    public Response trackReceivedMessageFromChatBot(
            @PathParam("courseId") Integer courseId,
            String data
    ) {
        return track(true, courseId, "received_message", "chat_bot", data);
    }

    //******************************************************************************************
    // track viewing events
    //******************************************************************************************

    /**
     * @param courseId - used to determine LRS store
     * @return OK
     */
    @POST
    @Path("/view/writingTasks/{courseId}/")
    @Produces(MediaType.TEXT_PLAIN)
    @RolesAllowed("authenticated")
    public Response trackViewingWritingTasks(
            @PathParam("courseId") Integer courseId
    ) {
        return track(false, courseId, "viewed", "text_for_writing_tasks", "");
    }

    /**
     * @param courseId - used to determine LRS store
     * @param data - stored as activity in context extensions of xAPI statement
     * @return OK
     */
    @POST
    @Path("/view/writingTaskFeedback/{courseId}/")
    @Produces(MediaType.TEXT_PLAIN)
    @RolesAllowed("authenticated")
    public Response trackViewingFeedbackForWritingTask(
            @PathParam("courseId") Integer courseId,
            String data
    ) {
        return track(true, courseId, "viewed", "feedback_writing_task", data);
    }

    /**
     * @param courseId - used to determine LRS store
     * @param data - stored as activity in context extensions of xAPI statement
     * @return OK
     */
    @POST
    @Path("/view/searchResult/{courseId}/")
    @Produces(MediaType.TEXT_PLAIN)
    @RolesAllowed("authenticated")
    public Response trackViewingSearchResult(
            @PathParam("courseId") Integer courseId,
            String data
    ) {
        return track(true, courseId, "viewed", "search_result", data);
    }

    /**
     * @param courseId - used to determine LRS store
     * @param data - stored as activity in context extensions of xAPI statement
     * @return OK
     */
    @POST
    @Path("/view/searchResultLocation/{courseId}/")
    @Produces(MediaType.TEXT_PLAIN)
    @RolesAllowed("authenticated")
    public Response trackViewingSearchResultLocation(
            @PathParam("courseId") Integer courseId,
            String data
    ) {
        return track(true, courseId, "viewed", "search_result_location", data);
    }

    /**
     * track viewing timeline item
     * @param itemId of timeline item
     * @param courseId - used to determine LRS store
     * @return OK
     */
    @POST
    @Path("/view/timelineItem/{itemId}/{courseId}")
    @Produces(MediaType.TEXT_PLAIN)
    @RolesAllowed("authenticated")
    public Response trackDetailViewForTimelineItem(
            @PathParam("courseId") Integer courseId,
            @PathParam("itemId") String itemId
    ) {
        String data = "{ \"itemId\": %s }".formatted(itemId);
        return track(false, courseId, "viewed_item", "timeline", data);
    }

    //******************************************************************************************
    // track using component full view
    //******************************************************************************************

    /**
     * track showing full view of timeline component
     * @param courseId - used to determine LRS store
     * @return OK
     */
    @POST
    @Path("/fullView/timeline/{courseId}/")
    @Produces(MediaType.TEXT_PLAIN)
    @RolesAllowed("authenticated")
    public Response trackFullViewForTimeline(
            @PathParam("courseId") Integer courseId
    ) {
        return track(false, courseId, "opened_full_view", "timeline", "");
    }

    //******************************************************************************************
    // track graph tool actions
    //******************************************************************************************

    /**
     * @param courseId - used to determine LRS store
     * @param data - stored as activity in context extensions of xAPI statement
     * @return OK
     */
    @POST
    @Path("/graphTool/action/{courseId}/")
    @Produces(MediaType.TEXT_PLAIN)
    @RolesAllowed("authenticated")
    public Response trackUsingGraphTool(
            @PathParam("courseId") Integer courseId,
            String data
    ) {
        return track(false, courseId, "used", "graph_comparison_tool", data);
    }

    //******************************************************************************************
    // public static methods for tracking
    //******************************************************************************************

    /**
     * @param connection - database connection
     * @param courseId - used to determine LRS store
     * @param actor - actor used for xAPI statement
     * @param verb - verb used for xAPI statement
     * @param object - object used for xAPI statement
     * @param activity - activity stored in context extensions of xAPI statement
     * @throws Exception with detailed description of what went wrong
     */
    public static void trackToLrs(Connection connection, Integer courseId, String actor, String verb, String object, String activity) throws Exception {
        LrsConnection lrsConnection = new LrsConnection(connection, courseId);
        trackToLrs(lrsConnection, actor, verb, object, activity);
    }

    /**
     * @param lrsConnection - connection to LRS store
     * @param actor - actor used for xAPI statement
     * @param verb - verb used for xAPI statement
     * @param object - object used for xAPI statement
     * @param activity - activity stored in context extensions of xAPI statement
     * @throws Exception with detailed description of what went wrong
     */
    public static void trackToLrs(LrsConnection lrsConnection, String actor, String verb, String object, String activity) throws Exception {
        String context = createActivityContext(activity);
        lrsConnection.postStatement(actor, verb, object, context, "Failed to track '"+verb+" "+object+"'");
    }


    //******************************************************************************************
    // private methods
    //******************************************************************************************

    private Response track(boolean needsAuthentication, Integer courseId, String verb, String object, String data) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            if (needsAuthentication)
                Authentication.checkAuthenticated();
            String actor = Authentication.pseudonymizeUser(connection.getConnection());
            trackToLrs(connection.getConnection(), courseId, actor, verb, object, data);
            return MwbService.responseOK();
        }
        catch (Exception exception) {
            return MwbService.responseError(exception);
        }
    }

    private static String createActivityContext(String activity) {
        if (!hasValue(activity) || !isProperJson(activity))
            return "";
        return """
            {\
            "extensions": {\
                    "%s": %s\
            }}\
            """.formatted(xApiUriForActivityData, activity);
    }
}
