package i5.las2peer.services.mwbService.database.dao.entity;


import i5.las2peer.services.mwbService.database.dao.DataAccessObject;
import i5.las2peer.services.mwbService.database.dto.CourseDto;
import i5.las2peer.services.mwbService.database.dto.CourseLastVisitedDto;
import i5.las2peer.services.mwbService.database.dto.entity.CourseEntityDto;
import i5.las2peer.services.mwbService.externalServices.ServiceException;
import lombok.Getter;

import java.sql.*;
import java.util.UUID;

/**
 * Entity dao to handle basic database operations for table "course"
 */
@Getter
public class CourseEntityDao extends DataAccessObject {
    private final CourseEntityDto courseEntityDto;

    /**
     * Table name for course data
     */
    public static final String TABLE = "course";

    /**
     * constructor
     * @param connection to the database
     * @param courseEntityDto -
     */
    public CourseEntityDao(Connection connection, CourseEntityDto courseEntityDto) {
        super(connection);
        this.courseEntityDto = courseEntityDto;
    }

    /**
     * constructor
     * @param connection to the database
     * @param id of the course
     */
    public CourseEntityDao(Connection connection, Integer id) throws Exception {
        super(connection);
        this.courseEntityDto = readData(connection, id);
    }

    /**
     * constructor
     * @param connection to the database
     * @param shortname of the course
     */
    public CourseEntityDao(Connection connection, String shortname) throws Exception {
        super(connection);
        this.courseEntityDto = readData(connection, shortname);
    }


    //******************************************************************************************
    // instance methods
    //******************************************************************************************

    public void assertNotIsNull() throws ServiceException {
        if (courseEntityDto == null)
            throw getInvalidDataException("No course found.");
    }

    public String getShortName() throws ServiceException {
        assertNotIsNull();
        return this.courseEntityDto.getShortname();
    }


    //******************************************************************************************
    // basic database operations
    //******************************************************************************************

    /**
     * insert a new record into the database
     *
     * @return id of the new record
     * @throws Exception if something went wrong
     */
    public int insert() throws Exception {
        String sqlStatement = "INSERT INTO " + TABLE +
                " (baseCourseId, shortname, surname, validFrom, validUntil) VALUES (?, ?, ?, ?, ?)" +
                " RETURNING id;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, courseEntityDto.getBaseCourseId());
            preparedStatement.setString(2, courseEntityDto.getShortname());
            preparedStatement.setString(3, courseEntityDto.getSurname());
            preparedStatement.setDate(4, courseEntityDto.getValidFrom());
            preparedStatement.setDate(5, courseEntityDto.getValidUntil());
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return resultSet.getInt("id");
        } catch (SQLException e) {
            throw DataAccessObject.createDatabaseException("Failed to insert course.", e);
        }
        throw DataAccessObject.createDatabaseException("Failed to retrieve id of inserted course.", null);
    }

    /**
     * update record
     *
     * @throws Exception if something went wrong
     */
    public void update() throws Exception {
        String sqlStatement = "UPDATE " + TABLE + " SET shortname=?, surname=?, validFrom=?, validUntil=?" +
                " WHERE id=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setString(1, courseEntityDto.getShortname());
            preparedStatement.setString(2, courseEntityDto.getSurname());
            preparedStatement.setDate(3, courseEntityDto.getValidFrom());
            preparedStatement.setDate(4, courseEntityDto.getValidUntil());
            preparedStatement.setInt(5, courseEntityDto.getId());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to update course.", e);
        }
    }


    //******************************************************************************************
    // static methods for database operations
    //******************************************************************************************

    public static CourseEntityDto readData(Connection connection, Integer id) throws Exception {
        String sqlStatement = "SELECT * FROM " + TABLE   + " WHERE id=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return createDto(resultSet);
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve data for course.", e);
        }
        return null;
    }

    public static CourseEntityDto readData(Connection connection, String shortname) throws Exception {
        String sqlStatement = "SELECT * FROM " + TABLE   + " WHERE shortname=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setString(1, shortname);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return createDto(resultSet);
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve data for course.", e);
        }
        return null;
    }

    /**
     * delete record with given id
     *
     * @param connection to database
     * @param id of the record to delete
     * @throws Exception -
     */
    public static void delete(Connection connection, Integer id) throws Exception {
        String sqlStatement = "DELETE FROM " + TABLE +
                " WHERE id=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to delete course.", e);
        }
    }


    //******************************************************************************************
    // static method to create DTO from a resultSet
    //******************************************************************************************

    /**
     * create CourseEntityDto using data from resultSet
     *
     * @param resultSet containing data
     * @return CourseEntityDto
     * @throws Exception -
     */
    public static CourseEntityDto createDto(ResultSet resultSet) throws Exception {
        Integer id = resultSet.getInt("id");
        Integer baseCourseId = resultSet.getInt("baseCourseId");
        String shortname = resultSet.getString("shortname");
        String surname = resultSet.getString("surname");
        Date validFrom = resultSet.getDate("validFrom");
        Date validUntil = resultSet.getDate("validUntil");
        return new CourseEntityDto(id, baseCourseId, shortname, surname, validFrom, validUntil);
    }
}
