package i5.las2peer.services.mwbService.database.dao.entity;


import i5.las2peer.services.mwbService.database.dao.DataAccessObject;
import i5.las2peer.services.mwbService.database.dto.entity.CourseLayoutEntityDto;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Entity dao to handle basic database operations for table "courseLayout"
 */
public class CourseLayoutEntityDao extends DataAccessObject {
    private final CourseLayoutEntityDto courseLayoutEntityDto;

    /**
     * Table name for data
     */
    public static final String TABLE = "courseLayout";

    /**
     * field list for copy course data
     */
    public static final List<String> copyFieldList = Arrays.asList("size", "json");

    /**
     * constructor
     * @param connection to the database
     * @param courseLayoutEntityDto -
     */
    public CourseLayoutEntityDao(Connection connection, CourseLayoutEntityDto courseLayoutEntityDto) {
        super(connection);
        this.courseLayoutEntityDto = courseLayoutEntityDto;
    }


    //******************************************************************************************
    // static methods for database operations
    //******************************************************************************************

    /**
     * get list of files belonging to a base course
     *
     * @param connection to database
     * @param courseId id of the course
     * @return FileBucketDto with basic and lms data of the base course
     * @throws Exception if something went wrong
     */
    public static List<CourseLayoutEntityDto> getRawLayout(Connection connection, Integer courseId) throws Exception {
        String sqlStatement = "SELECT * FROM " + TABLE +
                " WHERE courseId=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, courseId);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<CourseLayoutEntityDto> list = new ArrayList<>();
            while (resultSet.next())
                list.add(createCourseLayoutEntityDto(resultSet));
            return list;
        } catch (SQLException e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve layout for course.", e);
        }
    }


    //******************************************************************************************
    // basic database operations
    //******************************************************************************************

    /**
     * insert a new record / update existing record to the database
     *
     * @throws Exception if something went wrong
     */
    public void upsert() throws Exception {
        String sqlStatement = "INSERT INTO " + TABLE + " (courseId, size, json) VALUES(?, ?, ?)"+
                " ON CONFLICT ON CONSTRAINT " + TABLE + "_pKey DO UPDATE" +
                " SET json=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, courseLayoutEntityDto.getCourseId());
            preparedStatement.setString(2, courseLayoutEntityDto.getSize());
            preparedStatement.setString(3, courseLayoutEntityDto.getJson());
            preparedStatement.setString(4, courseLayoutEntityDto.getJson());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw DataAccessObject.createDatabaseException("Failed to insert/update chatbot intent.", e);
        }
    }


    //******************************************************************************************
    // static methods to create DTO from a resultSet
    //******************************************************************************************

    /**
     * create PrivacyStatementEntityDto using data from resultSet
     *
     * @param resultSet containing data
     * @return PrivacyStatementEntityDto
     * @throws Exception -
     */
    public static CourseLayoutEntityDto createCourseLayoutEntityDto(ResultSet resultSet) throws Exception {
        Integer courseId = resultSet.getInt("courseId");
        String size = resultSet.getString("size");
        String json = resultSet.getString("json");
        return new CourseLayoutEntityDto(courseId, size, json);
    }
}
