package i5.las2peer.services.mwbService;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Base64;

/**
 * handling credentials
 */
@Data
@AllArgsConstructor
public class Credentials {
    private String username;
    private String password;

    /**
     * default constructor
     */
    public Credentials() {}

    /**
     * @return Base64 encoded Basic Authentication
     */
    public String createBasicAuth() {
        String auth = this.username + ":" + this.password;
        return "Basic " + Base64.getEncoder().encodeToString(auth.getBytes());
    }

}
