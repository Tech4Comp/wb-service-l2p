package i5.las2peer.services.mwbService.database.dao.entity;


import i5.las2peer.services.mwbService.database.dao.DataAccessObject;
import i5.las2peer.services.mwbService.database.dto.entity.PersonEntityDto;
import lombok.Getter;

import java.sql.*;

/**
 * Entity dao to handle basic database operations for table "person"
 */
@Getter
public class PersonEntityDao extends DataAccessObject {
    private final PersonEntityDto personEntityDto;

    /**
     * Table name for course data
     */
    public static final String TABLE = "person";

    /**
     * constructor
     * @param connection to the database
     * @param personEntityDto -
     */
    public PersonEntityDao(Connection connection, PersonEntityDto personEntityDto) {
        super(connection);
        this.personEntityDto = personEntityDto;
    }

    /**
     * constructor
     *
     * @param connection to the database
     * @param personId -
     * @throws Exception if requested connection could not be established
     */
    public PersonEntityDao(Connection connection, Integer personId) throws Exception {
        super(connection);
        this.personEntityDto = readData(connection, personId) ;
    }

    /**
     * constructor
     *
     * @param connection to the database
     * @param email -
     * @throws Exception if requested connection could not be established
     */
    public PersonEntityDao(Connection connection, String email) throws Exception {
        super(connection);
        this.personEntityDto = readData(connection, email);
    }


    //******************************************************************************************
    // instance methods
    //******************************************************************************************

    public Integer getId() throws Exception {
        assertNotNull();
        return personEntityDto.getId();
    }

    public void assertNotNull() throws Exception {
        if (personEntityDto == null)
            throw getInvalidDataException("Person not found.");
    }


    //******************************************************************************************
    // basic database operations
    //******************************************************************************************

    /**
     * insert a new record into the database
     *
     * @return id of the new record
     * @throws Exception if something went wrong
     */
    public int insert() throws Exception {
        String sqlStatement = "INSERT INTO " + TABLE +
                " (email, firstName, lastName) VALUES(?, ?, ?)" +
                " RETURNING id;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setString(1, personEntityDto.getEmail());
            preparedStatement.setString(2, personEntityDto.getFirstName());
            preparedStatement.setString(3, personEntityDto.getLastName());
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return resultSet.getInt("id");
        } catch (SQLException e) {
            throw DataAccessObject.createDatabaseException("Failed to insert person.", e);
        }
        throw DataAccessObject.createDatabaseException("Failed to retrieve id of inserted person.", null);
    }


    //******************************************************************************************
    // static methods for database operations
    //******************************************************************************************

    public static Integer getOrCreatePersonId(Connection connection, String email) throws Exception {
        PersonEntityDao personEntityDao = new PersonEntityDao(connection, email);
        if (personEntityDao.getPersonEntityDto() != null)
            return personEntityDao.getPersonEntityDto().getId();
        personEntityDao = new PersonEntityDao(connection, new PersonEntityDto(email));
        return personEntityDao.insert();
    }

    public static PersonEntityDto readData(Connection connection, Integer personId) throws Exception {
        String sqlStatement = "SELECT * FROM " + TABLE   + " WHERE personId=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, personId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return createDto(resultSet);
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve data for person.", e);
        }
        return null;
    }

    public static PersonEntityDto readData(Connection connection, String email) throws Exception {
        String sqlStatement = "SELECT * FROM " + TABLE   + " WHERE email=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setString(1, email);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return createDto(resultSet);
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve data for person.", e);
        }
        return null;
    }

    /**
     * delete record with given id
     *
     * @param connection to database
     * @param id of the record to delete
     * @throws Exception -
     */
    public static void delete(Connection connection, Integer id) throws Exception {
        String sqlStatement = "DELETE FROM " + TABLE +
                " WHERE id=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to delete course.", e);
        }
    }


    //******************************************************************************************
    // static methods to create person related DTO from a resultSet
    //******************************************************************************************

    /**
     * create dto using data from resultSet
     *
     * @param resultSet containing data
     * @return PersonEntityDto
     * @throws Exception -
     */
    public static PersonEntityDto createDto(ResultSet resultSet) throws Exception {
        Integer id = resultSet.getInt("id");
        String email = resultSet.getString("email");
        String firstName = resultSet.getString("firstName");
        String lastName = resultSet.getString("lastName");
        return new PersonEntityDto(id, email, firstName, lastName);
    }
}
