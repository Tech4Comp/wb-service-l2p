package i5.las2peer.services.mwbService.database.dto.entity;


import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Entity dto corresponding to database table "hasRole"
 */
@Data
@AllArgsConstructor
public class HasRoleEntityDto {
    private Integer id;
    private Integer personId;
    private Integer roleId;
    private Integer objectForRoleId;
    private Integer objetId;

    /**
     * default constructor
     */
    public HasRoleEntityDto() {}
}
