package i5.las2peer.services.mwbService.configuration;

/**
 * use json file with database properties to access database on a local machine
 */
public class DBLocalConfiguration extends JsonConfiguration {
    private static final String jsonFileName = "db-authentication.json";

    /**
     * @throws Exception if reading configuration from json failed
     */
    public DBLocalConfiguration() throws Exception {
        super(jsonFileName);
    }
}
