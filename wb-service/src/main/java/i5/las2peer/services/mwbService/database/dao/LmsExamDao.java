package i5.las2peer.services.mwbService.database.dao;

import i5.las2peer.services.mwbService.database.dao.entity.OnyxExamEntityDao;
import i5.las2peer.services.mwbService.database.dto.TestToolDto;
import i5.las2peer.services.mwbService.database.dto.entity.CourseExamEntityDto;
import i5.las2peer.services.mwbService.database.dto.entity.OnyxExamEntityDto;
import i5.las2peer.services.mwbService.externalServices.ServiceHttpResponseException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.List;

import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;


/**
 * abstract Dao as base class to handle exam/quiz/test data for different test tools
 */
public abstract class LmsExamDao extends DataAccessObject {

    /**
     * constructor
     *
     * @param connection to the database
     */
    public LmsExamDao(Connection connection) {
        super(connection);
    }


    //******************************************************************************************
    // abstract methods
    //******************************************************************************************

    public abstract String getTestTool();
    public abstract TestToolDto getLmsExamDto();
    public abstract void upsert() throws Exception;


    //******************************************************************************************
    // basic static methods
    //******************************************************************************************
    public static List<String> getSupportedTestTools() { return List.of(OnyxExamEntityDao.TESTTOOL); }

    public static ServiceHttpResponseException createTestToolException(String message) {
        return new ServiceHttpResponseException("unknown test tool", HTTP_BAD_REQUEST, message);
    }

    public static TestToolDto readDataForTestTool(Connection connection, CourseExamEntityDto courseExamEntityDto) throws Exception {
        return switch(courseExamEntityDto.getTestTool()) {
            case OnyxExamEntityDao.TESTTOOL -> OnyxExamEntityDao.readData(connection, courseExamEntityDto.getId());
            default -> null;
        };
    }

    public static LmsExamDao createLmsExamDao(Connection connection, TestToolDto testToolDto) throws Exception {
        if (testToolDto instanceof OnyxExamEntityDto)
            return new OnyxExamEntityDao(connection, (OnyxExamEntityDto) testToolDto);
        throw createTestToolException("Method LmsExamDao.createLmsExamDao: unsupported subclass for testToolDto: " + testToolDto.getClass());
    }

    //******************************************************************************************
    // static method to create DTO from a resultSet
    //******************************************************************************************

    /**
     * create LmsExamDto using data from resultSet
     *
     * @param resultSet containing data
     * @return LmsExamDto
     * @throws Exception -
     */
    public static TestToolDto createFromResultSet(ResultSet resultSet) throws Exception {
        Integer courseExamId = resultSet.getInt("courseExamId");
        return new TestToolDto(courseExamId);
    }
}
