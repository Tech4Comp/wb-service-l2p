package i5.las2peer.services.mwbService.database.dao.entity;

import i5.las2peer.services.mwbService.database.dao.DataAccessObject;
import i5.las2peer.services.mwbService.database.dto.entity.CourseComponentEntityDto;
import i5.las2peer.services.mwbService.database.dto.entity.CourseEntityDto;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;


/**
 * Dao to handle basic database operations for table for course component
 */
public class CourseComponentEntityDao extends DataAccessObject {
    private final CourseComponentEntityDto courseComponentEntityDto;

    /**
     * Table name for course data
     */
    public static final String TABLE = "courseComponent";

    /**
     * field list for copy course data
     */
    public static final List<String> copyFieldList = Arrays.asList("componentTypeId", "allowedForAnonymous");

    /**
     * constructor
     * @param courseComponentEntityDto -
     * @param connection to the database
     */
    public CourseComponentEntityDao(Connection connection, CourseComponentEntityDto courseComponentEntityDto) {
        super(connection);
        this.courseComponentEntityDto = courseComponentEntityDto;
    }


    //******************************************************************************************
    // basic database operations
    //******************************************************************************************

    /**
     * insert a new record into the database
     *
     * @throws Exception if something went wrong
     */
    public void insert() throws Exception {
        String sqlStatement = "INSERT INTO " + TABLE + " (courseId, componentTypeId, allowedForAnonymous) VALUES(?, ?, ?)";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, courseComponentEntityDto.getCourseId());
            preparedStatement.setInt(2, courseComponentEntityDto.getComponentTypeId());
            setOptionalBooleanForResultSet(preparedStatement, 3, courseComponentEntityDto.getAllowedForAnonymous());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw DataAccessObject.createDatabaseException("Failed to insert course component.", e);
        }
    }

    /**
     * update record
     *
     * @throws Exception if something went wrong
     */
    public void update() throws Exception {
        String sqlStatement = "UPDATE " + TABLE + " SET allowedForAnonymous=?" +
                " WHERE courseId=? AND componentTypeId=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            setOptionalBooleanForResultSet(preparedStatement, 1, courseComponentEntityDto.getAllowedForAnonymous());
            preparedStatement.setInt(2, courseComponentEntityDto.getCourseId());
            preparedStatement.setInt(3, courseComponentEntityDto.getComponentTypeId());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to update course component.", e);
        }
    }

    /**
     * delete record
     *
     * @throws Exception -
     */
    public void delete() throws Exception {
        delete(this.connection, courseComponentEntityDto.getCourseId(), courseComponentEntityDto.getComponentTypeId());
    }

    //******************************************************************************************
    // static methods for database operations
    //******************************************************************************************

    public static List<CourseComponentEntityDto> readAllData(Connection connection, Integer courseId) throws Exception {
        String sqlStatement = "SELECT * FROM " + TABLE   + " WHERE courseId=?;";
        List<CourseComponentEntityDto> list = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, courseId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                CourseComponentEntityDto component = createDto(resultSet);
                list.add(component);
            }
            return list;
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve components for course.", e);
        }
    }

    /**
     * delete record with given id
     *
     * @param connection to database
     * @param courseId of the record to delete
     * @param componentTypeId of the record to delete
     * @throws Exception -
     */
    public static void delete(Connection connection, Integer courseId, Integer componentTypeId) throws Exception {
        String sqlStatement = "DELETE FROM " + TABLE +
                " WHERE courseId=? AND componentTypeId=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, courseId);
            preparedStatement.setInt(2, componentTypeId);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to delete course component.", e);
        }
    }


    //******************************************************************************************
    // static method to create DTO from a resultSet
    //******************************************************************************************

    /**
     * create CourseEntityDto using data from resultSet
     *
     * @param resultSet containing data
     * @return CourseEntityDto
     * @throws Exception -
     */
    public static CourseComponentEntityDto createDto(ResultSet resultSet) throws Exception {
        Integer courseId = resultSet.getInt("courseId");
        Integer componentTypeId = resultSet.getInt("componentTypeId");
        Boolean allowedForAnonymous = resultSet.getBoolean("allowedForAnonymous");
        return new CourseComponentEntityDto(courseId, componentTypeId, allowedForAnonymous);
    }

}
