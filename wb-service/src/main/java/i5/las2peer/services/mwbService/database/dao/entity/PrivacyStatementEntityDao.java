package i5.las2peer.services.mwbService.database.dao.entity;


import i5.las2peer.services.mwbService.database.dao.DataAccessObject;
import i5.las2peer.services.mwbService.database.dto.entity.PrivacyStatementEntityDto;
import lombok.Getter;

import java.sql.*;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;

import static i5.las2peer.services.mwbService.utilities.StringsUtilities.hasNoValue;

/**
 * Entity dao to handle basic database operations for table "privacyStatement"
 */
@Getter
public class PrivacyStatementEntityDao extends DataAccessObject {
    private final PrivacyStatementEntityDto privacyStatementEntityDto;

    /**
     * Table name for course data
     */
    public static final String TABLE = "privacyStatement";

    /**
     * used to mark the basic privacy statement for the course
     */
    public static final String NO_PURPOSE = "";

    /**
     * field list for copy course data
     */
    public static final List<String> copyFieldList = Arrays.asList("purpose", "link", "lastModified", "fileBucketId");

    /**
     * constructor
     * @param connection to the database
     * @param privacyStatementEntityDto -
     */
    public PrivacyStatementEntityDao(Connection connection, PrivacyStatementEntityDto privacyStatementEntityDto) {
        super(connection);
        this.privacyStatementEntityDto = privacyStatementEntityDto;
    }

    /**
     * constructor
     * @param connection to the database
     * @param id -
     */
    public PrivacyStatementEntityDao(Connection connection, Integer id) throws Exception {
        super(connection);
        this.privacyStatementEntityDto = readData(connection, id);
    }

    /**
     * constructor
     * @param connection to the database
     * @param courseId -
     * @param purpose -
     */
    public PrivacyStatementEntityDao(Connection connection, Integer courseId, String purpose) throws Exception {
        super(connection);
        this.privacyStatementEntityDto = readData(connection, courseId, purpose);
    }


    //******************************************************************************************
    // instance methods
    //******************************************************************************************

    public Integer getId() throws Exception {
        assertNotNull();
        return this.privacyStatementEntityDto.getId();
    }

    public void assertNotNull() throws Exception {
        if (this.privacyStatementEntityDto == null)
            throw getInvalidDataException("No privacy statement provided.");
    }


    //******************************************************************************************
    // basic database operations
    //******************************************************************************************

    /**
     * update/insert record to database
     *
     * @throws Exception if something went wrong
     */
    public void upsert() throws Exception {
        String sqlStatement = "INSERT INTO " + TABLE + " (courseId, purpose, fileBucketId, lastModified) VALUES (?, ?, ?, ?)" +
                " ON CONFLICT ON CONSTRAINT unique_privacystatement_purpose_per_course DO "+
                " UPDATE SET purpose=?, fileBucketId=?, lastModified=?;";
        try (PreparedStatement preparedStatement = this.connection.prepareStatement(sqlStatement)) {
            Timestamp now = Timestamp.from(Instant.now());
            preparedStatement.setInt(1, privacyStatementEntityDto.getCourseId());
            preparedStatement.setString(2, privacyStatementEntityDto.getPurpose());
            preparedStatement.setInt(3, privacyStatementEntityDto.getFileBucketId());
            preparedStatement.setTimestamp(4, now);
            preparedStatement.setString(5, privacyStatementEntityDto.getPurpose());
            preparedStatement.setInt(6, privacyStatementEntityDto.getFileBucketId());
            preparedStatement.setTimestamp(7, now);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw DataAccessObject.createDatabaseException("Failed to upsert privacy statement.", e);
        }
    }


    //******************************************************************************************
    // static methods for database operations
    //******************************************************************************************

    public static PrivacyStatementEntityDto readData(Connection connection, Integer id) throws Exception {
        String sqlStatement = "SELECT * FROM " + TABLE   + " WHERE id=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return createPrivacyStatementEntityDto(resultSet);
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve data for privacy statement.", e);
        }
        return null;
    }

    public static PrivacyStatementEntityDto readData(Connection connection, Integer courseId, String purpose) throws Exception {
        purpose = checkForDefaultPurpose(purpose);
        String sqlStatement = "SELECT * FROM " + TABLE   +
                " WHERE courseId=? AND purpose=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, courseId);
            preparedStatement.setString(2, purpose);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return createPrivacyStatementEntityDto(resultSet);
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve data for privacy statement.", e);
        }
        return null;
    }

    public static String checkForDefaultPurpose(String purpose) {
        return hasNoValue(purpose) ? NO_PURPOSE : purpose;
    }

    /**
     * delete record with given id
     *
     * @param connection to database
     * @param id of the record to delete
     * @throws Exception -
     */
    public static void delete(Connection connection, Integer id) throws Exception {
        String sqlStatement = "DELETE FROM " + TABLE +
                " WHERE id=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to delete privacy statement.", e);
        }
    }


    //******************************************************************************************
    // static methods to create DTO from a resultSet
    //******************************************************************************************

    /**
     * create PrivacyStatementEntityDto using data from resultSet
     *
     * @param resultSet containing data
     * @return PrivacyStatementEntityDto
     * @throws Exception -
     */
    public static PrivacyStatementEntityDto createPrivacyStatementEntityDto(ResultSet resultSet) throws Exception {
        Integer id = resultSet.getInt("id");
        Integer courseId = resultSet.getInt("courseId");
        String purpose = resultSet.getString("purpose");
        Integer fileBucketId = resultSet.getInt("fileBucketId");
        Timestamp lastModified = resultSet.getTimestamp("lastModified");
        return new PrivacyStatementEntityDto(id, courseId, purpose, fileBucketId, lastModified);
    }
}
