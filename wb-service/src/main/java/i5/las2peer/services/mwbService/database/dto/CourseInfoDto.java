package i5.las2peer.services.mwbService.database.dto;

import i5.las2peer.services.mwbService.database.dto.entity.PrivacyStatementEntityDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * Dto augmented with additional data of a course
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class CourseInfoDto extends CourseDto {
    private List<CourseLayoutDto> layout;
    private PrivacyStatementEntityDto privacyInfo;
    private List<CourseComponentDto> components;

    /**
     * default constructor
     */
    public CourseInfoDto() {}

    /**
     * constructor
     *
     * @param course data as CourseDto
     * @param layout for course
     * @param privacyInfo for course
     * @param components for course
     */
    public CourseInfoDto(CourseDto course, List<CourseLayoutDto> layout, PrivacyStatementEntityDto privacyInfo, List<CourseComponentDto> components) {
        super(course);
        this.layout = layout;
        this.privacyInfo = privacyInfo;
        this.components = components;
    }

    /**
     * copy constructor (used to initialise derived classes)
     *
     * @param copy as CourseInfoDto
     */
    public CourseInfoDto(CourseInfoDto copy) {
        super(copy);
        this.layout = copy.getLayout();
        this.privacyInfo = copy.getPrivacyInfo();
        this.components = copy.getComponents();
    }
}
