package i5.las2peer.services.mwbService.database.dao;


import i5.las2peer.services.mwbService.database.dao.entity.LmsWorkerEntityDao;
import i5.las2peer.services.mwbService.database.dto.entity.LmsWorkerEntityDto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/**
 * DAO to handle database requests related to lms workers
 */
public class LmsWorkerDao extends DataAccessObject {
    /**
     * constructor
     * @param connection to the database
     */
    public LmsWorkerDao(Connection connection) {
        super(connection);
    }


    //******************************************************************************************
    // class methods
    //******************************************************************************************

    /**
     * get a list with all LMS worker accounts in the database (NO password!)
     *
     * @return list with id, lms, shortname and username for workers
     * @throws Exception on failure
     */
    public List<LmsWorkerEntityDto> getLmsWorkerListWithoutCredentials() throws Exception {
        String sqlStatement = "SELECT id, lms, shortname, username, '' as password FROM " + LmsWorkerEntityDao.TABLE + ";";
        try (PreparedStatement preparedStatement = this.connection.prepareStatement(sqlStatement)) {
            ResultSet resultSet = preparedStatement.executeQuery();

            List<LmsWorkerEntityDto> lmsWorkerList = new ArrayList<>();
            while (resultSet.next()) {
                LmsWorkerEntityDto lmsWorker = LmsWorkerEntityDao.createLmsWorkerEntityDto(resultSet);
                lmsWorkerList.add(lmsWorker);
            }
            return lmsWorkerList;
        } catch (SQLException e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve list of lms workers from database.", e);
        }
    }

}
