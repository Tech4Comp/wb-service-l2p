package i5.las2peer.services.mwbService.database.dao.entity;


import i5.las2peer.services.mwbService.database.dao.DataAccessObject;
import i5.las2peer.services.mwbService.database.dto.entity.CourseTimelineItemEntityDto;
import lombok.Getter;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Entity dao to handle basic database operations for table "courseTimelineItem"
 */
@Getter
public class CourseTimelineItemEntityDao  extends DataAccessObject {
    private final CourseTimelineItemEntityDto courseTimelineItemEntityDto;

    /**
     * Table name for course data
     */
    public static final String TABLE = "courseTimelineItem";

    /**
     * field list for copy course data
     */
    public static final List<String> copyFieldList = Arrays.asList("title", "startsOn", "endsOn", "description", "editableTime", "editableTitle", "expandToCalendarWeek");

    /**
     * constructor
     * @param connection to the database
     * @param id of the course
     */
    public CourseTimelineItemEntityDao(Connection connection, Integer id) throws Exception {
        super(connection);
        this.courseTimelineItemEntityDto = readData(connection, id);
    }

    /**
     * constructor
     * @param connection to the database
     * @param courseTimelineItemEntityDto -
     */
    public CourseTimelineItemEntityDao(Connection connection, CourseTimelineItemEntityDto courseTimelineItemEntityDto) {
        super(connection);
        this.courseTimelineItemEntityDto = courseTimelineItemEntityDto;
    }


    //******************************************************************************************
    // instance methods
    //******************************************************************************************

    public Integer getRowId() throws Exception {
        assertNotNull();
        return courseTimelineItemEntityDto.getRowId();
    }

    public void assertNotNull() throws Exception {
        if (courseTimelineItemEntityDto == null)
            throw getInvalidDataException("Timeline item not found.");
    }


    //******************************************************************************************
    // basic database operations
    //******************************************************************************************

    /**
     * insert a new record into the database
     *
     * @return id of the new record
     * @throws Exception if something went wrong
     */
    public int insert() throws Exception {
        assertNotNull();
        String sqlStatement = "INSERT INTO " + TABLE +
                " (rowId, title, startsOn, endsOn, description, editableTime, editableTitle, expandToCalendarWeek)" +
                " VALUES(?, ?, ?, ?, ?, ?, ?, ?)" +
                " RETURNING id;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, courseTimelineItemEntityDto.getRowId());
            preparedStatement.setString(2, courseTimelineItemEntityDto.getTitle());
            setOptionalTimestampForResultSet(preparedStatement, 3, courseTimelineItemEntityDto.getStartsOn());
            setOptionalTimestampForResultSet(preparedStatement, 4, courseTimelineItemEntityDto.getEndsOn());
            setOptionalStringForResultSet(preparedStatement, 5, courseTimelineItemEntityDto.getDescription());
            preparedStatement.setBoolean(6, courseTimelineItemEntityDto.getEditableTime());
            preparedStatement.setBoolean(7, courseTimelineItemEntityDto.getEditableTitle());
            preparedStatement.setBoolean(8, courseTimelineItemEntityDto.getExpandToCalendarWeek());
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return resultSet.getInt("id");
        } catch (SQLException e) {
            throw DataAccessObject.createDatabaseException("Failed to insert new timeline item for course component TIMELINE.", e);
        }
        throw DataAccessObject.createDatabaseException("Failed to retrieve id of inserted timeline item.", null);
    }

    /**
     * update new record into the database
     *
     * @throws Exception if something went wrong
     */
    public void update() throws Exception {
        assertNotNull();
        String sqlStatement = "UPDATE " + TABLE +
                " SET rowId=?, title=?, startsOn=?, endsOn=?, description=?, editableTime=?, editableTitle=?, expandToCalendarWeek=?" +
                " WHERE id=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, courseTimelineItemEntityDto.getRowId());
            preparedStatement.setString(2, courseTimelineItemEntityDto.getTitle());
            setOptionalTimestampForResultSet(preparedStatement, 3, courseTimelineItemEntityDto.getStartsOn());
            setOptionalTimestampForResultSet(preparedStatement, 4, courseTimelineItemEntityDto.getEndsOn());
            setOptionalStringForResultSet(preparedStatement, 5, courseTimelineItemEntityDto.getDescription());
            preparedStatement.setBoolean(6, courseTimelineItemEntityDto.getEditableTime());
            preparedStatement.setBoolean(7, courseTimelineItemEntityDto.getEditableTitle());
            preparedStatement.setBoolean(8, courseTimelineItemEntityDto.getExpandToCalendarWeek());
            preparedStatement.setInt(9, courseTimelineItemEntityDto.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw DataAccessObject.createDatabaseException("Failed to update timeline item for course component TIMELINE.", e);
        }
    }
    public void upsert() throws Exception {
        assertNotNull();
        if (courseTimelineItemEntityDto.getId() == null)
            insert();
        else
            update();
    }


    //******************************************************************************************
    // static methods for database operations
    //******************************************************************************************

    public static CourseTimelineItemEntityDto readData(Connection connection, Integer id) throws Exception {
        String sqlStatement = "SELECT * FROM " + TABLE   + " WHERE id=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return createDto(resultSet);
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve data for timeline item.", e);
        }
        return null;
    }

    public static List<CourseTimelineItemEntityDto> readData(Connection connection, List<Integer> rowIds) throws Exception {
        String values = String.join(",", rowIds.stream().map(x -> "?").toList());
        String sqlStatement = "SELECT * FROM " + TABLE + " WHERE rowId IN ("+values+");";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            for (int i = 0; i < rowIds.size(); i++) {
                preparedStatement.setInt(1 + i, rowIds.get(i));
            }
            ResultSet resultSet = preparedStatement.executeQuery();
            List<CourseTimelineItemEntityDto> timelineItems = new ArrayList<>();
            while (resultSet.next()) {
                CourseTimelineItemEntityDto row = createDto(resultSet);
                timelineItems.add(row);
            }
            return timelineItems;
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve list of timeline items for component TIMELINE.", e);
        }
    }

    /**
     * delete record with given id
     *
     * @param connection to database
     * @param id of the record to delete
     * @throws Exception -
     */
    public static void delete(Connection connection, Integer id) throws Exception {
        String sqlStatement = "DELETE FROM " + TABLE +
                " WHERE id=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to delete timeline row.", e);
        }
    }


    //******************************************************************************************
    // static methods to create DTO from a resultSet
    //******************************************************************************************

    /**
     * create CourseTimelineItemEntityDto using data from resultSet
     *
     * @param resultSet containing data
     * @return CourseTimelineItemEntityDto
     * @throws Exception -
     */
    public static CourseTimelineItemEntityDto createDto(ResultSet resultSet) throws Exception {
        int id = resultSet.getInt("id");
        int rowId = resultSet.getInt("rowId");
        String title = resultSet.getString("title");
        Timestamp startsOn = resultSet.getTimestamp("startsOn");
        Timestamp endsOn = resultSet.getTimestamp("endsOn");
        String description = resultSet.getString("description");
        Boolean editableTime = resultSet.getBoolean("editableTime");
        Boolean editableTitle = resultSet.getBoolean("editableTitle");
        Boolean expandToCalendarWeek = resultSet.getBoolean("expandToCalendarWeek");
        return new CourseTimelineItemEntityDto(id, rowId, title, startsOn, endsOn, description, editableTime, editableTitle, expandToCalendarWeek);
    }
}
