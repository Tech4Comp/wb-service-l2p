package i5.las2peer.services.mwbService.database.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * Dto with details for exams of course, including base course lms, test tool and easlit data
 */
@Data
@AllArgsConstructor
public class ExamsManageDto {
    private String lms;
    private List<ExamDto> exams;
}
