package i5.las2peer.services.mwbService.extFeedback;

import lombok.Getter;

import java.util.List;

/**
 * data structure for an easLit item
 */
@Getter
public class EasLitItem {
    private final String questionId;
    private final List<String> topics;
    private final Knowledge.Dimension knowledgeType;
    private final Performance.Level performanceLevel;

    /**
     * @param questionId - onyx question id associated to the item
     * @param knowledgeDimension - knowledge dimension of the item
     * @param performanceLevel - performance level of the item
     * @param topics - topic of the item
     */
    public EasLitItem(String questionId, Knowledge.Dimension knowledgeDimension, Performance.Level performanceLevel, List<String> topics) {
        this.questionId = questionId;
        this.topics = topics;
        this.knowledgeType = knowledgeDimension;
        this.performanceLevel = performanceLevel;
    }

    /**
     * @return a string representation of the item (for testing purposes)
     */
    public String toString() {
        return "questionId: %s, topics: %s, knowledgeType: %s, performanceLevel: %s".formatted(getQuestionId(), getTopics(), getKnowledgeType().toString(), getPerformanceLevel().toString());
    }
}
