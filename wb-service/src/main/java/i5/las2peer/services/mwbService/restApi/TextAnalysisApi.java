package i5.las2peer.services.mwbService.restApi;

import i5.las2peer.services.mwbService.Authentication;
import i5.las2peer.services.mwbService.MwbService;
import i5.las2peer.services.mwbService.database.LazyPostgresConnection;
import i5.las2peer.services.mwbService.externalServices.LrsConnection;
import static i5.las2peer.services.mwbService.externalServices.LrsConnection.*;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.Connection;

/**
 *  handles REST API calls related to text analysis
 */
@Path("/textAnalysis")
public class TextAnalysisApi {
    public static final String xApiUriForVerbUploadFile = xApiStatementBaseUrlMwb +"/verb/uploaded_task";
    public static final String xApiUriForDataUploadedFile = xApiUriForActivityData;
    public static final String xApiUriForVerbReceivedFeedback = xApiStatementBaseUrlMwb +"/verb/received_feedback";
    public static final String xApiUriForDataFeedback = xApiUriForActivityData;


    /**
     * (deprecated) tracks requesting text for writing tasks in LRS store
     * returns empty string and tracks viewing tasks
     *
     * @param courseId - used to determine the LRS store
     * @return empty string
     */
    @GET
    @Path("/tasks/{courseId}")
    @Produces(MediaType.TEXT_PLAIN)
    @RolesAllowed("authenticated")
    public Response getTextForTasksApi(@PathParam("courseId") Integer courseId) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthenticated();
            return MwbService.responseOK(getTextForTasksAndTrack(connection.getConnection(), courseId));
        } catch (Exception exception) {
            return MwbService.responseError(exception);
        }
    }

    /**
     * returns basic info of all uploaded files of the current user in the LRS store associated with the course
     *
     * @param courseId - used to determine the LRS store
     * @return JSONArray as string with basic info for all uploaded files and feedbacks
     */
    @GET
    @Path("/list/{courseId}")
    @Produces(MediaType.TEXT_PLAIN)
    @RolesAllowed("authenticated")
    public Response getInfoFilesSentApi(@PathParam("courseId") Integer courseId) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthenticated();
            return MwbService.responseOK(getInfoForUploads(connection.getConnection(), courseId, Authentication.pseudonymizeUser(connection.getConnection())));
        } catch (Exception exception) {
            return MwbService.responseError(exception);
        }
    }

    //******************************************************************************************
    // private methods
    //******************************************************************************************

    private String getTextForTasksAndTrack(Connection connection, Integer courseId) throws Exception {
        // retrieve text from database is deprecated -> just track viewing
        String result = "";
        String actor = Authentication.pseudonymizeUser(connection);
        TrackApi.trackToLrs(connection, courseId, actor,"viewed", "text_for_writing_tasks", "");
        return result;
    }

    private String getQueryForFileInfoForFilesSent(Integer courseId, String actor) {
        // filter records with verb sent_file for requested user
        // select fields to retrieve and map fileInfo for easier reference
        String maskedXApiUri = maskXApiUri(xApiStatementBaseUrlMwb);
        String maskedDataUri = maskXApiUri(xApiUriForDataUploadedFile);
        return """
                [\
                {"$match": {"$and": [\
                    {"statement.actor.account.name": "%s"},\
                    {"statement.verb.id": "%s"},\
                    {"statement.object.definition.extensions.%s/object/course.id": %d}\
                ]}},\
                {"$project": {\
                    "stored": 1,\
                    "taskNr": "$statement.context.extensions.%s.taskNr",\
                    "fileId": "$statement.context.extensions.%s.id",\
                    "_id": 0\
                }}\
                """.formatted(actor, xApiUriForVerbUploadFile, maskedXApiUri, courseId, maskedDataUri, maskedDataUri);
    }

    private String getInfoForUploads(Connection connection, Integer courseId, String actor) throws Exception {
        String uploads = getInfoForFilesSent(connection, courseId, actor);
        String feedbacks = getInfoForFeedback(connection, courseId, actor);
        return """
                {"upload": %s,\
                "feedback": %s}
                """.formatted(uploads, feedbacks);
    }

    private String getInfoForFilesSent(Connection connection, Integer courseId, String actor) throws Exception {
        // sort on descending timestamp
        String sort = """
                ,\
                {"$sort": {\
                    "stored": -1\
                }}\
                ]\
                """;
        String query = getQueryForFileInfoForFilesSent(courseId, actor) + sort;
        return new LrsConnection(connection, courseId).query(query).toJSONString();
    }

    private String getInfoForFeedback(Connection connection, Integer courseId, String actor) throws Exception {
        String maskedXApiUri = maskXApiUri(xApiStatementBaseUrlMwb);
        String maskedDataUri = maskXApiUri(xApiUriForDataFeedback);
        LrsConnection lrsConnection = new LrsConnection(connection, courseId);
        String query = """
                [\
                {"$match": {"$and": [\
                    {"statement.actor.account.name": "%s"},\
                    {"statement.verb.id": "%s"},\
                    {"statement.object.definition.extensions.%s/object/course.id": %d}\
                ]}},\
                {"$project": {\
                    "stored": 1,\
                    "sourceId": "$statement.context.extensions.%s.source",\
                    "jsonId": "$statement.context.extensions.%s.graphfileId",\
                    "pdfId": "$statement.context.extensions.%s.feedbackId",\
                    "_id": 0\
                }}]\
                """.formatted(actor, xApiUriForVerbReceivedFeedback, maskedXApiUri, courseId, maskedDataUri, maskedDataUri, maskedDataUri);
        return lrsConnection.query(query).toJSONString();
    }
}