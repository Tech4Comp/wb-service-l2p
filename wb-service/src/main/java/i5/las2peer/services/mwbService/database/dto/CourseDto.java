package i5.las2peer.services.mwbService.database.dto;

import i5.las2peer.services.mwbService.database.dto.entity.CourseEntityDto;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * Dto with basic properties of a course
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class CourseDto extends CourseEntityDto {
    private String name;

    /**
     * default constructor
     */
    public CourseDto() {}

    /**
     * constructor
     *
     * @param courseEntityDto basic data as CourseEntity
     * @param name of base course
     */
    public CourseDto(CourseEntityDto courseEntityDto, String name) {
        super(courseEntityDto);
        this.name = name;
    }

    /**
     * copy constructor (used to initialise derived classes)
     *
     * @param copy as CourseDto
     */
    public CourseDto(CourseDto copy) {
        super(copy);
        this.name = copy.getName();
    }
}
