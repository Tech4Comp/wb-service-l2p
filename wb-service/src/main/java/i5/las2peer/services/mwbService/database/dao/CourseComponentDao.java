package i5.las2peer.services.mwbService.database.dao;

import i5.las2peer.services.mwbService.database.dao.entity.*;
import i5.las2peer.services.mwbService.database.dto.*;
import i5.las2peer.services.mwbService.database.dto.entity.*;
import i5.las2peer.services.mwbService.externalServices.ServiceHttpResponseException;

import java.sql.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;


/**
 * DAO to handle complex course component related database requests
 */
public class CourseComponentDao extends DataAccessObject {
    /**
     * component type for chatbot as defined in database
     */
    public static final String COMPONENT_TYPE_CHATBOT = "CHATBOT";
    /**
     * component type for knowledge graph tool as defined in database
     */
    public static final String COMPONENT_TYPE_KNOWLEDGEGRAPHTOOL = "KNOWLEDGEGRAPHTOOL";
    /**
     * component type for writing tasks as defined in database
     */
    public static final String COMPONENT_TYPE_WRITINGTASKS = "WRITINGTASKS";
    /**
     * component type for extended exam feedback as defined in database
     */
    public static final String COMPONENT_TYPE_EXTENDEDEXAMFEEDBACK = "EXTENDEDEXAMFEEDBACK";
    /**
     * component type for search tool as defined in database
     */
    public static final String COMPONENT_TYPE_SEARCHINRESOURCES = "SEARCHINRESOURCES";
    /**
     * component type for mwb hints as defined in database
     */
    public static final String COMPONENT_TYPE_MWBHINTS = "MWBHINTS";
    /**
     * component type for opal-onyx proxy as defined in database
     */
    public static final String COMPONENT_TYPE_OPALONYXPROXY = "OPALONYXPROXY";
    /**
     * component type for timeline as defined in database
     */
    public static final String COMPONENT_TYPE_TIMELINE = "TIMELINE";
    /**
     * component type for dashboard as defined in database
     */
    public static final String COMPONENT_TYPE_DASHBOARD = "DASHBOARD";

    /**
     * constructor
     * @param connection - database connection
     */
    public CourseComponentDao(Connection connection) {
        super(connection);
    }


    //******************************************************************************************
    // static method to create DTOs from a resultSet
    //******************************************************************************************

    /**
     * create CourseDto using data from resultSet
     *
     * @param resultSet containing data
     * @return CourseDto
     * @throws Exception -
     */
    public static CourseComponentDto createCourseComponentDto(ResultSet resultSet) throws Exception {
        CourseComponentEntityDto courseComponentEntityDto = CourseComponentEntityDao.createDto(resultSet);
        String componentType = resultSet.getString("type");
        return new CourseComponentDto(courseComponentEntityDto, componentType, null, null);
    }

    //******************************************************************************************
    // class methods
    //******************************************************************************************

    /**
     * insert data for a course component into the database
     * @param courseComponentDto - CourseComponentDto to insert
     * @throws Exception on failure
     */
    public void insertCourseComponent(CourseComponentDto courseComponentDto) throws Exception {
        new CourseComponentEntityDao(this.connection, courseComponentDto).insert();
        upsertDataForCourseComponent(courseComponentDto);
    }

    /**
     * update the data for a course component to the database
     * @param courseComponentDto - CourseComponentDto to update
     * @throws Exception on failure
     */
    public void updateCourseComponent(CourseComponentDto courseComponentDto) throws Exception {
        new CourseComponentEntityDao(this.connection, courseComponentDto).update();
        upsertDataForCourseComponent(courseComponentDto);
    }

    private void upsertDataForCourseComponent(CourseComponentDto courseComponentDto) throws Exception {
        CourseComponentDataDto data = courseComponentDto.getData();
        if (data != null) {
            if (data instanceof CourseChatbotEntityDto)
                new CourseChatbotEntityDao(this.connection, (CourseChatbotEntityDto) data).upsert();
            else if (data instanceof SearchIndexEntityDto)
                new SearchIndexEntityDao(this.connection, (SearchIndexEntityDto) data).upsert();
            else if (data instanceof CourseWritingTasksEntityDto)
                new CourseWritingTasksEntityDao(this.connection, (CourseWritingTasksEntityDto) data).upsert();
            else if (data instanceof CourseTimelineEntityDto)
                new CourseTimelineEntityDao(this.connection, (CourseTimelineEntityDto) data).upsert();
        }
    }

    /**
     * delete course component
     * @param courseId of the component to delete
     * @param componentTypeId of the component to delete
     * @throws Exception on failure
     */
    public void deleteCourseComponent(Integer courseId, Integer componentTypeId) throws Exception {
        CourseComponentEntityDao.delete(this.connection, courseId, componentTypeId);
        ComponentTypeEntityDto componentTypeEntityDto = ComponentTypeEntityDao.readData(this.connection, componentTypeId);
        if (componentTypeEntityDto != null) {
            deleteCourseComponentData(courseId, componentTypeEntityDto.getType());
        }
    }

    private void deleteCourseComponentData(Integer courseId, String componentType) throws Exception {
        switch (componentType) {
            case COMPONENT_TYPE_MWBHINTS -> CourseHintEntityDao.delete(this.connection, courseId);
            case COMPONENT_TYPE_CHATBOT -> CourseChatbotEntityDao.delete(this.connection, courseId);
            case COMPONENT_TYPE_WRITINGTASKS -> CourseWritingTasksEntityDao.delete(this.connection, courseId);
            case COMPONENT_TYPE_SEARCHINRESOURCES -> SearchIndexEntityDao.delete(this.connection, courseId);
            case COMPONENT_TYPE_TIMELINE -> CourseTimelineEntityDao.delete(this.connection, courseId);
            //case COMPONENT_TYPE_EXTENDEDEXAMFEEDBACK -> getDataExtendedExamFeedback(courseId);
        }
    }


    /**
     * retrieves a list course components associated with a course
     * @param courseId of the course to retrieve all components for
     * @return List of CourseComponentInfoDto
     * @throws Exception on failure
     */
    public List<CourseComponentDto> getCourseComponents(Integer courseId) throws Exception {
        String sqlStatement = "SELECT * FROM " + CourseComponentEntityDao.TABLE +
                " JOIN " + ComponentTypeEntityDao.TABLE + " ON componentTypeId=id WHERE courseId=?";
        try (PreparedStatement preparedStatement = this.connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, courseId);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<CourseComponentDto> courseComponentDtoList = new ArrayList<>();
            while (resultSet.next()) {
                CourseComponentDto componentTypeDto = createCourseComponentDto(resultSet);
                courseComponentDtoList.add(componentTypeDto);
            }
            return courseComponentDtoList;
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve components for course "+ courseId +" from database.", e);
        }
    }

    /**
     * retrieves a list with detailed data of all course components associated with a course
     * @param courseId of the course to retrieve all components for
     * @return List of CourseComponentInfoDto
     * @throws Exception on failure
     */
    public List<CourseComponentDto> getCourseComponentsInfo(Integer courseId) throws Exception {
        String sqlStatement = "SELECT * FROM " + CourseComponentEntityDao.TABLE +
                " JOIN " + ComponentTypeEntityDao.TABLE + " ON componentTypeId=id WHERE courseId=?";
        try (PreparedStatement preparedStatement = this.connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, courseId);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<CourseComponentDto> courseComponentDtoList = new ArrayList<>();
            while (resultSet.next()) {
                CourseComponentDto componentTypeDto = createCourseComponentDto(resultSet);
                String componentType = componentTypeDto.getComponentType();
                CourseComponentDataDto componentData = getCourseComponentData(courseId, componentType);
                componentTypeDto.setData(componentData);
                PrivacyStatementEntityDto privacyStatementEntityDto = PrivacyStatementEntityDao.readData(connection, courseId, componentType);
                if (privacyStatementEntityDto != null) {
                    FileBucketEntityDto fileBucketEntityDto = FileBucketEntityDao.readData(connection, privacyStatementEntityDto.getFileBucketId());
                    componentTypeDto.setPrivacyInfo(new PrivacyInfoDto(privacyStatementEntityDto, fileBucketEntityDto));
                }
                courseComponentDtoList.add(componentTypeDto);
            }
            return courseComponentDtoList;
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve components info for course "+ courseId +" from database.", e);
        }
    }

    private CourseComponentDataDto getCourseComponentData(Integer courseId, String componentType) throws Exception {
        return switch (componentType) {
            case COMPONENT_TYPE_MWBHINTS -> getDataMWBHints(courseId);
            case COMPONENT_TYPE_CHATBOT -> CourseChatbotEntityDao.readData(this.connection, courseId);
            case COMPONENT_TYPE_WRITINGTASKS -> getDataWritingTasks(courseId);
            case COMPONENT_TYPE_SEARCHINRESOURCES -> SearchIndexEntityDao.readData(this.connection, courseId);
            case COMPONENT_TYPE_TIMELINE -> getDataTimeline(courseId);
            case COMPONENT_TYPE_OPALONYXPROXY -> CourseOnyxProxyEntityDao.readData(this.connection, courseId);
            case COMPONENT_TYPE_EXTENDEDEXAMFEEDBACK -> getDataExams(courseId);
            default -> null;
        };
    }

    public CourseHintsDto getDataMWBHints(Integer courseId) throws Exception {
        List<CourseHintEntityDto> tasks = CourseHintEntityDao.readListData(this.connection, courseId);
        CourseComponentDataDto courseComponentDataDto = new CourseComponentDataDto(courseId);
        return new CourseHintsDto(courseComponentDataDto, tasks);
    }

    private CourseExamsDto getDataExams(Integer courseId) throws Exception {
        List<CourseExamEntityDto> exams = CourseExamEntityDao.readListData(this.connection, courseId);
        CourseComponentDataDto courseComponentDataDto = new CourseComponentDataDto(courseId);
        return new CourseExamsDto(courseComponentDataDto, exams);
    }

    public CourseWritingTasksDto getDataWritingTasks(Integer courseId) throws Exception {
        CourseWritingTasksEntityDto courseWritingTasksEntityDto = CourseWritingTasksEntityDao.readData(this.connection, courseId);
        if (courseWritingTasksEntityDto == null)
            return null;
        List<WritingTaskEntityDto> tasks = WritingTaskEntityDao.readData(this.connection, courseId);
        return new CourseWritingTasksDto(courseWritingTasksEntityDto, tasks);
    }

    public CourseTimelineDto getDataTimeline(Integer courseId) throws Exception {
        CourseTimelineEntityDto courseTimelineEntityDto = CourseTimelineEntityDao.readData(this.connection, courseId);
        if (courseTimelineEntityDto == null)
            return null;
        List<CourseTimelineRowEntityDto> rows = CourseTimelineRowEntityDao.getAllRows(this.connection, courseId);
        List<CourseTimelineItemEntityDto> items = rows.isEmpty() ? null :
            CourseTimelineItemEntityDao.readData(this.connection, rows.stream().map(CourseTimelineRowEntityDto::getId).toList());
        return new CourseTimelineDto(courseTimelineEntityDto, rows, items);
    }

    public ExamsManageDto getDataManageExams(CourseEntityDto courseEntityDto) throws Exception {
        List<CourseExamEntityDto> examEntityList = CourseExamEntityDao.readListData(this.connection, courseEntityDto.getId());
        List<ExamDto> examList = new ArrayList<>();
        BaseCourseEntityDto baseCourseEntityDto = BaseCourseEntityDao.readData(this.connection, courseEntityDto.getBaseCourseId());
        if (baseCourseEntityDto == null)
            throw getInvalidDataException("No base course found for course");
        for (CourseExamEntityDto examEntity: examEntityList) {
            TestToolDto testToolDto = LmsExamDao.readDataForTestTool(this.connection, examEntity);
            EaslitExamEntityDto easlitExamEntityDto = EaslitExamEntityDao.readData(this.connection,  examEntity.getId());
            ExamDto examDto = new ExamDto(examEntity, testToolDto, easlitExamEntityDto);
            examList.add(examDto);
        }
        return new ExamsManageDto(baseCourseEntityDto.getLms(), examList);
    }


    /**
     * get all user defined timeline items for a course
     * @param courseId -
     * @param uuid -
     * @return list as Json string
     * @throws Exception if something went wrong
     */
    public List<UserTimelineItemEntityDto> getUserTimelineItems(int courseId, String uuid) throws Exception {
        String sqlStatement = "SELECT * FROM " + UserTimelineItemEntityDao.TABLE +
                " JOIN " + CourseTimelineRowEntityDao.TABLE + " ON " + CourseTimelineRowEntityDao.TABLE + ".id=rowId" +
                " WHERE " + CourseTimelineRowEntityDao.TABLE + ".courseId=?" +
                " AND " + UserTimelineItemEntityDao.TABLE + ".uuid=?";
        try (PreparedStatement preparedStatement = this.connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, courseId);
            preparedStatement.setObject(2, UUID.fromString(uuid));
            ResultSet resultSet = preparedStatement.executeQuery();
            List<UserTimelineItemEntityDto> userTimelineItems = new ArrayList<>();
            while (resultSet.next()) {
                UserTimelineItemEntityDto userTimelineItemDto = UserTimelineItemEntityDao.createDtoWithoutUUID(resultSet);
                userTimelineItems.add(userTimelineItemDto);
            }
            return userTimelineItems;
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve user defined timeline items for course " + courseId + " from database.", e);
        }
    }



    //******************************************************************************************
    // methods to copy data from one course to another
    //******************************************************************************************

    /**
     * @param courseId id of the  new course to copy data for extended exam component to
     * @param courseIdSource id of the course to copy data for extended exam component from
     * @throws Exception if something went wrong
     */
    public void copyDataExam(Integer courseId, Integer courseIdSource) throws Exception {
        CourseExamsDto copyExams = getDataExams(courseIdSource);
        String fieldList = String.join(", ", CourseExamEntityDao.copyFieldList);
        String sqlStatement = String.format("INSERT INTO " + CourseExamEntityDao.TABLE + " (courseId, %s) " +
                " SELECT ?, %s FROM " + CourseExamEntityDao.TABLE + " WHERE id=?" +
                " RETURNING id;", fieldList, fieldList);
        try (PreparedStatement preparedStatement = this.connection.prepareStatement(sqlStatement)) {
            for (CourseExamEntityDto copyExam: copyExams.getExams()) {
                int examIdFrom = copyExam.getId();
                preparedStatement.setInt(1, courseId);
                preparedStatement.setInt(2, examIdFrom);
                ResultSet resultSet = preparedStatement.executeQuery();
                if (resultSet.next()) {
                    int examIdTo = resultSet.getInt("id");
                    copyEaslitExamDetails(examIdFrom, examIdTo);
                    copyTestToolDetails(copyExam.getTestTool(), examIdFrom, examIdTo);
                }
            }
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException(String.format("Failed to copy exam data from courseId %d to courseId %d.", courseIdSource, courseId), e);
        }
    }

    private void copyTestToolDetails(String testTool, int examIdFrom, int examIdTo) throws ServiceHttpResponseException {
        switch (testTool) {
            case OnyxExamEntityDao.TESTTOOL -> copyOnyxExamDetails(examIdFrom, examIdTo);
        }
    }

    private void copyOnyxExamDetails(int examIdFrom, int examIdTo) throws ServiceHttpResponseException {
        String fieldList = String.join(", ", OnyxExamEntityDao.copyFieldList);
        String sqlStatement = String.format("INSERT INTO " + OnyxExamEntityDao.TABLE + " (courseExamId, %s) " +
                " SELECT ?, %s FROM " + OnyxExamEntityDao.TABLE + " WHERE courseExamId=?;", fieldList, fieldList);
        try (PreparedStatement preparedStatement = this.connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, examIdTo);
            preparedStatement.setInt(2, examIdFrom);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException(String.format("Failed to copy onyx details from examId %d to examId %d.", examIdFrom, examIdTo), e);
        }
    }

    private void copyEaslitExamDetails(int examIdFrom, int examIdTo) throws ServiceHttpResponseException {
        String fieldList = String.join(", ", EaslitExamEntityDao.copyFieldList);
        String sqlStatement = String.format("INSERT INTO " + EaslitExamEntityDao.TABLE + " (courseExamId, %s) " +
                " SELECT ?, %s FROM " + EaslitExamEntityDao.TABLE + " WHERE courseExamId=?;", fieldList, fieldList);
        try (PreparedStatement preparedStatement = this.connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, examIdTo);
            preparedStatement.setInt(2, examIdFrom);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException(String.format("Failed to copy easlit details from examId %d to examId %d", examIdFrom, examIdTo), e);
        }
    }


    public void copyDataTimelineRows(CourseEntityDto courseEntityDto, Integer courseIdSource) throws Exception {
        Integer courseId = courseEntityDto.getId();
        CourseTimelineEntityDto timelineNewCourse = new CourseTimelineEntityDao(this.connection, courseEntityDto.getId()).getCourseTimelineEntityDto();
        CourseTimelineEntityDto timelineCopyCourse = new CourseTimelineEntityDao(this.connection, courseIdSource).getCourseTimelineEntityDto();
        long diffInDays = 0;
        if (timelineNewCourse != null && timelineCopyCourse != null) {
            long diffInMillies = Math.abs(timelineNewCourse.getStartsOn().getTime() - timelineCopyCourse.getStartsOn().getTime());
            diffInDays = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
        }
        List<CourseTimelineRowEntityDto> copyTimelineRows = CourseTimelineRowEntityDao.getAllRows(this.connection, courseIdSource);
        String fieldList = String.join(", ", CourseTimelineRowEntityDao.copyFieldList);
        String sqlStatement = String.format("INSERT INTO " + CourseTimelineRowEntityDao.TABLE + " (courseId, %s) " +
                " SELECT ?, %s FROM " + CourseTimelineRowEntityDao.TABLE + " WHERE id=?" +
                " RETURNING id;", fieldList, fieldList);
        try (PreparedStatement preparedStatement = this.connection.prepareStatement(sqlStatement)) {
            for (CourseTimelineRowEntityDto copyTimelineRow: copyTimelineRows) {
                int rowIdFrom = copyTimelineRow.getId();
                preparedStatement.setInt(1, courseId);
                preparedStatement.setInt(2, rowIdFrom);
                ResultSet resultSet = preparedStatement.executeQuery();
                if (resultSet.next()) {
                    int rowIdTo = resultSet.getInt("id");
                    copyDataTimelineItems(rowIdFrom, rowIdTo);
                    if (diffInDays != 0)
                        this.moveTimelineItems(rowIdTo, diffInDays);
                }
            }
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException(String.format("Failed to copy timeline rows from courseId %d to courseId %d", courseIdSource, courseId), e);
        }
    }

    private void moveTimelineItems(Integer rowId, long intervalInDays) throws Exception {
        String moveStatement = String.format("INTERVAL '%d days'", intervalInDays);
        String sqlStatement = String.format("UPDATE " + CourseTimelineItemEntityDao.TABLE +
            " SET startsOn = startsOn + %s, endsOn = endsOn + %s" +
            " WHERE rowId=?;", moveStatement, moveStatement);
        try (PreparedStatement preparedStatement = this.connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, rowId);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException(String.format("Failed to move timeline items for rowId %d", rowId), e);
        }
    }

    private void copyDataTimelineItems(int rowIdFrom, int rowIdTo) throws ServiceHttpResponseException {
        String fieldList = String.join(", ", CourseTimelineItemEntityDao.copyFieldList);
        String sqlStatement = String.format("INSERT INTO " + CourseTimelineItemEntityDao.TABLE + " (rowId, %s) " +
                " SELECT ?, %s FROM " + CourseTimelineItemEntityDao.TABLE + " WHERE rowId=?;", fieldList, fieldList);
        try (PreparedStatement preparedStatement = this.connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, rowIdTo);
            preparedStatement.setInt(2, rowIdFrom);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException(String.format("Failed to copy timeline items from rowId %d to rowId %d.", rowIdFrom, rowIdTo), e);
        }
    }
}
