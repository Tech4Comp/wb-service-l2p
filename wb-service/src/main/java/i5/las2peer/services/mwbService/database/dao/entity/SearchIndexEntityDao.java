package i5.las2peer.services.mwbService.database.dao.entity;

import i5.las2peer.services.mwbService.database.dao.DataAccessObject;
import i5.las2peer.services.mwbService.database.dto.CourseComponentDataDto;
import i5.las2peer.services.mwbService.database.dto.entity.SearchIndexEntityDto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

/**
 * Dao to handle basic database operations for table with data for course component SEARCHINRESOURCES
 */
public class SearchIndexEntityDao extends CourseComponentDataDao {
    private final SearchIndexEntityDto searchIndexEntityDto;

    /**
     * Table name for entity
     */
    public static final String TABLE = "courseSearchIndex";

    /**
     * field list for copy course data
     */
    public static final List<String> copyFieldList = Arrays.asList("searchId", "searchIndex");

    /**
     * constructor
     *
     * @param connection to the database
     * @param searchIndexEntityDto -
     */
    public SearchIndexEntityDao(Connection connection, SearchIndexEntityDto searchIndexEntityDto) {
        super(connection);
        this.searchIndexEntityDto = searchIndexEntityDto;
    }

    /**
     * constructor
     *
     * @param connection to the database
     * @param courseId -
     * @throws Exception if requested connection could not be established
     */
    public SearchIndexEntityDao(Connection connection, Integer courseId) throws Exception {
        super(connection);
        this.searchIndexEntityDto = readData(connection, courseId);
    }

    //******************************************************************************************
    // basic database operations
    //******************************************************************************************

    public static SearchIndexEntityDto readData(Connection connection, Integer courseId) throws Exception {
        String sqlStatement = "SELECT * FROM " + TABLE + " WHERE courseId=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, courseId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return createSearchIndexEntityDto(resultSet);
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve data for component SEARCHINRESOURCES for course " + courseId + " from database.", e);
        }
        return null;
    }

    /**
     * insert a new record / update existing record to the database
     *
     * @throws Exception if something went wrong
     */
    public void upsert() throws Exception {
        String sqlStatement = "INSERT INTO " + TABLE + " (courseId, searchId, searchIndex) VALUES(?, ?, ?)"+
                " ON CONFLICT ON CONSTRAINT " + TABLE + "_pKey DO UPDATE" +
                " SET searchId=?, searchIndex=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, searchIndexEntityDto.getCourseId());
            preparedStatement.setString(2, searchIndexEntityDto.getSearchId());
            preparedStatement.setString(3, searchIndexEntityDto.getSearchIndex());
            preparedStatement.setString(4, searchIndexEntityDto.getSearchId());
            preparedStatement.setString(5, searchIndexEntityDto.getSearchIndex());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw DataAccessObject.createDatabaseException("Failed to insert/update data for course component SEARCHINRESOURCES.", e);
        }
    }


    //******************************************************************************************
    // static methods for database operations
    //******************************************************************************************

    /**
     * delete record with given id
     *
     * @param connection to database
     * @param courseId of the record to delete
     * @throws Exception -
     */
    public static void delete(Connection connection, Integer courseId) throws Exception {
        CourseComponentDataDao.delete(connection, TABLE, courseId);
    }


    //******************************************************************************************
    // static methods to create DTOs from a resultSet
    //******************************************************************************************

    /**
     * create ChatbotEntityDto using data from resultSet
     *
     * @param resultSet containing data
     * @return ChatbotEntityDto
     * @throws Exception -
     */
    public static SearchIndexEntityDto createSearchIndexEntityDto(ResultSet resultSet) throws Exception {
        CourseComponentDataDto courseComponentDataDto = CourseComponentDataDao.createFromResultSet(resultSet);
        String searchId = resultSet.getString("searchId");
        String searchIndex = resultSet.getString("searchIndex");
        return new SearchIndexEntityDto(courseComponentDataDto, searchId, searchIndex);
    }
}
