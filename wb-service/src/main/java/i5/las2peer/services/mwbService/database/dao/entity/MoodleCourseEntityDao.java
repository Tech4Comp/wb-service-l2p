package i5.las2peer.services.mwbService.database.dao.entity;


import i5.las2peer.services.mwbService.database.dao.DataAccessObject;
import i5.las2peer.services.mwbService.database.dao.LmsCourseDao;
import i5.las2peer.services.mwbService.database.dto.entity.BaseCourseEntityDto;
import i5.las2peer.services.mwbService.database.dto.LmsCourseDto;
import i5.las2peer.services.mwbService.database.dto.entity.MoodleCourseEntityDto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Dao to handle basic database operations for table with data for moodle courses
 */
public class MoodleCourseEntityDao extends LmsCourseDao {
    private final MoodleCourseEntityDto moodleCourseEntityDto;

    /**
     * Table name for entity
     */
    public static final String TABLE = "moodleCourse";

    /**
     * LMS type
     */
    public static final String LMS =  "moodle";

    /**
     * constructor
     *
     * @param connection to the database
     * @param moodleCourseEntityDto -
     */
    public MoodleCourseEntityDao(Connection connection, MoodleCourseEntityDto moodleCourseEntityDto) {
        super(connection);
        this.moodleCourseEntityDto = moodleCourseEntityDto;
    }

    /**
     * constructor
     *
     * @param connection to the database
     * @param baseCourseId -
     * @throws Exception if requested connection could not be established
     */
    public MoodleCourseEntityDao(Connection connection, Integer baseCourseId) throws Exception {
        super(connection);
        this.moodleCourseEntityDto = readData(connection, baseCourseId);
    }


    //******************************************************************************************
    // basic database operations
    //******************************************************************************************

    /**
     * insert a new record / update existing record to the database
     *
     * @throws Exception if something went wrong
     */
    public void upsert() throws Exception {
        String sqlStatement = "INSERT INTO " + TABLE + " (baseCourseId, lmsWorkerId, resource) VALUES(?, ?, ?)"+
                " ON CONFLICT ON CONSTRAINT " + TABLE + "_pKey DO UPDATE" +
                " SET lmsWorkerId=?, resource=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, moodleCourseEntityDto.getBaseCourseId());
            preparedStatement.setInt(2, moodleCourseEntityDto.getLmsWorkerId());
            preparedStatement.setString(3, moodleCourseEntityDto.getMoodleResource());
            preparedStatement.setInt(4, moodleCourseEntityDto.getLmsWorkerId());
            preparedStatement.setString(5, moodleCourseEntityDto.getMoodleResource());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw DataAccessObject.createDatabaseException("Failed to insert/update moodle lms data for base course" + moodleCourseEntityDto.getBaseCourseId() + ".", e);
        }
    }


    //******************************************************************************************
    // static methods for database operations
    //******************************************************************************************

    /**
     * delete record with given id
     *
     * @param connection to database
     * @param baseCourseId of the record to delete
     * @throws Exception -
     */
    public static void delete(Connection connection, Integer baseCourseId) throws Exception {
        LmsCourseDao.delete(connection,TABLE, baseCourseId);
    }

    public static MoodleCourseEntityDto readData(Connection connection, Integer baseCourseId) throws Exception {
        String sqlStatement = "SELECT * FROM " + TABLE   + " WHERE baseCourseId=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, baseCourseId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return createMoodleCourseEntityDto(resultSet);
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve moodle lms data for base course " + baseCourseId + ".", e);
        }
        return null;
    }


    //******************************************************************************************
    // static methods to create DTOs from a resultSet
    //******************************************************************************************

    /**
     * create MoodleCourseEntityDto using data from resultSet
     *
     * @param resultSet containing data
     * @return MoodleCourseEntityDto
     * @throws Exception -
     */
    public static MoodleCourseEntityDto createMoodleCourseEntityDto(ResultSet resultSet) throws Exception {
        LmsCourseDto lmsCourseDto = LmsCourseDao.createFromResultSet(resultSet);
        String resource = resultSet.getString("resource");
        return new MoodleCourseEntityDto(lmsCourseDto, resource);
    }


    //******************************************************************************************
    // implementations for abstract methods of base class
    //******************************************************************************************

    /**
     * @return type of LMS as String
     */
    public String getLms() { return LMS; }
    public LmsCourseDto getLmsCourseDto() { return moodleCourseEntityDto; }

    public List<BaseCourseEntityDto> getBaseCoursesForLmsCourse(LmsCourseDto lmsCourseDto) throws Exception {
        throw methodNotYetImplementedForLms("getBaseCoursesForLmsCourse", LMS);
    }
}
