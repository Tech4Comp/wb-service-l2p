package i5.las2peer.services.mwbService.extFeedback;


import java.util.*;

/**
 * Handle statistics for correct answers annotated to
 * different knowledge dimensions, performance levels and topics
 */
public class ExtFeedbackStatistics {
    private final EnumMap<Knowledge.Dimension, CorrectAnswersStatistics> knowledgeStatistics;
    private final EnumMap<Performance.Level, CorrectAnswersStatistics> performanceStatistics;
    private final Map<String, CorrectAnswersStatistics> topicStatistics;

    /**
     * constructor initialising stats
     */
    public ExtFeedbackStatistics() {
        knowledgeStatistics = Knowledge.createCorrectAnswersStatistic();
        performanceStatistics = Performance.createCorrectAnswersStatistic();
        topicStatistics = new HashMap<>();
    }

    /**
     * @return Set of Strings with topics
     */
    public Set<String> getTopics() {
        return topicStatistics.keySet();
    }

    /**
     * @param topic -
     * @return stats with correct answers for given topic as CorrectAnswersStatistics
     */
    public CorrectAnswersStatistics getTopicStatistics(String topic) {
        return topicStatistics.get(topic);
    }

    /**
     * @param dimension -
     * @return stats with correct answers for given knowledge dimension as CorrectAnswersStatistics
     */
    public CorrectAnswersStatistics getKnowledgeStatistics(Knowledge.Dimension dimension) {
        return knowledgeStatistics.get(dimension);
    }

    /**
     * @param level -
     * @return stats with correct answers for given performance level as CorrectAnswersStatistics
     */
    public CorrectAnswersStatistics getPerformanceStatistics(Performance.Level level) {
        return performanceStatistics.get(level);
    }

    /**
     * @param dimension - knowledge dimension
     * @param level - performance level
     * @param topics - topics
     * @param value in points of the answer
     * @param max value in points for that question
     */
    public void sumUpStatistics(Knowledge.Dimension dimension, Performance.Level level, List<String> topics, Double value, Double max) {
        sumUpKnowledgeStatistics(dimension, value, max);
        sumUpPerformanceStatistics(level, value, max);
        sumUpTopicStatistics(topics, value, max);
    }

    private void sumUpKnowledgeStatistics(Knowledge.Dimension dimension, Double value, Double max) {
        Knowledge.sumUp(knowledgeStatistics, dimension, value, max);
    }

    private void sumUpPerformanceStatistics(Performance.Level level, Double value, Double max) {
        Performance.sumUp(performanceStatistics, level, value, max);
    }

    private void sumUpTopicStatistics(List<String> topics, Double value, Double max) {
        for (String topic: topics) {
            CorrectAnswersStatistics statistics = topicStatistics.get(topic);
            if (statistics == null) {
                topicStatistics.put(topic, new CorrectAnswersStatistics(value, max));
            } else {
                statistics.increase(value, max);
            }
        }
    }

    @Override
    public String toString() {
        return Knowledge.toString(knowledgeStatistics);
    }
}
