package i5.las2peer.services.mwbService.externalServices.LMS;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import i5.las2peer.services.mwbService.database.dao.entity.MoodleCourseEntityDao;
import i5.las2peer.services.mwbService.database.dao.entity.OpalCourseEntityDao;
import i5.las2peer.services.mwbService.database.dto.ExamDtoForFeedback;
import i5.las2peer.services.mwbService.database.dto.LmsCourseDto;
import i5.las2peer.services.mwbService.database.dto.entity.LmsWorkerEntityDto;
import i5.las2peer.services.mwbService.externalServices.ExternalHttpService;
import i5.las2peer.services.mwbService.externalServices.ServiceHttpResponseException;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.File;
import java.sql.Timestamp;
import java.util.List;

import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;


/**
 * wrapper class with basic functionality to establish a connection to an LMS
 * and provide an interface to the REST-API of the LMS
 */
public abstract class LmsConnection extends ExternalHttpService {

    /**
     * constructor
     */
    protected LmsConnection(String lmsType) {
        super("LMS-"+ lmsType, "", "", "");
    }


    //******************************************************************************************
    // static methods
    //******************************************************************************************

    /**
     * factory creating a new connection to the lms
     * @param lmsType -
     * @return appropriate subclass of LmsConnection
     * @throws Exception if something went wrong
     */
    public static LmsConnection createLmsConnection(String lmsType) throws Exception {
        return switch (lmsType) {
            case OpalCourseEntityDao.LMS -> new OpalConnection();
            case MoodleCourseEntityDao.LMS -> new MoodleConnection();
            default -> throw new ServiceHttpResponseException("LMS Connection", HTTP_BAD_REQUEST, "LMS connection - unsupported lms: " + lmsType);
        };
    }

    /**
     * factory creating and establishing a new connection to the lms
     * @param lmsWorkerEntityDto with data of lms course
     * @return connection to the lms
     * @throws Exception if something went wrong
     */
    public static LmsConnection setupLmsConnection(LmsWorkerEntityDto lmsWorkerEntityDto) throws Exception {
        if (lmsWorkerEntityDto == null)
            throw new ServiceHttpResponseException("LMS Connection", HTTP_BAD_REQUEST, "no data for lms worker provided");
        LmsConnection lmsConnection = LmsConnection.createLmsConnection(lmsWorkerEntityDto.getLms());
        lmsConnection.establishConnection(lmsWorkerEntityDto);
        return lmsConnection;
    }

    //******************************************************************************************
    // abstract methods
    //******************************************************************************************

    public abstract void establishConnection(LmsWorkerEntityDto lmsWorkerEntityDto) throws Exception;
    public abstract boolean checkWorkerAuthorization(LmsWorkerEntityDto lmsWorkerEntityDto, ObjectNode json);
    public abstract void checkWorkerAuthorizationForLmsCourse(LmsWorkerEntityDto lmsWorkerEntityDto, LmsCourseDto lmsCourseDto, ObjectNode json);


    //******************************************************************************************
    // class declarations for lms objects
    //******************************************************************************************

    /**
     * basic data for a test result
     */
    @Data
    @AllArgsConstructor
    public static class LmsResult {
        String id;
        Timestamp endDate;
        String state;
        Boolean scored;
        Double maxScore;
        Float score;
    }


    //******************************************************************************************
    // abstract methods
    //******************************************************************************************

    /**
     * This method checks if a given user has authorship rights to a specific LMS course.
     * It does this by making a request for the authors of the resourceId, parsing the response XML to get the emails of the authorized users
     * and then checking if the given user email exists in the set of authorized users.
     *
     * @param lmsCourseDto containing the data needed to access the course
     * @param userMail The email of the user to be checked
     * @return true if the user has authorship rights, false otherwise
     * @throws Exception if there is an error making the request or parsing the response
     */
    public abstract boolean hasAuthorizationForLmsCourse(LmsCourseDto lmsCourseDto, String userMail) throws Exception;

    /**
     * @param lmsCourseDto with data of the lms course as LmsCourseDto
     * @return course members as xml/json string, empty if no data could be retrieved
     * @throws Exception if something went wrong
     */
    public abstract String getCourseMembers(LmsCourseDto lmsCourseDto) throws Exception;

    /**
     * @param examDto - data of the base course including lms data
     * @param userEmail to determine the user the test results should be retrieved for
     * @return test results as xml/json string, empty if no data could be retrieved
     * @throws Exception if something went wrong
     */
    public abstract List<LmsResult> getTestResultIds(ExamDtoForFeedback examDto, String userEmail) throws Exception;

    /**
     * @param destinationDirectory to temporarily store the results to
     * @param examDto with data of exam including lms data
     * @param resultId of the result data to be retrieved
     * @return test results as json, empty if no data could be retrieved
     * @throws Exception if something went wrong
     */
    public abstract JsonNode getResultData(File destinationDirectory, ExamDtoForFeedback examDto, String resultId) throws Exception;
}
