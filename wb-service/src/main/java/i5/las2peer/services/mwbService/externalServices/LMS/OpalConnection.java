package i5.las2peer.services.mwbService.externalServices.LMS;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import i5.las2peer.services.mwbService.MwbService;
import i5.las2peer.services.mwbService.database.dao.entity.OpalCourseEntityDao;
import i5.las2peer.services.mwbService.database.dto.ExamDtoForFeedback;
import i5.las2peer.services.mwbService.database.dto.LmsCourseDto;
import i5.las2peer.services.mwbService.database.dto.entity.LmsWorkerEntityDto;
import i5.las2peer.services.mwbService.database.dto.entity.OnyxExamEntityDto;
import i5.las2peer.services.mwbService.database.dto.entity.OpalCourseEntityDto;
import i5.las2peer.services.mwbService.externalServices.ServiceException;
import i5.las2peer.services.mwbService.utilities.StringsUtilities;
import i5.las2peer.services.mwbService.utilities.xml.OpalXmlClasses;
import i5.las2peer.services.mwbService.utilities.xml.XmlParser;
import org.apache.commons.io.FileUtils;

import javax.ws.rs.core.Response;
import java.io.File;
import java.net.URI;
import java.net.URL;
import java.net.http.HttpHeaders;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;

import static i5.las2peer.services.mwbService.utilities.ZipUtilities.unzip;
import static i5.las2peer.services.mwbService.utilities.xml.XmlHandler.parseXmlToPojo;

/**
 * class to handle connection to the RestAPI for OPAL
 * NOTE:
 * using the rest api of OPAL you need to contact BPS to allow for your IP to run such requests
 * when running locally you have to set usePreview to true, because using the preview link for the rest api
 * will not request special rights for your IP.
 * PLEASE remember to set usePreview to false before pushing to repo to ensure using the live system in production mode
 */
public class OpalConnection extends LmsConnection {
    private static final boolean usePreview = false;
    private static final String url = "https://bildungsportal.sachsen.de/opal";
    private static final String previewUrl = "https://bildungsportal.sachsen.de/preview/opal";
    private static final String restApiBaseUrl = (usePreview ? previewUrl : url) + "/restapi/";
    private String authToken;

    private static boolean isUsingPreview() { return usePreview; }

    /**
     * constructor
     */
    public OpalConnection() {
        super(OpalCourseEntityDao.LMS);
    }


    /**
     * establishing a connection to Opal
     * @param lmsWorkerEntityDto with data for worker to access RestAPI
     * @throws Exception if failed to get authToken
     */
    public void establishConnection(LmsWorkerEntityDto lmsWorkerEntityDto) throws Exception {
        if (OpalConnection.usePreview)
            MwbService.logger.info("warning: using OPAL Preview");
        authToken = requestAuthToken(lmsWorkerEntityDto);
    }

    /**
     * @param lmsWorkerEntityDto with data of lms worker to check
     * @param json to store results of check
     */
    public boolean checkWorkerAuthorization(LmsWorkerEntityDto lmsWorkerEntityDto, ObjectNode json) {
        json.put("usingPreview", OpalConnection.isUsingPreview());
        try {
            establishConnection(lmsWorkerEntityDto);
            json.put("establishConnection", "ok");
            return true;
        } catch (ServiceException e) {
            json.put("establishConnection", e.getDetailedMessage());
        } catch (Exception e) {
            json.put("establishConnection", e.getMessage());
        }
        return false;
    }


    /**
     * @param lmsWorkerEntityDto with data of lms worker to check
     * @param json to store results of check
     */
    public void checkWorkerAuthorizationForLmsCourse(LmsWorkerEntityDto lmsWorkerEntityDto, LmsCourseDto lmsCourseDto, ObjectNode json) {
        try {
            String userEmail = "no@one.de";
            // worker must have course owner rights to check course owners...
            hasAuthorizationForLmsCourse(lmsCourseDto, userEmail);
            json.put("hasAuthorizationForCourse", true);
        } catch (ServiceException e) {
            json.put("failedAuthorizationForCourse", e.getDetailedMessage());
        } catch (Exception e) {
            json.put("failedAuthorizationForCourse", e.getMessage());
        }
    }


    public String getCourseMembers(LmsCourseDto lmsCourseDto) throws Exception {
        OpalCourseEntityDto opalCourse = (OpalCourseEntityDto) lmsCourseDto;
        String queryUrl = "repo/courses/"+opalCourse.getOpalResource() + "/groups";
        return request(queryUrl);
    }

    public boolean hasAuthorizationForLmsCourse(LmsCourseDto lmsCourseDto, String userMail) throws Exception {
        return isCourseOwner(lmsCourseDto, userMail);
    }

    private boolean isCourseOwner(LmsCourseDto lmsCourseDto, String userMail) throws Exception {
        userMail = convertUserMailToOpalUserMail(userMail);
        OpalCourseEntityDto opalCourse = (OpalCourseEntityDto) lmsCourseDto;
        String queryUrl = "repo/courses/" + opalCourse.getOpalResource() + "/authors";
        String xmlResults = request(queryUrl);
        OpalXmlClasses.UserVOes userVOes = parseXmlToPojo(xmlResults, OpalXmlClasses.UserVOes.class);
        return userVOes.getUsers().stream()
                .map(OpalXmlClasses.UserVO::getEmail)
                .anyMatch(userMail::equals);
    }

    public List<LmsResult> getTestResultIds(ExamDtoForFeedback examDto, String userEmail) throws Exception {
        if (!(examDto.getTestToolDto() instanceof OnyxExamEntityDto))
            throw getServiceExceptionBadRequest("invalid exam type");
        String lmsResourceId = ((OpalCourseEntityDto) examDto.getBaseCourseDto().getLmsCourseDto()).getOpalResource();
        String opalNodeId = ((OnyxExamEntityDto) examDto.getTestToolDto()).getOpalNodeId();
        String queryUrl = "repo/courses/"+lmsResourceId+"/assessments/"+opalNodeId+"/results";
        if (StringsUtilities.hasValue(userEmail))
            queryUrl += "?userId="+ convertUserMailToOpalUserMail(userEmail);
        String xmlResults = request(queryUrl);
        try {
            OpalXmlClasses.ResultsVOes resultsVOes = parseXmlToPojo(xmlResults, OpalXmlClasses.ResultsVOes.class);
            return resultsVOes.getResultsVO().get(0).getResultVO()
                    .stream().map(this::mapResultVOToResult).toList();
        } catch (Exception e) {
            throw getServiceExceptionInternalError("failed to parse result");
        }
    }

    public JsonNode getResultData(File destinationDirectory, ExamDtoForFeedback examDto, String resultId) throws Exception {
        if (!(examDto.getTestToolDto() instanceof OnyxExamEntityDto))
            throw getServiceExceptionBadRequest("invalid exam type");
        downloadResultXmlForResultId(destinationDirectory, examDto, resultId);
        String xml = unzipResult(destinationDirectory, (OnyxExamEntityDto) examDto.getTestToolDto(), resultId);
        return parseResultToJson(xml);
    }

    //******************************************************************************************
    // private methods
    //******************************************************************************************

    private void downloadResultXmlForResultId(File destinationDirectory, ExamDtoForFeedback examDto, String resultId) throws Exception {
        String lmsResourceId = ((OpalCourseEntityDto) examDto.getBaseCourseDto().getLmsCourseDto()).getOpalResource();
        String opalNodeId = ((OnyxExamEntityDto) examDto.getTestToolDto()).getOpalNodeId();
        String queryUrl = "repo/courses/"+lmsResourceId+"/assessments/"+opalNodeId+"/results?resultKey="+resultId;
        String xmlResults = request(queryUrl);
        OpalXmlClasses.ResultsVOes resultsVOes = parseXmlToPojo(xmlResults, OpalXmlClasses.ResultsVOes.class);
        String downloadLink = resultsVOes.getResultsVO().get(0).getData();
        FileUtils.copyURLToFile(new URL(downloadLink), new File(destinationDirectory, "result.zip"));
    }

    private String unzipResult(File destinationDirectory, OnyxExamEntityDto onyxExamDto, String resultId) throws Exception {
        String zipFile = destinationDirectory.getPath() + "/result.zip";
        String zipToPath = destinationDirectory.getPath();
        unzip(zipFile, zipToPath);
        zipFile = zipToPath + "/" + getStringForResultFilename(onyxExamDto, resultId)+".zip";
        unzip(zipFile, zipToPath);
        File onyxResultsFile = new File(zipToPath + "/result.xml");
        if(!onyxResultsFile.exists())
            throw getServiceExceptionInternalError("xml file not found");
        return new String(Files.readAllBytes(onyxResultsFile.toPath()));
    }

    private String getStringForResultFilename(OnyxExamEntityDto onyxExamDto, String resultId) {
        return onyxExamDto.getOpalNodeId()+"v"+resultId;
    }

    private JsonNode parseResultToJson(String xml) throws Exception {
        JsonNode parsedAssessmentResult = XmlParser.parseToJsonNode(xml);
        JsonNode assessmentResult = parsedAssessmentResult.get("assessmentResult");
        if (assessmentResult == null)
            throw new ServiceException("extFB", Response.Status.BAD_REQUEST, "xml file has no assessmentResult tag");
        return assessmentResult;
    }

    private LmsResult mapResultVOToResult(OpalXmlClasses.ResultVO resultVO) {
        return new LmsResult(
            resultVO.getKey(),
            resultVO.getEndDate(),
            resultVO.getState(),
            resultVO.getScored(),
            resultVO.getMaxScore(),
            resultVO.getScore()
        );
    }

    private String requestAuthToken(LmsWorkerEntityDto lmsWorkerEntityDto) throws Exception {
        String authUrl = restApiBaseUrl + "auth/" + lmsWorkerEntityDto.getUsername() + "?password=" + lmsWorkerEntityDto.getPassword();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(new URI(authUrl))
                .headers("Content-Type", "text/plain;charset=" + StandardCharsets.UTF_8)
                .build();
        String authToken;
        HttpResponse<String> response;
        try {
            response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (Exception e) {
            throw getServiceException(Response.Status.BAD_REQUEST, "failed to authenticate");
        }
        if (response.statusCode() != Response.Status.OK.getStatusCode()) {
            throw getServiceException(Response.Status.fromStatusCode(response.statusCode()), "failed to authenticate");
        }
        HttpHeaders responseHeaders = response.headers();
        authToken = responseHeaders.firstValue("X-OLAT-TOKEN").orElse("");
        if (StringsUtilities.hasNoValue(authToken))
            throw getServiceException(Response.Status.FORBIDDEN, "failed to retrieve auth token");
        return authToken;
    }

    private String request(String queryUrl) throws Exception {
        String url = restApiBaseUrl+queryUrl;
        HttpRequest request = HttpRequest.newBuilder()
                .uri(new URI(url))
                .headers("Content-Type", "text/plain;charset="+StandardCharsets.UTF_8)
                .headers("Accept", "application/xml;charset="+StandardCharsets.UTF_8)
                .headers("X-OLAT-TOKEN", authToken)
                .build();
        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        if (response.statusCode() != Response.Status.OK.getStatusCode()) {
            throw getServiceException(Response.Status.fromStatusCode(response.statusCode()), "request failed");
        }
        return response.body();
    }

    private String convertUserMailToOpalUserMail(String userMail) {
        return usePreview ?
                userMail.replace("@", "_at_")+"@catchall.bps-system.de":
                userMail;
    }
}