package i5.las2peer.services.mwbService.database.dto.entity;


import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Entity dto corresponding to database table "courseFAQ"
 */
@Data
@AllArgsConstructor
public class CourseFaqEntityDto {
    private Integer courseId;
    private String intent;
    private String answer;

    /**
     * default constructor
     */
    public CourseFaqEntityDto() {}
}
