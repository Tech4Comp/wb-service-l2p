package i5.las2peer.services.mwbService.database.dao.entity;


import i5.las2peer.services.mwbService.database.dao.DataAccessObject;
import i5.las2peer.services.mwbService.database.dto.entity.PersonMappingEntityDto;
import i5.las2peer.services.mwbService.database.dto.entity.PrivacyStatementEntityDto;
import lombok.Getter;

import java.sql.*;
import java.util.UUID;

/**
 * Entity dao to handle basic database operations for table "personMapping"
 */
@Getter
public class PersonMappingEntityDao extends DataAccessObject {
    private final PersonMappingEntityDto personMappingEntityDto;

    /**
     * Table name for course data
     */
    public static final String TABLE = "personMapping";

    /**
     * constructor
     * @param connection to the database
     * @param personMappingEntityDto -
     */
    public PersonMappingEntityDao(Connection connection, PersonMappingEntityDto personMappingEntityDto) {
        super(connection);
        this.personMappingEntityDto = personMappingEntityDto;
    }

    /**
     * constructor
     * @param connection to the database
     * @param personId -
     */
    public PersonMappingEntityDao(Connection connection, Integer personId) throws Exception {
        super(connection);
        this.personMappingEntityDto = readData(connection, personId);
    }


    //******************************************************************************************
    // basic database operations
    //******************************************************************************************

    /**
     * insert a new record into the database
     *
     * @throws Exception if something went wrong
     */
    public void insert() throws Exception {
        String sqlStatement = "INSERT INTO " + TABLE +
                " (uuid, personId) VALUES(?, ?);";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setObject(1, personMappingEntityDto.getUuid());
            preparedStatement.setInt(2, personMappingEntityDto.getPersonId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw DataAccessObject.createDatabaseException("Failed to insert person mapping.", e);
        }
    }


    //******************************************************************************************
    // static methods for database operations
    //******************************************************************************************

    public static PersonMappingEntityDto readData(Connection connection, Integer personId) throws Exception {
        String sqlStatement = "SELECT * FROM " + TABLE   + " WHERE personId=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, personId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return createPersonMappingEntityDto(resultSet);
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve data for personMapping.", e);
        }
        return null;
    }

    //******************************************************************************************
    // static methods to create course related DTOs from a resultSet
    //******************************************************************************************

    /**
     * create PrivacyStatementEntityDto using data from resultSet
     *
     * @param resultSet containing data
     * @return PrivacyStatementEntityDto
     * @throws Exception -
     */
    public static PersonMappingEntityDto createPersonMappingEntityDto(ResultSet resultSet) throws Exception {
        UUID uuid = (UUID) resultSet.getObject("uuid");
        Integer personId = resultSet.getInt("personId");
        return new PersonMappingEntityDto(uuid, personId);
    }
}
