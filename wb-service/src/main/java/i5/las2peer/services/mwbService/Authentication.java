package i5.las2peer.services.mwbService;

import i5.las2peer.api.Context;
import i5.las2peer.api.security.Agent;
import i5.las2peer.api.security.UserAgent;
import i5.las2peer.services.mwbService.database.dao.*;
import i5.las2peer.services.mwbService.database.dao.entity.CourseEntityDao;
import i5.las2peer.services.mwbService.database.dao.entity.FileBucketEntityDao;
import i5.las2peer.services.mwbService.database.dao.entity.UserTimelineItemEntityDao;
import i5.las2peer.services.mwbService.database.dto.entity.UserTimelineItemEntityDto;
import i5.las2peer.services.mwbService.database.dto.entity.CourseEntityDto;
import i5.las2peer.services.mwbService.database.dto.entity.LmsWorkerEntityDto;
import i5.las2peer.services.mwbService.externalServices.LMS.LmsConnection;
import i5.las2peer.services.mwbService.externalServices.ServiceException;

import javax.ws.rs.core.Response;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.UUID;

import static i5.las2peer.services.mwbService.utilities.StringsUtilities.hasValue;

/**
 * Handle authentication, anonymization and corresponding HTTP responses
 */
public class Authentication {
    /**
     * alias for unknown username, if user is not authenticated
     */
    public static final String anonymous = "Anonymous";

    /**
     * message for anonymous access denied
     */
    public static final String noAnonymous = "Anonymous access denied";

    /**
     * message for access denied due to missing rights
     */
    public static final String missingRights = "Missing rights, access denied.";


    /**
     * @return ServiceException with message noAnonymous
     */
    public static ServiceException noAnonymous() {
        return new ServiceException("authentication", Response.Status.UNAUTHORIZED, Authentication.noAnonymous);
    }
    /**
     * @return ServiceException with message missingRights
     */
    public static ServiceException missingRights() {
        return new ServiceException("authentication", Response.Status.FORBIDDEN, Authentication.missingRights);
    }



    /**
     * ensures UserAgent is authenticated and providing an email address
     * @throws ServiceException noAnonymous if user is not authenticated with an email address
     */
    public static void checkAuthenticated() throws Exception {
        if (!isAuthenticated())
            throw noAnonymous();
    }

    /**
     * ensures UserAgent is authenticated and listed as maintainer
     * @throws Exception noAnonymous/missingRights, if user is not authenticated or is not listed as maintainer
     */
    public static void checkMaintainer() throws Exception {
        checkAuthenticated();
        if (!isMaintainer())
            throw missingRights();
    }

    /**
     * ensures UserAgent is authenticated and listed as owner for the base course
     * @param connection - lazy connection to the postgres DB
     * @param baseCourseId - id of the base course to check authentication
     * @throws Exception noAnonymous/missingRights, if user is not authenticated or not listed as owner for the base course
     */
    public static void checkAuthorizationForBaseCourse(Connection connection, Integer baseCourseId) throws Exception {
        checkAuthenticated();
        RoleDao roleDao = new RoleDao(connection);
        String user = Authentication.getUserAgentEmail();
        boolean hasRights = roleDao.ownsBaseCourse(baseCourseId, user);
        if (!hasRights) {
            throw missingRights();
        }
    }

    /**
     * ensures UserAgent is authenticated and listed as owner for the course
     * @param connection - lazy connection to the postgres DB
     * @param courseId - id of the course to check authentication
     * @throws Exception noAnonymous/missingRights, if user is not authenticated or not listed as owner for the course
     */
    public static void checkAuthorizationForCourse(Connection connection, Integer courseId) throws Exception {
        checkAuthenticated();
        CourseEntityDto courseEntityDto = CourseEntityDao.readData(connection, courseId);
        if (courseEntityDto == null)
            throw new ServiceException("authentication", Response.Status.BAD_REQUEST, "unknown courseId");
        checkAuthorizationForBaseCourse(connection, courseEntityDto.getBaseCourseId());
    }

    /**
     * ensures UserAgent is authenticated and owner of the user timeline item
     * @param connection - lazy connection to the postgres DB
     * @param userTimelineItemId - id of the user timeline item
     * @param uuid of the user
     * @return the user timeline item, if user owns this item
     * @throws Exception noAnonymous/missingRights, if user is not authenticated or not listed as owner for the course
     */
    public static UserTimelineItemEntityDto checkAuthorizationForUserTimelineItem(Connection connection, Integer userTimelineItemId, String uuid) throws Exception {
        checkAuthenticated();
        UserTimelineItemEntityDto userTimelineItemEntityDto = UserTimelineItemEntityDao.readData(connection, userTimelineItemId);
        if (userTimelineItemEntityDto == null)
            throw new ServiceException("authentication", Response.Status.BAD_REQUEST, "unknown user timeline item");
        if (!userTimelineItemEntityDto.getUuid().equals(UUID.fromString(uuid)))
            throw missingRights();
        return userTimelineItemEntityDto;
    }

    /**
     * ensures UserAgent is authenticated and listed as owner for the base course associated to the fileBucketId
     * @throws Exception noAnonymous/missingRights, if user is not authenticated or not listed as owner for the base course
     */
    public static void checkAuthorizationForLms(LmsWorkerEntityDto lmsWorkerEntityDto) throws Exception {
        try {
            LmsConnection.setupLmsConnection(lmsWorkerEntityDto);
        } catch (Exception ignored) {
            throw missingRights();
        }
    }

    /**
     * ensures UserAgent is authenticated and listed as owner for the base course associated to the fileBucketId
     * @param fileBucketId -
     * @param connection - lazy connection to the postgres DB
     * @throws Exception noAnonymous/missingRights, if user is not authenticated or not listed as owner for the base course
     */
    public static void checkAuthorizationForFileBucket(Connection connection, int fileBucketId) throws Exception {
        checkAuthenticated();
        FileBucketEntityDao fileBucketEntityDao = new FileBucketEntityDao(connection, fileBucketId);
        checkAuthorizationForBaseCourse(connection, fileBucketEntityDao.getBaseCourseId());
    }


    /**
     * @return email of current user agent, empty string if not authenticated
     */
    public static String getUserAgentEmail() {
        Agent agent = Context.getCurrent().getMainAgent();
        return (agent instanceof UserAgent) ?
            ((UserAgent) agent).getEmail() :
            "";
    }

    /**
     * @param connection lazy database connection
     * @return uuid for email, anonymous if not authenticated
     * @throws Exception -
     */
    public static String pseudonymizeUser(Connection connection) throws Exception {
        return isAuthenticated() ?
                pseudonymize(connection, getUserAgentEmail()) :
                anonymous;
    }

    /**
     * @param connection lazy database connection
     * @param email to pseudonymize
     * @return uuid for email, anonymous if not authenticated
     * @throws Exception -
     */
    public static String pseudonymize(Connection connection, String email) throws Exception {
        return String.valueOf(PersonDao.getUUID(connection, email));
    }


    //******************************************************************************************
    // private methods
    //******************************************************************************************

    private static Boolean isAuthenticated() {
        return hasValue(getUserAgentEmail());
    }

    // todo: should be moved to mwb-configuration.json, as soon as configuration has been moved to service
    private static ArrayList<String> getMaintainerList() {
        ArrayList<String> maintainerList = new ArrayList<>();
        maintainerList.add("maria.bez@tu-dresden.de");
        return maintainerList;
    }

    private static Boolean isMaintainer() {
        return getMaintainerList().contains(getUserAgentEmail());
    }
}
