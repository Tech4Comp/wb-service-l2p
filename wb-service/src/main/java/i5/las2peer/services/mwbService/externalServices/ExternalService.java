package i5.las2peer.services.mwbService.externalServices;


import lombok.Getter;

import javax.ws.rs.core.Response;

/**
 * provides some basic structure to access an external service
 * including error handling and checking of response code
 */
@Getter
public class ExternalService {
    private final String serviceName;

    /**
     * constructor
     * @param serviceName name of the external service
     */
    public ExternalService(String serviceName) {
        this.serviceName = serviceName;
    }


    //******************************************************************************************
    // instance methods
    //******************************************************************************************

    /**
     * @param status of the HTTP response to pass through to the rest api call
     * @param message for the exception to be thrown
     * @return ServiceException with detailed info about failure
     */
    public ServiceException getServiceException(Response.Status status, String message) {
        return new ServiceException(getServiceName(), status, message);
    }

    /**
     * @param message for the exception to be thrown
     * @return ServiceException with detailed info about failure
     */
    public ServiceException getServiceExceptionBadRequest(String message) {
        return getServiceException(Response.Status.BAD_REQUEST, message);
    }

    /**
     * @param message for the exception to be thrown
     * @return ServiceException with detailed info about failure
     */
    public ServiceException getServiceExceptionInternalError(String message) {
        return getServiceException(Response.Status.INTERNAL_SERVER_ERROR,  message);
    }
}
