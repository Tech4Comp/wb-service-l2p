package i5.las2peer.services.mwbService.database.dto.entity;


import i5.las2peer.services.mwbService.database.dto.LmsCourseDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Entity dto corresponding to database table opalCourse
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class OpalCourseEntityDto extends LmsCourseDto {
    private String opalResource;

    /**
     * default constructor
     */
    public OpalCourseEntityDto() {}

    /**
     * constructor
     *
     * @param lmsCourseDto basic data as LmsCourseDto
     * @param resource -
     */
    public OpalCourseEntityDto(LmsCourseDto lmsCourseDto, String resource) {
        super(lmsCourseDto);
        this.opalResource = resource;
    }
}
