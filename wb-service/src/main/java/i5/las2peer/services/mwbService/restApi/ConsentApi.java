package i5.las2peer.services.mwbService.restApi;

import i5.las2peer.services.mwbService.Authentication;
import i5.las2peer.services.mwbService.MwbService;
import i5.las2peer.services.mwbService.database.LazyPostgresConnection;
import i5.las2peer.services.mwbService.database.dao.ConsentDao;
import i5.las2peer.services.mwbService.database.dao.PersonDao;
import i5.las2peer.services.mwbService.database.dao.entity.ConsentEntityDao;
import i5.las2peer.services.mwbService.database.dao.entity.PrivacyStatementEntityDao;
import i5.las2peer.services.mwbService.database.dto.entity.PrivacyStatementEntityDto;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.UUID;

/**
 *  REST API for consent management
 */
@Path("/consent")
public class ConsentApi {

    /**
     * Check if the current user has a consent for the specified courseId and purpose
     *
     * @param courseId -
     * @param purpose - optional purpose
     * @return 200 "Yes" if the consent exists
     *         200 "No" if no consent was given
     *         200 "Outdated" if consent was given, but is outdated
     *         500 Internal Server Error if the database connection is not established
     */
    @GET
    @Path("/course/{courseId}/{purpose}")
    @RolesAllowed("authenticated")
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(value = "Check the consent of the current user for the specified courseId and purpose", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Yes"),
            @ApiResponse(code = 200, message = "No"),
            @ApiResponse(code = 200, message = "Outdated"),
            @ApiResponse(code = 500, message = "Internal Server Error") })
    public Response getConsent(
            @ApiParam(value = "The íd of the course", required = true) @PathParam(value = "courseId") int courseId,
            @ApiParam(value = "The purpose", required = true) @PathParam(value = "purpose") String purpose
    ) {
        return getConsentForPurposeInCourse(courseId, purpose);
    }

    /**
     * Check if the current user has a consent for the specified courseId
     *
     * @param courseId -
     * @return 200 "ConsentOk" if the consent exists
     *         200 "NoConsent" if no consent was given
     *         200 "ConsentOutdated" if consent was given, but is outdated
     *         400 Bad Request if something goes wrong
     *         401 Unauthorized or missing rights
     *         500 Internal Server Error if the database connection is not established
     */
    @GET
    @Path("/course/{courseId}")
    @RolesAllowed("authenticated")
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(value = "Check the consent of the current user for the specified courseId", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Yes"),
            @ApiResponse(code = 200, message = "No"),
            @ApiResponse(code = 200, message = "Outdated"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 500, message = "Internal Server Error") })
    public Response getConsent(
            @ApiParam(value = "The íd of the course", required = true) @PathParam(value = "courseId") int courseId
    ) {
        return getConsentForPurposeInCourse(courseId, null);
    }

    private Response getConsentForPurposeInCourse(int courseId, String purpose) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthenticated();
            String email = Authentication.getUserAgentEmail();
            ConsentDao consentDao = new ConsentDao(connection.getConnection());
            String checkConsent = consentDao.checkConsent(email, courseId, purpose);
            return MwbService.responseOK(checkConsent);
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }


    /**
     * Create consent for the current user for the specified courseId and purpose
     *
     * @param courseId -
     * @param purpose -
     * @return 200 "OK" if consent is created successfully
     *         500 Internal Server Error if there is an error with database access
     *         400 Bad Request if something goes wrong
     *         401 Unauthorized or missing rights
     *         500 Internal Server Error if the database connection is not established
     */
    @POST
    @Path("/course/{courseId}/{purpose}")
    @RolesAllowed("authenticated")
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(value = "Create consent for the current user for the specified courseId and purpose", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 500, message = "Internal Server Error") })
    public Response createConsent(
            @ApiParam(value = "The íd of the course", required = true) @PathParam(value = "courseId") int courseId,
            @ApiParam(value = "The purpose", required = true) @PathParam(value = "purpose") String purpose
    ) {
        return giveConsentForPurposeInCourse(courseId, purpose);
    }

    /**
     * Create consent for the current user for the specified courseId
     *
     * @param courseId -
     * @return 200 "OK" if consent is created successfully
     *         400 Bad Request if something goes wrong
     *         401 Unauthorized or missing rights
     *         500 Internal Server Error if there is an error with database access
     */
    @POST
    @Path("/course/{courseId}")
    @RolesAllowed("authenticated")
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(value = "Create consent for the current user for the specified courseId", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 500, message = "Internal Server Error") })
    public Response giveConsent(
            @ApiParam(value = "The íd of the course", required = true) @PathParam(value = "courseId") int courseId
    ) {
        return giveConsentForPurposeInCourse(courseId, null);
    }

    private Response giveConsentForPurposeInCourse(int courseId, String purpose) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthenticated();
            String email = Authentication.getUserAgentEmail();
            ConsentDao consentDao = new ConsentDao(connection.getConnection());
            consentDao.upsertConsent(email, courseId, purpose);
            return MwbService.responseOK();
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }


    /**
     * Delete consent for the current user for the specified courseId and purpose
     *
     * @param courseId -
     * @param purpose -
     * @return 200 "OK" if consent is deleted successfully
     *         400 Bad Request if something goes wrong
     *         401 Unauthorized or missing rights
     *         500 Internal Server Error if there is an error with database access
     */
    @DELETE
    @Path("/course/{courseId}/{purpose}")
    @RolesAllowed("authenticated")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Delete consent for the current user for the specified courseId and purpose", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 500, message = "Internal Server Error") })
    public Response revokeConsent(
            @ApiParam(value = "The íd of the course", required = true) @PathParam(value = "courseId") int courseId,
            @ApiParam(value = "The purpose", required = true) @PathParam(value = "purpose") String purpose
    ) {
        return deleteConsentForPurposeInCourse(courseId, purpose);
    }

    /**
     * Delete consent for the current user for the specified courseId
     *
     * @param courseId -
     * @return 200 "OK" if consent is deleted successfully
     *         400 Bad Request if something goes wrong
     *         401 Unauthorized or missing rights
     *         500 Internal Server Error if there is an error with database access
     */
    @DELETE
    @Path("/course/{courseId}")
    @RolesAllowed("authenticated")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Delete consent for the current user for the specified courseId", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 500, message = "Internal Server Error") })
    public Response revokeConsent(
            @ApiParam(value = "The íd of the course", required = true) @PathParam(value = "courseId") int courseId
    ) {
        return deleteConsentForPurposeInCourse(courseId, null);
    }

    private Response deleteConsentForPurposeInCourse(int courseId, String purpose) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthenticated();
            String email = Authentication.getUserAgentEmail();
            UUID uuid = PersonDao.getUUID(connection.getConnection(), email);
            if (uuid != null) {
                PrivacyStatementEntityDao privacyStatementEntityDao = new PrivacyStatementEntityDao(connection.getConnection(), courseId, purpose);
                PrivacyStatementEntityDto privacyStatementEntityDto = privacyStatementEntityDao.getPrivacyStatementEntityDto();
                if (privacyStatementEntityDto != null)
                    ConsentEntityDao.delete(connection.getConnection(), uuid, privacyStatementEntityDto.getId());
            }
            return MwbService.responseOK();
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }
}
