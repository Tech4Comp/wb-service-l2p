package i5.las2peer.services.mwbService.configuration;

import i5.las2peer.services.mwbService.MwbService;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;
import net.minidev.json.parser.ParseException;

import javax.annotation.Nullable;
import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 * basic support for reading configuration in JSON files
 */
public abstract class JsonConfiguration {
    /**
     * parsed content of the configuration
     */
    protected final JSONObject configuration;

    /**
     * @param jsonFileName containing the configuration
     * @throws Exception - exception with detailed info about failure
     */
    public JsonConfiguration(String jsonFileName) throws Exception {
        try (final FileReader fileReader = new FileReader(MwbService.RESOURCE_DIR + jsonFileName)) {
            configuration = (JSONObject) new JSONParser(JSONParser.MODE_PERMISSIVE).parse(fileReader);
        } catch (FileNotFoundException exception) {
            throw new Exception("missing configuration file "+ jsonFileName);
        } catch (ParseException exception) {
            throw new Exception("error parsing configuration file "+ jsonFileName);
        } catch (Exception exception) {
            throw new Exception("error in handling configuration file "+ jsonFileName+": "+exception.getMessage());
        }
    }

    /**
     * @param key for the data to be retrieved
     * @return value associated with the key as JSONObject
     */
    @Nullable
    public JSONObject getDataForKey(String key) {
        return configuration == null ? null : (JSONObject) (configuration.get(key));
    }


    /**
     * @param key for the data to be retrieved
     * @return value associated with the key as String
     */
    public String getValue(String key) {
        return this.configuration.getAsString(key);
    }

}
