package i5.las2peer.services.mwbService.database.dto.entity;


import lombok.AllArgsConstructor;
import lombok.Data;


/**
 * Entity dto corresponding to database table "courseHint"
 */
@Data
@AllArgsConstructor
public class CourseHintEntityDto {
    private Integer id;
    private Integer courseId;
    private Integer sequence;
    private String text;

    /**
     * default constructor
     */
    public CourseHintEntityDto() {}
}
