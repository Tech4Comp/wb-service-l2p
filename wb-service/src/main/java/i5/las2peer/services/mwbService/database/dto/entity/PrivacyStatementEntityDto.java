package i5.las2peer.services.mwbService.database.dto.entity;


import i5.las2peer.services.mwbService.database.dto.CourseDto;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.sql.Timestamp;

/**
 * Entity dto corresponding to database table "privacyStatement"
 */
@Data
@AllArgsConstructor
public class PrivacyStatementEntityDto {
    private Integer id;
    private Integer courseId;
    private String purpose;
    private Integer fileBucketId;
    private Timestamp lastModified;

    /**
     * default constructor
     */
    public PrivacyStatementEntityDto() {}

    /**
     * copy constructor (used to initialise derived classes)
     *
     * @param copy as CourseDto
     */
    public PrivacyStatementEntityDto(PrivacyStatementEntityDto copy) {
        this.id = copy.getId();
        this.courseId = copy.getCourseId();
        this.purpose = copy.getPurpose();
        this.fileBucketId = copy.getFileBucketId();
        this.lastModified = copy.getLastModified();
    }
}
