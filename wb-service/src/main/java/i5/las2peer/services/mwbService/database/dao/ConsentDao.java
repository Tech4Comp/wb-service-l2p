package i5.las2peer.services.mwbService.database.dao;

import java.sql.*;
import java.util.UUID;

import i5.las2peer.services.mwbService.database.dao.entity.ConsentEntityDao;
import i5.las2peer.services.mwbService.database.dao.entity.PersonEntityDao;
import i5.las2peer.services.mwbService.database.dao.entity.PersonMappingEntityDao;
import i5.las2peer.services.mwbService.database.dao.entity.PrivacyStatementEntityDao;
import i5.las2peer.services.mwbService.database.dto.entity.ConsentEntityDto;


/**
 * DAO to handle complex database requests related to consent management
 */
public class ConsentDao extends DataAccessObject {

	/**
	 * Response, if consent is valid
	 */
	public static final String CONSENT_OK = "ConsentOk";

	/**
	 * Response, if no consent is given
	 */
	public static final String NO_CONSENT = "NoConsent";

	/**
	 * Response, if consent is outdated
	 */
	public static final String CONSENT_OUTDATED = "ConsentOutdated";

	/**
	 * constructor
	 * @param connection to the database
	 */
	public ConsentDao(Connection connection) {
		super(connection);
	}

	/**
	 * check for consent given
	 * @param email of the user to check the consent
	 * @param courseId of the course
	 * @param purpose to check the consent for, empty, if consent is for course
	 * @return  status of consent as String
	 * @throws Exception if something went wrong
	 */
	public String checkConsent(String email, int courseId, String purpose) throws Exception {
		purpose = PrivacyStatementEntityDao.checkForDefaultPurpose(purpose);
		String sqlStatement = "SELECT " + PersonEntityDao.TABLE + ".email, courseId, purpose, " + PrivacyStatementEntityDao.TABLE + ".lastModified AS privacyStatement_lastModified, " + ConsentEntityDao.TABLE + ".timestamp AS consent_timestamp" +
				" FROM " + ConsentEntityDao.TABLE +
				" JOIN " + PersonMappingEntityDao.TABLE + " ON " + ConsentEntityDao.TABLE + ".uuid = " + PersonMappingEntityDao.TABLE + ".uuid" +
				" JOIN " + PersonEntityDao.TABLE + " ON " + PersonEntityDao.TABLE + ".id = " + PersonMappingEntityDao.TABLE + ".personId" +
				" JOIN " + PrivacyStatementEntityDao.TABLE + " ON " + PrivacyStatementEntityDao.TABLE + ".id = privacyStatementId" +
				" WHERE " + PersonEntityDao.TABLE + ".email=? AND courseId=? AND purpose=?;";
		try (PreparedStatement preparedStatement = this.connection.prepareStatement(sqlStatement)) {
			preparedStatement.setString(1, email);
			preparedStatement.setInt(2, courseId);
			preparedStatement.setString(3, purpose);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				Timestamp privacyStatement_lastModified = resultSet.getTimestamp("privacyStatement_lastModified");
				Timestamp consent_timestamp = resultSet.getTimestamp("consent_timestamp");
				return consent_timestamp.after(privacyStatement_lastModified) ? CONSENT_OK : CONSENT_OUTDATED;
			}
		} catch (Exception e) {
			throw DataAccessObject.createDatabaseException("Failed to retrieve consent from database.", e);
		}
		return NO_CONSENT;
	}


	/**
	 * create/update consent in database
	 * @param email of the user
	 * @param courseId of the course the consent belongs to
	 * @param purpose of the purpose (empty if consent is for base course)
	 * @throws Exception if something went wrong
	 */
	public void upsertConsent(String email, int courseId, String purpose) throws Exception {
		PrivacyStatementEntityDao privacyStatementEntityDao = new PrivacyStatementEntityDao(connection, courseId, purpose);
		Integer privacyStatementId = privacyStatementEntityDao.getId();
		UUID uuid = PersonDao.getOrCreateUUID(connection, email);
		ConsentEntityDto consentEntityDto = new ConsentEntityDto(uuid, privacyStatementId, null);
		ConsentEntityDao consentEntityDao = new ConsentEntityDao(connection, consentEntityDto);
		consentEntityDao.upsert();
	}
}
