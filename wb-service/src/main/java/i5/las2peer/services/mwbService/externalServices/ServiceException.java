package i5.las2peer.services.mwbService.externalServices;

import lombok.Getter;

import javax.ws.rs.core.Response.Status;

/**
 * provides additional service name and response status
 */
@Getter
public class ServiceException extends Exception {
    private final String serviceName;
    private final String baseMessage;
    private final Status responseStatus;

    /**
     * @param serviceName - name of the service that raised the exception
     * @param responseStatus - to pass through to the rest api endpoint
     * @param message - base message associated with the exception
     */
    public ServiceException(String serviceName, Status responseStatus, String message) {
        this.serviceName = serviceName;
        this.baseMessage = message;
        this.responseStatus = responseStatus;
    }

    /**
     * @return the message associated with the exception with serviceName added
     */
    @Override
    public String getMessage() {
        return serviceName+": "+getBaseMessage();
    }

    public String getDetailedMessage() {
        return getResponseStatus().getStatusCode()+" - " +
               getResponseStatus().getReasonPhrase()+": "+
               getMessage();
    }
}
