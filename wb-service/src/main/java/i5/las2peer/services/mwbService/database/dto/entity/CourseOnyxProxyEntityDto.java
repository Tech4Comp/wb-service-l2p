package i5.las2peer.services.mwbService.database.dto.entity;


import i5.las2peer.services.mwbService.database.dto.CourseComponentDataDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.sql.Date;

/**
 * Entity dto corresponding to database table 'courseOnyxProxy' for course component OPALONYXPROXY
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class CourseOnyxProxyEntityDto extends CourseComponentDataDto {
    private Date validFrom;
    private Date validUntil;

    /**
     * default constructor
     */
    public CourseOnyxProxyEntityDto() {}

    /**
     * constructor
     *
     * @param courseComponentDataDto basic data as ComponentDataDto
     * @param validFrom -
     * @param validUntil -
     */
    public CourseOnyxProxyEntityDto(CourseComponentDataDto courseComponentDataDto, Date validFrom, Date validUntil) {
        super(courseComponentDataDto);
        this.validFrom = validFrom;
        this.validUntil = validUntil;
    }
}
