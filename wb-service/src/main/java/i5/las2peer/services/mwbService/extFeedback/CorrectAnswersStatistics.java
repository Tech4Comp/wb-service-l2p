package i5.las2peer.services.mwbService.extFeedback;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Class to handle statistics for correct answers
 */
@Data
@AllArgsConstructor
public class CorrectAnswersStatistics {
    private Integer cntAnswers;
    private Double maximumScore;
    private Double correct;

    /**
     * default constructor
     */
    public CorrectAnswersStatistics() {
        cntAnswers = 0;
        maximumScore = 0.0;
        correct = 0.0;
    }

    /**
     * constructor for initialising the stats with the first answer
     * @param value -
     * @param max -
     */
    public CorrectAnswersStatistics(Double value, Double max) {
        cntAnswers = 1;
        correct = value;
        maximumScore = max;
    }

    /**
     * calculates the current value of correct answers as percentage
     * @return value of correct answers as percentage
     */
    public Double getCorrectPercentage() {
        if (maximumScore == 0) {
            return 0.0;
        } else {
            return (correct/maximumScore) * 100;
        }
    }

    /**
     * adds the properties of a new answer to stats
     * @param scoreCorrect - score of the answer
     * @param maxScore - maximum score possible
     */
    public void increase(Double scoreCorrect, Double maxScore) {
        cntAnswers++;
        correct += scoreCorrect;
        maximumScore += maxScore;
    }

    @Override
    public String toString() {
        return """
                {\
                cntAnswers: %s,\
                correct: %s,\
                maximumScore: %s\
                }\
                """.formatted(cntAnswers, correct, maximumScore);
    }
}
