package i5.las2peer.services.mwbService.database.dao.entity;


import i5.las2peer.services.mwbService.database.dao.DataAccessObject;
import i5.las2peer.services.mwbService.database.dto.CourseComponentDataDto;
import i5.las2peer.services.mwbService.database.dto.entity.CourseTimelineEntityDto;
import lombok.Getter;

import java.sql.*;

/**
 * Dao to handle basic database operations for table 'courseTimeline' for course component TIMELINE
 */
@Getter
public class CourseTimelineEntityDao extends CourseComponentDataDao {
    private final CourseTimelineEntityDto courseTimelineEntityDto;

    /**
     * Table name for entity
     */
    public static final String TABLE = "courseTimeline";

    /**
     * constructor
     *
     * @param connection to the database
     * @param courseTimelineEntityDto -
     */
    public CourseTimelineEntityDao(Connection connection, CourseTimelineEntityDto courseTimelineEntityDto) {
        super(connection);
        this.courseTimelineEntityDto = courseTimelineEntityDto;
    }

    /**
     * constructor
     *
     * @param connection to the database
     * @param courseId -
     * @throws Exception if requested connection could not be established
     */
    public CourseTimelineEntityDao(Connection connection, Integer courseId) throws Exception {
        super(connection);
        this.courseTimelineEntityDto = readData(connection, courseId) ;
    }


    //******************************************************************************************
    // basic database operations
    //******************************************************************************************

    /**
     * insert a new record / update existing record to the database
     *
     * @throws Exception if something went wrong
     */
    public void upsert() throws Exception {
        String sqlStatement = "INSERT INTO " + TABLE + " (courseId, startsOn, endsOn) VALUES(?, ?, ?)"+
                " ON CONFLICT ON CONSTRAINT " + TABLE + "_pKey DO UPDATE" +
                " SET startsOn=?, endsOn=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, courseTimelineEntityDto.getCourseId());
            setOptionalDateForResultSet(preparedStatement, 2, courseTimelineEntityDto.getStartsOn());
            setOptionalDateForResultSet(preparedStatement, 3, courseTimelineEntityDto.getEndsOn());
            setOptionalDateForResultSet(preparedStatement, 4, courseTimelineEntityDto.getStartsOn());
            setOptionalDateForResultSet(preparedStatement, 5, courseTimelineEntityDto.getEndsOn());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw DataAccessObject.createDatabaseException("Failed to insert/update data for course component TIMELINE.", e);
        }
    }


    //******************************************************************************************
    // static methods for database operations
    //******************************************************************************************

    public static CourseTimelineEntityDto readData(Connection connection, Integer courseId) throws Exception {
        String sqlStatement = "SELECT * FROM " + TABLE   + " WHERE courseId=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, courseId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return createDto(resultSet);
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve data for component TIMELINE for course " + courseId + " from database.", e);
        }
        return null;
    }

    /**
     * delete record with given id
     *
     * @param connection to database
     * @param courseId of the record to delete
     * @throws Exception -
     */
    public static void delete(Connection connection, Integer courseId) throws Exception {
        CourseComponentDataDao.delete(connection,TABLE, courseId);
    }


    //******************************************************************************************
    // static method to create DTO from a resultSet
    //******************************************************************************************

    /**
     * create CourseTimelineEntityDto using data from resultSet
     *
     * @param resultSet containing data
     * @return CourseTimelineEntityDto
     * @throws Exception -
     */
    public static CourseTimelineEntityDto createDto(ResultSet resultSet) throws Exception {
        CourseComponentDataDto courseComponentDataDto = CourseComponentDataDao.createFromResultSet(resultSet);
        Date startsOn = resultSet.getDate("startsOn");
        Date endsOn = resultSet.getDate("endsOn");
        return new CourseTimelineEntityDto(courseComponentDataDto, startsOn, endsOn);
    }
}
