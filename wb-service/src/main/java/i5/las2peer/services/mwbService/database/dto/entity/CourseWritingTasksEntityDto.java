package i5.las2peer.services.mwbService.database.dto.entity;


import i5.las2peer.services.mwbService.database.dto.CourseComponentDataDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.sql.Timestamp;

/**
 * Entity dto corresponding to database table for course component WRITINGTASKS
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class CourseWritingTasksEntityDto extends CourseComponentDataDto {
    private Timestamp validFrom;
    private Timestamp validUntil;
    private Integer timelineRowId;
    private Boolean movable;

    /**
     * default constructor
     */
    public CourseWritingTasksEntityDto() {}

    /**
     * constructor
     *
     * @param courseComponentDataDto basic data as ComponentDataDto
     * @param validFrom -
     * @param validUntil -
     * @param timelineRowId -
     * @param movable -
     */
    public CourseWritingTasksEntityDto(CourseComponentDataDto courseComponentDataDto,
                                       Timestamp validFrom, Timestamp validUntil, Integer timelineRowId, Boolean movable) {
        super(courseComponentDataDto);
        this.validFrom = validFrom;
        this.validUntil = validUntil;
        this.timelineRowId = timelineRowId;
        this.movable = movable;
    }

    /**
     * copy constructor (used to initialise derived classes)
     *
     * @param copy - CourseEntity with data
     */
    public CourseWritingTasksEntityDto(CourseWritingTasksEntityDto copy) {
        super(copy);
        this.validFrom = copy.getValidFrom();
        this.validUntil = copy.getValidUntil();
        this.timelineRowId = copy.getTimelineRowId();
        this.movable = copy.getMovable();
    }
}
