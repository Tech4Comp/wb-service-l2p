package i5.las2peer.services.mwbService.database.dao.entity;


import i5.las2peer.services.mwbService.database.dao.DataAccessObject;
import i5.las2peer.services.mwbService.database.dto.entity.ConsentEntityDto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;

/**
 * Entity dao to handle basic database operations for table "consent"
 */
public class ConsentEntityDao extends DataAccessObject {
    private final ConsentEntityDto consentEntityDto;

    /**
     * Table name for data
     */
    public static final String TABLE = "consent";

    /**
     * constructor
     * @param connection to the database
     * @param consentEntityDto -
     */
    public ConsentEntityDao(Connection connection, ConsentEntityDto consentEntityDto) {
        super(connection);
        this.consentEntityDto = consentEntityDto;
    }


    //******************************************************************************************
    // basic database operations
    //******************************************************************************************

    /**
     * insert a new record / update existing record to the database
     *
     * @throws Exception if something went wrong
     */
    public void upsert() throws Exception {
        String sqlStatement = "INSERT INTO " + TABLE + " (uuid, privacyStatementId, timestamp) VALUES(?, ?, ?)"+
                " ON CONFLICT ON CONSTRAINT " + TABLE + "_pKey DO UPDATE" +
                " SET timestamp=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            Timestamp now = Timestamp.from(Instant.now());
            preparedStatement.setObject(1, consentEntityDto.getUuid());
            preparedStatement.setInt(2, consentEntityDto.getPrivacyStatementId());
            preparedStatement.setTimestamp(3, now);
            preparedStatement.setTimestamp(4, now);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw DataAccessObject.createDatabaseException("Failed to insert/update consent.", e);
        }
    }


    //******************************************************************************************
    // static methods for database operations
    //******************************************************************************************

    /**
     * delete record with given id
     *
     * @param connection to database
     * @param uuid of the record to delete
     * @param privacyStatementId of the record to delete
     * @throws Exception -
     */
    public static void delete(Connection connection, UUID uuid, Integer privacyStatementId) throws Exception {
        String sqlStatement = "DELETE FROM " + TABLE +
                " WHERE uuid=? AND privacyStatementId=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setObject(1, uuid);
            preparedStatement.setInt(2, privacyStatementId);
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to delete consent.", e);
        }
    }
}
