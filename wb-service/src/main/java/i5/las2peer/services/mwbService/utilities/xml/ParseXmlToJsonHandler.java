package i5.las2peer.services.mwbService.utilities.xml;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.util.Stack;

/**
 * Parses XML and converts it into a JSONObject
 * attributes are converted to JSON properties
 * tag values are converted to a JSONObject with key "value" and the data as value
 * if a xml tag is not unique for the current parent, the key will be replaced with key+"s" and the value
 * will be a JSONArray containing all entries with that key
 */
 /* Example:
 <parent>
     <simpleChild>childValue</simpleChild>
     <item>item1</item>
     <item>item2</item>
      <child>
         <property1 id="prop1">value1</property1>
         <property2 id="prop2">value2</property2>
     </child>
 </parent>
 will be converted to
 {
  "parent": {
    "simpleChild": {
      "value": "childValue"
    },
    "items": [
      {
        "value": "item1"
      },
      {
        "value": "item2"
      }
    ],
    "child": {
      "property2": {
        "id": "prop2",
        "value": "value2"
      },
      "property1": {
        "id": "prop1",
        "value": "value1"
      }
    }
  }
}
*/
public class ParseXmlToJsonHandler extends DefaultHandler {
    private final JSONObject jsonResult;
    private final Stack<JSONObject> parents;
    private StringBuilder data;

    /**
     *
     */
    public ParseXmlToJsonHandler() {
        super();
        parents = new Stack<>();
        jsonResult = new JSONObject();
        parents.push(jsonResult);
    }

    /**
     * @return -
     */
    public JsonNode getResultsAsJsonNode() throws JsonProcessingException {
        return new ObjectMapper().readTree( jsonResult.toJSONString());
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        JSONObject currentElement = new JSONObject();
        addAttributesToJson(currentElement, attributes);
        addJsonObjectToParentJson(qName, currentElement);
        parents.push(currentElement);
        data = new StringBuilder();
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        String value = data.toString().strip();
        if (!value.isEmpty()) {
            JSONObject parent = parents.peek();
            if (parent.isEmpty()) {
                parent.put("value", value);
            } else {
                addJsonObjectToParentJson("value", value);
            }
        }
        data = new StringBuilder();
        parents.pop();
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        data.append(new String(ch, start, length));
    }

    private void addAttributesToJson(JSONObject jsonObject, Attributes attributes) {
        for (int i = 0; i < attributes.getLength(); i++) {
            jsonObject.put(attributes.getQName(i), attributes.getValue(i));
        }
    }

    private void addJsonObjectToParentJson(String key, Object current) {
        JSONObject parent = parents.peek();

        String multipleEntryKey = key+"s";
        JSONObject valueForKey = (JSONObject) parent.get(key);
        JSONArray multipleValuesForKey = (JSONArray) parent.get(multipleEntryKey);
        if (multipleValuesForKey != null) { // entry is already JSONArray
            multipleValuesForKey.add(current);
        } else if (valueForKey != null) { // has single value -> convert to JSONArray
            JSONArray jsonArray = new JSONArray();
            jsonArray.add(valueForKey);
            jsonArray.add(current);
            parent.remove(key);
            parent.put(multipleEntryKey, jsonArray);
        } else { // new entry
            parent.put(key, current);
        }
    }
}
