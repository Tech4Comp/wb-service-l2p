package i5.las2peer.services.mwbService.externalServices;


import javax.ws.rs.core.Response;
import java.net.http.HttpResponse;

/**
 * extension of ServiceException to show the error code of an HTTP response
 */
public class ServiceHttpResponseException extends ServiceException {
    /**
     * HTTP response code
     */
    private final Integer errorCode;

    /**
     * @param serviceName name of the service that raised the exception
     * @param errorCode - HTTP response code to display
     * @param message error message to display
     */
    public ServiceHttpResponseException(String serviceName, int errorCode, String message) {
        super(serviceName, Response.Status.BAD_REQUEST, message);
        this.errorCode = errorCode;
    }

    /**
     * @return detailed description of the error
     */
    @Override
    public String getMessage() {
        return "External service "+getServiceName()+", code "+ errorCode +": "+getBaseMessage();
    }


    /**
     * @param serviceName - name of the service which triggered the request
     * @param requiredResponseCode -
     * @param responseCode to assert
     * @param messageIfAssertFails - exception message if assertion fails
     * @throws ServiceHttpResponseException if assertion fails
     */
    public static void assertResponseCode(String serviceName, Response.Status requiredResponseCode, int responseCode, String messageIfAssertFails) throws ServiceHttpResponseException {
        if (responseCode != requiredResponseCode.getStatusCode())
            throw new ServiceHttpResponseException(serviceName, responseCode, messageIfAssertFails);
    }

    /**
     * @param serviceName - name of the service which triggered the request
     * @param responseCode to assert as OK
     * @param messageIfAssertFails - exception message if assertion fails
     * @throws ServiceHttpResponseException if assertion fails
     */
    public static void assertResponseCodeOk(String serviceName, int responseCode, String messageIfAssertFails) throws ServiceHttpResponseException {
        assertResponseCode(serviceName, Response.Status.OK, responseCode, messageIfAssertFails);
    }

    /**
     * @param serviceName - name of the service which triggered the request
     * @param response - HTTPResponse of a request
     * @throws ServiceHttpResponseException if assertion fails
     */
    public static void assertResponseCodeOk(String serviceName, HttpResponse<String> response) throws ServiceHttpResponseException {
        assertResponseCodeOk(serviceName, response.statusCode(), response.body());
    }
}
