package i5.las2peer.services.mwbService.database.dto.entity;


import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Entity dto corresponding to database table "courseLayout"
 */
@Data
@AllArgsConstructor
public class CourseLayoutEntityDto {
    private Integer courseId;
    private String size;
    private String json;

    /**
     * default constructor
     */
    public CourseLayoutEntityDto() {}
}
