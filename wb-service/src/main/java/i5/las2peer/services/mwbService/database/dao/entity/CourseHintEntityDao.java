package i5.las2peer.services.mwbService.database.dao.entity;

import i5.las2peer.services.mwbService.database.dao.DataAccessObject;
import i5.las2peer.services.mwbService.database.dto.entity.CourseHintEntityDto;
import lombok.Getter;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Dao to handle basic database operations for table with data for course component MWBHINTS "courseHint"
 */
@Getter
public class CourseHintEntityDao extends CourseComponentDataDao {
    private final CourseHintEntityDto courseHintEntityDto;

    /**
     * Table name for entity
     */
    public static final String TABLE = "courseHint";

    /**
     * field list for copy course data
     */
    public static final List<String> copyFieldList = Arrays.asList("sequence", "text");

    /**
     * constructor
     *
     * @param connection to the database
     * @param courseHintEntityDto -
     */
    public CourseHintEntityDao(Connection connection, CourseHintEntityDto courseHintEntityDto) {
        super(connection);
        this.courseHintEntityDto = courseHintEntityDto;
    }

    /**
     * constructor
     *
     * @param connection to the database
     * @param id of the hint to retrieve
     * @throws Exception if requested connection could not be established
     */
    public CourseHintEntityDao(Connection connection, Integer id) throws Exception {
        super(connection);
        this.courseHintEntityDto = readData(connection, id) ;
    }


    //******************************************************************************************
    // instance methods
    //******************************************************************************************

    public void assertNotNull() throws Exception {
        if (this.courseHintEntityDto == null)
            throw getInvalidDataException("No data provided.");
    }


    //******************************************************************************************
    // basic database operations
    //******************************************************************************************

    /**
     * insert a new record into the database
     *
     * @return id of the new record
     * @throws Exception if something went wrong
     */
    public int insert() throws Exception {
        assertNotNull();
        String sqlStatement = "INSERT INTO " + TABLE + " (courseId, sequence, text) VALUES(?, ?, ?)" +
                " RETURNING id;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, courseHintEntityDto.getCourseId());
            preparedStatement.setInt(2, courseHintEntityDto.getSequence());
            preparedStatement.setString(3, courseHintEntityDto.getText());
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return resultSet.getInt("id");
        } catch (SQLException e) {
            throw DataAccessObject.createDatabaseException("Failed to insert new hint for course component MWBHINTS.", e);
        }
        throw DataAccessObject.createDatabaseException("Failed to retrieve id of inserted hint.", null);
    }

    /**
     * update record
     *
     * @throws Exception if something went wrong
     */
    public void update() throws Exception {
        assertNotNull();
        String sqlStatement = "UPDATE " + TABLE + " SET sequence=?, text=?" +
                " WHERE id=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, courseHintEntityDto.getSequence());
            preparedStatement.setString(2, courseHintEntityDto.getText());
            preparedStatement.setInt(3, courseHintEntityDto.getId());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to update hint for course component MWBHINTS.", e);
        }
    }

    public void upsert() throws Exception {
        assertNotNull();
        if (courseHintEntityDto.getId() == null)
            insert();
        else
            update();
    }

    //******************************************************************************************
    // static methods for database operations
    //******************************************************************************************

    public static CourseHintEntityDto readData(Connection connection, Integer id) throws Exception {
        String sqlStatement = "SELECT * FROM " + TABLE   + " WHERE id=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return createDto(resultSet);
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve data for hint.", e);
        }
        return null;
    }

    public static List<CourseHintEntityDto> readListData(Connection connection, Integer courseId) throws Exception {
        String sqlStatement = "SELECT * FROM " + TABLE + " WHERE courseId=? ORDER BY sequence;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, courseId);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<CourseHintEntityDto> courseHintEntityDtoList = new ArrayList<>();
            while (resultSet.next()) {
                CourseHintEntityDto courseHintEntityDto = createDto(resultSet);
                courseHintEntityDtoList.add(courseHintEntityDto);
            }
            return courseHintEntityDtoList;
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve list of hints for component MWBHINTS for course.", e);
        }
    }

    /**
     * delete record with given id
     *
     * @param connection to database
     * @param id of the record to delete
     * @throws Exception -
     */
    public static void delete(Connection connection, Integer id) throws Exception {
        String sqlStatement = "DELETE FROM " + TABLE +
                " WHERE id=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to delete hint for component MWBHINTS.", e);
        }
    }


    //******************************************************************************************
    // static method to create DTO from a resultSet
    //******************************************************************************************

    /**
     * create CourseHintEntityDto using data from resultSet
     *
     * @param resultSet containing data
     * @return CourseHintEntityDto
     * @throws Exception -
     */
    public static CourseHintEntityDto createDto(ResultSet resultSet) throws Exception {
        Integer id = resultSet.getInt("id");
        Integer courseId = resultSet.getInt("courseId");
        Integer sequence = resultSet.getInt("sequence");
        String text = resultSet.getString("text");
        return new CourseHintEntityDto(id, courseId, sequence, text);
    }
}
