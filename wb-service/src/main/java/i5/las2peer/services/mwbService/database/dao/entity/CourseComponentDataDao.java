package i5.las2peer.services.mwbService.database.dao.entity;

import i5.las2peer.services.mwbService.database.dao.DataAccessObject;
import i5.las2peer.services.mwbService.database.dto.CourseComponentDataDto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class CourseComponentDataDao extends DataAccessObject {

    /**
     * constructor
     *
     * @param connection to the database
     */
    public CourseComponentDataDao(Connection connection) {
        super(connection);
    }

    //******************************************************************************************
    // static methods for database operations
    //******************************************************************************************

    /**
     * delete record with given id
     *
     * @param connection to database
     * @param courseId of the record to delete
     * @param table for data
     * @throws Exception -
     */
    public static void delete(Connection connection, String table, Integer courseId) throws Exception {
        String sqlStatement = "DELETE FROM " + table +
                " WHERE courseId=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, courseId);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to delete data for component.", e);
        }
    }


    //******************************************************************************************
    // static methods to create DTOs from a resultSet
    //******************************************************************************************

    /**
     * create ComponentDataDto using data from resultSet
     *
     * @param resultSet containing data
     * @return CourseComponentDataDto
     * @throws Exception -
     */
    public static CourseComponentDataDto createFromResultSet(ResultSet resultSet) throws Exception {
        Integer courseId = resultSet.getInt("courseId");
        return new CourseComponentDataDto(courseId);
    }
}
