package i5.las2peer.services.mwbService.database.dao.entity;


import i5.las2peer.services.mwbService.Credentials;
import i5.las2peer.services.mwbService.database.dao.DataAccessObject;
import i5.las2peer.services.mwbService.database.dto.entity.ExternalServiceEntityDto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Entity dao to handle basic database operations for table "externalService"
 */
public class ExternalServiceEntityDao extends DataAccessObject {
    private final ExternalServiceEntityDto externalServiceEntityDto;

    /**
     * Table name for course data
     */
    public static final String TABLE = "externalService";

    /**
     * constructor
     * @param connection to the database
     * @param externalServiceEntityDto -
     */
    public ExternalServiceEntityDao(Connection connection, ExternalServiceEntityDto externalServiceEntityDto) {
        super(connection);
        this.externalServiceEntityDto = externalServiceEntityDto;
    }

    /**
     * constructor
     *
     * @param connection to the database
     * @param serviceName -
     * @throws Exception if requested connection could not be established
     */
    public ExternalServiceEntityDao(Connection connection, String serviceName) throws Exception {
        super(connection);
        this.externalServiceEntityDto = readData(connection, serviceName) ;
    }


    //******************************************************************************************
    // instance methods
    //******************************************************************************************

    public String getUrl() throws Exception {
        assertNotNull();
        return externalServiceEntityDto.getUrl();
    }

    public Credentials getCredentials() throws Exception {
        assertNotNull();
        return new Credentials(externalServiceEntityDto.getUsername(), externalServiceEntityDto.getPassword());
    }

    public void assertNotNull() throws Exception {
        if (externalServiceEntityDto == null)
            throw getInvalidDataException("No service data found.");
    }


    //******************************************************************************************
    // static methods for database operations
    //******************************************************************************************

    public static ExternalServiceEntityDto readData(Connection connection, String serviceName) throws Exception {
        String sqlStatement = "SELECT * FROM " + TABLE   + " WHERE serviceName=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setString(1, serviceName);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return createDto(resultSet);
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve data for service "+serviceName+".", e);
        }
        return null;
    }


    //******************************************************************************************
    // static method to create DTO from a resultSet
    //******************************************************************************************

    /**
     * create dto using data from resultSet
     *
     * @param resultSet containing data
     * @return LrsStoreForCourseEntityDto
     * @throws Exception -
     */
    public static ExternalServiceEntityDto createDto(ResultSet resultSet) throws Exception {
        String serviceName = resultSet.getString("serviceName");
        String url = resultSet.getString("url");
        String username = resultSet.getString("username");
        String password = resultSet.getString("password");
        return new ExternalServiceEntityDto(serviceName, url, username, password);
    }
}
