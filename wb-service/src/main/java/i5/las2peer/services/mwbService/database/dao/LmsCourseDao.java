package i5.las2peer.services.mwbService.database.dao;

import i5.las2peer.services.mwbService.database.dao.entity.MoodleCourseEntityDao;
import i5.las2peer.services.mwbService.database.dao.entity.OpalCourseEntityDao;
import i5.las2peer.services.mwbService.database.dto.entity.BaseCourseEntityDto;
import i5.las2peer.services.mwbService.database.dto.LmsCourseDto;
import i5.las2peer.services.mwbService.database.dto.entity.MoodleCourseEntityDto;
import i5.las2peer.services.mwbService.database.dto.entity.OpalCourseEntityDto;
import i5.las2peer.services.mwbService.externalServices.ServiceHttpResponseException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;


/**
 * abstract Dao as base class to handle course data for different lms
 */
public abstract class LmsCourseDao extends DataAccessObject {

    /**
     * constructor
     *
     * @param connection to the database
     */
    public LmsCourseDao(Connection connection) {
        super(connection);
    }


    //******************************************************************************************
    // abstract methods
    //******************************************************************************************

    public abstract String getLms();
    public abstract LmsCourseDto getLmsCourseDto();
    public abstract List<BaseCourseEntityDto> getBaseCoursesForLmsCourse(LmsCourseDto lmsCourseDto) throws Exception;


    //******************************************************************************************
    // basic static methods
    //******************************************************************************************
    public static List<String> getSupportedLms() { return List.of(OpalCourseEntityDao.LMS, MoodleCourseEntityDao.LMS); }

    public static ServiceHttpResponseException createLmsException(String message) {
        return new ServiceHttpResponseException("LMS data", HTTP_BAD_REQUEST, message);
    }

    public static ServiceHttpResponseException methodNotYetImplementedForLms(String methodName, String lms) {
        return new ServiceHttpResponseException("LMS data", HTTP_BAD_REQUEST, String.format("LmsCourseDao: Method %s not yet implemented in subclass for lms %s", methodName, lms));
    }

    public static LmsCourseDao createLmsCourseDao(Connection connection, BaseCourseEntityDto baseCourseEntityDto) throws Exception {
        return switch(baseCourseEntityDto.getLms()) {
            case OpalCourseEntityDao.LMS -> new OpalCourseEntityDao(connection, baseCourseEntityDto.getId());
            case MoodleCourseEntityDao.LMS -> new MoodleCourseEntityDao(connection, baseCourseEntityDto.getId());
            default -> throw createLmsException("Method LmsCourseDao.createLmsCourseDao: unsupported lms type for base course: " + baseCourseEntityDto.getLms());
        };
    }

    public static LmsCourseDao createLmsCourseDao(Connection connection, LmsCourseDto lmsCourseDto) throws Exception {
        if (lmsCourseDto instanceof OpalCourseEntityDto)
            return new OpalCourseEntityDao(connection, (OpalCourseEntityDto) lmsCourseDto);
        else if (lmsCourseDto instanceof MoodleCourseEntityDto)
            return new MoodleCourseEntityDao(connection, (MoodleCourseEntityDto) lmsCourseDto);
        throw createLmsException("Method LmsCourseDao.createLmsCourseDao: unsupported subclass for lmsCourseDto: " + lmsCourseDto.getClass());
    }

    public static String getLmsType(Connection connection, LmsCourseDto lmsCourseDto) throws Exception {
        return createLmsCourseDao(connection, lmsCourseDto).getLms();
    }


    //******************************************************************************************
    // static methods for database operations
    //******************************************************************************************

    /**
     * delete record with given id
     *
     * @param connection to database
     * @param baseCourseId of the record to delete
     * @param table for data
     * @throws Exception -
     */
    public static void delete(Connection connection, String table, Integer baseCourseId) throws Exception {
        String sqlStatement = "DELETE FROM " + table +
                " WHERE baseCourseId=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, baseCourseId);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to delete lms course data for base course "+baseCourseId+".", e);
        }
    }


    //******************************************************************************************
    // static methods to create DTOs from a resultSet
    //******************************************************************************************

    /**
     * create ComponentDataDto using data from resultSet
     *
     * @param resultSet containing data
     * @return ChatbotEntityDto
     * @throws Exception -
     */
    public static LmsCourseDto createFromResultSet(ResultSet resultSet) throws Exception {
        Integer baseCourseId = resultSet.getInt("baseCourseId");
        Integer lmsWorkerId = resultSet.getInt("lmsWorkerId");
        return new LmsCourseDto(baseCourseId, lmsWorkerId);
    }


    //******************************************************************************************
    // abstract methods
    //******************************************************************************************

    public abstract void upsert() throws Exception;
}
