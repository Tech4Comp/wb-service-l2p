package i5.las2peer.services.mwbService.database.dto.entity;


import lombok.AllArgsConstructor;
import lombok.Data;

import java.sql.Timestamp;
import java.util.UUID;

/**
 * Entity dto corresponding to database table "userTimelineItem"
 */
@Data
@AllArgsConstructor
public class UserTimelineItemEntityDto {
    private int id;
    private UUID uuid;
    private int courseTimelineItemId;
    private int writingTaskNr;
    private int rowId;
    private String title;
    private String description;
    private Timestamp startsOn;
    private Timestamp endsOn;
    private boolean done;
    private boolean expandToCalendarWeek;

    /**
     * default constructor
     */
    public UserTimelineItemEntityDto() {
    }
}
