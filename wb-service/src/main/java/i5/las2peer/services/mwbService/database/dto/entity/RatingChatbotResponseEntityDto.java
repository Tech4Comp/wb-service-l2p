package i5.las2peer.services.mwbService.database.dto.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.sql.Timestamp;
import java.util.UUID;


/**
 * Entity dto corresponding to database table "ratingChatbotResponse"
 */
@Data
@AllArgsConstructor
public class RatingChatbotResponseEntityDto {
    private Integer courseId;
    private UUID uuid;
    private boolean isThumbsUp;
    private String originalMessage;
    private String responseMessage;
    private String comment;
    private Timestamp timestamp;

    /**
     * default constructor
     */
    public RatingChatbotResponseEntityDto() {}
}
