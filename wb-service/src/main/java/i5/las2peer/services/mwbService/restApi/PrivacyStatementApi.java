package i5.las2peer.services.mwbService.restApi;


import i5.las2peer.services.mwbService.Authentication;
import i5.las2peer.services.mwbService.MwbService;
import i5.las2peer.services.mwbService.database.LazyPostgresConnection;
import i5.las2peer.services.mwbService.database.dao.entity.PrivacyStatementEntityDao;
import i5.las2peer.services.mwbService.database.dto.entity.PrivacyStatementEntityDto;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *  REST API for managing privacy statements
 */
@Path("/privacyStatement")
public class PrivacyStatementApi {
    /**
     * Creates a new privacy statement or updates an existing one
     * @param privacyStatementEntityDto to create/update as PrivacyStatementEntityDto as JSON object.
     * @return  200 OK, if successful
     *          400 Bad Request if something went wrong
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @POST
    @RolesAllowed("authenticated")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Upsert privacy statement",
            response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Failed to upsert privacy statement"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Missing rights"),
            @ApiResponse(code = 500, message = "Failed to establish connection to PostgresDB")
    })
    public Response upsertPrivacyStatement(
            @ApiParam(value = "The privacy statement to create/update as a JSON object", required = true) PrivacyStatementEntityDto privacyStatementEntityDto
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthorizationForCourse(connection.getConnection(), privacyStatementEntityDto.getCourseId());
            PrivacyStatementEntityDao privacyStatementEntityDao = new PrivacyStatementEntityDao(connection.getConnection(), privacyStatementEntityDto);
            privacyStatementEntityDao.upsert();
            return MwbService.responseOK();
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * Deletes a privacy statement
     * @param id of the privacy statement to be deleted
     * @return  200 OK, if successful
     *          400 Bad Request if something went wrong
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @DELETE
    @Path("/{id}")
    @RolesAllowed("authenticated")
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Delete privacy statement",
            response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Failed to delete privacy statement"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Missing rights"),
            @ApiResponse(code = 500, message = "Failed to establish connection to PostgresDB")
    })
    public Response deletePrivacyStatement(
            @ApiParam(value = "The íd of the privacy statement", required = true) @PathParam(value = "id") int id
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthenticated();
            PrivacyStatementEntityDao privacyStatementEntityDao = new PrivacyStatementEntityDao(connection.getConnection(), id);
            PrivacyStatementEntityDto privacyStatementEntityDto = privacyStatementEntityDao.getPrivacyStatementEntityDto();
            if (privacyStatementEntityDto != null) {
                Authentication.checkAuthorizationForCourse(connection.getConnection(), privacyStatementEntityDto.getCourseId());
                PrivacyStatementEntityDao.delete(connection.getConnection(), id);
            }
            return MwbService.responseOK();
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }
}
