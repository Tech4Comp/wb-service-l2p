package i5.las2peer.services.mwbService.database.dto;

import i5.las2peer.services.mwbService.database.dto.entity.BaseCourseEntityDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * DTO with basic properties of a base course
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class BaseCourseDto extends BaseCourseEntityDto {
    private LmsCourseDto lmsCourseDto;

    /**
     * default constructor
     */
    public  BaseCourseDto() {}

    /**
     * constructor
     *
     * @param baseCourseEntityDto basic data as BaseCourseEntityDto
     * @param lmsCourseDto with lms data for the base course
     */
    public BaseCourseDto(BaseCourseEntityDto baseCourseEntityDto, LmsCourseDto lmsCourseDto) {
        super(baseCourseEntityDto);
        this.lmsCourseDto = lmsCourseDto;
    }

    /**
     * copy constructor (used to initialise derived classes)
     *
     * @param copy - BaseCourseDto with data
     */
    public BaseCourseDto(BaseCourseDto copy) {
        super(copy);
        this.lmsCourseDto = copy.getLmsCourseDto();
    }
}
