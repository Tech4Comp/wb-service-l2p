package i5.las2peer.services.mwbService.database.dto.entity;


import lombok.AllArgsConstructor;
import lombok.Data;

import java.sql.Timestamp;

/**
 * Entity dto corresponding to database table "courseExam"
 */
@Data
@AllArgsConstructor
public class CourseExamEntityDto {
    private Integer id;
    private Integer courseId;
    private String testTool;
    private String name;
    private Timestamp validFrom;
    private Timestamp validUntil;

    /**
     * default constructor
     */
    public CourseExamEntityDto() {}

    /**
     * copy constructor (used to initialise derived classes)
     *
     * @param copy - CourseEntity with data
     */
    public CourseExamEntityDto(CourseExamEntityDto copy) {
        this.id = copy.getId();
        this.courseId = copy.getCourseId();
        this.testTool = copy.getTestTool();
        this.name = copy.getName();
        this.validFrom = copy.getValidFrom();
        this.validUntil = copy.getValidUntil();
    }
}
