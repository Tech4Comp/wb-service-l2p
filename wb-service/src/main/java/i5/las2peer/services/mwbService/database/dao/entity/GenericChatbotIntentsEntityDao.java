package i5.las2peer.services.mwbService.database.dao.entity;


import i5.las2peer.services.mwbService.database.dao.DataAccessObject;
import i5.las2peer.services.mwbService.database.dto.entity.GenericChatbotIntentsEntityDto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Entity dao to handle basic database operations for table "genericChatbotIntents"
 * NOTE: this table is not yet part of the live database
 */
public class GenericChatbotIntentsEntityDao extends DataAccessObject {
    private final GenericChatbotIntentsEntityDto genericChatbotIntentsEntityDto;

    /**
     * Table name for data
     */
    public static final String TABLE = "genericChatbotIntents";

    /**
     * constructor
     * @param connection to the database
     * @param genericChatbotIntentsEntityDto -
     */
    public GenericChatbotIntentsEntityDao(Connection connection, GenericChatbotIntentsEntityDto genericChatbotIntentsEntityDto) {
        super(connection);
        this.genericChatbotIntentsEntityDto = genericChatbotIntentsEntityDto;
    }


    //******************************************************************************************
    // static methods for database operations
    //******************************************************************************************

    public static GenericChatbotIntentsEntityDto readData(Connection connection, String intent) throws Exception {
        String sqlStatement = "SELECT * FROM " + TABLE + " WHERE intent=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setString(1, intent);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return createDto(resultSet);
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve generic answer for bot intent.", e);
        }
        return null;
    }

    //******************************************************************************************
    // static method to create DTO from a resultSet
    //******************************************************************************************

    /**
     * create CourseHintEntityDto using data from resultSet
     *
     * @param resultSet containing data
     * @return GenericChatbotIntentsEntityDto
     * @throws Exception -
     */
    public static GenericChatbotIntentsEntityDto createDto(ResultSet resultSet) throws Exception {
        String intent = resultSet.getString("intent");
        String answer = resultSet.getString("answer");
        return new GenericChatbotIntentsEntityDto(intent, answer);
    }


}
