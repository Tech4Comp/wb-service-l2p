package i5.las2peer.services.mwbService.externalServices.LMS;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import i5.las2peer.services.mwbService.database.dao.entity.MoodleCourseEntityDao;
import i5.las2peer.services.mwbService.database.dto.ExamDtoForFeedback;
import i5.las2peer.services.mwbService.database.dto.LmsCourseDto;
import i5.las2peer.services.mwbService.database.dto.entity.LmsWorkerEntityDto;
import i5.las2peer.services.mwbService.externalServices.ServiceException;

import java.io.File;
import java.util.List;

/**
 * class to handle connection to the RestAPI for Moodle
 */
public class MoodleConnection extends LmsConnection {

    /**
     * constructor
     */
    public MoodleConnection() {
        super(MoodleCourseEntityDao.LMS);

    }

    /**
     * establishing a connection to Moodle
     * @param lmsWorkerEntityDto with data for worker to access RestAPI
     * @throws Exception if failed to establish connection
     */
    public void establishConnection(LmsWorkerEntityDto lmsWorkerEntityDto) throws Exception {
        throw getServiceExceptionInternalError("Method 'establishConnection' is not yet implemented");
    }

    /**
     * @param lmsWorkerEntityDto with data of lms worker to check
     * @param json with result of check as JSONObject
     */
    public boolean checkWorkerAuthorization(LmsWorkerEntityDto lmsWorkerEntityDto, ObjectNode json) {
        try {
            establishConnection(lmsWorkerEntityDto);
            json.put("establishConnection", "ok");
            return true;
        } catch (ServiceException e) {
            json.put("establishConnection", e.getDetailedMessage());
        } catch (Exception e) {
            json.put("establishConnection", e.getMessage());
        }
        return false;
    }

    /**
     * @param lmsWorkerEntityDto with data of lms worker to check
     * @param json to store results of check
     */
    public void checkWorkerAuthorizationForLmsCourse(LmsWorkerEntityDto lmsWorkerEntityDto, LmsCourseDto lmsCourseDto, ObjectNode json) {
        try {
            throw getServiceExceptionInternalError("checkWorkerAuthorizationForLmsCourse not yet implemented");
            //json.put("hasAuthorizationForCourse", true);
        } catch (Exception e) {
            json.put("failedAuthorizationForCourse", e.getMessage());
        }
    }

    public boolean hasAuthorizationForLmsCourse(LmsCourseDto lmsCourseDto, String userMail) throws Exception {
        throw getServiceExceptionBadRequest("Method 'hasAuthorizationForLmsCourse' is not yet implemented");
    }
    public String getCourseMembers(LmsCourseDto lmsCourseDto) throws Exception {
        throw getServiceExceptionBadRequest("Method 'getCourseMembers' is not yet implemented");
    }

    public List<LmsResult> getTestResultIds(ExamDtoForFeedback examDto, String userEmail) throws Exception {
        throw getServiceExceptionBadRequest("Method 'getTestResultIds' is not yet implemented");
    }
    public JsonNode getResultData(File destinationDirectory, ExamDtoForFeedback examDto, String resultId) throws Exception {
        throw getServiceExceptionBadRequest("Method 'getResultData' is not yet implemented");
    }
}
