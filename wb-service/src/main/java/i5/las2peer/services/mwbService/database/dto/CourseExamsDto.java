package i5.las2peer.services.mwbService.database.dto;


import i5.las2peer.services.mwbService.database.dto.entity.CourseExamEntityDto;
import i5.las2peer.services.mwbService.database.dto.entity.CourseHintEntityDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * dto for course component EXTENDEDEXAMFEEDBACK
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class CourseExamsDto extends CourseComponentDataDto {
    private List<CourseExamEntityDto> exams;

    /**
     * default constructor
     */
    public CourseExamsDto() {}

    /**
     * constructor
     *
     * @param courseComponentDataDto basic data as CourseComponentDataDto
     * @param exams -
     */
    public CourseExamsDto(CourseComponentDataDto courseComponentDataDto,
                          List<CourseExamEntityDto> exams) {
        super(courseComponentDataDto);
        this.exams = exams;
    }

    /**
     * copy constructor (used to initialise derived classes)
     *
     * @param copy - CourseEntity with data
     */
    public CourseExamsDto(CourseExamsDto copy) {
        super(copy);
        this.exams = copy.getExams();
    }
}
