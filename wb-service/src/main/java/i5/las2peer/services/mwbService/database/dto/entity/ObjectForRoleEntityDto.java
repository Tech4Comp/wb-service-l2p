package i5.las2peer.services.mwbService.database.dto.entity;


import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Entity dto corresponding to database table "objectForRole"
 */
@Data
@AllArgsConstructor
public class ObjectForRoleEntityDto {
    private Integer id;
    private String objectType;

    /**
     * default constructor
     */
    public ObjectForRoleEntityDto() {}
}
