package i5.las2peer.services.mwbService.database.dao.entity;


import i5.las2peer.services.mwbService.database.dao.DataAccessObject;
import i5.las2peer.services.mwbService.database.dto.entity.UserTimelineItemEntityDto;

import java.sql.*;
import java.util.UUID;


/**
 * Entity dao to handle basic database operations for table "userTimelineItem"
 */
public class UserTimelineItemEntityDao extends DataAccessObject {
    private final UserTimelineItemEntityDto userTimelineItemEntityDto;

    /**
     * Table name for course data
     */
    public static final String TABLE = "userTimelineItem";

    /**
     * constructor
     * @param connection to the database
     * @param userTimelineItemEntityDto -
     */
    public UserTimelineItemEntityDao(Connection connection, UserTimelineItemEntityDto userTimelineItemEntityDto) {
        super(connection);
        this.userTimelineItemEntityDto = userTimelineItemEntityDto;
    }

    /**
     * constructor
     * @param connection to the database
     * @param id of the course
     */
    public UserTimelineItemEntityDao(Connection connection, Integer id) throws Exception {
        super(connection);
        this.userTimelineItemEntityDto = readDataWithoutUUID(connection, id);
    }


    //******************************************************************************************
    // basic database operations
    //******************************************************************************************

    /**
     * insert a new record into the database
     *
     * @return id of the new record
     * @throws Exception if something went wrong
     */
    public int insert() throws Exception {
        String sqlStatement = "INSERT INTO " + TABLE +
                " (uuid, courseTimelineItemId, writingTaskNr, rowId, title, description, startsOn, endsOn, done, expandToCalendarWeek) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" +
                " RETURNING id;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setObject(1, userTimelineItemEntityDto.getUuid());
            setOptionalIntegerForResultSet(preparedStatement, 2, userTimelineItemEntityDto.getCourseTimelineItemId());
            setOptionalIntegerForResultSet(preparedStatement, 3, userTimelineItemEntityDto.getWritingTaskNr());
            preparedStatement.setInt(4, userTimelineItemEntityDto.getRowId());
            setOptionalStringForResultSet(preparedStatement, 5, userTimelineItemEntityDto.getTitle());
            setOptionalStringForResultSet(preparedStatement, 6, userTimelineItemEntityDto.getDescription());
            preparedStatement.setTimestamp(7, userTimelineItemEntityDto.getStartsOn());
            preparedStatement.setTimestamp(8, userTimelineItemEntityDto.getEndsOn());
            preparedStatement.setBoolean(9, userTimelineItemEntityDto.isDone());
            preparedStatement.setBoolean(10, userTimelineItemEntityDto.isExpandToCalendarWeek());
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return resultSet.getInt("id");
        } catch (SQLException e) {
            throw DataAccessObject.createDatabaseException("Failed to insert data for user timeline item.", e);
        }
        throw DataAccessObject.createDatabaseException("Failed to retrieve id of inserted user timeline item.", null);
    }

    /**
     * update record
     *
     * @throws Exception if something went wrong
     */
    public void update() throws Exception {
        String sqlStatement = "UPDATE " + TABLE + " SET title=?, description=?, startsOn=?, endsOn=?, done=?, expandToCalendarWeek=?" +
                " WHERE id=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            setOptionalStringForResultSet(preparedStatement, 1, userTimelineItemEntityDto.getTitle());
            setOptionalStringForResultSet(preparedStatement, 2, userTimelineItemEntityDto.getDescription());
            preparedStatement.setTimestamp(3, userTimelineItemEntityDto.getStartsOn());
            preparedStatement.setTimestamp(4, userTimelineItemEntityDto.getEndsOn());
            preparedStatement.setBoolean(5, userTimelineItemEntityDto.isDone());
            preparedStatement.setBoolean(6, userTimelineItemEntityDto.isExpandToCalendarWeek());
            preparedStatement.setInt(7, userTimelineItemEntityDto.getId());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to update user timeline item.", e);
        }
    }


    //******************************************************************************************
    // static methods for database operations
    //******************************************************************************************

    public static UserTimelineItemEntityDto readData(Connection connection, Integer id, Boolean withUUID) throws Exception {
        String sqlStatement = "SELECT * FROM " + TABLE   + " WHERE id=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return withUUID ? createDto(resultSet) : createDtoWithoutUUID(resultSet);
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve data for userTimelineItem.", e);
        }
        return null;
    }

    public static UserTimelineItemEntityDto readData(Connection connection, Integer id) throws Exception {
        return readData(connection, id, true);
    }

    public static UserTimelineItemEntityDto readDataWithoutUUID(Connection connection, Integer id) throws Exception {
        return readData(connection, id, false);
    }

    /**
     * delete record with given id
     *
     * @param connection to database
     * @param id of the record to delete
     * @throws Exception -
     */
    public static void delete(Connection connection, Integer id) throws Exception {
        String sqlStatement = "DELETE FROM " + TABLE +
                " WHERE id=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to delete userTimelineItem.", e);
        }
    }


    //******************************************************************************************
    // static method to create DTO from a resultSet
    //******************************************************************************************

    /**
     * create UserTimelineItemEntityDto using data from resultSet
     *
     * @param resultSet containing data
     * @return UserTimelineItemEntityDto
     * @throws Exception -
     */
    public static UserTimelineItemEntityDto createDto(ResultSet resultSet) throws Exception {
        UserTimelineItemEntityDto userTimelineItem = createDtoWithoutUUID(resultSet);
        UUID uuid = resultSet.getObject("uuid", java.util.UUID.class);
        userTimelineItem.setUuid(uuid);
        return userTimelineItem;
    }

    /**
     * create UserTimelineItemEntityDto with uuid set to null using data from resultSet
     *
     * @param resultSet containing data
     * @return UserTimelineItemEntityDto
     * @throws Exception -
     */
    public static UserTimelineItemEntityDto createDtoWithoutUUID(ResultSet resultSet) throws Exception {
        int id = resultSet.getInt("id");
        int timelineItemId = resultSet.getInt("courseTimelineItemId");
        int writingTaskNr = resultSet.getInt("writingTaskNr");
        String title = resultSet.getString("title");
        Timestamp startsOn = resultSet.getTimestamp("startsOn");
        Timestamp endsOn = resultSet.getTimestamp("endsOn");
        String description = resultSet.getString("description");
        int rowId = resultSet.getInt("rowId");
        boolean done = resultSet.getBoolean("done");
        boolean expandToCalendarWeek = resultSet.getBoolean("expandToCalendarWeek");
        return new UserTimelineItemEntityDto(id, null, timelineItemId, writingTaskNr, rowId, title, description, startsOn, endsOn, done, expandToCalendarWeek);
    }
}
