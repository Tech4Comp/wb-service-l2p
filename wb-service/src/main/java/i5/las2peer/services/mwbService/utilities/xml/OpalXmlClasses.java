package i5.las2peer.services.mwbService.utilities.xml;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Data;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


/**
 * Classes needed to parse XML sent as response of the OPAL RestAPI
 */
public class OpalXmlClasses {
    /**
     * User virtual object with basic user data
     */
    @Data
    public static class UserVO {
        private String key;
        private String firstName;
        private String lastName;
        private String email;
        private UserVOes.Properties properties;
    }

    /**
     * List of UserVO
     */
    @Data
    public static class UserVOes {
        @JacksonXmlElementWrapper(useWrapping = false)
        @JacksonXmlProperty(localName = "userVO")
        private List<UserVO> users = new ArrayList<>();


        /**
         * List of properties
         */
        @Data
        public static class Properties {
            @JacksonXmlElementWrapper(useWrapping = false)
            @JacksonXmlProperty(localName = "property")
            private List<UserVOes.Property> propertyList = new ArrayList<>();
        }

        /**
         * Property stored as name/value pair
         */
        @Data
        public static class Property {
            private String name;
            private String value;
        }
    }

    /**
     * data for a single test result
     */
    @Data
    public static class ResultVO {
        private String key;
        private Timestamp startDate;
        private Timestamp endDate;
        private Long duration;
        private Double maxScore;
        private Float score;
        private Boolean passed;
        private String state;
        private Boolean scored;
    }

    /**
     * class wrapping the results for a user
     */
    @Data
    public static class ResultsVO {
        private UserVO userVO;
        @JacksonXmlElementWrapper(useWrapping = false)
        private List<ResultVO> resultVO = new ArrayList<>();
        private String data;
    }

    /**
     * List of ResultsVO
     */
    @Data
    public static class ResultsVOes {
        @JacksonXmlElementWrapper(useWrapping = false)
        private List<ResultsVO> resultsVO = new ArrayList<>();
    }
}
