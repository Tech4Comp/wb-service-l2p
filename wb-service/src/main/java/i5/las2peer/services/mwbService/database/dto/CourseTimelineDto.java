package i5.las2peer.services.mwbService.database.dto;


import i5.las2peer.services.mwbService.database.dto.entity.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * Dto with basic properties of a course timeline
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class CourseTimelineDto extends CourseTimelineEntityDto {
    private List<CourseTimelineRowEntityDto> rows;
    private List<CourseTimelineItemEntityDto> items;

    /**
     * default constructor
     */
    public CourseTimelineDto() {}

    /**
     * constructor
     *
     * @param courseTimelineEntityDto basic data as WritingTasksEntityDto
     * @param rows list of CourseTimelineRowEntityDto
     * @param items list of CourseTimelineItemEntityDto
     */
    public CourseTimelineDto(CourseTimelineEntityDto courseTimelineEntityDto, List<CourseTimelineRowEntityDto> rows, List<CourseTimelineItemEntityDto> items) {
        super(courseTimelineEntityDto);
        this.rows = rows;
        this.items = items;
    }
}
