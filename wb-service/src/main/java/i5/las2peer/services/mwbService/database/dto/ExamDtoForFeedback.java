package i5.las2peer.services.mwbService.database.dto;


import i5.las2peer.services.mwbService.database.dto.entity.CourseExamEntityDto;
import i5.las2peer.services.mwbService.database.dto.entity.EaslitExamEntityDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Dto with all exam details needed to generate a feedback
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class ExamDtoForFeedback extends ExamDto {
    private BaseCourseDto baseCourseDto;

    /**
     * constructor setting the basic exam data
     * @param baseCourseDto with base course data
     */
    public ExamDtoForFeedback(ExamDto examDto, BaseCourseDto baseCourseDto) {
        super(examDto);
        this.baseCourseDto = baseCourseDto;
    }
}
