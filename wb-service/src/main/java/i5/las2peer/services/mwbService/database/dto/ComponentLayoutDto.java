package i5.las2peer.services.mwbService.database.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Dto to handle details for the layout of a given component type
 * data is stored as json in database
 */
@Data
@AllArgsConstructor
public class ComponentLayoutDto {
    private String componentType;
    private int cols;
    private int rows;

    /**
     * default constructor
     */
    public ComponentLayoutDto() {
    }
}
