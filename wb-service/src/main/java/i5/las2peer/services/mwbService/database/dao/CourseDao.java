package i5.las2peer.services.mwbService.database.dao;

import com.fasterxml.jackson.databind.ObjectMapper;
import i5.las2peer.services.mwbService.database.dao.entity.*;
import i5.las2peer.services.mwbService.database.dto.*;
import i5.las2peer.services.mwbService.database.dto.entity.*;
import i5.las2peer.services.mwbService.externalServices.ServiceHttpResponseException;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static i5.las2peer.services.mwbService.database.dao.CourseComponentDao.*;

/**
 * DAO to handle complex course related database requests
 */
public class CourseDao extends DataAccessObject {
    /**
     * constructor
     * @param connection to the database
     */
    public CourseDao(Connection connection) {
        super(connection);
    }


    //******************************************************************************************
    // instance methods
    //******************************************************************************************

    public CourseDto getCourse(Integer id) throws Exception {
        String sqlStatement = getSqlStatementForGetCourse("id");
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return createCourseDto(resultSet);
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve data for course.", e);
        }
        return null;
    }

    public CourseDto getCourse(String shortname) throws Exception {
        String sqlStatement = getSqlStatementForGetCourse("shortname");
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setString(1, shortname);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return createCourseDto(resultSet);
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve data for course.", e);
        }
        return null;
    }

    private String getSqlStatementForGetCourse(String selector) {
        return "SELECT "+CourseEntityDao.TABLE+".*, "+BaseCourseEntityDao.TABLE+".name FROM " + CourseEntityDao.TABLE +
            " JOIN " + BaseCourseEntityDao.TABLE + " ON " + BaseCourseEntityDao.TABLE + ".id=baseCourseId" +
            " WHERE " + CourseEntityDao.TABLE + "."+selector+"=?";
    }


    /**
     * retrieve course data needed for the manager view
     *
     * @param courseId of the course
     * @return CourseManageDto with the data
     * @throws Exception if something went wrong
     */
    public CourseManageDto getCourseManageInfo(Integer courseId) throws Exception {
        CourseDto courseDto = getCourse(courseId);
        CourseInfoDto courseInfoDto = addAdditionalCourseInfo(courseDto);
        boolean hasLrsData = LrsStoreForCourseEntityDao.hasLrsData(this.connection, courseId);
        List<CourseFaqEntityDto> botIntents = CourseFaqEntityDao.readListData(this.connection, courseId);
        return new CourseManageDto(courseInfoDto, botIntents, hasLrsData);
    }

    /**
     * retrieve the details of a course from the database
     *
     * @param shortname of the course
     * @return CourseInfoDto with the all details available
     * @throws Exception if something went wrong
     */
    public CourseInfoDto getCourseInfo(String shortname) throws Exception {
        CourseDto courseDto = getCourse(shortname);
        if (courseDto == null)
            return null;
        return addAdditionalCourseInfo(courseDto);
    }

    private CourseInfoDto addAdditionalCourseInfo(CourseDto courseDto) throws Exception {
        List<CourseLayoutDto> layout = getLayout(courseDto.getId());
        PrivacyStatementEntityDto privacyStatementEntityDto = PrivacyStatementEntityDao.readData(this.connection, courseDto.getId(), null);
        PrivacyInfoDto privacyInfo = null;
        if (privacyStatementEntityDto != null) {
            FileBucketEntityDto fileBucketEntityDto = FileBucketEntityDao.readData(connection, privacyStatementEntityDto.getFileBucketId());
            privacyInfo = new PrivacyInfoDto(privacyStatementEntityDto, fileBucketEntityDto);
        }
        CourseComponentDao courseComponentDao = new CourseComponentDao(this.connection);
        List<CourseComponentDto> components = courseComponentDao.getCourseComponentsInfo(courseDto.getId());
        return new CourseInfoDto(courseDto, layout, privacyInfo, components);
    }


    /**
     * get list of courses already visited by a user
     *
     * @param uuid of user
     * @return list of CourseLastVisitedDto
     * @throws Exception if something went wrong
     */
    public List<CourseLastVisitedDto> getCoursesLastVisited(UUID uuid) throws Exception {
        String sqlStatement = "SELECT * FROM " + LastVisitedEntityDao.TABLE +
                " JOIN " + CourseEntityDao.TABLE + " ON " + CourseEntityDao.TABLE + ".id=" + LastVisitedEntityDao.TABLE + ".courseId" +
                " JOIN " + BaseCourseEntityDao.TABLE + " ON " + BaseCourseEntityDao.TABLE + ".id=baseCourseId" +
                " WHERE uuid=?";
        try (PreparedStatement preparedStatement = this.connection.prepareStatement(sqlStatement)) {
            preparedStatement.setObject(1, uuid);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<CourseLastVisitedDto> courses = new ArrayList<>();
            while (resultSet.next()) {
                CourseLastVisitedDto course = createCourseLastVisitedDto(resultSet);
                courses.add(course);
            }
            return courses;
        } catch (SQLException e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve list of courses last visited from database.", e);
        }
    }


    //******************************************************************************************
    // managing course layout
    //******************************************************************************************

    private List<CourseLayoutDto> getLayout(Integer courseId) throws Exception {
        List<CourseLayoutEntityDto> rawLayout = CourseLayoutEntityDao.getRawLayout(this.connection, courseId);
        List<CourseLayoutDto> layout = new ArrayList<>(rawLayout.size());
        ObjectMapper objectMapper = new ObjectMapper();

        for (CourseLayoutEntityDto item : rawLayout) {
            ComponentLayoutDto[] layoutItem = objectMapper.readValue(item.getJson(), ComponentLayoutDto[].class);
            layout.add(new CourseLayoutDto(item.getSize(), layoutItem));
        }
        return layout;
    }



    //******************************************************************************************
    // static methods to create course related DTOs from a resultSet
    //******************************************************************************************

    /**
     * create CourseDto using data from resultSet
     *
     * @param resultSet containing data
     * @return CourseDto
     * @throws Exception -
     */
    public static CourseDto createCourseDto(ResultSet resultSet) throws Exception {
        CourseEntityDto courseEntityDto = CourseEntityDao.createDto(resultSet);
        String name = resultSet.getString("name");
        return new CourseDto(courseEntityDto, name);
    }

    /**
     * create CourseLastVisitedDto using data from resultSet
     *
     * @param resultSet containing data
     * @return CourseLastVisitedDto
     * @throws Exception -
     */
    public static CourseLastVisitedDto createCourseLastVisitedDto(ResultSet resultSet) throws Exception {
        CourseDto courseDto = createCourseDto(resultSet);
        Timestamp lastVisited = resultSet.getTimestamp("timestamp");
        return new CourseLastVisitedDto(courseDto, lastVisited);
    }






    //******************************************************************************************
    // interim methods used to copy data from one course to another
    //******************************************************************************************

    /**
     * copy course data including all detail tables
     *
     * @param courseEntityDto course to copy data to
     * @param courseIdSource course id to copy data from
     */
    public void copyCourseData(CourseEntityDto courseEntityDto, Integer courseIdSource, List<String> details) throws Exception {
        CourseComponentDao courseComponentDao = new CourseComponentDao(this.connection);
        if (details == null)
            details = getCompleteListOfDetailsToCopy(courseComponentDao, courseIdSource);
        Integer courseId = courseEntityDto.getId();
        for (String detail: details) {
            switch (detail) {
                case "components" -> copyBasicComponentData(courseId, courseIdSource);
                case "layout" -> copyLayout(courseId, courseIdSource);
                case "privacystatement" -> copyPrivacyStatement(courseId, courseIdSource);
                case "lrs" -> copyLrsData(courseId, courseIdSource);
                case COMPONENT_TYPE_MWBHINTS -> copyHints(courseId, courseIdSource);
                case COMPONENT_TYPE_CHATBOT -> copyDataChatBot(courseId, courseIdSource);
                case "botIntents" -> copyBotIntents(courseId, courseIdSource);
                case COMPONENT_TYPE_SEARCHINRESOURCES -> copyDataSearch(courseId, courseIdSource);
                case COMPONENT_TYPE_OPALONYXPROXY -> copyDataOnyxProxy(courseEntityDto);
                case COMPONENT_TYPE_WRITINGTASKS -> copyDataWritingTasks(courseEntityDto, courseIdSource);
                case "writingtasks" -> copyWritingTasks(courseId, courseIdSource);
                case COMPONENT_TYPE_TIMELINE -> copyDataTimeline(courseEntityDto, courseIdSource);
                case "timelinerowsanditems" -> courseComponentDao.copyDataTimelineRows(courseEntityDto, courseIdSource);
                case COMPONENT_TYPE_EXTENDEDEXAMFEEDBACK -> courseComponentDao.copyDataExam(courseId, courseIdSource);
            }
        }
    }

    private List<String> getCompleteListOfDetailsToCopy(CourseComponentDao courseComponentDao, Integer courseId) throws Exception {
        List<String> copyList = new ArrayList<>(Arrays.asList("lrs", "privacystatement", "layout", "components"));
        List<CourseComponentDto> components = courseComponentDao.getCourseComponents(courseId);
        for (CourseComponentDto component: components) {
            String type = component.getComponentType();
            copyList.add(type);
            // add extra items needed for some component types
            switch (type) {
                case COMPONENT_TYPE_CHATBOT:
                    copyList.add("botIntents");
                    break;
                case COMPONENT_TYPE_WRITINGTASKS:
                    copyList.add("writingtasks");
                    break;
                case COMPONENT_TYPE_TIMELINE :
                    copyList.add("timelinerowsanditems");
                    break;
            }
        }
        return copyList;
    }

    private void copyCourseData(String table, List<String> fieldList, Integer courseId, Integer courseIdSource, String copyMessage) throws ServiceHttpResponseException {
        String fields = String.join(", ", fieldList);
        String sqlStatement = String.format("INSERT INTO %s (courseId, %s) " +
                " SELECT ?, %s FROM %s WHERE courseId=?", table, fields, fields, table);
        try (PreparedStatement preparedStatement = this.connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, courseId);
            preparedStatement.setInt(2, courseIdSource);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException(String.format("Failed to copy %s from courseId %d to courseId %d", copyMessage, courseIdSource, courseId), e);
        }
    }

    public void copyCourseDataKeepValidFromTo(String table, List<String> fieldListInsert, List<String> fieldListKeep, CourseEntityDto courseEntityDto, Integer courseIdSource, String copyMessage) throws ServiceHttpResponseException {
        List<String> fieldListCopy = new ArrayList<>();
        for (String field: fieldListInsert)
            if (!fieldListKeep.contains(field))
                fieldListCopy.add(field);
        String fieldsKeep = String.join(", ", fieldListKeep);
        String fieldsCopy = String.join(", ", fieldListCopy);
        String sqlStatement = String.format("INSERT INTO %s (courseId, %s, %s) " +
                " SELECT ?, ?, ?, %s FROM %s WHERE courseId=?", table, fieldsKeep, fieldsCopy, fieldsCopy, table);
        try (PreparedStatement preparedStatement = this.connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, courseEntityDto.getId());
            setOptionalDateForResultSet(preparedStatement, 2, courseEntityDto.getValidFrom());
            setOptionalDateForResultSet(preparedStatement, 3, courseEntityDto.getValidUntil());
            preparedStatement.setInt(4, courseIdSource);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException(String.format("Failed to copy %s from courseId %d to courseId %d", copyMessage, courseIdSource, courseEntityDto.getId()), e);
        }
    }

    private void copyLayout(Integer courseId, Integer courseIdSource) throws ServiceHttpResponseException {
        copyCourseData(CourseLayoutEntityDao.TABLE, CourseLayoutEntityDao.copyFieldList, courseId, courseIdSource, "layout data");
    }
    private void copyPrivacyStatement(Integer courseId, Integer courseIdSource) throws ServiceHttpResponseException {
        copyCourseData(PrivacyStatementEntityDao.TABLE, PrivacyStatementEntityDao.copyFieldList, courseId, courseIdSource, "privacy statement data");
    }
    private void copyLrsData(Integer courseId, Integer courseIdSource) throws ServiceHttpResponseException {
        copyCourseData(LrsStoreForCourseEntityDao.TABLE, LrsStoreForCourseEntityDao.copyFieldList, courseId, courseIdSource, "lrs data");
    }
    private void copyBasicComponentData(Integer courseId, Integer courseIdSource) throws ServiceHttpResponseException {
        copyCourseData(CourseComponentEntityDao.TABLE, CourseComponentEntityDao.copyFieldList, courseId, courseIdSource, "list of components");
    }
    private void copyBotIntents(Integer courseId, Integer courseIdSource) throws ServiceHttpResponseException {
        copyCourseData(CourseFaqEntityDao.TABLE, CourseFaqEntityDao.copyFieldList, courseId, courseIdSource, "chatbot intents");
    }
    private void copyDataChatBot(Integer courseId, Integer courseIdSource) throws ServiceHttpResponseException {
        copyCourseData(CourseChatbotEntityDao.TABLE, CourseChatbotEntityDao.copyFieldList, courseId, courseIdSource, "data for component chatbot");
    }
    private void copyHints(Integer courseId, Integer courseIdSource) throws ServiceHttpResponseException {
        copyCourseData(CourseHintEntityDao.TABLE, CourseHintEntityDao.copyFieldList, courseId, courseIdSource, "hints");
    }
    private void copyDataSearch(Integer courseId, Integer courseIdSource) throws ServiceHttpResponseException {
        copyCourseData(SearchIndexEntityDao.TABLE, SearchIndexEntityDao.copyFieldList, courseId, courseIdSource, "data for component search");
    }
    private void copyDataOnyxProxy(CourseEntityDto courseEntityDto) throws Exception {
        // no special data to copy: just use data for validFrom and validUntil from new course
        CourseOnyxProxyEntityDto courseOnyxProxyEntityDto = new CourseOnyxProxyEntityDto(new CourseComponentDataDto(courseEntityDto.getId()), courseEntityDto.getValidFrom(), courseEntityDto.getValidUntil());
        new CourseOnyxProxyEntityDao(this.connection, courseOnyxProxyEntityDto).upsert();
    }
    private void copyDataWritingTasks(CourseEntityDto courseEntityDto, Integer courseIdSource) throws ServiceHttpResponseException {
        copyCourseDataKeepValidFromTo(CourseWritingTasksEntityDao.TABLE, CourseWritingTasksEntityDao.copyFieldList, CourseWritingTasksEntityDao.keepFieldList, courseEntityDto, courseIdSource, "basic data for component writingtasks");
    }
    private void copyWritingTasks(Integer courseId, Integer courseIdSource) throws ServiceHttpResponseException {
        copyCourseData(WritingTaskEntityDao.TABLE, WritingTaskEntityDao.copyFieldList, courseId, courseIdSource, "writingtasks");
    }
    private void copyDataTimeline(CourseEntityDto courseEntityDto, Integer courseIdSource) throws Exception {
        // get validFrom/validUntil either from course validFrom/validUntil or from course to copy timeline validFrom/validUntil, if no start is given
        Date startsOn = courseEntityDto.getValidFrom();
        Date endsOn = courseEntityDto.getValidUntil();
        if (startsOn == null) {
            CourseTimelineEntityDto courseTimelineEntityDto = new CourseTimelineEntityDao(this.connection, courseIdSource).getCourseTimelineEntityDto();
            if (courseTimelineEntityDto != null) {
                startsOn = courseTimelineEntityDto.getStartsOn();
                if (endsOn == null)
                    endsOn = courseTimelineEntityDto.getEndsOn();
            }
        }
        // no special data to copy: just use data for validFrom and validUntil from new course
        CourseTimelineEntityDto courseTimelineEntityDto = new CourseTimelineEntityDto(new CourseComponentDataDto(courseEntityDto.getId()), startsOn, endsOn);
        new CourseTimelineEntityDao(this.connection, courseTimelineEntityDto).upsert();
    }
}
