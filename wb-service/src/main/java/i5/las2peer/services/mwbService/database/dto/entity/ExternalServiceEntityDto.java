package i5.las2peer.services.mwbService.database.dto.entity;

import lombok.*;

/**
 * Entity dto corresponding to database table "externalService"
 */
@AllArgsConstructor
@Data
public class ExternalServiceEntityDto {
    private String serviceName;
    private String url;
    private String username;
    private String password;
}
