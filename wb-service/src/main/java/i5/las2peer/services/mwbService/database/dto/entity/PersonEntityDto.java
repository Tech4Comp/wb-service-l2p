package i5.las2peer.services.mwbService.database.dto.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Entity dto corresponding to database table "person"
 */
@Data
@AllArgsConstructor
public class PersonEntityDto {
    private Integer id;
    private String email;
    private String firstName;
    private String lastName;

    /**
     * default constructor
     */
    public PersonEntityDto() {}

    /**
     * constructor
     * @param email -
     */
    public PersonEntityDto(String email) {
        this(null, email, null, null);
    }
}
