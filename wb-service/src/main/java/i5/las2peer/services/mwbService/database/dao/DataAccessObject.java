package i5.las2peer.services.mwbService.database.dao;

import i5.las2peer.services.mwbService.externalServices.ExternalService;
import i5.las2peer.services.mwbService.externalServices.ServiceException;
import i5.las2peer.services.mwbService.externalServices.ServiceHttpResponseException;
import lombok.Getter;

import java.sql.*;

import static i5.las2peer.services.mwbService.utilities.StringsUtilities.hasValue;
import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;

/**
 * basic wrapper to handle data in a database
 */
@Getter
public abstract class DataAccessObject extends ExternalService {

    protected final Connection connection;
    protected final String dao;

    /**
     * constructor
     * @param connection to the database
     */
    public DataAccessObject(Connection connection) {
        super("Database");
        this.dao = this.getClass().getSimpleName();
        this.connection = connection;
    }

    @Override
    public String getServiceName() {
        return super.getServiceName()+" "+this.dao;
    }

    /**
     * @param message - error message
     * @return ServiceHttpResponseException for service database with HTTP status Bad Request and message
     */
    public ServiceException getInvalidDataException(String message) {
        return getServiceExceptionBadRequest("Invalid data: " + message);
    }

    /**
     * @param message - error message
     * @param e - triggering exception
     * @return ServiceHttpResponseException for service database with HTTP status Bad Request and message
     */
    public static ServiceHttpResponseException createDatabaseException(String message, Exception e) {
        if (e != null) {
            String additionalInfo;
            if (e instanceof ServiceHttpResponseException)
                additionalInfo = ((ServiceHttpResponseException) e).getBaseMessage();
            else
                additionalInfo = e.getMessage();
            message = message + " " + additionalInfo;
        }
        return new ServiceHttpResponseException("database", HTTP_BAD_REQUEST, message);
    }

    public void setOptionalIntegerForResultSet(PreparedStatement preparedStatement, int index, int data) throws SQLException {
        if (data > 0)
            preparedStatement.setInt(index, data);
        else
            preparedStatement.setNull(index, Types.INTEGER);
    }

    public void setOptionalStringForResultSet(PreparedStatement preparedStatement, int index, String data) throws SQLException {
        if (data != null)
            preparedStatement.setString(index, data);
        else
            preparedStatement.setNull(index, Types.VARCHAR);
    }

    public void setOptionalTimestampForResultSet(PreparedStatement preparedStatement, int index, Timestamp data) throws SQLException {
        if (data != null)
            preparedStatement.setTimestamp(index, data);
        else
            preparedStatement.setNull(index, Types.TIMESTAMP);
    }

    public void setOptionalDateForResultSet(PreparedStatement preparedStatement, int index, Date data) throws SQLException {
        if (data != null)
            preparedStatement.setDate(index, data);
        else
            preparedStatement.setNull(index, Types.DATE);
    }

    public void setOptionalBooleanForResultSet(PreparedStatement preparedStatement, int index, Boolean data) throws SQLException {
        if (data != null)
            preparedStatement.setBoolean(index, data);
        else
            preparedStatement.setNull(index, Types.BOOLEAN);
    }
}
