package i5.las2peer.services.mwbService.database.dto.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

/**
 * Entity dto corresponding to database table "personMapping"
 */
@Data
@AllArgsConstructor
public class PersonMappingEntityDto {
    private UUID uuid;
    private Integer personId;

    /**
     * default constructor
     */
    public PersonMappingEntityDto() {}
}
