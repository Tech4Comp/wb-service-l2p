package i5.las2peer.services.mwbService.database.dao.entity;


import i5.las2peer.services.mwbService.database.dao.DataAccessObject;
import i5.las2peer.services.mwbService.database.dto.entity.CourseFaqEntityDto;
import i5.las2peer.services.mwbService.database.dto.entity.CourseHintEntityDto;
import i5.las2peer.services.mwbService.database.dto.entity.FileBucketEntityDto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Entity dao to handle basic database operations for table "courseFAQ"
 */
public class CourseFaqEntityDao extends DataAccessObject {
    private final CourseFaqEntityDto courseFaqEntityDto;

    /**
     * Table name for data
     */
    public static final String TABLE = "courseFAQ";

    /**
     * field list for copy course data
     */
    public static final List<String> copyFieldList = Arrays.asList("intent", "answer");

    /**
     * constructor
     * @param connection to the database
     * @param courseFaqEntityDto -
     */
    public CourseFaqEntityDao(Connection connection, CourseFaqEntityDto courseFaqEntityDto) {
        super(connection);
        this.courseFaqEntityDto = courseFaqEntityDto;
    }


    //******************************************************************************************
    // basic database operations
    //******************************************************************************************

    /**
     * insert a new record / update existing record to the database
     *
     * @throws Exception if something went wrong
     */
    public void upsert() throws Exception {
        String sqlStatement = "INSERT INTO " + TABLE + " (courseId, intent, answer) VALUES(?, ?, ?)"+
                " ON CONFLICT ON CONSTRAINT " + TABLE + "_pKey DO UPDATE" +
                " SET answer=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, courseFaqEntityDto.getCourseId());
            preparedStatement.setString(2, courseFaqEntityDto.getIntent());
            preparedStatement.setString(3, courseFaqEntityDto.getAnswer());
            preparedStatement.setString(4, courseFaqEntityDto.getAnswer());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw DataAccessObject.createDatabaseException("Failed to insert/update chatbot intent.", e);
        }
    }


    //******************************************************************************************
    // static methods for database operations
    //******************************************************************************************

    public static CourseFaqEntityDto readData(Connection connection, Integer courseId, String intent) throws Exception {
        String sqlStatement = "SELECT * FROM " + TABLE + " WHERE courseId=? AND intent=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, courseId);
            preparedStatement.setString(2, intent);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return createDto(resultSet);
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve answer for bot intent.", e);
        }
        return null;
    }

    public static List<CourseFaqEntityDto> readListData(Connection connection, Integer courseId) throws Exception {
        String sqlStatement = "SELECT * FROM " + TABLE + " WHERE courseId=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, courseId);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<CourseFaqEntityDto> list = new ArrayList<>();
            while (resultSet.next()) {
                CourseFaqEntityDto courseFaqEntity = createDto(resultSet);
                list.add(courseFaqEntity);
            }
            return list;
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve list of bot intents for course.", e);
        }
    }

    /**
     * delete record with given courseId and intent
     *
     * @param connection to database
     * @param courseId the record to delete
     * @param intent the record to delete
     * @throws Exception -
     */
    public static void delete(Connection connection, Integer courseId, String intent) throws Exception {
        String sqlStatement = "DELETE FROM " + TABLE +
                " WHERE courseId=? AND intent=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, courseId);
            preparedStatement.setString(2, intent);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to delete intent for component CHATBOT.", e);
        }
    }

    //******************************************************************************************
    // static method to create DTO from a resultSet
    //******************************************************************************************

    /**
     * create CourseHintEntityDto using data from resultSet
     *
     * @param resultSet containing data
     * @return CourseFaqEntityDto
     * @throws Exception -
     */
    public static CourseFaqEntityDto createDto(ResultSet resultSet) throws Exception {
        Integer courseId = resultSet.getInt("courseId");
        String intent = resultSet.getString("intent");
        String answer = resultSet.getString("answer");
        return new CourseFaqEntityDto(courseId, intent, answer);
    }
}
