package i5.las2peer.services.mwbService.database.dto;

import i5.las2peer.services.mwbService.database.dto.entity.CourseComponentEntityDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * dto with basic properties of a course component
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class CourseComponentDto extends CourseComponentEntityDto {
    private String componentType;
    private PrivacyInfoDto privacyInfo;
    private CourseComponentDataDto data;

    /**
     * default constructor
     */
    public CourseComponentDto() {}

    /**
     * constructor
     *
     * @param courseComponentEntityDto basic data as CourseComponentEntityDto
     * @param componentType as String
     * @param privacyInfo for course component (optional)
     * @param data for course component (optional)
     */
    public CourseComponentDto(CourseComponentEntityDto courseComponentEntityDto,
                              String componentType, PrivacyInfoDto privacyInfo, CourseComponentDataDto data) {
        super(courseComponentEntityDto);
        this.componentType = componentType;
        this.privacyInfo = privacyInfo;
        this.data = data;
    }

    /**
     * copy constructor (used to initialise derived classes)
     *
     * @param copy as CourseComponentDto
    */
    public CourseComponentDto(CourseComponentDto copy) {
        super(copy);
        this.componentType = copy.getComponentType();
        this.privacyInfo = copy.getPrivacyInfo();
        this.data = copy.getData();
    }
}
