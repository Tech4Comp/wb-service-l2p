package i5.las2peer.services.mwbService.database.dto.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Entity corresponding to database table for course components
 */
@Data
@AllArgsConstructor
public class CourseComponentEntityDto {
    private Integer courseId;
    private Integer componentTypeId;
    private Boolean allowedForAnonymous;

    /**
     * default constructor
     */
    public CourseComponentEntityDto() {}

    /**
     * copy constructor (used to initialise derived classes)
     *
     * @param copy - CourseEntity with data
     */
    public CourseComponentEntityDto(CourseComponentEntityDto copy) {
        this.courseId = copy.getCourseId();
        this.componentTypeId = copy.getComponentTypeId();
        this.allowedForAnonymous = copy.getAllowedForAnonymous();
    }
}
