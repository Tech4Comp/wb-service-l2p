package i5.las2peer.services.mwbService.database.dto.entity;


import lombok.AllArgsConstructor;
import lombok.Data;


/**
 * Entity dto corresponding to database table "courseTimelineRow"
 */
@Data
@AllArgsConstructor
public class CourseTimelineRowEntityDto {
    private Integer id;
    private Integer courseId;
    private Integer row;
    private String description;
    private Boolean isDetail;
    private Boolean createNewItems;

    /**
     * default constructor
     */
    public CourseTimelineRowEntityDto() {}
}
