package i5.las2peer.services.mwbService.database.dto.entity;


import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Entity dto corresponding to database table "genericChatbotIntents"
 * NOTE: this table is not yet part of the live database
 */
@Data
@AllArgsConstructor
public class GenericChatbotIntentsEntityDto {
    private String intent;
    private String answer;

    /**
     * default constructor
     */
    public GenericChatbotIntentsEntityDto() {}
}
