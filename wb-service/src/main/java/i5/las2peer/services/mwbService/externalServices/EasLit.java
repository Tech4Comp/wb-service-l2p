package i5.las2peer.services.mwbService.externalServices;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import i5.las2peer.services.mwbService.MwbService;
import i5.las2peer.services.mwbService.extFeedback.EasLitItem;
import i5.las2peer.services.mwbService.extFeedback.Knowledge;
import i5.las2peer.services.mwbService.extFeedback.Performance;
import i5.las2peer.services.mwbService.utilities.StringsUtilities;

import javax.ws.rs.core.Response;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.util.*;

import static i5.las2peer.services.mwbService.utilities.StringsUtilities.hasValue;


/**
 * Reading data for easLit items
 */
public class EasLit extends ExternalHttpService {
    private final String feedbackTopic;
    private final String projectId;
    private final Map<String, String> topicTitles;
    private final ObjectMapper objectMapper;

    /**
     * @param connection - database connection
     * @param feedbackTopic - topic to select the correct easlit items
     * @param projectId - easlit project id
     * @throws Exception with detailed info about failure
     */
    public EasLit(Connection connection, String feedbackTopic, String projectId) throws Exception {
        super(connection, MwbService.EXTERNAL_SERVICE_EASLIT);
        this.objectMapper = new ObjectMapper();
        if (StringsUtilities.hasNoValue(feedbackTopic))
            throw getServiceException(Response.Status.NOT_FOUND, "configuration for 'feedbackTopic'");
        this.feedbackTopic = feedbackTopic;
        if (StringsUtilities.hasNoValue(projectId))
            throw getServiceException(Response.Status.NOT_FOUND, "configuration for 'projectId'");
        this.projectId = projectId;
        this.topicTitles = getTopicTitles();
    }

    /**
     * @return map of easLitItems with the questionId as key, that are valid for this interventionId
     * @throws Exception with detailed info about failure
     */
    public Map<String, EasLitItem> getItems() throws Exception {
        Map<String, EasLitItem> items = new HashMap<>();
        try {
            List<String> itemIds = getItemIds();
            for (String id : itemIds) {
                EasLitItem newItem = getItemDetails(id);
                if (newItem != null) {
                    items.put(newItem.getQuestionId(), newItem);
                }
            }
        } catch (Exception e) {
            throw getServiceExceptionBadRequest("Failed to get items for "+ projectId +": "+e.getMessage());
        }
        return items;
    }

    private EasLitItem getItemDetails(String id) throws Exception {
        URL url = new URL(getUrl() + "item/remote/" + id);
        try (InputStream inputStream = url.openStream()) {
            Reader reader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
            JsonNode jsonItem = this.objectMapper.readTree(reader);
            String questionId = extractQuestionId(jsonItem);
            JsonNode annotations = jsonItem.get("annotations");
            ArrayList<String> topics = getTopicsForAnnotations(annotations);
            /* that seems to be a misuse of topics, should be possible to ignore
                if (!topics.contains(feedbackTopic))
                    return null;
             */
            if (hasValue(feedbackTopic))
                topics.remove(feedbackTopic); // remove topic used for selection (for compatibility)
            JsonNode firstBloom = annotations.get("bloom").get(0);
            Knowledge.Dimension knowledgeDimension = extractKnowledgeDimension(firstBloom);
            Performance.Level performanceLevel = extractPerformanceLevel(firstBloom);
            return new EasLitItem(questionId, knowledgeDimension, performanceLevel, topics);
        } catch (Exception e) {
            return null;
        }
    }

    private ArrayList<String> getTopicsForAnnotations(JsonNode jsonItem) {
        JsonNode jsonTopics = jsonItem.get("topics");
        ArrayList<String> topics = new ArrayList<>();
        if (jsonTopics == null)
            return topics;
        // map topic ids to titles
        for (JsonNode jsonTopic : jsonTopics) {
            String topicId = StringsUtilities.splitStringAndGetLastChunk(jsonTopic.asText(), "/");
            topics.add(topicTitles.get(topicId));
        }
        return topics;
    }

    private static String extractQuestionId(JsonNode jsonItem) {
        return StringsUtilities.splitStringAndGetLastChunk(jsonItem.get("remoteItemURL").asText(), "item=");
    }

    private static Knowledge.Dimension extractKnowledgeDimension(JsonNode jsonItem) {
        String knowledgeDimension = StringsUtilities.splitStringAndGetLastChunk(jsonItem.get("knowledgeDimension").asText(), "/");
        return Knowledge.Dimension.valueOf(knowledgeDimension.toUpperCase(Locale.ROOT));
    }

    private static Performance.Level extractPerformanceLevel(JsonNode jsonItem) {
        String performanceLevel = StringsUtilities.splitStringAndGetLastChunk(jsonItem.get("performanceLevel").asText(), "/");
        return Performance.Level.valueOf(performanceLevel.toUpperCase(Locale.ROOT));
    }

    private ArrayList<String> getItemIds() throws Exception {
        ArrayList<String> itemIds = new ArrayList<>();
        String projectBaseUrl = getUrl().replaceFirst("https", "http");
        URL url = new URL(getUrl() + "item/*?project="+projectBaseUrl+"project/" + projectId);
        try (InputStream inputStream = url.openStream()) {
            Reader reader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
            JsonNode jsonNode = this.objectMapper.readTree(reader);
            JsonNode graph = jsonNode.get("@graph");
            for (JsonNode jsonItem : graph) {
                String id = jsonItem.get("@id").asText();
                itemIds.add(StringsUtilities.splitStringAndGetLastChunk(id, "/"));
            }
        }
        return itemIds;
    }

    private Map<String, String> getTopicTitles() throws Exception {
        Map<String, String> topicTitles = new HashMap<>();
        URL url = new URL(getUrl()+"topic/*");
        try (InputStream inputStream = url.openStream()) {
            Reader reader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
            JsonNode jsonNode = this.objectMapper.readTree(reader);
            JsonNode graph = jsonNode.get("@graph");
            for (JsonNode jsonItem : graph) {
                String id = StringsUtilities.splitStringAndGetLastChunk(jsonItem.get("@id").asText(), "/");
                String title = jsonItem.get("title").asText();
                topicTitles.put(id, title);
            }
        } catch (Exception e) {
            throw getServiceExceptionBadRequest("Failed to read topic titles: "+ e.getMessage());
        }
        return topicTitles;
    }
}
