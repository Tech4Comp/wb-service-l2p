package i5.las2peer.services.mwbService.restApi;

import com.fasterxml.jackson.databind.JsonNode;
import com.openhtmltopdf.pdfboxout.PdfRendererBuilder;
import i5.las2peer.services.mwbService.Authentication;
import i5.las2peer.services.mwbService.MwbService;
import i5.las2peer.services.mwbService.database.LazyPostgresConnection;
import i5.las2peer.services.mwbService.database.dao.BaseCourseDao;
import i5.las2peer.services.mwbService.database.dao.ExamDao;
import i5.las2peer.services.mwbService.database.dao.entity.CourseExamEntityDao;
import i5.las2peer.services.mwbService.database.dto.ExamDto;
import i5.las2peer.services.mwbService.database.dto.ExamDtoForFeedback;
import i5.las2peer.services.mwbService.extFeedback.*;
import i5.las2peer.services.mwbService.externalServices.*;
import i5.las2peer.services.mwbService.externalServices.LMS.LmsConnection;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.apache.commons.io.FileUtils;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.util.*;

import static i5.las2peer.services.mwbService.externalServices.LrsConnection.*;
import static i5.las2peer.services.mwbService.utilities.JsonUtilities.getJsonNodeFromParsedJson;
import static i5.las2peer.services.mwbService.utilities.JsonUtilities.getJsonNodeWithValueForKey;

/**
 *  handles REST API calls related to the extended test feedback
 */
@Path("/examFeedback")
@RolesAllowed("authenticated")
public class ExamFeedbackApi {

    //******************************************************************************************
    // endpoints for authenticated users
    //******************************************************************************************

    /**
     * check settings for an exams
     * @param examId -
     * @return check result as json
     */
    @GET
    @Path("/check/{examId}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response checkTest(
            @PathParam("examId") int examId
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            ExamDtoForFeedback examDto = new ExamDao(connection.getConnection()).getExamDtoForFeedback(examId);
            Authentication.checkAuthorizationForCourse(connection.getConnection(), examDto.getCourseId());
            LmsConnection lmsConnection = new BaseCourseDao(connection.getConnection()).setupLmsConnection(examDto.getBaseCourseDto());
            lmsConnection.getTestResultIds(examDto, "");
            return MwbService.responseOK();
        }
        catch (Exception exception) {
            return MwbService.responseError(exception);
        }
    }

    /**
     * process onyx results for current user
     * calling this endpoint implies that the user has consented to processing test results
     * @param examId -
     * @return list with results as json if there are new results, empty string else
     */
    @POST
    @Path("/processTestResults/{examId}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getTestResultsApi(
            @PathParam("examId") int examId
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthenticated();
            LrsConnection lrsConnection = setupLrsConnectionForExamId(connection.getConnection(), examId);
            if (processTestResults(connection.getConnection(), lrsConnection, examId, Authentication.getUserAgentEmail())) {
                String response = getResultInfoFromLrs(lrsConnection, examId, Authentication.pseudonymizeUser(connection.getConnection()));
                return MwbService.responseOK(response);
            }
            return MwbService.responseOK("");
        }
        catch (Exception exception) {
            return MwbService.responseError(exception);
        }
    }


    /**
     * Returns info stored in LRS about available results for the exam for the current user
     *
     * @param examId -
     * @return list of resultInfo stored in LRS
     */
    @GET
    @Path("/getResultInfoFromLrs/{examId}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getResultInfoFromLrsApi(
            @PathParam("examId") int examId
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthenticated();
            LrsConnection lrsConnection = setupLrsConnectionForExamId(connection.getConnection(), examId);
            String response = getResultInfoFromLrs(lrsConnection, examId, Authentication.pseudonymizeUser(connection.getConnection()));
            return MwbService.responseOK(response);
        }
        catch (Exception exception) {
            return MwbService.responseError(exception);
        }
    }


    /**
     * Returns exam feedback for the result of the exam for the current user
     *
     * @param examId -
     * @param resultId for the result, the feedback is based on
     * @return feedback file as base64 String
     */
    @GET
    @Path("/{examId}/{resultId}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getExamFeedbackForResultId(
        @PathParam("examId") int examId,
        @PathParam("resultId") String resultId
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthenticated();
            String actor = Authentication.pseudonymizeUser(connection.getConnection());
            LrsConnection lrsConnection = setupLrsConnectionForExamId(connection.getConnection(), examId);
            String feedbackBase64 = getExamFeedbackForResultId(lrsConnection, examId, resultId, actor);
            trackViewingFeedback(lrsConnection, examId, resultId, actor);
            return MwbService.responseOK(feedbackBase64);
        }
        catch (Exception exception) {
            return MwbService.responseError(exception);
        }
    }

    /**
     * Creates exam feedback for the result of the exam for the current user, if not yet stored in LRS
     * calling this endpoint implies that the user has consented to processing
     * the results for the specified resultId
     *
     * @param examId -
     * @param resultId for the result, the feedback is based on
     * @return OK on success, Bad request with error message else
     */
    @POST
    @Path("/{examId}/{resultId}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response createExamFeedbackForResultIdApi(
            @PathParam("examId") int examId,
            @PathParam("resultId") String resultId
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthenticated();
            String pseudonymizedUser = Authentication.pseudonymizeUser(connection.getConnection());
            createExamFeedbackForResultId(connection.getConnection(), examId, resultId, pseudonymizedUser);
            return MwbService.responseOK();
        }
        catch (Exception exception) {
            return MwbService.responseError(exception);
        }
    }

    //******************************************************************************************
    // private methods
    //******************************************************************************************
    private static LrsConnection setupLrsConnectionForExamId(Connection connection, int examId) throws Exception {
        int courseId = new CourseExamEntityDao(connection, examId).getCourseId();
        return new LrsConnection(connection, courseId);
    }

    private static String getTempDirForProcessingResult(String resultId) {
        return MwbService.TEMP_DIR+"exam_%s/".formatted(resultId);
    }

    private static String getResultInfoFromLrs(LrsConnection lrsConnection, int examId, String user) throws Exception {
        String basicResultInfo = getResultIdsStoredInLrs(lrsConnection, examId, user).toString();
        String hasFeedback = getExtFeedbackStoredInLrs(lrsConnection, examId, user).toString();
        return """
            {
                "basicResultInfo": %s,
                "hasFeedback": %s
            }
            """.formatted(basicResultInfo, hasFeedback);
    }

    private static JSONArray getExtFeedbackStoredInLrs (LrsConnection lrsConnection, int examId, String user) throws Exception {
        Integer courseId = lrsConnection.courseId;
        String match = matchStatementForExamFeedback(courseId, examId, user);
        String project = """
            {"$project": {\
                "resultId": "$statement.context.extensions.%s.resultId",\
                "_id": 0\
            }}\
            """.formatted(maskXApiUri(xApiUriForActivityData));
        String query = combineMatchAndProject(match, project);
        return lrsConnection.query(query);
    }

    private static JSONArray getResultIdsStoredInLrs(LrsConnection lrsConnection, int examId, String actor) throws Exception {
        String xApiUri = xApiStatementBaseUrlMwb;
        String maskedXApiUri = maskXApiUri(xApiUri);
        String maskedDataUri = maskXApiUri(xApiUriForActivityData);
        Integer courseId = lrsConnection.courseId;
        String query = """
                [\
                {"$match": {"$and": [\
                    {"statement.actor.account.name": "%s"},\
                    {"statement.verb.id": "%s/verb/generated"},\
                    {"statement.object.id": "%s/object/test_result_info"},\
                    {"statement.object.definition.extensions.%s/object/course.id": %d},\
                    {"statement.context.extensions.%s.examId": %d}\
                ]}},\
                {"$project": {\
                    "resultInfo": "$statement.context.extensions.%s",\
                    "_id": 0\
                }},\
                {"$sort": {\
                    "resultInfo.ended": 1\
                }}]\
                """.formatted(actor, xApiUri, xApiUri, maskedXApiUri, courseId, maskedDataUri, examId, maskedDataUri);
        return lrsConnection.query(query);
    }

    private static boolean hasExamFeedbackForResultId(LrsConnection lrsConnection, int examId, String resultId, String actor) throws Exception {
        Integer courseId = lrsConnection.courseId;
        String match = matchStatementForExamFeedback(courseId, examId, resultId, actor);
        String project = """
            {"$project": {\
                "stored": "$statement.stored",\
                "_id": 0\
            }}\
            """;
        String query = combineMatchAndProject(match, project);
        JSONArray result = lrsConnection.query(query);
        return !result.isEmpty();
    }

    private static String getExamFeedbackForResultId(LrsConnection lrsConnection, int examId, String resultId, String actor) throws Exception {
        Integer courseId = lrsConnection.courseId;
        String match = matchStatementForExamFeedback(courseId, examId, resultId, actor);
        String project = """
            {"$project": {\
                "examFeedback": "$statement.context.extensions.%s.feedbackBase64",\
                "_id": 0\
            }}\
            """.formatted(maskXApiUri(xApiUriForActivityData));
        String query = combineMatchAndProject(match, project);
        JSONArray result = lrsConnection.query(query);
        return result.isEmpty() ? "" : ((JSONObject) result.get(0)).getAsString("examFeedback");
    }

    private static void trackViewingFeedback(LrsConnection lrsConnection, int examId, String resultKey, String actor) throws Exception {
        String verb = "viewed";
        String object = "extended_feedback_for_test_result";
        String activity = """
                {\
                    "examId": %d,\
                    "resultId": "%s"\
                }\
                """.formatted(examId, resultKey);
        TrackApi.trackToLrs(lrsConnection, actor, verb, object, activity);
    }

    private static Boolean processTestResults(Connection connection, LrsConnection lrsConnection, int examId, String userMail) throws Exception {
        ExamDtoForFeedback examDto = new ExamDao(connection).getExamDtoForFeedback(examId);
        LmsConnection lmsConnection = new BaseCourseDao(connection).setupLmsConnection(examDto.getBaseCourseDto());
        List<LmsConnection.LmsResult> results = lmsConnection.getTestResultIds(examDto, userMail);
        if (results.isEmpty())
            return false;
        boolean hasNewResults = false;
        String user = Authentication.pseudonymize(connection, userMail);
        List<String> storedResultIds = extractResultIdsFromJson(getResultIdsStoredInLrs(lrsConnection, examId, user));
        for (LmsConnection.LmsResult result : results) {
            if (storedResultIds.stream().noneMatch(storedId -> storedId.equals(result.getId()))) {
                hasNewResults = true;
                generateResultInfoEntryForLrs(lrsConnection, examId, result, user);
            }
        }
        return hasNewResults;
    }

    private static List<String> extractResultIdsFromJson(JSONArray jsonArray) {
        List<String> list = new ArrayList<>(jsonArray.size());
        for (Object item: jsonArray) {
            JSONObject resultInfo = (JSONObject) ((JSONObject) item).get("resultInfo");
            list.add(resultInfo.getAsString("resultId"));
        }
        return list;
    }

    private static void generateResultInfoEntryForLrs(LrsConnection lrsConnection, int examId, LmsConnection.LmsResult result, String user) throws Exception {
        String verb = "generated";
        String object = "test_result_info";
        String context = """
                {\
                "extensions": {\
                    "%s": {\
                        "examId": %d,\
                        "resultId": "%s",\
                        "ended": "%s",\
                        "state": "%s",\
                        "scored": %s,\
                        "maxScore": %s,\
                        "score": %s
                    }}}\
                """.formatted(xApiUriForActivityData, examId, result.getId(), result.getEndDate().toString(), result.getState(), result.getScored(), result.getMaxScore(), result.getScore());
        lrsConnection.postStatement(user, verb, object, context, "Failed to generate LRS entry for resultKey");
    }

    private static void createExamFeedbackForResultId(Connection connection, int examId, String resultId, String user) throws Exception {
        ExamDtoForFeedback examDto = new ExamDao(connection).getExamDtoForFeedback(examId);
        LrsConnection lrsConnection = new LrsConnection(connection, examDto.getCourseId());
        if (hasExamFeedbackForResultId(lrsConnection, examId, resultId, user))
            return;

        File destinationDirectory = new File(getTempDirForProcessingResult(resultId));
        if (!destinationDirectory.mkdir())
            throw new ServiceException("extFB", Response.Status.BAD_REQUEST, "already processing");

        try {
            LmsConnection lmsConnection = new BaseCourseDao(connection).setupLmsConnection(examDto.getBaseCourseDto());
            JsonNode assessmentResult = lmsConnection.getResultData(destinationDirectory, examDto, resultId);
            String easlitProjectId = examDto.getEasLitExamDto().getEaslitProjectId();
            String feedbackTopic = examDto.getEasLitExamDto().getTopic();
            Map<String, EasLitItem> easLitItems = new EasLit(connection, feedbackTopic, easlitProjectId).getItems();
            ExtFeedbackStatistics statistics = generateExtTestFeedbackStatistics(assessmentResult, easLitItems);
            createPdfFeedbackFile(lrsConnection, statistics, examDto, resultId, user);
        } finally {
            cleanUpLocalFilesForCreatingFeedback(resultId);
        }
    }

    private static void cleanUpLocalFilesForCreatingFeedback(String resultId) {
        File destinationDirectory = new File(getTempDirForProcessingResult(resultId));
        try {
            FileUtils.deleteDirectory(destinationDirectory);
        } catch (Exception e) {
            MwbService.logger.info("failed to delete temporary directory for user result after creating extended exam feedback: "+e.getMessage());
        }
    }

    private static void createPdfFeedbackFile(LrsConnection lrsConnection, ExtFeedbackStatistics statistics, ExamDto examDto, String resultId, String user) throws Exception {
        String template = Files.readString(Paths.get(MwbService.RESOURCE_DIR +"examFeedbackTemplate.html"), StandardCharsets.UTF_8);
        String topicTemplate = Files.readString(Paths.get(MwbService.RESOURCE_DIR + "examFeedbackTopicTemplate.html"), StandardCharsets.UTF_8);
        String pdfFilename = getTempDirForProcessingResult(resultId) + "extendedExamFeedback.pdf";

        StringBuilder topicsHtml = new StringBuilder();
        for (String topic : statistics.getTopics()) {
            topicsHtml.append(insertDataForTopic(topicTemplate, statistics, topic));
        }
        template = insertDataToTemplate(template, "topicsHtml", topicsHtml.toString());
        template = insertDataForKnowledge(template, statistics);
        template = insertDataForPerformance(template, statistics);

        try (FileOutputStream outputStream = new FileOutputStream(pdfFilename)) {
            PdfRendererBuilder builder = new PdfRendererBuilder();
            builder.useFastMode();
            builder.withHtmlContent(template, "");
            builder.toStream(outputStream);
            builder.run();
            byte[] byteArray = Base64.getEncoder().encode(Files.readAllBytes(Paths.get(pdfFilename)));
            postPdfToLrs(lrsConnection, new String(byteArray, StandardCharsets.UTF_8), examDto, resultId, user);
        } finally {
            File pdfFile = new File(pdfFilename);
            if (!pdfFile.delete()) {
                MwbService.logger.info("Failed to delete Pdf feedback file for " + resultId + " from the output folder");
            }
        }
    }

    private static void postPdfToLrs(LrsConnection lrsConnection, String pdfBase64, ExamDto examDto, String resultId, String user) throws Exception {
        String verb = "generated";
        String object = "extended_feedback_for_test_result";
        String context = """
            {\
            "extensions": {\
                 "%s": {\
                     "examId": %d,\
                     "resultId": "%s",\
                     "feedbackBase64": "%s"\
                 }\
            }}\
            """.formatted(xApiUriForActivityData, examDto.getId(), resultId, pdfBase64);
        lrsConnection.postStatement(user, verb, object, context, "Failed to generate LRS entry with extended feedback");
    }

    private static String insertDataToTemplate(String template, Map<String, String> map) {
        for (Map.Entry<String, String> entry: map.entrySet()) {
            template = insertDataToTemplate(template, entry.getKey(), entry.getValue());
        }
        return template;
    }

    private static String insertDataToTemplate(String template, String key, String value) {
        return template.replace("{" + key + "}", value);
    }

    private static String insertDataForTopic(String template, ExtFeedbackStatistics statistics, String topic) {
        CorrectAnswersStatistics topicStatistics = statistics.getTopicStatistics(topic);
        template = insertDataToTemplate(template,"titleTOPIC", topic);
        template = insertDataForCategory(template, "TOPIC", topicStatistics);
        return template;
    }

    private static String insertDataForKnowledge(String template, ExtFeedbackStatistics statistics) {
        for (Knowledge.Dimension dimension : Knowledge.Dimension.values()) {
            CorrectAnswersStatistics knowledgeStatistics = statistics.getKnowledgeStatistics(dimension);
            String category = dimension.toString();
            template = insertDataForCategory(template, category, knowledgeStatistics);
        }
        return template;
    }

    private static String insertDataForPerformance(String template, ExtFeedbackStatistics statistics) {
        for (Performance.Level level : Performance.Level.values()) {
            CorrectAnswersStatistics performanceStatistics = statistics.getPerformanceStatistics(level);
            String category = level.toString();
            template = insertDataForCategory(template, category, performanceStatistics);
        }
        return template;
    }

    private static String insertDataForCategory(String template, String category, CorrectAnswersStatistics statistics) {
        Map<String, String> map = new HashMap<>();
        map.put("cntAnswers"+category, statistics.getCntAnswers().toString());
        map.put("correctScore"+category, String.format("%.2f", statistics.getCorrect()));
        map.put("maximumScore"+category, String.format("%.2f", statistics.getMaximumScore()));
        map.put("correctPercentage"+category, String.format("%d", Math.round(statistics.getCorrectPercentage())));
        return insertDataToTemplate(template, map);
    }

    private static ExtFeedbackStatistics generateExtTestFeedbackStatistics(JsonNode assessmentResult, Map<String, EasLitItem> easLitItems) {
        ExtFeedbackStatistics statistics = new ExtFeedbackStatistics();
        JsonNode itemResults = getJsonNodeFromParsedJson(assessmentResult, "itemResult");
        for (JsonNode itemResult : itemResults) {
            addItemResultToStatistics(statistics, itemResult, easLitItems);
        }
        return statistics;
    }

    private static double getValueFromOutcomeVariables(JsonNode outcome, String key) {
        double defaultResult = 0.0;
        JsonNode json = getJsonNodeWithValueForKey(outcome, "identifier", key);
        if (json == null)
            return defaultResult;
        JsonNode valueJson = json.get("value");
        String valueAsString = valueJson == null ? null : valueJson.get("value").asText();
        return valueAsString == null ? defaultResult : Double.parseDouble(valueAsString);
    }

    private static void addItemResultToStatistics(ExtFeedbackStatistics statistics, JsonNode itemResult, Map<String, EasLitItem> easLitItems) {
        String id = itemResult.get("identifier").asText().split("_")[0];
        JsonNode outcome = getJsonNodeFromParsedJson(itemResult, "outcomeVariable");
        double maxScore = 1.0;
        double score = 0.0;
        if (outcome != null) {
            maxScore = getValueFromOutcomeVariables(outcome, "MAXSCORE");
            if (maxScore == 0) { // questions not meant to be scored
                return;
            }
            score = getValueFromOutcomeVariables(outcome, "SCORE");
        }
        EasLitItem easLitItem = easLitItems.get(id);
        if (easLitItem == null) { // todo: decide how to handle questions without annotation in easlit
            System.out.println("extFB: no easLitItem for question id " + id);
            return;
        }
        //System.out.println(id+" "+score+" "+maxScore);
        statistics.sumUpStatistics(easLitItem.getKnowledgeType(), easLitItem.getPerformanceLevel(), easLitItem.getTopics(), score, maxScore);
    }




    //******************************************************************************************
    // some strings to query LRS
    //******************************************************************************************

    private static String combineMatchAndProject(String match, String project) {
        return "[" + match + ", " + project + "]";
    }

    private static String matchStatementForExamFeedback(Integer courseId, int examId, String resultId, String actor) {
        String xApiUri = xApiStatementBaseUrlMwb;
        String maskedXApiUri = maskXApiUri(xApiUri);
        String maskedDataUri = maskXApiUri(xApiUriForActivityData);
        return """
            {"$match": {"$and": [\
                {"statement.actor.account.name": "%s"},\
                {"statement.verb.id": "%s/verb/generated"},\
                {"statement.object.id": "%s/object/extended_feedback_for_test_result"},\
                {"statement.object.definition.extensions.%s/object/course.id": %d},\
                {"statement.context.extensions.%s.examId": %d},\
                {"statement.context.extensions.%s.resultId": "%s"}\
            ]}}\
            """.formatted(actor, xApiUri,xApiUri, maskedXApiUri, courseId, maskedDataUri, examId, maskedDataUri, resultId);
    }
    private static String matchStatementForExamFeedback(Integer courseId, int examId, String actor) {
        String xApiUri = xApiStatementBaseUrlMwb;
        String maskedXApiUri = maskXApiUri(xApiUri);
        String maskedDataUri = maskXApiUri(xApiUriForActivityData);
        return """
            {"$match": {"$and": [\
                {"statement.actor.account.name": "%s"},\
                {"statement.verb.id": "%s/verb/generated"},\
                {"statement.object.id": "%s/object/extended_feedback_for_test_result"},\
                {"statement.object.definition.extensions.%s/object/course.id": %d},\
                {"statement.context.extensions.%s.examId": %d}\
            ]}}\
            """.formatted(actor, xApiUri, xApiUri, maskedXApiUri,  courseId, maskedDataUri, examId);
    }
}
