package i5.las2peer.services.mwbService.utilities.xml;

import com.fasterxml.jackson.databind.JsonNode;
import org.xml.sax.InputSource;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.StringReader;

/**
 *
 */
public class XmlParser {

    /**
     * @param xml -
     * @return -
     * @throws Exception -
     */
    public static JsonNode parseToJsonNode(String xml) throws Exception {
        JsonNode jsonResult;
        try {
            SAXParser saxParser = SAXParserFactory.newInstance().newSAXParser();
            ParseXmlToJsonHandler handler = new ParseXmlToJsonHandler();
            StringReader reader = new StringReader(xml);
            saxParser.parse(new InputSource(reader), handler);
            jsonResult = handler.getResultsAsJsonNode();
        } catch (Exception e) {
            throw new Exception("failed to parse xml into json");
        }

        return jsonResult;
    }
}


