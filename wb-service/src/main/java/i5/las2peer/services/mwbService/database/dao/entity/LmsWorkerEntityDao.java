package i5.las2peer.services.mwbService.database.dao.entity;

import i5.las2peer.services.mwbService.database.dao.DataAccessObject;
import i5.las2peer.services.mwbService.database.dto.entity.LmsWorkerEntityDto;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Entity dao to handle basic database operations for table "lmsWorker"
 */
public class LmsWorkerEntityDao extends DataAccessObject {
    private final LmsWorkerEntityDto lmsWorkerEntityDto;

    /**
     * Table name for lms worker data
     */
    public static final String TABLE = "lmsWorker";

    /**
     * constructor
     * @param connection to the database
     * @param lmsWorkerEntityDto -
     */
    public LmsWorkerEntityDao(Connection connection, LmsWorkerEntityDto lmsWorkerEntityDto) {
        super(connection);
        this.lmsWorkerEntityDto = lmsWorkerEntityDto;
    }


    //******************************************************************************************
    // basic database operations
    //******************************************************************************************

    /**
     * insert a new record into the database
     *
     * @return id of the new record
     * @throws Exception if something went wrong
     */
    public int insert() throws Exception {
        String sqlStatement = "INSERT INTO " + TABLE + " (lms, shortname, username, password) VALUES (?, ?, ?, ?)" +
                " RETURNING id;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setString(1, lmsWorkerEntityDto.getLms());
            preparedStatement.setString(2, lmsWorkerEntityDto.getShortname());
            preparedStatement.setString(3, lmsWorkerEntityDto.getUsername());
            preparedStatement.setString(4, lmsWorkerEntityDto.getPassword());
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return resultSet.getInt("id");
        } catch (SQLException e) {
            throw DataAccessObject.createDatabaseException("Failed to insert lms worker.", e);
        }
        throw DataAccessObject.createDatabaseException("Failed to retrieve id of inserted lms worker.", null);
    }

    /**
     * update record
     *
     * @throws Exception if something went wrong
     */
    public void update() throws Exception {
        String sqlStatement = "UPDATE " + TABLE + " SET shortname=?, username=?, password=?" +
                " WHERE id=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setString(1, lmsWorkerEntityDto.getShortname());
            preparedStatement.setString(2, lmsWorkerEntityDto.getUsername());
            preparedStatement.setString(3, lmsWorkerEntityDto.getPassword());
            preparedStatement.setInt(4, lmsWorkerEntityDto.getId());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to update course.", e);
        }
    }


    //******************************************************************************************
    // static methods for database operations
    //******************************************************************************************

    public static LmsWorkerEntityDto readData(Connection connection, Integer id) throws Exception {
        String sqlStatement = "SELECT * FROM " + TABLE   + " WHERE id=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return createLmsWorkerEntityDto(resultSet);
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve data for lms worker " + id + ".", e);
        }
        return null;
    }

    /**
     * get a list with all LMS worker accounts in the database (NO password!)
     *
     * @return list with id, lms, shortname and username for workers
     * @throws Exception on failure
     */
    public static List<LmsWorkerEntityDto> getLmsWorkerListWithoutCredentials(Connection connection) throws Exception {
        String sqlStatement = "SELECT id, lms, shortname, username, '' as password FROM " + TABLE + ";";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            ResultSet resultSet = preparedStatement.executeQuery();

            List<LmsWorkerEntityDto> lmsWorkerList = new ArrayList<>();
            while (resultSet.next()) {
                LmsWorkerEntityDto lmsWorker = LmsWorkerEntityDao.createLmsWorkerEntityDto(resultSet);
                lmsWorkerList.add(lmsWorker);
            }
            return lmsWorkerList;
        } catch (SQLException e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve list of lms workers from database.", e);
        }
    }

    /**
     * delete record with given id
     *
     * @param connection to database
     * @param id of the record to delete
     * @throws Exception -
     */
    public static void delete(Connection connection, Integer id) throws Exception {
        String sqlStatement = "DELETE FROM " + TABLE +
                " WHERE id=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to delete lms worker.", e);
        }
    }


    //******************************************************************************************
    // static methods to create lms worker related DTOs from a resultSet
    //******************************************************************************************

    /**
     * create CourseEntityDto using data from resultSet
     *
     * @param resultSet containing data
     * @return LmsWorkerEntityDto
     * @throws Exception -
     */
    public static LmsWorkerEntityDto createLmsWorkerEntityDto(ResultSet resultSet) throws Exception {
        Integer id = resultSet.getInt("id");
        String lms = resultSet.getString("lms");
        String shortname = resultSet.getString("shortname");
        String username = resultSet.getString("username");
        String password = resultSet.getString("password");
        return new LmsWorkerEntityDto(id, lms, shortname, username, password);
    }
}
