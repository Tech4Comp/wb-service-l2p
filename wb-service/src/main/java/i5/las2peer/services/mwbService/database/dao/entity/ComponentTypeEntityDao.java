package i5.las2peer.services.mwbService.database.dao.entity;


import i5.las2peer.services.mwbService.database.dao.DataAccessObject;
import i5.las2peer.services.mwbService.database.dto.entity.ComponentTypeEntityDto;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Entity dao to handle basic database operations for table "componentType"
 */
public class ComponentTypeEntityDao extends DataAccessObject {
    private final ComponentTypeEntityDto componentTypeEntityDto;

    /**
     * Table name for data
     */
    public static final String TABLE = "componentType";

    /**
     * constructor
     * @param connection to the database
     * @param componentTypeEntityDto -
     */
    public ComponentTypeEntityDao(Connection connection, ComponentTypeEntityDto componentTypeEntityDto) {
        super(connection);
        this.componentTypeEntityDto = componentTypeEntityDto;
    }


    //******************************************************************************************
    // static methods for database operations
    //******************************************************************************************

    public static ComponentTypeEntityDto readData(Connection connection, Integer id) throws Exception {
        String sqlStatement = "SELECT * FROM " + TABLE + " WHERE id=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return createDto(resultSet);
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve data for component type.", e);
        }
        return null;
    }

    public static List<ComponentTypeEntityDto> readAll(Connection connection) throws Exception {
        String sqlStatement = "SELECT * FROM " + TABLE + ";";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            List<ComponentTypeEntityDto> componentDtoList = new ArrayList<>();
            while (resultSet.next())
                componentDtoList.add(createDto(resultSet));
            return componentDtoList;
        } catch (SQLException e) {
            throw DataAccessObject.createDatabaseException("Failed to select component types from database.", e);
        }
    }

    //******************************************************************************************
    // static method to create DTO from a resultSet
    //******************************************************************************************

    /**
     * create ComponentTypeEntityDto using data from resultSet
     *
     * @param resultSet containing data
     * @return PrivacyStatementEntityDto
     * @throws Exception -
     */
    public static ComponentTypeEntityDto createDto(ResultSet resultSet) throws Exception {
        Integer id = resultSet.getInt("id");
        String type = resultSet.getString("type");
        boolean supportsAnonymousView = resultSet.getBoolean("supportsAnonymousView");
        boolean supportsComponentPrivacy = resultSet.getBoolean("supportsComponentPrivacy");
        return new ComponentTypeEntityDto(id, type, supportsAnonymousView, supportsComponentPrivacy);
    }
}
