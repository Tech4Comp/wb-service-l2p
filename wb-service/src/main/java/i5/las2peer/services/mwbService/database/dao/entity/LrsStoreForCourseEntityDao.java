package i5.las2peer.services.mwbService.database.dao.entity;


import i5.las2peer.services.mwbService.Credentials;
import i5.las2peer.services.mwbService.database.dao.DataAccessObject;
import i5.las2peer.services.mwbService.database.dto.entity.LrsStoreForCourseEntityDto;

import java.sql.*;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;

/**
 * Entity dao to handle basic database operations for table "lrsStoreForCourse"
 */
public class LrsStoreForCourseEntityDao extends DataAccessObject {
    private final LrsStoreForCourseEntityDto lrsStoreForCourseEntityDto;

    /**
     * Table name for course data
     */
    public static final String TABLE = "lrsStoreForCourse";

    /**
     * field list for copy course data
     */
    public static final List<String> copyFieldList = Arrays.asList("clientKey", "clientSecret");

    /**
     * constructor
     * @param connection to the database
     * @param lrsStoreForCourseEntityDto -
     */
    public LrsStoreForCourseEntityDao(Connection connection, LrsStoreForCourseEntityDto lrsStoreForCourseEntityDto) {
        super(connection);
        this.lrsStoreForCourseEntityDto = lrsStoreForCourseEntityDto;
    }

    /**
     * constructor
     *
     * @param connection to the database
     * @param courseId -
     * @throws Exception if requested connection could not be established
     */
    public LrsStoreForCourseEntityDao(Connection connection, Integer courseId) throws Exception {
        super(connection);
        this.lrsStoreForCourseEntityDto = readData(connection, courseId) ;
    }


    //******************************************************************************************
    // instance methods
    //******************************************************************************************

    public Credentials getCredentials() throws Exception {
        assertNotNull();
        return new Credentials(lrsStoreForCourseEntityDto.getClientKey(), lrsStoreForCourseEntityDto.getClientSecret());
    }

    public void assertNotNull() throws Exception {
        if (lrsStoreForCourseEntityDto == null)
            throw getInvalidDataException("No lrs data found.");
    }


    //******************************************************************************************
    // basic database operations
    //******************************************************************************************

    /**
     * insert a new record / update existing record to the database
     *
     * @throws Exception if something went wrong
     */
    public void upsert() throws Exception {
        String sqlStatement = "INSERT INTO " + TABLE + " (courseId, clientKey, clientSecret) VALUES(?, ?, ?)"+
                " ON CONFLICT ON CONSTRAINT " + TABLE + "_pKey DO UPDATE" +
                " SET clientKey=?, clientSecret=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            Timestamp now = Timestamp.from(Instant.now());
            preparedStatement.setInt(1, lrsStoreForCourseEntityDto.getCourseId());
            preparedStatement.setString(2, lrsStoreForCourseEntityDto.getClientKey());
            preparedStatement.setString(3, lrsStoreForCourseEntityDto.getClientSecret());
            preparedStatement.setString(4, lrsStoreForCourseEntityDto.getClientKey());
            preparedStatement.setString(5, lrsStoreForCourseEntityDto.getClientSecret());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw DataAccessObject.createDatabaseException("Failed to insert/update lrs data for course.", e);
        }
    }


    //******************************************************************************************
    // static methods for database operations
    //******************************************************************************************

    public static LrsStoreForCourseEntityDto readData(Connection connection, Integer courseId) throws Exception {
        String sqlStatement = "SELECT * FROM " + TABLE   + " WHERE courseId=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, courseId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return createDto(resultSet);
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve lrs data for course.", e);
        }
        return null;
    }

    public static boolean hasLrsData(Connection connection, Integer courseId) throws Exception {
        return readData(connection, courseId) != null;
    }


    //******************************************************************************************
    // static method to create DTO from a resultSet
    //******************************************************************************************

    /**
     * create dto using data from resultSet
     *
     * @param resultSet containing data
     * @return LrsStoreForCourseEntityDto
     * @throws Exception -
     */
    public static LrsStoreForCourseEntityDto createDto(ResultSet resultSet) throws Exception {
        Integer courseId = resultSet.getInt("courseId");
        String clientKey = resultSet.getString("clientKey");
        String clientSecret = resultSet.getString("clientSecret");
        return new LrsStoreForCourseEntityDto(courseId, clientKey, clientSecret);
    }
}
