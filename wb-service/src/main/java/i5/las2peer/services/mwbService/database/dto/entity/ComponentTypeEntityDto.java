package i5.las2peer.services.mwbService.database.dto.entity;


import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Entity dto corresponding to database table "componentType"
 */
@Data
@AllArgsConstructor
public class ComponentTypeEntityDto {
    private Integer id;
    private String type;
    private Boolean supportsAnonymousView;
    private Boolean supportsComponentPrivacy;

    /**
     * default constructor
     */
    public ComponentTypeEntityDto() {}
}
