package i5.las2peer.services.mwbService.database.dao.entity;

import i5.las2peer.services.mwbService.database.dao.DataAccessObject;
import i5.las2peer.services.mwbService.database.dto.CourseComponentDataDto;
import i5.las2peer.services.mwbService.database.dto.entity.CourseWritingTasksEntityDto;

import java.sql.*;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;


/**
 * Dao to handle basic database operations for table with data for course component WRITINGTASKS
 */
public class CourseWritingTasksEntityDao extends CourseComponentDataDao {
    private final CourseWritingTasksEntityDto courseWritingTasksEntityDto;

    /**
     * Table name for entity
     */
    public static final String TABLE = "courseWritingTasks";

    /**
     * field list for copy course data
     */
    public static final List<String> copyFieldList = Arrays.asList("validFrom", "validUntil", "timelineRowId", "movable");

    /**
     * field list to fill with validFrom/validUntil from new course for copy course data
     */
    public static final List<String> keepFieldList = Arrays.asList("validFrom", "validUntil");

    /**
     * constructor
     *
     * @param connection to the database
     * @param courseWritingTasksEntityDto -
     */
    public CourseWritingTasksEntityDao(Connection connection, CourseWritingTasksEntityDto courseWritingTasksEntityDto) {
        super(connection);
        this.courseWritingTasksEntityDto = courseWritingTasksEntityDto;
    }

    /**
     * constructor
     *
     * @param connection to the database
     * @param courseId -
     * @throws Exception if requested connection could not be established
     */
    public CourseWritingTasksEntityDao(Connection connection, Integer courseId) throws Exception {
        super(connection);
        this.courseWritingTasksEntityDto = readData(connection, courseId) ;
    }


    //******************************************************************************************
    // basic database operations
    //******************************************************************************************

    /**
     * insert a new record / update existing record to the database
     *
     * @throws Exception if something went wrong
     */
    public void upsert() throws Exception {
        String sqlStatement = "INSERT INTO " + TABLE + " (courseId, validFrom, validUntil, timelineRowId, movable) VALUES(?, ?, ?, ?, ?)"+
                " ON CONFLICT ON CONSTRAINT " + TABLE + "_pKey DO UPDATE" +
                " SET validFrom=?, validUntil=?, timelineRowId=?, movable=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, courseWritingTasksEntityDto.getCourseId());
            preparedStatement.setTimestamp(2, courseWritingTasksEntityDto.getValidFrom());
            preparedStatement.setTimestamp(6, courseWritingTasksEntityDto.getValidFrom());
            preparedStatement.setTimestamp(3, courseWritingTasksEntityDto.getValidUntil());
            preparedStatement.setTimestamp(7, courseWritingTasksEntityDto.getValidUntil());
            if (Optional.ofNullable(courseWritingTasksEntityDto.getTimelineRowId()).orElse(0) == 0) {
                preparedStatement.setNull(4, Types.INTEGER);
                preparedStatement.setNull(8, Types.INTEGER);
            } else {
                preparedStatement.setInt(4, courseWritingTasksEntityDto.getTimelineRowId());
                preparedStatement.setInt(8, courseWritingTasksEntityDto.getTimelineRowId());
            }
            if (courseWritingTasksEntityDto.getMovable() == null) {
                preparedStatement.setNull(5, Types.BOOLEAN);
                preparedStatement.setNull(9, Types.BOOLEAN);
            } else {
                preparedStatement.setBoolean(5, courseWritingTasksEntityDto.getMovable());
                preparedStatement.setBoolean(9, courseWritingTasksEntityDto.getMovable());
            }
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw DataAccessObject.createDatabaseException("Failed to insert/update data for course component WRITINGTASKS.", e);
        }
    }


    //******************************************************************************************
    // static methods for database operations
    //******************************************************************************************

    public static CourseWritingTasksEntityDto readData(Connection connection, Integer courseId) throws Exception {
        String sqlStatement = "SELECT * FROM " + TABLE   + " WHERE courseId=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, courseId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return createDto(resultSet);
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve data for component WRITINGTASKS for course " + courseId + " from database.", e);
        }
        return null;
    }

    /**
     * delete record with given id
     *
     * @param connection to database
     * @param courseId of the record to delete
     * @throws Exception -
     */
    public static void delete(Connection connection, Integer courseId) throws Exception {
        CourseComponentDataDao.delete(connection, TABLE, courseId);
    }


    //******************************************************************************************
    // static methods to create DTOs from a resultSet
    //******************************************************************************************

    /**
     * create dto using data from resultSet
     *
     * @param resultSet containing data
     * @return ChatbotEntityDto
     * @throws Exception -
     */
    public static CourseWritingTasksEntityDto createDto(ResultSet resultSet) throws Exception {
        CourseComponentDataDto courseComponentDataDto = CourseComponentDataDao.createFromResultSet(resultSet);
        Timestamp validFrom = resultSet.getTimestamp("validFrom");
        Timestamp validUntil = resultSet.getTimestamp("validUntil");
        Integer timelineRowId = resultSet.getInt("timelineRowId");
        Boolean movable = resultSet.getBoolean("movable");
        return new CourseWritingTasksEntityDto(courseComponentDataDto, validFrom, validUntil, timelineRowId, movable);
    }
}
