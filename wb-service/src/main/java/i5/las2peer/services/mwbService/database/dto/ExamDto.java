package i5.las2peer.services.mwbService.database.dto;

import i5.las2peer.services.mwbService.database.dto.entity.CourseExamEntityDto;
import i5.las2peer.services.mwbService.database.dto.entity.EaslitExamEntityDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Dto with details for an exam, including test tool data and easlit data
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class ExamDto extends CourseExamEntityDto {
    private TestToolDto testToolDto;
    private EaslitExamEntityDto easLitExamDto;

    /**
     * default constructor
     */
    public ExamDto() {}

    /**
     * constructor setting the basic exam data
     * @param testToolDto with test data for the specific test tool
     * @param easLitExamDto with easlit data
     */
    public ExamDto(CourseExamEntityDto courseExamEntityDto, TestToolDto testToolDto, EaslitExamEntityDto easLitExamDto) {
        super(courseExamEntityDto);
        this.testToolDto = testToolDto;
        this.easLitExamDto = easLitExamDto;
    }

    /**
     * copy constructor (used to initialise derived classes)
     *
     * @param copy - ExamDto with data
     */
    public ExamDto(ExamDto copy) {
        super(copy);
        this.testToolDto = copy.getTestToolDto();
        this.easLitExamDto = copy.getEasLitExamDto();
    }
}
