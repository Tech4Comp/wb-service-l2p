package i5.las2peer.services.mwbService.database.dao.entity;


import i5.las2peer.services.mwbService.database.dao.DataAccessObject;
import i5.las2peer.services.mwbService.database.dto.entity.BaseCourseEntityDto;

import java.sql.*;

/**
 * Entity dao to handle basic database operations for table "baseCourse"
 */
public class BaseCourseEntityDao extends DataAccessObject {
    private final BaseCourseEntityDto baseCourseEntityDto;

    /**
     * Table name for course data
     */
    public static final String TABLE = "baseCourse";

    /**
     * constructor
     * @param connection to the database
     * @param baseCourseEntityDto -
     */
    public BaseCourseEntityDao(Connection connection, BaseCourseEntityDto baseCourseEntityDto) {
        super(connection);
        this.baseCourseEntityDto = baseCourseEntityDto;
    }

    /**
     * constructor
     * @param connection to the database
     * @param id of the baseCourse
     * @throws Exception -
     */
    public BaseCourseEntityDao(Connection connection, Integer id) throws Exception {
        super(connection);
        this.baseCourseEntityDto = readData(connection, id);
    }


    //******************************************************************************************
    // basic database operations
    //******************************************************************************************

    /**
     * insert a new record into the database
     *
     * @return id of the new record
     * @throws Exception if something went wrong
     */
    public int insert() throws Exception {
        String sqlStatement = "INSERT INTO " + TABLE + " (name, lms) VALUES (?, ?)" +
                " RETURNING id;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setString(1, baseCourseEntityDto.getName());
            preparedStatement.setString(2, baseCourseEntityDto.getLms());
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return resultSet.getInt("id");
        } catch (SQLException e) {
            throw DataAccessObject.createDatabaseException("Failed to insert base course.", e);
        }
        throw DataAccessObject.createDatabaseException("Failed to retrieve id of inserted base course.", null);
    }

    /**
     * update record
     *
     * @throws Exception if something went wrong
     */
    public void update() throws Exception {
        String sqlStatement = "UPDATE " + TABLE + " SET name=?, lms=?" +
                " WHERE id=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setString(1, baseCourseEntityDto.getName());
            preparedStatement.setString(2, baseCourseEntityDto.getLms());
            preparedStatement.setInt(3, baseCourseEntityDto.getId());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to update base course.", e);
        }
    }


    //******************************************************************************************
    // static methods for database operations
    //******************************************************************************************

    /**
     * @param connection to the database
     * @param id of BaseCourseEntityDto to retrieve
     * @return BaseCourseEntityDto
     * @throws Exception -
     */
    public static BaseCourseEntityDto readData(Connection connection, Integer id) throws Exception {
        String sqlStatement = "SELECT * FROM " + TABLE   + " WHERE id=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return createBaseCourseEntityDto(resultSet);
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve data for base course.", e);
        }
        return null;
    }


    /**
     * delete record with given id
     *
     * @param connection to database
     * @param id of the record to delete
     * @throws Exception -
     */
    public static void delete(Connection connection, Integer id) throws Exception {
        String sqlStatement = "DELETE FROM " + TABLE +
                " WHERE id=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to delete base course.", e);
        }
    }


    //******************************************************************************************
    // static methods to create base course related DTOs from a resultSet
    //******************************************************************************************

    /**
     * create BaseCourseEntityDto using data from resultSet
     *
     * @param resultSet containing data
     * @return BaseCourseEntityDto
     * @throws Exception -
     */
    public static BaseCourseEntityDto createBaseCourseEntityDto(ResultSet resultSet) throws Exception {
        Integer id = resultSet.getInt("id");
        String name = resultSet.getString("name");
        String lms = resultSet.getString("lms");
        return new BaseCourseEntityDto(id, name, lms);
    }
}
