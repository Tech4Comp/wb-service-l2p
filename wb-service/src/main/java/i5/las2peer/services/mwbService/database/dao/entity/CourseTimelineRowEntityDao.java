package i5.las2peer.services.mwbService.database.dao.entity;


import i5.las2peer.services.mwbService.database.dao.DataAccessObject;
import i5.las2peer.services.mwbService.database.dto.entity.CourseTimelineItemEntityDto;
import i5.las2peer.services.mwbService.database.dto.entity.CourseTimelineRowEntityDto;
import lombok.Getter;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Entity dao to handle basic database operations for table "courseTimelineRow"
 */
@Getter
public class CourseTimelineRowEntityDao extends DataAccessObject {
    private final CourseTimelineRowEntityDto courseTimelineRowEntityDto;

    /**
     * Table name for course data
     */
    public static final String TABLE = "courseTimelineRow";

    /**
     * field list for copy course data
     */
    public static final List<String> copyFieldList = Arrays.asList("row", "description", "isDetail", "createNewItems");

    /**
     * constructor
     * @param connection to the database
     * @param id of the course
     */
    public CourseTimelineRowEntityDao(Connection connection, Integer id) throws Exception {
        super(connection);
        this.courseTimelineRowEntityDto = readData(connection, id);
    }

    /**
     * constructor
     * @param connection to the database
     * @param courseTimelineRowEntityDto -
     */
    public CourseTimelineRowEntityDao(Connection connection, CourseTimelineRowEntityDto courseTimelineRowEntityDto) {
        super(connection);
        this.courseTimelineRowEntityDto = courseTimelineRowEntityDto;
    }

    //******************************************************************************************
    // instance methods
    //******************************************************************************************

    public Integer getCourseId() throws Exception {
        assertNotNull();
        return courseTimelineRowEntityDto.getCourseId();
    }

    public void assertNotNull() throws Exception {
        if (courseTimelineRowEntityDto == null)
            throw getInvalidDataException("Timeline row not found.");
    }

    public boolean isValidRowForCourseId(int courseId) throws Exception {
        assertNotNull();
        int rowId = courseTimelineRowEntityDto.getId();
        List<CourseTimelineRowEntityDto> allRows = getAllRows(this.connection, courseId);
        for(CourseTimelineRowEntityDto rowDto: allRows)
            if (rowDto.getId() == rowId)
                return true;
        return false;
    }


    //******************************************************************************************
    // basic database operations
    //******************************************************************************************

    /**
     * insert a new record into the database
     *
     * @return id of the new record
     * @throws Exception if something went wrong
     */
    public int insert() throws Exception {
        String sqlStatement = "INSERT INTO " + TABLE +
                " (courseId, row, description, isDetail, createNewItems) VALUES (?, ?, ?, ?, ?)" +
                " RETURNING id;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, courseTimelineRowEntityDto.getCourseId());
            preparedStatement.setInt(2, courseTimelineRowEntityDto.getRow());
            preparedStatement.setString(3, courseTimelineRowEntityDto.getDescription());
            preparedStatement.setBoolean(4, courseTimelineRowEntityDto.getIsDetail());
            preparedStatement.setBoolean(5, courseTimelineRowEntityDto.getCreateNewItems());
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return resultSet.getInt("id");
        } catch (SQLException e) {
            throw DataAccessObject.createDatabaseException("Failed to insert timeline row.", e);
        }
        throw DataAccessObject.createDatabaseException("Failed to retrieve id of inserted timeline row.", null);
    }

    /**
     * update record
     *
     * @throws Exception if something went wrong
     */
    public void update() throws Exception {
        String sqlStatement = "UPDATE " + TABLE + " SET row=?, description=?, isDetail=?, createNewItems=?" +
                " WHERE id=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, courseTimelineRowEntityDto.getRow());
            preparedStatement.setString(2, courseTimelineRowEntityDto.getDescription());
            preparedStatement.setBoolean(3, courseTimelineRowEntityDto.getIsDetail());
            preparedStatement.setBoolean(4, courseTimelineRowEntityDto.getCreateNewItems());
            preparedStatement.setInt(5, courseTimelineRowEntityDto.getId());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to update timeline row.", e);
        }
    }


    //******************************************************************************************
    // static methods for database operations
    //******************************************************************************************

    public static CourseTimelineRowEntityDto readData(Connection connection, Integer id) throws Exception {
        String sqlStatement = "SELECT * FROM " + TABLE   + " WHERE id=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return createDto(resultSet);
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve data for timeline row.", e);
        }
        return null;
    }

    public static List<CourseTimelineRowEntityDto> getAllRows(Connection connection, Integer courseId) throws Exception {
        String sqlStatement = "SELECT * FROM " + TABLE + " WHERE courseId=? ORDER BY row;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, courseId);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<CourseTimelineRowEntityDto> timelineRows = new ArrayList<>();
            while (resultSet.next()) {
                CourseTimelineRowEntityDto row = createDto(resultSet);
                timelineRows.add(row);
            }
            return timelineRows;
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve list of timeline rows for component TIMELINE for course " + courseId + " from database.", e);
        }
    }

    /**
     * delete record with given id
     *
     * @param connection to database
     * @param id of the record to delete
     * @throws Exception -
     */
    public static void delete(Connection connection, Integer id) throws Exception {
        String sqlStatement = "DELETE FROM " + TABLE +
                " WHERE id=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to delete timeline row.", e);
        }
    }


    //******************************************************************************************
    // static methods to create course related DTOs from a resultSet
    //******************************************************************************************

    /**
     * create CourseTimelineRowEntityDto using data from resultSet
     *
     * @param resultSet containing data
     * @return CourseTimelineRowEntityDto
     * @throws Exception -
     */
    public static CourseTimelineRowEntityDto createDto(ResultSet resultSet) throws Exception {
        Integer id = resultSet.getInt("id");
        Integer courseId = resultSet.getInt("courseId");
        Integer row = resultSet.getInt("row");
        String description = resultSet.getString("description");
        Boolean isDetail = resultSet.getBoolean("isDetail");
        Boolean createNewItems = resultSet.getBoolean("createNewItems");
        return new CourseTimelineRowEntityDto(id, courseId, row, description, isDetail, createNewItems);
    }
}
