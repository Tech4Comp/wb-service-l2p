package i5.las2peer.services.mwbService.restApi;

import com.fasterxml.jackson.databind.ObjectMapper;
import i5.las2peer.services.mwbService.Authentication;
import i5.las2peer.services.mwbService.MwbService;
import i5.las2peer.services.mwbService.database.LazyPostgresConnection;
import i5.las2peer.services.mwbService.database.dao.*;
import i5.las2peer.services.mwbService.database.dao.entity.*;
import i5.las2peer.services.mwbService.database.dto.*;
import i5.las2peer.services.mwbService.database.dto.entity.*;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.UUID;

/**
 *  REST API to manage courses
 */
@Path("/course")
public class CourseApi {

    /**
     * Retrieves the basic data for a given course id
     * @param courseId -
     * @return  200 shortname for the course, if successful
     *          400 Bad Request if something went wrong
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @GET
    @Path("/{courseId}")
    @RolesAllowed("authenticated")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(
            value = "Retrieves the basic data for a given course id",
            response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200,  message = "[CourseEntityDto]", response = CourseEntityDto.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Missing rights"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public Response getCourse(
            @PathParam("courseId") Integer courseId
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthorizationForCourse(connection.getConnection(), courseId);
            CourseDao courseDao = new CourseDao(connection.getConnection());
            CourseDto courseDto = courseDao.getCourse(courseId);
            if (courseDto == null)
                throw DataAccessObject.createDatabaseException("no course found", null);
            return MwbService.responseOK(new ObjectMapper().writeValueAsString(courseDto));
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * Retrieves the courseInfo for a course using the shortname
     * @param shortname of the course
     * @return  200 course info as JSON object
     *          400 Bad Request if something went wrong
     *          500 Internal Server Error if the database connection is not established.
     */
    @GET
    @Path("/info/{shortname}")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(
            value = "Retrieves the courseInfo for a course using the shortname",
            response = CourseInfoDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "[CourseInfo]", response = CourseInfoDto.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public Response getCourseInfo(
            @PathParam("shortname") String shortname
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            CourseDao courseDAO = new CourseDao(connection.getConnection());
            CourseInfoDto courseInfo = courseDAO.getCourseInfo(shortname);
            return MwbService.responseOK(new ObjectMapper().writeValueAsString(courseInfo));
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * returns list of courses already visited by current user
     * @return  200 with list of courses
     *          400 Bad Request if something goes wrong,
     *          401 not Authenticated or missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @GET
    @Path("/lastVisited")
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed("authenticated")
    public Response getCoursesVisited(
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthenticated();
            UUID uuid = PersonDao.getUUID(connection.getConnection(), Authentication.getUserAgentEmail());
            CourseDao courseDao = new CourseDao(connection.getConnection());
            List<CourseLastVisitedDto> courses = courseDao.getCoursesLastVisited(uuid);
            return MwbService.responseOK(new ObjectMapper().writeValueAsString(courses));
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * store current timestamp in database as last visited for current user
     * @param courseId of the course visited
     * @return  200 OK
     *          400 Bad Request if something goes wrong,
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @POST
    @Path("/lastVisited/{courseId}")
    @Produces(MediaType.TEXT_PLAIN)
    @RolesAllowed("authenticated")
    public Response storeLastVisitedCourse(
        @PathParam("courseId") int courseId
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthenticated();
            UUID uuid = PersonDao.getUUID(connection.getConnection(), Authentication.getUserAgentEmail());
            LastVisitedEntityDto lastVisitedEntityDto = new LastVisitedEntityDto(uuid, courseId);
            LastVisitedEntityDao lastVisitedEntityDao = new LastVisitedEntityDao(connection.getConnection(), lastVisitedEntityDto);
            lastVisitedEntityDao.upsert();
            return MwbService.responseOK();
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /*//*
     * returns answer for an intent sent from the bot
     * @param courseId -
     * @param intent to get the answer for from the database
     * @return  200 with list of courses
     *          400 Bad Request if something goes wrong,
     *          401 not Authenticated or missing rights
     *          500 Internal Server Error if the database connection is not established.
     */

    /* this endpoint is currently deactivated. NOTE: it would require database changes to allow for dynamic intents

    @GET
    @Path("/answerForIntent/{courseId}/{intent}")
    @Produces(MediaType.TEXT_PLAIN)
    @RolesAllowed("authenticated")
    public Response getAnswerForIntent(
        @PathParam("courseId") int courseId,
        @PathParam("intent") String intent
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthorizationForCourse(connection.getConnection(), courseId);
            CourseFaqEntityDto courseFaqEntityDto = CourseFaqEntityDao.readData(connection.getConnection(), courseId, intent);
            if (courseFaqEntityDto == null) {
                GenericChatbotIntentsEntityDto genericChatbotIntentsEntityDto = GenericChatbotIntentsEntityDao.readData(connection.getConnection(), intent);
                if (genericChatbotIntentsEntityDto == null)
                    throw DataAccessObject.createDatabaseException("intent not found", null);
                return MwbService.responseOK(genericChatbotIntentsEntityDto.getAnswer());
            }
            else
                return MwbService.responseOK(courseFaqEntityDto.getAnswer());
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }
    */

    /**
     * Creates a new course in the database.
     *
     * @param courseDto the course to be created as CourseDto as JSON object.
     * @return  200 with the id of the new course if it is successfully created.
     *          400 Bad Request if something went wrong
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @POST
    @RolesAllowed("authenticated")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Create a new course in the database.",
            notes = "Returns the id of the new course if it is successfully created.",
            response = Integer.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "[id]", response = Integer.class),
            @ApiResponse(code = 400, message = "Failed to insert course"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Missing rights"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public Response createCourse(
            @ApiParam(value = "The course to create, as a JSON object", required = true) CourseDto courseDto
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthorizationForBaseCourse(connection.getConnection(), courseDto.getBaseCourseId());
            CourseEntityDao courseEntityDao = new CourseEntityDao(connection.getConnection(), courseDto);
            int id = courseEntityDao.insert();
            return MwbService.responseOK(String.valueOf(id));
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * Creates a new course and populate it with data from another course.
     *
     * @param fromCourseId id to the course to copy data.
     * @param courseDto the course to be created as CourseDto as JSON object.
     * @return  200 with the id of the new course if it is successfully created.
     *          400 Bad Request if something went wrong
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @POST
    @Path("/createAndCopy/{fromCourseId}")
    @RolesAllowed("authenticated")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Creates a new course and populate it with data from another course.",
            notes = "Returns the id of the new course if it is successfully created.",
            response = Integer.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "[id]", response = Integer.class),
            @ApiResponse(code = 400, message = "Failed to insert course"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Missing rights"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public Response createAndCopyCourse(
            @PathParam("fromCourseId") Integer fromCourseId,
            @ApiParam(value = "The course to create, as a JSON object", required = true) CourseDto courseDto
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthorizationForBaseCourse(connection.getConnection(), courseDto.getBaseCourseId());
            Authentication.checkAuthorizationForCourse(connection.getConnection(), fromCourseId);
            CourseEntityDao courseEntityDao = new CourseEntityDao(connection.getConnection(), courseDto);
            try {
                connection.getConnection().setAutoCommit(false);
                int id = courseEntityDao.insert();
                courseDto.setId(id);
                CourseDao courseDao = new CourseDao(connection.getConnection());
                courseDao.copyCourseData(courseDto, fromCourseId, null);
                connection.getConnection().commit();
                return MwbService.responseOK(String.valueOf(id));
            } catch (Exception e) {
                Exception exception = MwbService.postgresDBConnectionManager.rollback(connection.getConnection(), e);
                return MwbService.responseError(exception);
            }
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * Updates data of a course in the database.
     *
     * @param courseDto with the new data of the course as CourseDto as JSON object.
     * @return  200 if update was successful
     *          400 Bad Request if something went wrong
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @PUT
    @RolesAllowed("authenticated")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(value = "Updating a course in the database.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad request, Failed to update course"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Missing rights"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public Response updateCourse(
            @ApiParam(value = "The new data of the course as a JSON object", required = true) CourseDto courseDto
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthorizationForCourse(connection.getConnection(), courseDto.getId());
            CourseEntityDao courseEntityDao = new CourseEntityDao(connection.getConnection(), courseDto);
            courseEntityDao.update();
            return MwbService.responseOK();
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * Deletes a course in the database.
     *
     * @param courseId with the id of the course to be deleted
     * @return  200 if course was deleted successfully
     *          400 Bad Request if something went wrong
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @DELETE
    @Path("/{courseId}")
    @RolesAllowed("authenticated")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(value = "Delete a course in the database.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Failed to delete course"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Missing rights"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public Response deleteCourse(
            @PathParam("courseId") Integer courseId
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthorizationForCourse(connection.getConnection(), courseId);
            CourseEntityDao.delete(connection.getConnection(), courseId);
            return MwbService.responseOK();
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }


    /**
     * Checks whether user has managing rights for a course
     * @param id of the course
     * @return  200 Yes, if current user has rights for the course
     *          200 No, if current user has no rights for the course or something went wrong
     */
    @GET
    @Path("/hasRights/{id}")
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Checks whether user has managing rights for a course",
            response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Yes"),
            @ApiResponse(code = 200, message = "No")
    })
    public Response isCourseOwner(
            @PathParam("id") Integer id
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthorizationForCourse(connection.getConnection(), id);
            return MwbService.responseYes();
        } catch (Exception e) {
            return MwbService.responseNo();
        }
    }


    /**
     * Retrieves data for course management for requested id
     * @param id of the course
     * @return  200 course info as JSON object if successful.
     *          400 Bad Request if retrieving course info failed.
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @GET
    @Path("/manage/info/{id}")
    @RolesAllowed("authenticated")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(
            value = "Retrieves data for course management for requested id",
            response = CourseInfoDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "[CourseInfo]", response = CourseInfoDto.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Missing rights"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public Response getCourseInfo(
            @PathParam("id") Integer id
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthorizationForCourse(connection.getConnection(), id);
            CourseDao courseDao = new CourseDao(connection.getConnection());
            CourseManageDto courseInfo = courseDao.getCourseManageInfo(id);
            return MwbService.responseOK(new ObjectMapper().writeValueAsString(courseInfo));
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * store current timestamp in database as last visited for current user
     * @param courseId of the course visited
     * @param courseLayoutDto layout as json
     * @return  200 OK
     *          400 Bad Request if something goes wrong,
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @POST
    @Path("/layout/{courseId}")
    @Produces(MediaType.TEXT_PLAIN)
    @RolesAllowed("authenticated")
    public Response upsertLayout(
            @PathParam("courseId") int courseId,
            @ApiParam(value = "data of the layout as JSON object", required = true) CourseLayoutDto courseLayoutDto
        ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthorizationForCourse(connection.getConnection(), courseId);
            String layout = new ObjectMapper().writeValueAsString(courseLayoutDto.getLayout());
            CourseLayoutEntityDto courseLayoutEntityDto = new CourseLayoutEntityDto(courseId, courseLayoutDto.getSize(), layout);
            CourseLayoutEntityDao courseLayoutEntityDao = new CourseLayoutEntityDao(connection.getConnection(), courseLayoutEntityDto);
            courseLayoutEntityDao.upsert();
            return MwbService.responseOK();
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    //******************************************************************************************
    // endpoints allowed only for maintainers (mostly for testing purposes)
    //******************************************************************************************

    /**
     * copy data and settings of course components from one course to another
     */
    @POST
    @Path("/copy/{courseId}/{courseIdSource}")
    @RolesAllowed("authenticated")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getBaseCourse(
            @PathParam(value = "courseId") Integer courseId,
            @PathParam(value = "courseIdSource") Integer courseIdSource,
            @ApiParam(value = "List of details to be copied", required = true) List<String> copyDetails
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkMaintainer();
            Authentication.checkAuthorizationForCourse(connection.getConnection(), courseId);
            Authentication.checkAuthorizationForCourse(connection.getConnection(), courseIdSource);
            CourseEntityDto courseEntityDto = CourseEntityDao.readData(connection.getConnection(), courseId);
            CourseDao courseDao = new CourseDao(connection.getConnection());
            courseDao.copyCourseData(courseEntityDto, courseIdSource, copyDetails);
            return MwbService.responseOK();
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }
}
