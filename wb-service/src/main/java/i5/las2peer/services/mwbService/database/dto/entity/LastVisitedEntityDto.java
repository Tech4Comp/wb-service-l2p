package i5.las2peer.services.mwbService.database.dto.entity;


import lombok.AllArgsConstructor;
import lombok.Data;

import java.sql.Timestamp;
import java.util.UUID;

/**
 * Entity dto corresponding to database table "lastVisited"
 */
@Data
@AllArgsConstructor
public class LastVisitedEntityDto {
    private UUID uuid;
    private Integer courseId;
    private Timestamp timestamp;

    /**
     * default constructor
     */
    public LastVisitedEntityDto() {}


    /**
     * constructor
     * @param uuid -
     * @param courseId -
     */
    public LastVisitedEntityDto(UUID uuid, Integer courseId) {
        this(uuid, courseId, null);
    }
}
