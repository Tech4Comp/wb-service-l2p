package i5.las2peer.services.mwbService.database.dto.entity;


import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Entity dto corresponding to database table course
 */
@Data
@AllArgsConstructor
public class LmsWorkerEntityDto {
    private Integer id;
    private String lms;
    private String shortname;
    private String username;
    private String password;

    /**
     * default constructor
     */
    public LmsWorkerEntityDto() {}
}
