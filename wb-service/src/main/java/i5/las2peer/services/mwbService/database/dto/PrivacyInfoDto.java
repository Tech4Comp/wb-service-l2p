package i5.las2peer.services.mwbService.database.dto;

import i5.las2peer.services.mwbService.database.dto.entity.CourseEntityDto;
import i5.las2peer.services.mwbService.database.dto.entity.FileBucketEntityDto;
import i5.las2peer.services.mwbService.database.dto.entity.PrivacyStatementEntityDto;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * Dto with info of privacy statement
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class PrivacyInfoDto extends PrivacyStatementEntityDto {
    private FileBucketEntityDto bucketFile;

    /**
     * default constructor
     */
    public PrivacyInfoDto() {}

    /**
     * constructor
     *
     * @param privacyStatementEntityDto basic data as CourseEntity
     * @param bucketFile with details of file for privacy statement
     */
    public PrivacyInfoDto(PrivacyStatementEntityDto privacyStatementEntityDto, FileBucketEntityDto bucketFile) {
        super(privacyStatementEntityDto);
        this.bucketFile = bucketFile;
    }
}
