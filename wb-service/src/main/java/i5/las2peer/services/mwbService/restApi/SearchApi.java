package i5.las2peer.services.mwbService.restApi;

import i5.las2peer.services.mwbService.MwbService;
import i5.las2peer.services.mwbService.database.LazyPostgresConnection;
import i5.las2peer.services.mwbService.externalServices.SearchResources;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *  handles REST API calls related to search in resources
 */
@Path("/search")
public class SearchApi {

    /**
     * returns the information (index name, category, examples, title) of resources for the specific search_id
     *
     * @param searchId - id of the search. Supported by now: ul, ud
     * @return JSONArray
     */
    @GET
    @Path("/{searchId}/info")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSearchResource(
            @PathParam("searchId") String searchId)
    {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            String response = new SearchResources(connection.getConnection()).getInfoForSearch(searchId);
            return MwbService.responseOK(response);
        }
        catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * queries the search service for the search term, tracks the search and returns the results
     *
     * @param searchId - currently supported: ul, ud
     * @param courseId - courseId triggering the search
     * @param searchRequest - JSON string containing search request
     * @return search results as JSON string
     */
    @POST
    @Path("/{searchId}/course/{courseId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSearchResults(
            @PathParam("searchId") String searchId,
            @PathParam("courseId") Integer courseId,
            String searchRequest)
    {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            String response = new SearchResources(connection.getConnection()).getSearchResultsAndTrackSearch(connection.getConnection(), courseId, searchId, searchRequest);
            return MwbService.responseOK(response);
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }
}
