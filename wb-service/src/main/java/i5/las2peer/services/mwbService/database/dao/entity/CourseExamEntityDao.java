package i5.las2peer.services.mwbService.database.dao.entity;


import i5.las2peer.services.mwbService.database.dao.DataAccessObject;
import i5.las2peer.services.mwbService.database.dto.entity.CourseExamEntityDto;
import lombok.Getter;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Dao to handle basic database operations for table with data for course component EXTENDEDEXAMFEEDBACK "courseExam"
 */
@Getter
public class CourseExamEntityDao extends CourseComponentDataDao {
    final CourseExamEntityDto courseExamEntityDto;

    /**
     * Table name for entity
     */
    public static final String TABLE = "courseExam";

    /**
     * field list for copy course data
     */
    public static final List<String> copyFieldList = Arrays.asList("name", "validFrom", "validUntil", "testTool");

    /**
     * constructor
     *
     * @param connection to the database
     * @param courseExamEntityDto -
     */
    public CourseExamEntityDao(Connection connection, CourseExamEntityDto courseExamEntityDto) {
        super(connection);
        this.courseExamEntityDto = courseExamEntityDto;
    }

    /**
     * constructor
     *
     * @param connection to the database
     * @param id of the hint to retrieve
     * @throws Exception if requested connection could not be established
     */
    public CourseExamEntityDao(Connection connection, Integer id) throws Exception {
        super(connection);
        this.courseExamEntityDto = readData(connection, id) ;
    }


    //******************************************************************************************
    // instance methods
    //******************************************************************************************

    public void assertNotNull() throws Exception {
        if (this.courseExamEntityDto == null)
            throw getInvalidDataException("No data provided.");
    }

    public Integer getCourseId() throws Exception {
        assertNotNull();
        return this.courseExamEntityDto.getCourseId();
    }

    //******************************************************************************************
    // basic database operations
    //******************************************************************************************

    /**
     * insert a new record into the database
     *
     * @return id of the new record
     * @throws Exception if something went wrong
     */
    public int insert() throws Exception {
        assertNotNull();
        String sqlStatement = "INSERT INTO " + TABLE + " (courseId, testTool, name, validFrom, validUntil) VALUES(?, ?, ?, ?, ?)" +
                " RETURNING id;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, courseExamEntityDto.getCourseId());
            preparedStatement.setString(2, courseExamEntityDto.getTestTool());
            preparedStatement.setString(3, courseExamEntityDto.getName());
            setOptionalTimestampForResultSet(preparedStatement,4, courseExamEntityDto.getValidFrom());
            setOptionalTimestampForResultSet(preparedStatement,5, courseExamEntityDto.getValidUntil());
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return resultSet.getInt("id");
        } catch (SQLException e) {
            throw DataAccessObject.createDatabaseException("Failed to insert new exam for course component EXTENDEDEXAMFEEDBACK.", e);
        }
        throw DataAccessObject.createDatabaseException("Failed to retrieve id of inserted course exam.", null);
    }

    /**
     * update record
     *
     * @throws Exception if something went wrong
     */
    public void update() throws Exception {
        assertNotNull();
        String sqlStatement = "UPDATE " + TABLE + " SET name=?, validFrom=?, validUntil=?" +
                " WHERE id=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setString(1, courseExamEntityDto.getName());
            preparedStatement.setTimestamp(2, courseExamEntityDto.getValidFrom());
            preparedStatement.setTimestamp(3, courseExamEntityDto.getValidUntil());
            preparedStatement.setInt(4, courseExamEntityDto.getId());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to update exam for course component EXTENDEDEXAMFEEDBACK.", e);
        }
    }


    //******************************************************************************************
    // static methods for database operations
    //******************************************************************************************

    public static CourseExamEntityDto readData(Connection connection, Integer id) throws Exception {
        String sqlStatement = "SELECT * FROM " + TABLE   + " WHERE id=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return createDto(resultSet);
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve data for exam.", e);
        }
        return null;
    }

    public static List<CourseExamEntityDto> readListData(Connection connection, Integer courseId) throws Exception {
        String sqlStatement = "SELECT * FROM " + TABLE + " WHERE courseId=? ORDER BY validFrom;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, courseId);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<CourseExamEntityDto> courseExamEntityDtoList = new ArrayList<>();
            while (resultSet.next()) {
                CourseExamEntityDto courseExamEntityDto = createDto(resultSet);
                courseExamEntityDtoList.add(courseExamEntityDto);
            }
            return courseExamEntityDtoList;
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve list of exams for component EXTENDEDEXAMFEEDBACK.", e);
        }
    }

    /**
     * delete record with given id
     *
     * @param connection to database
     * @param id of the record to delete
     * @throws Exception -
     */
    public static void delete(Connection connection, Integer id) throws Exception {
        String sqlStatement = "DELETE FROM " + TABLE +
                " WHERE id=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to delete exam for component EXTENDEDEXAMFEEDBACK.", e);
        }
    }


    //******************************************************************************************
    // static methods to create DTO from a resultSet
    //******************************************************************************************

    /**
     * create CourseExamEntityDto using data from resultSet
     *
     * @param resultSet containing data
     * @return CourseExamEntityDto
     * @throws Exception -
     */
    public static CourseExamEntityDto createDto(ResultSet resultSet) throws Exception {
        Integer id = resultSet.getInt("id");
        Integer courseId = resultSet.getInt("courseId");
        String testTool = resultSet.getString("testTool");
        String name = resultSet.getString("name");
        Timestamp validFrom = resultSet.getTimestamp("validFrom");
        Timestamp validUntil = resultSet.getTimestamp("validUntil");
        return new CourseExamEntityDto(id, courseId, testTool, name, validFrom, validUntil);
    }
}
