package i5.las2peer.services.mwbService.database.dto.entity;


import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Entity dto corresponding to database table "role"
 */
@Data
@AllArgsConstructor
public class RoleEntityDto {
    private Integer id;
    private String roleType;

    /**
     * default constructor
     */
    public RoleEntityDto() {}
}
