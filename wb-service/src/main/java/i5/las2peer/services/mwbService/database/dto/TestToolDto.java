package i5.las2peer.services.mwbService.database.dto;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import i5.las2peer.services.mwbService.database.dao.entity.OnyxExamEntityDao;
import i5.las2peer.services.mwbService.database.dto.entity.OnyxExamEntityDto;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Dto for lms related test/quiz/exam data
 * Base class for specialised classes for different lms types
 */
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "testTool"
)
@JsonSubTypes({
        @JsonSubTypes.Type(value = OnyxExamEntityDto.class, name = OnyxExamEntityDao.TESTTOOL)
})

@Data
@AllArgsConstructor
public class TestToolDto {
    private Integer courseExamId;

    /**
     * default constructor
     */
    public TestToolDto() {}

    /**
     * copy constructor (used to initialise derived classes)
     *
     * @param copy - CourseEntity with data
     */
    public TestToolDto(TestToolDto copy) {
        this.courseExamId = copy.getCourseExamId();
    }
}
