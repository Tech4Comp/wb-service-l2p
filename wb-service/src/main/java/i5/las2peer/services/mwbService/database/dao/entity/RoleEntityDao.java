package i5.las2peer.services.mwbService.database.dao.entity;


import i5.las2peer.services.mwbService.database.dao.DataAccessObject;
import i5.las2peer.services.mwbService.database.dto.entity.RoleEntityDto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Entity dao to handle basic database operations for table "role"
 */
public class RoleEntityDao extends DataAccessObject {
    private final RoleEntityDto roleEntityDto;

    /**
     * Table name for data
     */
    public static final String TABLE = "role";
    public static final String ROLE_TYPE_CREATE = "CREATE";
    public static final String ROLE_TYPE_OWN = "OWN";

    /**
     * constructor
     * @param connection to the database
     * @param id -
     */
    public RoleEntityDao(Connection connection, Integer id) throws Exception {
        super(connection);
        this.roleEntityDto = readData(connection, id);
        if (this.roleEntityDto == null)
            throw getInvalidDataException("invalid role id");
    }

    /**
     * constructor
     * @param connection to the database
     * @param roleType -
     */
    public RoleEntityDao(Connection connection, String roleType) throws Exception {
        super(connection);
        this.roleEntityDto = readData(connection, roleType);
        if (this.roleEntityDto == null)
            throw getInvalidDataException("invalid role type");
    }


    //******************************************************************************************
    // instance methods
    //******************************************************************************************

    public Integer getId() throws Exception {
        assertNotNull();
        return this.roleEntityDto.getId();
    }

    public void assertNotNull() throws Exception {
        if (this.roleEntityDto == null)
            throw getInvalidDataException("Role not found.");
    }


    //******************************************************************************************
    // static methods for database operations
    //******************************************************************************************

    public static RoleEntityDto readData(Connection connection, Integer id) throws Exception {
        String sqlStatement = "SELECT * FROM " + TABLE   + " WHERE id=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return createRoleEntityDto(resultSet);
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve data for role.", e);
        }
        return null;
    }

    public static RoleEntityDto readData(Connection connection, String roleType) throws Exception {
        String sqlStatement = "SELECT * FROM " + TABLE   + " WHERE roleType=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setString(1, roleType);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return createRoleEntityDto(resultSet);
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve data for roleType.", e);
        }
        return null;
    }

    //******************************************************************************************
    // static methods to create related DTO from a resultSet
    //******************************************************************************************

    /**
     * create RoleEntityDto using data from resultSet
     *
     * @param resultSet containing data
     * @return RoleEntityDto
     * @throws Exception -
     */
    public static RoleEntityDto createRoleEntityDto(ResultSet resultSet) throws Exception {
        Integer id = resultSet.getInt("id");
        String roleType = resultSet.getString("roleType");
        return new RoleEntityDto(id, roleType);
    }
}
