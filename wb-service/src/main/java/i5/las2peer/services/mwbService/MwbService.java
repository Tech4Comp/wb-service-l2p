package i5.las2peer.services.mwbService;

import com.fasterxml.jackson.databind.ObjectMapper;
import i5.las2peer.services.mwbService.configuration.DBLocalConfiguration;
import i5.las2peer.services.mwbService.database.PostgresDBConnectionManager;
import i5.las2peer.services.mwbService.database.MongoDBConnectionManager;
import i5.las2peer.services.mwbService.restApi.*;
import i5.las2peer.services.mwbService.externalServices.ServiceException;
import i5.las2peer.api.ManualDeployment;
import i5.las2peer.logging.L2pLogger;
import i5.las2peer.restMapper.RESTService;
import i5.las2peer.restMapper.annotations.ServicePath;
import io.swagger.annotations.*;
import org.apache.commons.io.FileUtils;
import org.glassfish.jersey.media.multipart.MultiPartFeature;

import javax.ws.rs.*;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import static i5.las2peer.services.mwbService.utilities.StringsUtilities.hasNoValue;

/**
 * Backend supporting the MWB frontend
 */
@Api
@SwaggerDefinition(info = @Info(title = "Mentoring Workbench Service", version = MwbService.VERSION, description = "A service to connect the Mentoring Workbench frontend with las2Peer services.", contact = @Contact(name = "Maria Bez", email = "maria.bez@tu-dresden.de")))
@ManualDeployment
@ServicePath("/mwb")
public class MwbService extends RESTService {
	/**
	 * MWB-Version
	 */
	public static final String VERSION = "1.1.0";

	/**
	 * directory to store resources this directory will be used when uploading JSON
	 * files
	 */
	public static final String RESOURCE_DIR = "./wb-service/resources/";

	/**
	 * directory used for temporary files
	 */
	public static final String TEMP_DIR = RESOURCE_DIR + "temp/";

	/**
	 * short code for external service 'Learning Record Store'
	 */
	public static final String EXTERNAL_SERVICE_LRS = "lrs";

	/**
	 * value for actor homepage used for lrs statements
	 */
	public static final String LRS_ACTOR_HOMEPAGE = "https://workbench.tech4comp.dbis.rwth-aachen.de";

	/**
	 * short code for external service 'search in resources'
	 */
	public static final String EXTERNAL_SERVICE_SEARCH = "search";

	/**
	 * short code for external service 'EasLit'
	 */
	public static final String EXTERNAL_SERVICE_EASLIT = "easlit";

	/**
	 * The las2peer logger
	 */
	public static final L2pLogger logger = L2pLogger.getInstance(MwbService.class.getName());

	/**
	 * Postgres database connection manager
	 */
	public static final PostgresDBConnectionManager postgresDBConnectionManager = new PostgresDBConnectionManager();
	/**
	 * MongoDB database connection manager
	 */
	public static final MongoDBConnectionManager mongoDBConnectionManager = new MongoDBConnectionManager();

	/**
	 * Values set in properties
	 */
	private String DB_HOST;
	private int DB_PORT;
	private String DB_NAME;
	private String DB_USERNAME;
	private String DB_PASSWORD;

	/**
	 * Constructor of the Service.
	 */
	public MwbService() {
		setFieldValues();
		setupLocalDirectoriesOnStartup();
		setupDatabaseConnectionManagerOnStartup();
	}

	@Override
	public void onStop() {
		mongoDBConnectionManager.close();
		super.onStop();
	}

	@Override
	protected void initResources() {
		setApplication(new Application() {
			@Override
			public Set<Class<?>> getClasses() {
				Set<Class<?>> classes = new HashSet<>();
				classes.add(MultiPartFeature.class);
				classes.add(MwbService.class);
				classes.add(FileBucketApi.class);
				classes.add(BaseCourseApi.class);
				classes.add(LmsWorkerApi.class);
				classes.add(CourseApi.class);
				classes.add(CourseComponentApi.class);
				classes.add(ConsentApi.class);
				classes.add(PrivacyStatementApi.class);
				classes.add(TimelineApi.class);
				classes.add(LrsApi.class);
				classes.add(TextAnalysisApi.class);
				classes.add(SearchApi.class);
				classes.add(ExamFeedbackApi.class);
				classes.add(TrackApi.class);
				classes.add(RatingApi.class);
				return classes;
			}
		});
	}

	private void setupDatabaseConnectionManagerOnStartup() {
		if (!postgresDBConnectionManager.isInitialized()) {
			logger.info("postgres: setConnectionProperties");
			setupPostgresConnectionManager();
		}
		if (!mongoDBConnectionManager.isInitialized()) {
			logger.info("mongoDB: setConnectionProperties");
			setupMongoDBConnectionManager();
		}
	}

	private void setupLocalDirectoriesOnStartup() {
		File localTempDir = new File(MwbService.TEMP_DIR);
		if (localTempDir.exists()) {
			try {
				FileUtils.deleteDirectory(localTempDir);
			} catch (Exception e) {
				logger.info("failed to delete temporary directory: "+e.getMessage());
			}
		}
		if (!localTempDir.mkdir()) {
			logger.info("failed to create temporary directory on startup");
		}
	}

	private void setupPostgresConnectionManager() {
		// check if variables have been set in environment
		if (hasNoValue(DB_USERNAME)) {
			logger.info("use local authentication for DB connection");
			try {
				DBLocalConfiguration localConfiguration = new DBLocalConfiguration();
				DB_HOST = localConfiguration.getValue("host");
				DB_PORT = Integer.parseInt(localConfiguration.getValue("port"));
				DB_NAME = localConfiguration.getValue("name");
				DB_USERNAME = localConfiguration.getValue("user");
				DB_PASSWORD = localConfiguration.getValue("password");
			} catch (Exception e) {
				logger.warning("setup database connection: failed to read local values "+e.getMessage());
				return;
			}
		}
		postgresDBConnectionManager.setConnectionProperties(DB_HOST, DB_PORT, DB_NAME, DB_USERNAME, DB_PASSWORD);
	}

	private void setupMongoDBConnectionManager() {
		// set values from environment variables
		String mongodbHost = System.getenv("MONGODB_HOST");
		String mongodbPort = System.getenv("MONGODB_PORT");
		String mongodbUsername = System.getenv("MONGODB_USERNAME");
		String mongodbPassword = System.getenv("MONGODB_PASSWORD");

		// check if variables have been set in environment
		if (hasNoValue(mongodbHost)) {
			logger.info("use local authentication for mongoDB connection");
			try {
				DBLocalConfiguration localConfiguration = new DBLocalConfiguration();
				mongodbHost = localConfiguration.getValue("mongodb-host");
				mongodbPort = localConfiguration.getValue("mongodb-port");
				//MONGODB_DATABASE = localConfiguration.getValue("mongodb-name");
				mongodbUsername = localConfiguration.getValue("mongodb-user");
				mongodbPassword = localConfiguration.getValue("mongodb-password");
			} catch (Exception e) {
				logger.warning("setup mongo database connection: failed to read local values "+e.getMessage());
				return;
			}
		}
		mongoDBConnectionManager.setupConnection(mongodbHost, mongodbPort, mongodbUsername, mongodbPassword);
	}


	/**
	 * @param message - response message
	 * @return HTTP response 'OK' with given message
	 */
	public static Response responseOK(String message) {
		return Response.ok(message).build();
	}

	/**
	 * just a shortcut for easier handling
	 *
	 * @return HTTP response status OK with message 'OK'
	 */
	public static Response responseOK() {
		return responseOK("OK");
	}

	/**
	 * @return HTTP response status OK with message 'No'
	 */
	public static Response responseNo() {
		return responseOK("No");
	}

	/**
	 * @return HTTP response status OK with message 'Yes'
	 */
	public static Response responseYes() {
		return responseOK("Yes");
	}

	/**
	 * @param value boolean to determine response message
	 * @return HTTP response status OK with message yes/no according to value
	 */
	public static Response response(boolean value) {
		return value ? responseYes() : responseNo();
	}

	/**
	 * @param status  - HTTP response status to pass through to the rest api call
	 * @param message - response message
	 * @return HTTP response 'Bad request' with given message
	 */
	public static Response responseError(Response.Status status, String message) {
		System.out.println("Error " + status + ", " + message);
		return Response.status(status).entity(message).build();
	}

	/**
	 * @param exception - exception raised while processing a Rest api call
	 * @return response with status of the exception, if it is of type
	 *         ServiceException, else 'Bad request'
	 */
	public static Response responseError(Exception exception) {
		return responseError(exception, exception.getMessage());
	}

	/**
	 * @param exception - exception raised while processing a Rest api call
	 * @param message   - response message
	 * @return response with status of the exception, if it is of type
	 *         ServiceException, else 'Bad request'
	 */
	public static Response responseError(Exception exception, String message) {
		if (exception instanceof ServiceException)
			return responseError(((ServiceException) exception).getResponseStatus(), message);

		return responseError(Response.Status.BAD_REQUEST, message);
	}

	// ******************************************************************************************
	// basic rest api endpoints
	// ******************************************************************************************

	/**
	 * @return text "Service-Version: " and the current version as String
	 */
	@GET
	@Path("/version")
	@Produces(MediaType.TEXT_PLAIN)
	public Response version() {
		return MwbService.responseOK(MwbService.VERSION);
	}


	/**
	 * checking MongoDb connection for test purposes
	 */
	@GET
	@Path("/checkConnectionToMongoDB")
	@Produces(MediaType.TEXT_PLAIN)
	public Response checkConnectionToMongoDB() {
		try {
			Authentication.checkMaintainer();
			boolean pingOk = MwbService.mongoDBConnectionManager.ping(MongoDBConnectionManager.DB_MWB_FILE_BUCKET);
			return MwbService.response(pingOk);
		} catch (Exception e) {
			return MwbService.responseError(e);
		}
	}
}