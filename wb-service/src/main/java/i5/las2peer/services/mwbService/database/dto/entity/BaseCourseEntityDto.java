package i5.las2peer.services.mwbService.database.dto.entity;


import lombok.AllArgsConstructor;
import lombok.Data;

import java.sql.Date;

/**
 * Entity dto corresponding to database table baseCourse
 */
@Data
@AllArgsConstructor
public class BaseCourseEntityDto {
    private Integer id;
    private String name;
    private String lms;

    /**
     * default constructor
     */
    public BaseCourseEntityDto() {}

    /**
     * copy constructor (used to initialise derived classes)
     *
     * @param copy - BaseCourseEntityDto with data
     */
    public BaseCourseEntityDto(BaseCourseEntityDto copy) {
        this.id = copy.getId();
        this.name = copy.getName();
        this.lms = copy.getLms();
    }
}
