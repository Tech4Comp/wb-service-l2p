package i5.las2peer.services.mwbService.database.dao.entity;


import i5.las2peer.services.mwbService.database.dao.DataAccessObject;
import i5.las2peer.services.mwbService.database.dto.entity.LastVisitedEntityDto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;

/**
 * Entity dao to handle basic database operations for table "lastVisited"
 */
public class LastVisitedEntityDao extends DataAccessObject {
    private final LastVisitedEntityDto lastVisitedEntityDto;

    /**
     * Table name for data
     */
    public static final String TABLE = "lastVisited";

    /**
     * constructor
     * @param connection to the database
     * @param lastVisitedEntityDto -
     */
    public LastVisitedEntityDao(Connection connection, LastVisitedEntityDto lastVisitedEntityDto) {
        super(connection);
        this.lastVisitedEntityDto = lastVisitedEntityDto;
    }

    //******************************************************************************************
    // basic database operations
    //******************************************************************************************

    /**
     * insert a new record / update existing record to the database
     *
     * @throws Exception if something went wrong
     */
    public void upsert() throws Exception {
        String sqlStatement = "INSERT INTO " + TABLE + " (uuid, courseId, timestamp) VALUES(?, ?, ?)"+
                " ON CONFLICT ON CONSTRAINT " + TABLE + "_pKey DO UPDATE" +
                " SET timestamp=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            Timestamp now = Timestamp.from(Instant.now());
            preparedStatement.setObject(1, lastVisitedEntityDto.getUuid());
            preparedStatement.setInt(2, lastVisitedEntityDto.getCourseId());
            preparedStatement.setTimestamp(3, now);
            preparedStatement.setTimestamp(4, now);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw DataAccessObject.createDatabaseException("Failed to insert/update consent.", e);
        }
    }
}
