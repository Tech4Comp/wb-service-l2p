package i5.las2peer.services.mwbService.externalServices;

import i5.las2peer.services.mwbService.MwbService;
import i5.las2peer.services.mwbService.database.dao.entity.LrsStoreForCourseEntityDao;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;

import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;

import static i5.las2peer.services.mwbService.utilities.JsonUtilities.isProperJson;
import static i5.las2peer.services.mwbService.utilities.StringsUtilities.hasValue;

/**
 * Connection to a  store in an LRS
 * providing methods to read and post statements
 */
public class LrsConnection extends ExternalHttpService {
    /**
     * basic xAPI url for statements
     */
    public static final String xApiStatementBaseUrl = "https://xapi.tech4comp.dbis.rwth-aachen.de/definitions/";
    /**
     * basic xAPI url for mwb related statements
     */
    public static final String xApiStatementBaseUrlMwb = xApiStatementBaseUrl + "mwb";
    /**
     * basic xAPI url for data for mwb related activity
     */
    public static final String xApiUriForActivityData = xApiStatementBaseUrlMwb +"/extensions/context/activity_data";
    /**
     * courseId to be stored in xAPI statement
     */
    public final Integer courseId;

    /**
     * replace chars invalid in xAPI urls with corresponding HTML code
     * @param uri to be masked
     * @return valid xAPI uri String
     */
    public static String maskXApiUri(String uri) {
        return uri.replace(".", "&46;");
    }

    /**
     * constructor
     * @param connection to the database
     * @param courseId to determine the LRS store to connect to
     * @throws SQLException -
     */
    public LrsConnection(Connection connection, Integer courseId) throws Exception {
        super(connection, MwbService.EXTERNAL_SERVICE_LRS);
        this.courseId = courseId;
        LrsStoreForCourseEntityDao lrsStoreForCourseEntityDao = new LrsStoreForCourseEntityDao(connection, courseId);
        this.setCredentials(lrsStoreForCourseEntityDao.getCredentials());
    }

    //******************************************************************************************
    // public methods
    //******************************************************************************************

    /**
     * @param query - JSON formatted query for the pipeline,
     *     see <a href="https://learninglocker.atlassian.net/wiki/spaces/DOCS/pages/106037259/Aggregation+API">xAPI documentation</a>
     * @return response for the query
     * @throws Exception - exception with detailed info about failure
     */
    public JSONArray query(String query) throws Exception {
        HttpResponse<String> response = httpClient
                .send(setupRequestForQuery(query), HttpResponse.BodyHandlers.ofString());
        String responseBody = response.body();
        assertResponseCodeOk(response.statusCode(), "query failed: "+responseBody);
        try {
            return (JSONArray) new JSONParser(JSONParser.MODE_PERMISSIVE).parse(responseBody);
        } catch (Exception e) {
            throw getServiceExceptionBadRequest("error in response: " + responseBody);
        }
    }

    /**
     * @param actor - actor for the statement to be posted
     * @param statement - JSON formatted statement that should be added to the LRS
     * @param messageOnFailure - additional text for exception message in case of failure
     * @throws Exception - exception with detailed info about failure
     */
    public void postStatement(String actor, String statement, String messageOnFailure) throws Exception {
        String postStatement = "[{"+ createJsonForActor(actor)+","+statement+"}]";
        HttpResponse<String> response = httpClient
                .send(setupRequestForPostStatement(postStatement), HttpResponse.BodyHandlers.ofString());
        assertResponseCodeOk(response.statusCode(), messageOnFailure+ ", post statement failed: "+response.body());
    }

    /**
     * post xAPI statement
     * @param actor -
     * @param verb -
     * @param object -
     * @param context -
     * @param messageOnFailure -
     * @throws Exception if something went wrong
     */
    public void postStatement(String actor, String verb, String object, String context, String messageOnFailure) throws Exception {
        String statement =
                createJsonForActor(actor)+"," +
                createJsonForVerb(verb) + "," +
                createJsonForObject(object);
        String contextJson = createJsonForContext(context);
        if (hasValue(contextJson))
            statement += ","+contextJson;
        postStatement("{" + statement + "}", messageOnFailure);
    }

    /**
     * @return -
     * @throws Exception - exception with detailed info about failure
     */
    public boolean checkAuthorization() throws Exception {
        String query = """
                [{"$match": {"statement.id": "dummyStatementId"}}]
                """;
        HttpResponse<String> response = httpClient
                .send(setupRequestForQuery(query), HttpResponse.BodyHandlers.ofString());
        int statusCode = response.statusCode();
        boolean hasAuthorization = statusCode == Response.Status.OK.getStatusCode();
        return hasAuthorization;
    }

    /**
     * @param statementId - id of the statement to be retrieved
     * @return statement as JSONObject
     * @throws Exception - exception with detailed info about failure
     */
    public JSONObject retrieveStatement(String statementId) throws Exception {
        String query = """
                [\
                {"$match": {"statement.id": "%s"}\
                }\
                ]\
                """.formatted(statementId);
        JSONArray response = query(query);
        if (response == null || response.isEmpty()) {
            throw getServiceExceptionBadRequest(getStoreInfo() + ": there is no statement with id " + statementId);
        }
        return (JSONObject) response.get(0);
    }

    /**
     * @param statementId - id of the statement to be retrieved
     * @param actor - actor of the statement to be retrieved
     * @param details - String with JSON formatted project expression
     * @return statement as JSONObject
     * @throws Exception - cause of failure is mentioned in exception message
     */
    public JSONObject retrieveStatementDetailsForActor(String statementId, String actor, String details) throws Exception {
        String queryStatement = """
                [\
                {"$match": {"$and": [\
                    {"statement.actor.account.name": "%s"},\
                    {"statement.id": "%s"}\
                ]}},\
                %s
                ]\
                """.formatted(actor, statementId, details);
        JSONArray response = query(queryStatement);
        if (response == null || response.isEmpty())
            throw getServiceException(Response.Status.NOT_FOUND, getStoreInfo() + ": there is no statement for this user with id " + statementId);
        return (JSONObject) response.get(0);
    }


    /**
     * @param statementId - id of the statement to be deleted
     * @throws Exception - cause of failure is mentioned in exception message
     */
    public void deleteStatement(String statementId) throws Exception {
        JSONObject statement = retrieveStatement(statementId);
        String lrsItemId = statement.getAsString("_id");
        deleteItem(lrsItemId);
    }

    /**
     * @param lrsItemId - internal _id of the statement to be deleted
     * @throws Exception - cause of failure is mentioned in exception message
     */
    public void deleteItem(String lrsItemId) throws Exception {
        HttpResponse<String> response = httpClient
                .send(setupRequestForDeleteStatement(lrsItemId), HttpResponse.BodyHandlers.ofString());
        String messageIfAssertFails = "deleting lrs item "+lrsItemId+" failed: "+response.body();
        assertResponseCode(Response.Status.NO_CONTENT, response.statusCode(), messageIfAssertFails);
    }

    //******************************************************************************************
    // private methods
    //******************************************************************************************
    private void postStatement(String statement, String messageOnFailure) throws Exception {
        String postStatement = "["+ statement+"]";
        HttpResponse<String> response = httpClient
                .send(setupRequestForPostStatement(postStatement), HttpResponse.BodyHandlers.ofString());
        assertResponseCodeOk(response.statusCode(), messageOnFailure+ ", post statement failed: "+response.body());
    }

    private HttpRequest setupRequestForQuery(String query) throws Exception {
        return HttpRequest.newBuilder()
                .uri(getUriForQuery(query))
                .header("Content-Type", "application/json; charset="+ StandardCharsets.UTF_8)
                .header("Authorization", getBasicAuth())
                .GET()
                .build();
    }

    private URI getUriForQuery(String pipeline) throws Exception {
        return new URI(getUrl() + "/api/statements/aggregate?pipeline=" + URLEncoder.encode(pipeline, StandardCharsets.UTF_8));
    }

    private URI getUriForPost() throws Exception {
        return new URI(getUrl() + "/data/xAPI/statements");
    }

    private URI getUriForDelete(String lrsItemId) throws Exception {
        return new URI(getUrl() + "/api/v2/statement/" + lrsItemId);
    }

    private HttpRequest setupRequestForPostStatement(String statement) throws Exception {
        return HttpRequest.newBuilder()
                .uri(getUriForPost())
                .header("Content-Type", "application/json; charset="+StandardCharsets.UTF_8)
                .header("X-Experience-API-Version", "1.0.3")
                .header("Cache-Control", "no-cache")
                .header("Authorization", getBasicAuth())
                .POST(HttpRequest.BodyPublishers.ofString(statement))
                .build();
    }

    private HttpRequest setupRequestForDeleteStatement(String lrsItemId) throws Exception {
        return HttpRequest.newBuilder()
                .uri(getUriForDelete(lrsItemId))
                //.header("Content-Type", "application/json; charset="+StandardCharsets.UTF_8)
                //.header("X-Experience-API-Version", "1.0.3")
                //.header("Cache-Control", "no-cache")
                .header("Authorization", getBasicAuth())
                .DELETE()
                .build();
    }

    private String getStoreInfo() {
        return "LRS store for "+courseId;
    }



    private String createJsonForActor(String actor) {
        String actorHomePage = MwbService.LRS_ACTOR_HOMEPAGE;
        return """
            "actor": {\
                "account": {\
                    "name": "%s",\
                    "homePage": "%s"\
            }}
            """.formatted(actor, actorHomePage);
    }

    private String createJsonForVerb(String verb) {
        return """
            "verb": {\
                "id": "%s/verb/%s",\
                "display": {"en-US": "%s"}\
            }\
            """.formatted(xApiStatementBaseUrlMwb, verb, displayVerb(verb));
    }

    private String createJsonForObject(String object) {
        return """
            "object": {\
                "id": "%s/object/%s",\
                "definition": {\
                    "name": {"en-US": "%s"},\
                    "extensions": {\
                       "%s/object/course": {"id": %d}\
            }}}\
            """.formatted(xApiStatementBaseUrlMwb, object, nameObject(object), xApiStatementBaseUrlMwb, this.courseId);
    }

    private String createJsonForContext(String context) {
        if (!hasValue(context) || !isProperJson(context))
            return "";
        return """
            "context": %s\
            """.formatted(context);
    }

    private static String displayVerb(String verb) {
        return verb.replace('_', ' ');
    }
    private static String nameObject(String object) {
        return switch (object) {
            case "feedback_writing_task" -> "feedback for writing task";
            default -> object.replace('_', ' ');
        };
    }

}
