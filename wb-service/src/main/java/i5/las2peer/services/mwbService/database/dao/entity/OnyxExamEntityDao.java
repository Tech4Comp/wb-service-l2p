package i5.las2peer.services.mwbService.database.dao.entity;


import i5.las2peer.services.mwbService.database.dao.DataAccessObject;
import i5.las2peer.services.mwbService.database.dao.LmsExamDao;
import i5.las2peer.services.mwbService.database.dto.TestToolDto;
import i5.las2peer.services.mwbService.database.dto.entity.OnyxExamEntityDto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

/**
 * Entity dao to handle basic database operations for table "onyxExam"
 */
public class OnyxExamEntityDao extends LmsExamDao {
    private final OnyxExamEntityDto onyxExamEntityDto;

    /**
     * Table name for entity
     */
    public static final String TABLE = "onyxExam";

    /**
     * Exam tool for test
     */
    public static final String TESTTOOL =  "onyx";

    /**
     * field list for copy course data
     */
    public static final List<String> copyFieldList = Arrays.asList("onyxTestId", "opalNodeId");

    /**
     * constructor
     *
     * @param connection to the database
     * @param onyxExamEntityDto -
     */
    public OnyxExamEntityDao(Connection connection, OnyxExamEntityDto onyxExamEntityDto) {
        super(connection);
        this.onyxExamEntityDto = onyxExamEntityDto;
    }

    /**
     * constructor
     *
     * @param connection to the database
     * @param id -
     * @throws Exception if requested connection could not be established
     */
    public OnyxExamEntityDao(Connection connection, Integer id) throws Exception {
        super(connection);
        this.onyxExamEntityDto = readData(connection, id) ;
    }


    //******************************************************************************************
    // static methods for database operations
    //******************************************************************************************

    public static OnyxExamEntityDto readData(Connection connection, Integer courseExamId) throws Exception {
        String sqlStatement = "SELECT * FROM " + TABLE   + " WHERE courseExamId=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, courseExamId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return createDto(resultSet);
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve data for onyx exam.", e);
        }
        return null;
    }


    //******************************************************************************************
    // basic database operations
    //******************************************************************************************

    /**
     * insert a new record / update existing record to the database
     *
     * @throws Exception if something went wrong
     */
    public void upsert() throws Exception {
        String sqlStatement = "INSERT INTO " + TABLE + " (courseExamId, onyxTestId, opalNodeId) VALUES(?, ?, ?)"+
                " ON CONFLICT ON CONSTRAINT " + TABLE + "_pKey DO UPDATE" +
                " SET onyxTestId=?, opalNodeId=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, onyxExamEntityDto.getCourseExamId());
            preparedStatement.setString(2, onyxExamEntityDto.getOnyxTestId());
            preparedStatement.setString(3, onyxExamEntityDto.getOpalNodeId());
            preparedStatement.setString(4, onyxExamEntityDto.getOnyxTestId());
            preparedStatement.setString(5, onyxExamEntityDto.getOpalNodeId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw DataAccessObject.createDatabaseException("Failed to insert/update data for onyx test tool.", e);
        }
    }

    //******************************************************************************************
    // static method to create DTO from a resultSet
    //******************************************************************************************

    /**
     * create OnyxExamEntityDto using data from resultSet
     *
     * @param resultSet containing data
     * @return OnyxExamEntityDto
     * @throws Exception -
     */
    public static OnyxExamEntityDto createDto(ResultSet resultSet) throws Exception {
        TestToolDto testToolDto = LmsExamDao.createFromResultSet(resultSet);
        String onyxTestId = resultSet.getString("onyxTestId");
        String opalNodeId = resultSet.getString("opalNodeId");
        return new OnyxExamEntityDto(testToolDto, onyxTestId, opalNodeId);
    }


    //******************************************************************************************
    // implementations for abstract methods of base class
    //******************************************************************************************

    /**
     * @return type of LMS as String
     */
    public String getTestTool() { return TESTTOOL; }
    public TestToolDto getLmsExamDto() { return onyxExamEntityDto; }
}
