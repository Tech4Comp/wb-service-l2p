package i5.las2peer.services.mwbService.restApi;


import com.fasterxml.jackson.databind.ObjectMapper;
import i5.las2peer.services.mwbService.Authentication;
import i5.las2peer.services.mwbService.MwbService;
import i5.las2peer.services.mwbService.database.LazyPostgresConnection;
import i5.las2peer.services.mwbService.database.dao.CourseComponentDao;
import i5.las2peer.services.mwbService.database.dao.LmsExamDao;
import i5.las2peer.services.mwbService.database.dao.entity.*;
import i5.las2peer.services.mwbService.database.dto.*;
import i5.las2peer.services.mwbService.database.dto.entity.*;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 *  REST API to manage courses
 */
@Path("/courseComponent")
public class CourseComponentApi {

    /**
     * Retrieves all available types for course components.
     * @return  200 list with all component types as JSON object
     *          400 Bad Request if something went wrong
     *          500 Internal Server Error if the database connection is not established.
     */
    @GET
    @Path("/list")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(
            value = "Retrieves all available types for course components.",
            response = ComponentTypeEntityDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "[ComponentType]", response = ComponentTypeEntityDto.class),
            @ApiResponse(code = 400, message = "Bad Request")
    })
    public Response getAllComponentTypes() {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            List<ComponentTypeEntityDto> components = ComponentTypeEntityDao.readAll(connection.getConnection());
            String result = new ObjectMapper().writeValueAsString(components);
            return MwbService.responseOK(result);
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * Inserts a course component
     * @param courseComponentDto to be created as a JSON object.
     * @return  200 if successful
     *          400 Bad Request if something went wrong
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @POST
    @RolesAllowed("authenticated")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Insert course component to the database.",
            notes = "Returns success message if it is successfully created.",
            response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Missing rights")
    })
    public Response createCourseComponent(
            @ApiParam(value = "Course component as a JSON object", required = true) CourseComponentDto courseComponentDto
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthorizationForCourse(connection.getConnection(), courseComponentDto.getCourseId());
            CourseComponentDao courseComponentDao = new CourseComponentDao(connection.getConnection());
            courseComponentDao.insertCourseComponent(courseComponentDto);
            return MwbService.responseOK();
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * Updates a course component
     * @param courseComponentDto to be updated as a JSON object.
     * @return  200 if successful
     *          400 Bad Request if something went wrong
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @PUT
    @RolesAllowed("authenticated")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Update data of course component in the database.",
            notes = "Returns OK if update was successful.",
            response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Missing rights")
    })
    public Response updateCourseComponent(
            @ApiParam(value = "Course component as a JSON object", required = true) CourseComponentDto courseComponentDto
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthorizationForCourse(connection.getConnection(), courseComponentDto.getCourseId());
            CourseComponentDao courseComponentDao = new CourseComponentDao(connection.getConnection());
            courseComponentDao.updateCourseComponent(courseComponentDto);
            return MwbService.responseOK();
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * Deletes a course component
     * @param courseId -
     * @param componentTypeId -
     * @return  200 if successful
     *          400 Bad Request if something went wrong
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @DELETE
    @Path("/{courseId}/{componentTypeId}")
    @RolesAllowed("authenticated")
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Delete course component to the database.",
            notes = "Returns success message if it is successful.",
            response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Missing rights")
    })
    public Response deleteCourseComponent(
            @PathParam("courseId") Integer courseId,
            @PathParam("componentTypeId") Integer componentTypeId
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthorizationForCourse(connection.getConnection(), courseId);
            CourseComponentDao courseComponentDao = new CourseComponentDao(connection.getConnection());
            try {
                connection.getConnection().setAutoCommit(false);
                courseComponentDao.deleteCourseComponent(courseId,  componentTypeId);
                connection.getConnection().commit();
                return MwbService.responseOK();
            } catch (Exception e) {
                Exception exception = MwbService.postgresDBConnectionManager.rollback(connection.getConnection(), e);
                return MwbService.responseError(exception);
            }
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    //******************************************************************************************
    // managing writing tasks for component WRITINGTASKS
    //******************************************************************************************

    /**
     * Retrieves writing tasks for courseId as CourseWritingTasksDto
     * @param courseId -
     * @return  200 CourseWritingTasksDto as Json
     *          400 Bad Request if something went wrong
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @GET
    @Path("/writingTasks/{courseId}")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(
            value = "Retrieves writing tasks for courseId as CourseWritingTasksDto.",
            response = CourseWritingTasksDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = CourseWritingTasksDto.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Missing rights"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public Response getWritingTasks(
        @PathParam("courseId") Integer courseId
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            CourseWritingTasksDto courseWritingTasksDto = new CourseComponentDao(connection.getConnection()).getDataWritingTasks(courseId);
            String result = new ObjectMapper().writeValueAsString(courseWritingTasksDto);
            return MwbService.responseOK(result);
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * Inserts a task for component WRITINGTASKS
     * @param writingTaskEntityDto to be added as a JSON object.
     * @return  200 if successful
     *          400 Bad Request if something went wrong
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @POST
    @Path("/writingTask")
    @RolesAllowed("authenticated")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Insert task for component WRITINGTASKS.",
            notes = "Returns success message if successful.",
            response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Missing rights"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public Response insertWritingTask(
            @ApiParam(value = "Task for component WRITINGTASKS as a JSON object", required = true) WritingTaskEntityDto writingTaskEntityDto
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthorizationForCourse(connection.getConnection(), writingTaskEntityDto.getCourseId());
            WritingTaskEntityDao writingTaskEntityDao = new WritingTaskEntityDao(connection.getConnection(), writingTaskEntityDto);
            writingTaskEntityDao.insert();
            return MwbService.responseOK();
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * Updates a task for component WRITINGTASKS
     * @param writingTaskEntityDto to be updated as a JSON object.
     * @return  200 if successful
     *          400 Bad Request if something went wrong
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @PUT
    @Path("/writingTask")
    @RolesAllowed("authenticated")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Update task for component WRITINGTASKS.",
            notes = "Returns success message if successful.",
            response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Missing rights"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public Response updateWritingTask(
            @ApiParam(value = "Task for component WRITINGTASKS as a JSON object", required = true) WritingTaskEntityDto writingTaskEntityDto
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthorizationForCourse(connection.getConnection(), writingTaskEntityDto.getCourseId());
            WritingTaskEntityDao writingTaskEntityDao = new WritingTaskEntityDao(connection.getConnection(), writingTaskEntityDto);
            writingTaskEntityDao.update();
            return MwbService.responseOK();
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * Deletes a task of component WRITINGTASKS
     * @param courseId -
     * @param nr of the task to delete
     * @return  200 if successful
     *          400 Bad Request if something went wrong
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @DELETE
    @Path("/writingTask/{courseId}/{nr}")
    @RolesAllowed("authenticated")
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Delete task for component WRITINGTASKS.",
            notes = "Returns success message if it is successful.",
            response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Missing rights"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public Response deleteWritingTask(
            @PathParam("courseId") Integer courseId,
            @PathParam("nr") Integer nr
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthorizationForCourse(connection.getConnection(), courseId);
            WritingTaskEntityDao.delete(connection.getConnection(), courseId, nr);
            return MwbService.responseOK();
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    //******************************************************************************************
    // managing hints for component MWBHINTS
    //******************************************************************************************

    /**
     * Retrieves course hints for courseId as CourseHintsDto
     * @param courseId -
     * @return  200 CourseWritingTasksDto as Json
     *          400 Bad Request if something went wrong
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @GET
    @Path("/hints/{courseId}")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(
            value = "Retrieves MWB hints for courseId as CourseHintsDto",
            response = CourseHintsDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = CourseHintsDto.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Missing rights"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public Response getCourseHints(
            @PathParam("courseId") Integer courseId
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            CourseHintsDto courseHintsDto = new CourseComponentDao(connection.getConnection()).getDataMWBHints(courseId);
            String result = new ObjectMapper().writeValueAsString(courseHintsDto);
            return MwbService.responseOK(result);
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * Inserts/updates a hint for component MWBHINTS
     * @param courseHintEntityDto to be created/updated as a JSON object.
     * @return  200 if successful
     *          400 Bad Request if something went wrong
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @POST
    @Path("/hint")
    @RolesAllowed("authenticated")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Insert/update hint for component MWBHINTS into the database.",
            notes = "Returns success message if it is successfully inserted.",
            response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Missing rights"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public Response createMWBHint(
            @ApiParam(value = "Hint for component MWBHINTS as a JSON object", required = true) CourseHintEntityDto courseHintEntityDto
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            if (courseHintEntityDto.getId() != null) {
                CourseHintEntityDao setCorrectCourseId = new CourseHintEntityDao(connection.getConnection(), courseHintEntityDto.getId());
                courseHintEntityDto.setCourseId(setCorrectCourseId.getCourseHintEntityDto().getCourseId());
            }
            Authentication.checkAuthorizationForCourse(connection.getConnection(), courseHintEntityDto.getCourseId());
            CourseHintEntityDao courseHintEntityDao = new CourseHintEntityDao(connection.getConnection(), courseHintEntityDto);
            courseHintEntityDao.upsert();
            return MwbService.responseOK();
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * Deletes a hint for component MWBHINTS
     * @param id of the hint to be deleted
     * @return  200 if successful
     *          400 Bad Request if something went wrong
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @DELETE
    @Path("/hint/{id}")
    @RolesAllowed("authenticated")
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Deletes hint for component MWBHINTS into the database.",
            notes = "Returns success message if it is successful.",
            response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Missing rights"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public Response deleteMWBHint(
            @PathParam("id") Integer id
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            CourseHintEntityDao courseHintEntityDao = new CourseHintEntityDao(connection.getConnection(), id);
            if (courseHintEntityDao.getCourseHintEntityDto() != null) {
                Authentication.checkAuthorizationForCourse(connection.getConnection(), courseHintEntityDao.getCourseHintEntityDto().getCourseId());
                CourseHintEntityDao.delete(connection.getConnection(), id);
            }
            return MwbService.responseOK();
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    //******************************************************************************************
    // managing bot intents for component CHATBOT
    //******************************************************************************************

    /**
     * Retrieves bot intents for courseId as list of CourseFaqEntityDto
     * @param courseId -
     * @return  200 List of CourseFaqEntityDto as Json
     *          400 Bad Request if something went wrong
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @GET
    @Path("/botIntents/{courseId}")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(
            value = "Retrieves chatbot intents for courseId as json list of CourseFaqEntityDto",
            response = CourseHintsDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = CourseFaqEntityDto.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Missing rights"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public Response getBotIntents(
            @PathParam("courseId") Integer courseId
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            List<CourseFaqEntityDto> botIntents = CourseFaqEntityDao.readListData(connection.getConnection(), courseId);
            String result = new ObjectMapper().writeValueAsString(botIntents);
            return MwbService.responseOK(result);
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * Inserts/updates a chatbot intent for component CHATBOT
     * @param courseFaqEntityDto to be created/updated as a JSON object.
     * @return  200 if successful
     *          400 Bad Request if something went wrong
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @POST
    @Path("/botIntent")
    @RolesAllowed("authenticated")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Insert/update chatbot intent for component CHATBOT.",
            notes = "Returns success message if it is successfully inserted.",
            response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Missing rights"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public Response upsertBotIntent(
            @ApiParam(value = "Intent for component CHATBOT as a JSON object", required = true) CourseFaqEntityDto courseFaqEntityDto
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthorizationForCourse(connection.getConnection(), courseFaqEntityDto.getCourseId());
            CourseFaqEntityDao courseFaqEntityDao = new CourseFaqEntityDao(connection.getConnection(), courseFaqEntityDto);
            courseFaqEntityDao.upsert();
            return MwbService.responseOK();
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * Deletes a chatbot intent for component CHATBOT
     * @param courseId of the intent to be deleted
     * @param intent to be deleted
     * @return  200 if successful
     *          400 Bad Request if something went wrong
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @DELETE
    @Path("/botIntent/{courseId}/{intent}")
    @RolesAllowed("authenticated")
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Deletes hint for component MWBHINTS into the database.",
            notes = "Returns success message if it is successful.",
            response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Missing rights"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public Response deleteBotIntent(
            @PathParam("courseId") Integer courseId,
            @PathParam("intent") String intent
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthorizationForCourse(connection.getConnection(), courseId);
            CourseFaqEntityDao.delete(connection.getConnection(), courseId, intent);
            return MwbService.responseOK();
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }


    //******************************************************************************************
    // managing timeline data for component TIMELINE
    //******************************************************************************************

    /**
     * Retrieves timeline data for courseId as CourseTimelineDto
     * @param courseId -
     * @return  200 CourseTimelineDto as Json
     *          400 Bad Request if something went wrong
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @GET
    @Path("/timeline/{courseId}")
    @RolesAllowed("authenticated")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(
            value = "Retrieves timeline data for courseId as CourseTimelineDto as Json",
            response = CourseTimelineDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = CourseTimelineDto.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Missing rights"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public Response getTimelineManageData(
            @PathParam("courseId") Integer courseId
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            CourseEntityDto courseEntityDto = CourseEntityDao.readData(connection.getConnection(), courseId);
            if (courseEntityDto == null)
                return MwbService.responseError(Response.Status.BAD_REQUEST, "invalid course id");
            Authentication.checkAuthorizationForCourse(connection.getConnection(), courseEntityDto.getId());
            CourseTimelineDto courseTimelineDto = new CourseComponentDao(connection.getConnection()).getDataTimeline(courseId);
            String result = new ObjectMapper().writeValueAsString(courseTimelineDto);
            return MwbService.responseOK(result);
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * Inserts a new row for the timeline
     * @param timelineRow to be created as a JSON object.
     * @return  200 if successful
     *          400 Bad Request if something went wrong
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @POST
    @Path("/timeline/row")
    @RolesAllowed("authenticated")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Inserts a new timeline row for component TIMELINE.",
            notes = "Returns success message if it is successfully inserted.",
            response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Missing rights"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public Response createTimelineRow(
            @ApiParam(value = "Row for component TIMELINE as a JSON object", required = true) CourseTimelineRowEntityDto timelineRow
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthorizationForCourse(connection.getConnection(), timelineRow.getCourseId());
            CourseTimelineRowEntityDao courseTimelineRowEntityDao = new CourseTimelineRowEntityDao(connection.getConnection(), timelineRow);
            courseTimelineRowEntityDao.insert();
            return MwbService.responseOK();
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * Update a row for the timeline
     * @param timelineRow to be updated as a JSON object.
     * @return  200 if successful
     *          400 Bad Request if something went wrong
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @PUT
    @Path("/timeline/row")
    @RolesAllowed("authenticated")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Update a timeline row for component TIMELINE.",
            notes = "Returns success message if it is successfully updated.",
            response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Missing rights"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public Response updateTimelineRow(
            @ApiParam(value = "Row for component TIMELINE as a JSON object", required = true) CourseTimelineRowEntityDto timelineRow
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthorizationForCourse(connection.getConnection(), timelineRow.getCourseId());
            CourseTimelineRowEntityDao courseTimelineRowEntityDao = new CourseTimelineRowEntityDao(connection.getConnection(), timelineRow);
            courseTimelineRowEntityDao.update();
            return MwbService.responseOK();
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * Deletes a timeline row
     * @param id of the row to be deleted
     * @return  200 if successful
     *          400 Bad Request if something went wrong
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @DELETE
    @Path("/timeline/row/{id}")
    @RolesAllowed("authenticated")
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Deletes row for component TIMELINE.",
            notes = "Returns success message if it is successful.",
            response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Missing rights"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public Response deleteTimelineRow(
            @PathParam("id") Integer id
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            CourseTimelineRowEntityDao courseTimelineRowEntityDao = new CourseTimelineRowEntityDao(connection.getConnection(), id);
            if (courseTimelineRowEntityDao.getCourseTimelineRowEntityDto() != null) {
                Authentication.checkAuthorizationForCourse(connection.getConnection(), courseTimelineRowEntityDao.getCourseTimelineRowEntityDto().getCourseId());
                CourseTimelineRowEntityDao.delete(connection.getConnection(), id);
            }
            return MwbService.responseOK();
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * Inserts/updates a timeline item
     * @param courseTimelineItemEntityDto to be created/updated as a JSON object.
     * @return  200 if successful
     *          400 Bad Request if something went wrong
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @POST
    @Path("/timeline/item")
    @RolesAllowed("authenticated")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Insert/update a timeline item for component TIMELINE.",
            notes = "Returns success message if it is successfully inserted.",
            response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Missing rights"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public Response createTimelineItem(
            @ApiParam(value = "Timeline item for component TIMELINE as a JSON object", required = true) CourseTimelineItemEntityDto courseTimelineItemEntityDto
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            CourseTimelineRowEntityDao rowDao = new CourseTimelineRowEntityDao(connection.getConnection(), courseTimelineItemEntityDto.getRowId());
            int courseId = rowDao.getCourseId();
            Authentication.checkAuthorizationForCourse(connection.getConnection(), courseId);
            if (courseTimelineItemEntityDto.getId() != null) {
                // in case of an update: ensure the new row belongs to the same course as the old row
                if (!rowDao.isValidRowForCourseId(courseId))
                    return MwbService.responseError(Response.Status.BAD_REQUEST, "Invalid row id. ");
            }
            CourseTimelineItemEntityDao courseTimelineItemEntityDao = new CourseTimelineItemEntityDao(connection.getConnection(), courseTimelineItemEntityDto);
            courseTimelineItemEntityDao.upsert();
            return MwbService.responseOK();
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * Deletes a timeline item
     * @param id of the timeline item be deleted
     * @return  200 if successful
     *          400 Bad Request if something went wrong
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @DELETE
    @Path("/timeline/item/{id}")
    @RolesAllowed("authenticated")
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Deletes item component TIMELINE.",
            notes = "Returns success message if it is successful.",
            response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Missing rights"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public Response deleteTimelineItem(
            @PathParam("id") Integer id
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            CourseTimelineItemEntityDao itemDao = new CourseTimelineItemEntityDao(connection.getConnection(), id);
            CourseTimelineItemEntityDto courseTimelineItemEntityDto = itemDao.getCourseTimelineItemEntityDto();
            if (courseTimelineItemEntityDto != null) {
                CourseTimelineRowEntityDao rowDao = new CourseTimelineRowEntityDao(connection.getConnection(), courseTimelineItemEntityDto.getRowId());
                Authentication.checkAuthorizationForCourse(connection.getConnection(), rowDao.getCourseId());
                CourseTimelineItemEntityDao.delete(connection.getConnection(), id);
            }
            return MwbService.responseOK();
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    //******************************************************************************************
    // managing exams for component EXTENDEDEXAMFEEDBACK
    //******************************************************************************************

    /**
     * Retrieves course exams for courseId as list of ExamDto
     * @param courseId -
     * @return  200 list of ExamDto as Json
     *          400 Bad Request if something went wrong
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @GET
    @Path("/exams/{courseId}")
    @RolesAllowed("authenticated")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(
            value = "Retrieves data for course exams for courseId as ExamsManageDto as Json",
            response = ExamsManageDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = ExamsManageDto.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Missing rights"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public Response getExamsManageData(
            @PathParam("courseId") Integer courseId
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            CourseEntityDto courseEntityDto = CourseEntityDao.readData(connection.getConnection(), courseId);
            if (courseEntityDto == null)
                return MwbService.responseError(Response.Status.BAD_REQUEST, "invalid course id");
            Authentication.checkAuthorizationForCourse(connection.getConnection(), courseEntityDto.getId());
            ExamsManageDto examDtoList = new CourseComponentDao(connection.getConnection()).getDataManageExams(courseEntityDto);
            String result = new ObjectMapper().writeValueAsString(examDtoList);
            return MwbService.responseOK(result);
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * Inserts a new exam
     * @param examDto to be created as a JSON object.
     * @return  200 if successful
     *          400 Bad Request if something went wrong
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @POST
    @Path("/exam")
    @RolesAllowed("authenticated")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Insert exam for component EXTENDEDEXAMFEEDBACK into the database.",
            notes = "Returns success message if it is successfully inserted.",
            response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Missing rights"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public Response createExam(
            @ApiParam(value = "Data for exam for component EXTENDEDEXAMFEEDBACK as a JSON object", required = true) ExamDto examDto
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            Authentication.checkAuthorizationForCourse(connection.getConnection(), examDto.getCourseId());
            CourseExamEntityDao courseExamEntityDao = new CourseExamEntityDao(connection.getConnection(), examDto);
            try {
                connection.getConnection().setAutoCommit(false);
                int examId = courseExamEntityDao.insert();
                EaslitExamEntityDto easlitExamEntityDto = examDto.getEasLitExamDto();
                easlitExamEntityDto.setCourseExamId(examId);
                EaslitExamEntityDao easlitExamEntityDao = new EaslitExamEntityDao(connection.getConnection(), easlitExamEntityDto);
                easlitExamEntityDao.upsert();
                TestToolDto testToolDto = examDto.getTestToolDto();
                testToolDto.setCourseExamId(examId);
                LmsExamDao.createLmsExamDao(connection.getConnection(), testToolDto).upsert();
                connection.getConnection().commit();
                return MwbService.responseOK();
            } catch (Exception e) {
                Exception exception = MwbService.postgresDBConnectionManager.rollback(connection.getConnection(), e);
                return MwbService.responseError(exception);
            }
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * Updates data for an  exam
     * @param examDto to be updated as a JSON object.
     * @return  200 if successful
     *          400 Bad Request if something went wrong
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @PUT
    @Path("/exam")
    @RolesAllowed("authenticated")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Updates exam for component EXTENDEDEXAMFEEDBACK.",
            notes = "Returns success message if successful.",
            response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Missing rights"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public Response updateExam(
            @ApiParam(value = "Data for exam for component EXTENDEDEXAMFEEDBACK as a JSON object", required = true) ExamDto examDto
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            // ensure correct courseId for exam
            CourseExamEntityDao courseExamEntityDao = new CourseExamEntityDao(connection.getConnection(), examDto.getId());
            examDto.setCourseId(courseExamEntityDao.getCourseId());
            Authentication.checkAuthorizationForCourse(connection.getConnection(), examDto.getCourseId());
            try {
                connection.getConnection().setAutoCommit(false);
                courseExamEntityDao.update();
                EaslitExamEntityDto easlitExamEntityDto = examDto.getEasLitExamDto();
                easlitExamEntityDto.setCourseExamId(examDto.getId());
                EaslitExamEntityDao easlitExamEntityDao = new EaslitExamEntityDao(connection.getConnection(), easlitExamEntityDto);
                easlitExamEntityDao.upsert();
                TestToolDto testToolDto = examDto.getTestToolDto();
                testToolDto.setCourseExamId(examDto.getId());
                LmsExamDao.createLmsExamDao(connection.getConnection(), testToolDto).upsert();
                connection.getConnection().commit();
                return MwbService.responseOK();
            } catch (Exception e) {
                Exception exception = MwbService.postgresDBConnectionManager.rollback(connection.getConnection(), e);
                return MwbService.responseError(exception);
            }
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }

    /**
     * Deletes an exam
     * @param id of the exam to be deleted
     * @return  200 if successful
     *          400 Bad Request if something went wrong
     *          401 Unauthorized
     *          403 Missing rights
     *          500 Internal Server Error if the database connection is not established.
     */
    @DELETE
    @Path("/exam/{id}")
    @RolesAllowed("authenticated")
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation(
            value = "Deletes exam for component EXTENDEDEXAMFEEDBACK",
            notes = "Returns success message if successful.",
            response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Missing rights"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public Response deleteExam(
            @PathParam("id") Integer id
    ) {
        try (LazyPostgresConnection connection = new LazyPostgresConnection()) {
            CourseExamEntityDao courseExamEntityDao = new CourseExamEntityDao(connection.getConnection(), id);
            if (courseExamEntityDao.getCourseExamEntityDto() != null) {
                Authentication.checkAuthorizationForCourse(connection.getConnection(), courseExamEntityDao.getCourseExamEntityDto().getCourseId());
                CourseExamEntityDao.delete(connection.getConnection(), id);
            }
            return MwbService.responseOK();
        } catch (Exception e) {
            return MwbService.responseError(e);
        }
    }
}
