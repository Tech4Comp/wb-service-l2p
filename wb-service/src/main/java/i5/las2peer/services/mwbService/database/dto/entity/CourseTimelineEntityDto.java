package i5.las2peer.services.mwbService.database.dto.entity;


import i5.las2peer.services.mwbService.database.dto.CourseComponentDataDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.sql.Date;

/**
 * Entity dto corresponding to database table 'courseTimeline' for course component TIMELINE
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class CourseTimelineEntityDto extends CourseComponentDataDto {
    private Date startsOn;
    private Date endsOn;

        /**
         * default constructor
         */
    public CourseTimelineEntityDto() {}

        /**
         * constructor
         *
         * @param courseComponentDataDto basic data as ComponentDataDto
         * @param startsOn -
         * @param endsOn -
         */
    public CourseTimelineEntityDto(CourseComponentDataDto courseComponentDataDto, Date startsOn, Date endsOn) {
            super(courseComponentDataDto);
            this.startsOn = startsOn;
            this.endsOn = endsOn;
        }

    /**
     * copy constructor (used to initialise derived classes)
     *
     * @param copy - CourseEntity with data
     */
    public CourseTimelineEntityDto(CourseTimelineEntityDto copy) {
        super(copy);
        this.startsOn = copy.getStartsOn();
        this.endsOn = copy.getEndsOn();
    }
}
