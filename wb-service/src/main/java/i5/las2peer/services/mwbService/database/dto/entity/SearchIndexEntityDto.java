package i5.las2peer.services.mwbService.database.dto.entity;


import i5.las2peer.services.mwbService.database.dto.CourseComponentDataDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Entity dto corresponding to database table for course component SEARCHINRESOURCES
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class SearchIndexEntityDto extends CourseComponentDataDto {
    private String searchId;
    private String searchIndex;

    /**
     * default constructor
     */
    public SearchIndexEntityDto() {}

    /**
     * constructor
     *
     * @param courseComponentDataDto basic data as ComponentDataDto
     * @param searchId -
     * @param searchIndex -
     */
    public SearchIndexEntityDto(CourseComponentDataDto courseComponentDataDto, String searchId, String searchIndex) {
        super(courseComponentDataDto);
        this.searchId = searchId;
        this.searchIndex = searchIndex;
    }
}
