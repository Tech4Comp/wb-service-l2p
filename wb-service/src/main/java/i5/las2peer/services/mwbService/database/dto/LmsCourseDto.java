package i5.las2peer.services.mwbService.database.dto;


import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import i5.las2peer.services.mwbService.database.dao.entity.MoodleCourseEntityDao;
import i5.las2peer.services.mwbService.database.dao.entity.OpalCourseEntityDao;
import i5.las2peer.services.mwbService.database.dto.entity.MoodleCourseEntityDto;
import i5.las2peer.services.mwbService.database.dto.entity.OpalCourseEntityDto;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * base class for DTOs with lms specific course data
 */
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "lms"
)
@JsonSubTypes({
        @JsonSubTypes.Type(value = OpalCourseEntityDto.class, name = OpalCourseEntityDao.LMS),
        @JsonSubTypes.Type(value = MoodleCourseEntityDto.class, name = MoodleCourseEntityDao.LMS)
})
@Data
@AllArgsConstructor
public class LmsCourseDto {
    private Integer baseCourseId;
    private Integer lmsWorkerId;


    /**
     * default constructor
     */
    public LmsCourseDto() {}

    /**
     * copy constructor (used to initialise derived classes)
     *
     * @param copy - ComponentDataDto with data
     */
    public LmsCourseDto(LmsCourseDto copy) {
        this.baseCourseId = copy.getBaseCourseId();
        this.lmsWorkerId = copy.getLmsWorkerId();
    }
}
