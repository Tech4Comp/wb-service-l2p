package i5.las2peer.services.mwbService.database.dto.entity;


import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Entity dto corresponding to database table "easlitExam"
 */
@Data
@AllArgsConstructor
public class EaslitExamEntityDto {
    private Integer courseExamId;
    private String easlitProjectId;
    private String topic;

    /**
     * default constructor
     */
    public EaslitExamEntityDto() {}
}
