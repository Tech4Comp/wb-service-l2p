package i5.las2peer.services.mwbService.database.dao;


import i5.las2peer.services.mwbService.database.dao.entity.PersonEntityDao;
import i5.las2peer.services.mwbService.database.dao.entity.PersonMappingEntityDao;
import i5.las2peer.services.mwbService.database.dto.entity.PersonMappingEntityDto;
import i5.las2peer.services.mwbService.externalServices.ServiceException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.UUID;


/**
 * DAO to handle complex database requests related to person and person mapping
 */
public class PersonDao extends DataAccessObject {

    /**
     * constructor
     * @param connection to the database
     */
    public PersonDao(Connection connection) {
        super(connection);
    }


    //******************************************************************************************
    // instance methods
    //******************************************************************************************

    public void assertNotIsNull(UUID uuid) throws ServiceException {
        if (uuid == null)
            throw getInvalidDataException("No uuid found.");
    }


    //******************************************************************************************
    // static methods for database operations
    //******************************************************************************************

    public static UUID getOrCreateUUID(Connection connection, String email) throws Exception {
        int personId = PersonEntityDao.getOrCreatePersonId(connection, email);
        PersonMappingEntityDao personMappingEntityDao = new PersonMappingEntityDao(connection, personId);
        PersonMappingEntityDto personMappingEntityDto = personMappingEntityDao.getPersonMappingEntityDto();
        if (personMappingEntityDto != null)
            return personMappingEntityDto.getUuid();
        return createPersonMapping(connection, personId);
    }

    public static UUID createPersonMapping(Connection connection, Integer personId) throws Exception {
        UUID uuid = UUID.randomUUID();
        PersonMappingEntityDto personMappingEntityDto = new PersonMappingEntityDto(uuid, personId);
        PersonMappingEntityDao personMappingEntityDao = new PersonMappingEntityDao(connection, personMappingEntityDto);
        personMappingEntityDao.insert();
        return uuid;
    }

    /**
     * retrieve the UUID corresponding to the email if available
     * @param connection to database
     * @param email of the user
     * @return 	UUID, if successful
     * 			null, if no uuid is mapped to this email
     * @throws Exception if something went wrong
     */
    public static UUID getUUID(Connection connection, String email) throws Exception {
        String sqlStatement = "SELECT uuid FROM " + PersonMappingEntityDao.TABLE +
                " JOIN " + PersonEntityDao.TABLE + " ON " + PersonEntityDao.TABLE + ".id = " + PersonMappingEntityDao.TABLE + ".personId" +
                " WHERE " + PersonEntityDao.TABLE + ".email=?;";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement)) {
            preparedStatement.setString(1, email);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                return UUID.fromString(resultSet.getString(1));
        } catch (Exception e) {
            throw DataAccessObject.createDatabaseException("Failed to retrieve uuid for email from database.", e);
        }
        return null;
    }
}
