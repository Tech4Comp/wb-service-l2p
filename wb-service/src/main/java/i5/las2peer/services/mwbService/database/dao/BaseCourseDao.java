package i5.las2peer.services.mwbService.database.dao;

import i5.las2peer.services.mwbService.database.dao.entity.*;
import i5.las2peer.services.mwbService.database.dto.*;
import i5.las2peer.services.mwbService.database.dto.entity.BaseCourseEntityDto;
import i5.las2peer.services.mwbService.database.dto.entity.FileBucketEntityDto;
import i5.las2peer.services.mwbService.database.dto.LmsCourseDto;
import i5.las2peer.services.mwbService.database.dto.entity.LmsWorkerEntityDto;
import i5.las2peer.services.mwbService.database.dto.entity.PersonEntityDto;
import i5.las2peer.services.mwbService.externalServices.LMS.LmsConnection;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static i5.las2peer.services.mwbService.utilities.StringsUtilities.hasNoValue;

/**
 * DAO to handle complex base course related database requests
 */
public class BaseCourseDao extends DataAccessObject {
    /**
     * constructor
     * @param connection to the database
     */
    public BaseCourseDao(Connection connection) {
        super(connection);
    }


    //******************************************************************************************
    // instance methods
    //******************************************************************************************

    public LmsCourseDao checkAndGetLmsDataAndWorker(BaseCourseDto baseCourseDto) throws Exception {
        LmsCourseDao lmsCourseDao = checkAndGetLmsData(baseCourseDto);
        if (lmsCourseDao != null)
            checkAndGetLmsWorker(baseCourseDto, lmsCourseDao);
        return lmsCourseDao;
    }

    public LmsCourseDao checkAndGetLmsData(BaseCourseDto baseCourseDto) throws Exception {
        if (hasNoValue(baseCourseDto.getLms()))
            baseCourseDto.setLmsCourseDto(null);
        if (baseCourseDto.getLmsCourseDto() == null)
            return null;
        LmsCourseDao lmsCourseDao = LmsCourseDao.createLmsCourseDao(connection, baseCourseDto.getLmsCourseDto());
        lmsCourseDao.getLmsCourseDto().setBaseCourseId(baseCourseDto.getId());
        if (!lmsCourseDao.getLms().equals(baseCourseDto.getLms()))
            throw getInvalidDataException("lms provided in BaseCourseDto doesn't match lms course data");
        LmsWorkerEntityDto lmsWorker = LmsWorkerEntityDao.readData(connection, lmsCourseDao.getLmsCourseDto().getLmsWorkerId());
        if (lmsWorker == null)
            throw getInvalidDataException("invalid lms worker id: no data found");
        if (!lmsWorker.getLms().equals(baseCourseDto.getLms()))
            throw getInvalidDataException("lms provided for lms worker doesn't match lms course data");
        return lmsCourseDao;
    }



    public LmsWorkerEntityDto checkAndGetLmsWorker(BaseCourseDto baseCourseDto, LmsCourseDao lmsCourseDao) throws Exception {
        if (lmsCourseDao == null)
            throw getInvalidDataException("no lms data provided");
        LmsWorkerEntityDto lmsWorkerEntityDto = LmsWorkerEntityDao.readData(connection, lmsCourseDao.getLmsCourseDto().getLmsWorkerId());
        if (lmsWorkerEntityDto == null)
            throw getInvalidDataException("invalid lms worker id: no data found");
        if (!lmsWorkerEntityDto.getLms().equals(baseCourseDto.getLms()))
            throw getInvalidDataException("lms provided for lms worker doesn't match lms course data");
        if (!LmsCourseDao.getSupportedLms().contains(lmsWorkerEntityDto.getLms()))
            throw getInvalidDataException("unsupported lms: " + lmsWorkerEntityDto.getLms());
        return lmsWorkerEntityDto;
    }

    /**
     * factory creating a new connection to a lms depending on the settings for the given BaseCourseEntityDto
     * @param baseCourseDto with data of base course as BaseCourseDto
     * @return connection to the lms
     * @throws Exception if something went wrong
     */
    public LmsConnection setupLmsConnection(BaseCourseDto baseCourseDto) throws Exception {
        LmsCourseDao lmsCourseDao = LmsCourseDao.createLmsCourseDao(connection, baseCourseDto.getLmsCourseDto());
        LmsWorkerEntityDto lmsWorkerEntityDto = LmsWorkerEntityDao.readData(connection, lmsCourseDao.getLmsCourseDto().getLmsWorkerId());
        return LmsConnection.setupLmsConnection(lmsWorkerEntityDto);
    }

    /**
     * get the basic and lms related data for a base course from the database
     *
     * @param baseCourseId id of the base course
     * @return BaseCourseDto with basic and lms data of the base course
     * @throws Exception if something went wrong
     */
    public BaseCourseDto getBaseCourse(Integer baseCourseId) throws Exception {
        BaseCourseEntityDto baseCourseEntityDto = BaseCourseEntityDao.readData(this.connection, baseCourseId);
        if (baseCourseEntityDto == null)
            throw DataAccessObject.createDatabaseException("No data for base course with id " + baseCourseId, null);
        if (hasNoValue(baseCourseEntityDto.getLms()))
            return new BaseCourseDto(baseCourseEntityDto, null);
        LmsCourseDto lmsCourseDto = LmsCourseDao.createLmsCourseDao(this.connection, baseCourseEntityDto).getLmsCourseDto();
        return new BaseCourseDto(baseCourseEntityDto, lmsCourseDto);
    }

    /**
     * get the complete data for a base course from the database including list of courses and uploaded files
     *
     * @param baseCourseId of the base course
     * @return BaseCourseInfoDto with data of the base course and a list of associated courses
     * @throws Exception if something went wrong
     */
    public BaseCourseInfoDto getBaseCourseInfo(Integer baseCourseId) throws Exception {
        BaseCourseDto baseCourseDto = getBaseCourse(baseCourseId);
        List<CourseDto> courses = getCourseListForBaseCourse(baseCourseId);
        List<FileBucketEntityDto> files = FileBucketEntityDao.getFileBucketList(this.connection, baseCourseId);
        RoleDao roleDao = new RoleDao(this.connection);
        List<PersonEntityDto> owners = roleDao.getBaseCourseOwners(baseCourseId);
        return new BaseCourseInfoDto(baseCourseDto, courses, files, owners);
    }

    /**
     * @param baseCourseId -
     * @return list of CourseDto belonging to the basecourse
     * @throws Exception on failure
     */
    private List<CourseDto> getCourseListForBaseCourse(Integer baseCourseId) throws Exception {
        String sqlStatement = "SELECT *, '' as name FROM " + CourseEntityDao.TABLE +
                " WHERE baseCourseId=?";
        try (PreparedStatement preparedStatement = this.connection.prepareStatement(sqlStatement)) {
            preparedStatement.setInt(1, baseCourseId);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<CourseDto> courses = new ArrayList<>();
            while (resultSet.next()) {
                CourseDto courseDto = CourseDao.createCourseDto(resultSet);
                courses.add(courseDto);
            }
            return courses;
        } catch (SQLException e) {
            throw DataAccessObject.createDatabaseException("Failed to get list of courses for base course.", e);
        }
    }

    /**
     * retrieve list of base courses for a lms course
     * @param lmsCourseDto with lms data of course as LmsCourseDto
     * @return list of BaseCourseEntityDto
     * @throws Exception if something went wrong
     */
    public List<BaseCourseEntityDto> getBaseCoursesForLmsCourse(LmsCourseDto lmsCourseDto) throws Exception {
        LmsCourseDao lmsCourseDao = LmsCourseDao.createLmsCourseDao(this.connection, lmsCourseDto);
        return lmsCourseDao.getBaseCoursesForLmsCourse(lmsCourseDto);
    }
}
