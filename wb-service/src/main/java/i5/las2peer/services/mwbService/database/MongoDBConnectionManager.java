package i5.las2peer.services.mwbService.database;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;
import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.gridfs.GridFSBucket;
import com.mongodb.client.gridfs.GridFSBuckets;
import com.mongodb.client.gridfs.GridFSDownloadStream;
import com.mongodb.client.gridfs.model.GridFSFile;
import i5.las2peer.services.mwbService.MwbService;
import i5.las2peer.services.mwbService.database.dao.DataAccessObject;
import com.mongodb.client.MongoIterable;
import org.bson.Document;
import org.bson.UuidRepresentation;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.bson.types.ObjectId;

import java.io.InputStream;

import static i5.las2peer.services.mwbService.utilities.StringsUtilities.hasValue;

import java.util.ArrayList;
import java.util.List;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

/**
 * class to manage the database connection to MongoDB
 */
public class MongoDBConnectionManager implements AutoCloseable {
    private String connectionString;
    private MongoClient mongoClient;
    private GridFSBucket mwbFileBucket = null;
    public static final String DB_MWB_FILE_BUCKET = "mwb-file-bucket";
    public static final String DB_SBF_FILE_BUCKET = "sbf";

    /**
     * default constructor
     */
    public MongoDBConnectionManager() {}

    @Override
    public void close() {
        if (isEstablished()) {
            MwbService.logger.info("close MongoDB client");
            mongoClient.close();
            mongoClient = null;
        }
    }

    /**
     * check whether connection properties are set
     * @return true if connection properties are already set
     */
    public boolean isInitialized() {
        return hasValue(connectionString);
    }

    /**
     * check whether connection is established
     * @return true if connection is already established
     */
    public boolean isEstablished() {
        return mongoClient != null;
    }

    /**
     * set connection properties
     * @param dbHost -
     * @param dbPort -
     * @param dbUser -
     * @param dbPassword -
     */
    public void setupConnection(String dbHost, String dbPort, String dbUser, String dbPassword) {
        connectionString = String.format("mongodb://%s:%s@%s:%s/", dbUser, dbPassword, dbHost, dbPort);
    }

    /**
     * establish connection to MongoDb
     */
    public void createClient() {
        if (isEstablished())
            return;
        MongoClientSettings clientSettings = MongoClientSettings.builder()
                .uuidRepresentation(UuidRepresentation.STANDARD)
                .applyConnectionString(new ConnectionString(connectionString))
                .codecRegistry(getCodecRegistry())
                .build();
        mongoClient = MongoClients.create(clientSettings);
    }

    private static CodecRegistry getCodecRegistry() {
        CodecRegistry pojoCodecRegistry = fromProviders(PojoCodecProvider.builder().automatic(true).build());
        return fromRegistries(MongoClientSettings.getDefaultCodecRegistry(), pojoCodecRegistry);
    }


    public boolean ping(String databaseName) {
        MongoDatabase database = getDatabase(databaseName);
        Document ping = new Document("ping", 1);
        Document commandResult = database.runCommand(ping);
        return (commandResult.getDouble("ok") == 1.0);
    }

    public List<String> listDatabaseNames() {
        createClient();
        MongoIterable<String> databases = mongoClient.listDatabaseNames();
        List<String> result = new ArrayList<>();
        for (String dbName : databases) {
            result.add(dbName);
        }
        return result;
    }

    /**
     * retrieve the mongoDB database object for the given name
     * @param databaseName for the database
     * @return MongoDatabase object
     */
    public MongoDatabase getDatabase(String databaseName) {
        createClient();
        return mongoClient.getDatabase(databaseName);
    }

    private GridFSBucket getMwbFileBucket() {
        if (mwbFileBucket == null) {
            mwbFileBucket = MongoDBConnectionManager.createGridFSBucket(DB_MWB_FILE_BUCKET, "bucket");
        }
        return mwbFileBucket;
    }

    /**
     * store inputStream in the MongoDB file bucket
     * @param fileName of the file
     * @param inputStream to be stored
     * @return fileId as ObjectId
     */
    public ObjectId storeFileInFileBucket(String fileName, InputStream inputStream) {
        return getMwbFileBucket().uploadFromStream(fileName, inputStream);
    }

    /**
     * create GridFSBucket to interact with MongoDB
     * @param database -
     * @param bucketName -
     * @return GridFSBucket
     */
    public static GridFSBucket createGridFSBucket(String database, String bucketName) {
        MongoDatabase db = MwbService.mongoDBConnectionManager.getDatabase(database);
        return GridFSBuckets.create(db, bucketName);
    }

    /**
     * retrieve a file from MongoDB bucket
     * @param database -
     * @param bucketName -
     * @param fileId of the file to retrieve
     * @return file content as byte[]
     */
    public byte[] retrieveFileFromMongoDb(String database, String bucketName, String fileId) throws Exception {
        MongoDatabase db = MwbService.mongoDBConnectionManager.getDatabase(database);
        GridFSBucket bucket = GridFSBuckets.create(db, bucketName);
        return retrieveFileFromGridFSBucket(bucket, fileId);
    }

    /**
     * retrieve meta data for file from MongoDB bucket
     * @param database -
     * @param bucketName -
     * @param fileId of the file to retrieve
     * @return meta data as gridFSFile
     */
    public GridFSFile retrieveMetaDataForFile(String database, String bucketName, String fileId) {
        MongoDatabase db = MwbService.mongoDBConnectionManager.getDatabase(database);
        GridFSBucket gridFSBucket = GridFSBuckets.create(db, bucketName);
        return gridFSBucket.find(new Document("_id", new ObjectId(fileId))).first();
    }

    /**
     * retrieve a file from MongoDB bucket
     * @param fileId of the file to retrieve
     * @return file content as byte[]
     */
    public byte[] retrieveFileFromFileBucket(String fileId) throws Exception {
        return retrieveFileFromGridFSBucket(getMwbFileBucket(), fileId);
    }

    public byte[] retrieveFileFromGridFSBucket(GridFSBucket bucket, String fileId) throws Exception {
        try (GridFSDownloadStream downloadStream = bucket.openDownloadStream(new ObjectId(fileId))) {
            int fileLength = (int) downloadStream.getGridFSFile().getLength();
            byte[] buffer = new byte[fileLength];
            downloadStream.read(buffer);
            return buffer;
        } catch (Exception e){
            throw DataAccessObject.createDatabaseException("error reading file from MongoDb", e);
        }
    }

    /**
     * delete file in the MongoDB file bucket
     * @param fileId as ObjectId
     */
    public void deleteFileInFileBucket(ObjectId fileId) {
        getMwbFileBucket().delete(fileId);
    }
}

