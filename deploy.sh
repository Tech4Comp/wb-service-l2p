#!/bin/sh
CA_CERT=/var/run/secrets/kubernetes.io/serviceaccount/ca.crt
TOKEN=$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)
POD_NAME=$(curl -k --cacert $CA_CERT -H "Authorization: Bearer $TOKEN" "https://137.226.232.175:6443/api/v1/namespaces/mztud-test/pods" | jq '.items[] | .metadata.name' | grep -m 1 't4c-workbench-service' | sed 's/"//g')
curl -k -X DELETE --cacert $CA_CERT -H "Authorization: Bearer $TOKEN" "https://137.226.232.175:6443/api/v1/namespaces/mztud-test/pods/$POD_NAME"
